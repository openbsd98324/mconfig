all:
	  cc  src/mconfig.c   -o src/mconfig 


all-static:
	  cc -static src/mconfig.c   -o src/mconfig 



test:
	  cc -static src/mconfig.c   -o /tmp/mconfig-test  
	  /tmp/mconfig-test  


install:
	  cc  src/mconfig.c   -o src/mconfig 
	  cp  src/mconfig  /usr/local/bin/mconfig 


