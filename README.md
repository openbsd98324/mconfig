


# Introduction

A busy code that compiles with : cc -static mconfig.c -o mconfig 

# Installation

```
zypper install make gcc 
make install
```





# Support

It was tested and it can be compiled on NetBSD, OpenBSD, FreeBSD, and on Linux.

using :  cc -static ... 


# Content 



It does few things, on base-less machines (no base, only a BSD or linux kernel with /dev)

It compiles with smallest C compilers.



Example to download a port/source code: 

````
  mconfig store irssi 
````

