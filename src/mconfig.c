
// MCONFIG --- OPEN FOR EDITION !!!

#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h> 
#include <time.h>

int debug_mode = 1; 
int os_bsd_type = 0;
// freebsd openbsd netbsd 
int os_linux_type = 0; 
// os type, i.e.: 1: freebsd, 2:openbsd, 3:netbsd 


#if defined(__linux__) //linux
#define MYOS 1
#elif defined(_WIN32)
#define MYOS 2
#elif defined(_WIN64)
#define MYOS 3
#elif defined(__unix__) 
#define MYOS 4  // freebsd
#define PATH_MAX 2500
#else
#define MYOS 0
#endif

/// Alpine Linux (default, base) 
#ifdef HAVE_NOT_PATHMAX
#define PATH_MAX 2500
#endif


#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>


#include <time.h>

#define KRED  "\x1B[31m"
#define KGRE  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KBGRE  "\x1B[92m"
#define KBYEL  "\x1B[93m"
#define KNRM  "\x1B[0m"


// printf("%s", KGRE);
// printf("%s", KNRM);
// #define KNRM  "\x1B[0m"





int fexist(const char *a_option)
{
	char dir1[PATH_MAX]; 
	char *dir2;
	DIR *dip;
	strncpy( dir1 , "",  PATH_MAX  );
	strncpy( dir1 , a_option,  PATH_MAX  );

	struct stat st_buf; 
	int status; 
	int fileordir = 0 ; 

	status = stat ( dir1 , &st_buf);
	if (status != 0) {
		fileordir = 0;
	}
	FILE *fp2check = fopen( dir1  ,"r");
	if( fp2check ) {
		fileordir = 1; 
		fclose(fp2check);
	} 

	if (S_ISDIR (st_buf.st_mode)) {
		fileordir = 2; 
	}
	return fileordir;
}



















void nsystem( char *mycmd )
{
        char foocharo[PATH_MAX];
	printf( "< nsystem Start: %s>\n", mycmd );
	snprintf( foocharo , sizeof( foocharo ), "%s" , mycmd ); 
	system(   foocharo ); 
	printf( "< nsystem Completed: %s>\n", mycmd );
}



void ncmdwith( char *mycmd, char *myfile )
{
	char cmdi[PATH_MAX];
	printf( "** CMD (start) (OS: %d)\n" , MYOS );
        char foocharo[PATH_MAX];
	snprintf( foocharo , sizeof( foocharo ), " %s \"%s\" " , mycmd , myfile ); 
	nsystem(   foocharo ); 
	printf( "** CMD (completed) (OS: %d)\n" , MYOS );
}













void npkg_update( )
{
        if (  os_bsd_type == 1 ) 
	{
	    printf( "FreeBSD\n" ); 
	    nsystem( " pkg update " ); 
	}
	else if ( MYOS == 1 ) 
	{
		if ( fexist( "/usr/bin/zypper" ) == 1 ) 
			nsystem( " cd ; zypper refresh  " ); 
		else if ( fexist( "/etc/arch-release" ) == 1 ) 
			nsystem( " cd ; pacman -Syy "); 
		else if ( fexist( "/usr/sbin/slapt-get" ) == 1 ) 
			nsystem( " slapt-get --update " ); 
		else
			nsystem( "  cd ; apt-get update  " );
	}

	else if ( fexist( "/usr/bin/pacman" ) == 1 )     // manjaro  
	{
		nsystem( "  pacman -Sy   " );
	}

	else if ( fexist( "/etc/wscons.conf" ) == 1 )     // netbsd 
	{
		nsystem( "  pkgin update   " );
	}
	else
	{
		nsystem( "  cd ; apt-get update  " );
	}
}












int ckeypress()
{
	printf( "|--------------|\n"  );
	printf( "|   Press Key  |\n"  );
	printf( "|--------------|\n"  );
	printf( "| >  Choice ?  |\n"  );
	int choix = getchar();
        printf( "KEY: %c (%d)\n" , choix , choix ); 
	return choix;
}		







void nlsdir()
{ 
	DIR *dirp;
	struct dirent *dp;
	dirp = opendir( "." );
	while  ((dp = readdir( dirp )) != NULL ) 
	{
		if (  strcmp( dp->d_name, "." ) != 0 )
			if (  strcmp( dp->d_name, ".." ) != 0 )
			{
				if ( fexist( dp->d_name ) == 2 ) 
					if ( dp->d_name[0] != '.' )
						printf( "%s\n", dp->d_name );
			}
	}
	closedir( dirp );
}



void nls( char *foodirectory )
{ 
	DIR *dirp;
	struct dirent *dp;
	dirp = opendir( foodirectory );
	while  ((dp = readdir( dirp )) != NULL ) 
	{
		if (  strcmp( dp->d_name, "." ) != 0 )
			if (  strcmp( dp->d_name, ".." ) != 0 )
				printf( "%s\n", dp->d_name );
	}
	closedir( dirp );
}






char *fbasename(char *name)
{
	char *base = name;
	while (*name)
	{
		if (*name++ == '/')
		{
			base = name;
		}
	}
	return (base);
}












void fetch_file_ftp( char *pattern )
{
	char foocwd[PATH_MAX];
        int fetch_file_ftp_force_httpwget = 0; 
        int fetch_file_ftp_force_bsdwget  = 0; 
        int fetch_file_ftp_force_links    = 0; 
	printf( "=========================\n" );
	printf( " ===  Fetch File FTP === \n" );
	printf( "=========================\n" );
	printf( "PWD: %s\n", getcwd( foocwd , PATH_MAX ) );
	printf( " ...\n" );

        if ( fexist( "/etc/flde_cmlinks" ) == 1 ) 
	   fetch_file_ftp_force_httpwget = 5; 

        if ( fexist( "/etc/mconfig_httpwget" ) == 1 ) 
	   fetch_file_ftp_force_httpwget = 1; 

        if ( fexist( "/etc/mconfig_bsdwget" ) == 1 ) 
	   fetch_file_ftp_force_bsdwget = 1; 


        //// 
	if ( fetch_file_ftp_force_httpwget == 5 )   // freebsd raspi rpi3, server
	    ncmdwith( " /usr/local/bin/cmlinks " ,  pattern   );

	else if ( ( os_bsd_type == 1 ) && ( MYOS != 1 ) ) 
	{
	    printf( " Likely, FreeBSD \n" ); 
	    ncmdwith( " fetch -R  " ,  pattern   );
	}

	else if ( MYOS != 1 )  // BSD
	    ncmdwith( "  ftp  " ,  pattern   );

        else if ( ( fexist( "/etc/mconfig_links" ) == 1 ) || ( fexist( "/etc/mconfig-links" ) == 1 ) )
	    ncmdwith( " /usr/bin/links  " ,  pattern );

        else if ( ( fexist( "/etc/mconfig_cmlinks" ) == 1 ) || ( fexist( "/etc/mconfig-cmlinks" ) == 1 ) )
	    ncmdwith( "  /usr/local/bin/cmlinks  " ,  pattern );

        /// rescue for ssl handshake
        else if ( fetch_file_ftp_force_bsdwget == 1 ) 
	{
	    printf( " => FORCE\n"); 
	    ncmdwith( "  /usr/pkg/bin/wget -c --no-check-certificate " ,  pattern   );
        }
	else if ( fetch_file_ftp_force_httpwget == 1 ) 
	{
	    printf( " => FORCE\n"); 
	    ncmdwith( " httpwget  " ,  pattern   );
	}

	else 
	    ncmdwith( " wget -c --no-check-certificate  " ,  pattern   );

	printf( " ...\n" );
	printf( "======================\n" );
}










//// alpine linux: apk add  ... insert list.
/// dnf install tmux (fedora)
/// This npkg will install the package on Linux and BSD, including Gentoo
int mode_gpg_allow_unauthenticated = 0;  
int mode_apt_get_allow_force_yes = 0;  
int mode_apt_get_check = 0;  
void npkg( char *mycmd )
{
	// 
        // id-20211204-160535
        // bookworm : --force-yes   is deprecated, please use --allow 
	// su - 
	// fdisk?  
	// 
	char cmdi[PATH_MAX];
	printf( "< npkg (start) (OS: %d)\n" , MYOS );
        if ( MYOS == 1 ) 
        if ( mode_apt_get_check == 0 )   
	{
	    if ( fexist( "/etc/apt/gpg" ) == 1 )  
   	        mode_gpg_allow_unauthenticated = 1; 

            if ( fexist( "/etc/apt/force" ) == 1 ) 
                mode_apt_get_allow_force_yes = 1;  

            mode_apt_get_check = 1;  
	}



	if ( MYOS == 1 )
	{
		if ( mode_gpg_allow_unauthenticated == 1 ) 
			strncpy( cmdi , " apt-get install -y --allow-unauthenticated  " , PATH_MAX );

		else if ( mode_apt_get_allow_force_yes == 1 ) 
			//strncpy( cmdi , " apt-get install -y --allow  " , PATH_MAX );
			strncpy( cmdi , " apt-get install -y --force-yes  " , PATH_MAX );

                // /etc/slackware-version 
                //  cd ; slackstrap update ; slackstrap installpkg   xyz-... example: artikulate  
		else if ( fexist( "/etc/slackware-version" ) == 1 ) 
		{
			printf( "Slackware\n" ); 
			strncpy( cmdi , "   cd ; slackstrap update ; slackstrap installpkg  " , PATH_MAX );
		}

		else if (  ( fexist( "/etc/fedora" ) == 1 ) &&  ( fexist( "/usr/bin/dnf" ) == 1 ) )
		{
			printf( "Fedora\n" ); 
			strncpy( cmdi , " dnf install  " , PATH_MAX );
		}

		else if ( fexist( "/usr/bin/zypper" ) == 1 ) 
		{
			printf( "OpenSUSE\n" ); 
			strncpy( cmdi , " zypper install -y  " , PATH_MAX );
		}

		else if ( fexist( "/usr/bin/apt-get" ) == 1 ) 
			strncpy( cmdi , " apt-get install -y  " , PATH_MAX );

		else if ( fexist( "/usr/sbin/slapt-get" ) == 1 ) 
			//strncpy( cmdi , "  slapt-get --update ; slapt-get -i  " , PATH_MAX );
			strncpy( cmdi , "  slapt-get -i  " , PATH_MAX );

		else if ( fexist( "/etc/arch-release" ) == 1 ) 
			strncpy( cmdi , " pacman -S --noconfirm  " , PATH_MAX ); // check fix, noconfig is risky

		else if ( fexist( "/usr/bin/emerge" ) == 1 ) 
			strncpy( cmdi , " emerge  " , PATH_MAX );
		else 
			strncpy( cmdi , " apt-get install -y  " , PATH_MAX );
	}
	else 
	{
		if ( fexist( "/etc/freebsd-update.conf" ) == 1 )  // freebsd 
			strncpy( cmdi , " pkg install -y  " , PATH_MAX );

		else if ( fexist( "/etc/myname" ) == 1 )          // openbsd
			strncpy( cmdi , " pkg_add " , PATH_MAX );

		else if ( fexist( "/etc/wscons.conf" ) == 1 )     // netbsd 
			strncpy( cmdi , " pkgin -y  install " , PATH_MAX );
	}
	strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
	strncat( cmdi , mycmd , PATH_MAX - strlen( cmdi ) -1 );
	strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );

	if ( strcmp( mycmd, "" ) != 0 ) 
	   nsystem( cmdi ); 

	printf( "  npkg (completed) (OS: %d)\n" , MYOS );
}













#include <fcntl.h>
#include <unistd.h>
static int cat_fd(int fd) 
{
  char buf[4096];
  ssize_t nread;

  while ((nread = read(fd, buf, sizeof buf)) > 0) 
  {
    ssize_t ntotalwritten = 0;
    while (ntotalwritten < nread) {
      ssize_t nwritten = write(STDOUT_FILENO, buf + ntotalwritten, nread - ntotalwritten);
      if (nwritten < 1)
        return -1;
      ntotalwritten += nwritten;
    }
  }
  return nread == 0 ? 0 : -1;
}

static int ncat_static(const char *fname) 
{
  int fd, success;
  if ((fd = open(fname, O_RDONLY)) == -1)
    return -1;

  success = cat_fd(fd);

  if (close(fd) != 0)
    return -1;

  return success;
}











void npkg_upgrade( )
{
       nsystem( " pacman -Suy --noconfirm  "); 
}









/*
menu=Boot normally:rndseed /var/db/entropy-file;boot netbsd
menu=Boot single user:rndseed /var/db/entropy-file;boot netbsd -s
menu=Disable ACPI:rndseed /var/db/entropy-file;boot netbsd -2
menu=Disable ACPI and SMP:rndseed /var/db/entropy-file;boot netbsd -12
menu=Drop to boot prompt:prompt
default=1
timeout=5
clear=1
*/

void void_print_boot_banner()
{
			printf( "banner=Welcome to NetBSD Banner\n" );
			printf( "banner=========================\n" );
			printf( "banner=\n" );
			printf( "banner=------------------------------------------\n" );
			printf( "menu=Boot safe normally without i915:rndseed /var/db/entropy-file;userconf disable i915drmkms;boot\n" );
			printf( "menu=Boot Vesa 0x17f:rndseed /var/db/entropy-file;vesa 0x17f;boot\n" );
			printf( "menu=Boot normally:rndseed /var/db/entropy-file;boot\n" );
			printf( "menu=Boot single user:rndseed /var/db/entropy-file;boot -s\n" );
			printf( "menu=Boot rescue safe normally without i915:rndseed /var/db/entropy-file;userconf disable i915drmkms;boot -s \n" );
			printf( "banner=------------------------------------------\n" );
			///printf( "menu=Boot Rescue hd1a Disk normally without i915:rndseed /var/db/entropy-file;userconf disable i915drmkms;boot hd1a:netbsd \n" );
			printf( "banner=------------------------------------------\n" );
			//printf( "menu=Boot safe Install without i915:rndseed /var/db/entropy-file;userconf disable i915drmkms;boot netbsd-INSTALL\n" );
			printf( "menu=Boot safe Install gz without i915:rndseed /var/db/entropy-file;userconf disable i915drmkms;boot netbsd-INSTALL.gz\n" );
			printf( "banner=------------------------------------------\n" );
			///printf( "menu=Boot Vesa 0x17f:rndseed /var/db/entropy-file;vesa 0x17f;boot\n" );
			//(old) printf( "menu=Boot Rescue hd1a Disk without i915:rndseed /var/db/entropy-file;userconf disable i915drmkms;boot hd1a:netbsd \n" );
			printf( "menu=Multiboot root sd0a Disk,hd0a without i915:rndseed /var/db/entropy-file;userconf disable i915drmkms;multiboot hd0a:netbsd root=sd0a   \n" );
			printf( "banner=------------------------------------------\n" );
			printf( "menu=Drop to boot prompt:prompt\n" );
			printf( "default=1\n" );
			printf( "timeout=15\n" );
			printf( "clear=1\n" );
			/// menu=Boot Xen with 256MB for dom0 (serial):load /netbsd-XEN3_DOM0 console=com0;multiboot /usr/pkg/xen3-kernel/xen.gz dom0_mem=256M console=com1 com1=115200,8n1
}



















void procedure_video_player(  char * myfoourl )
{
	printf( " ========================\n"  ); 
	printf( " PROCEDURE VIDEO PLAYER \n" ); 
	printf( "    (mconfig)  \n" ); 
	printf( " ========================\n"  ); 
	printf( " Check sys /usr/bin/mpv (%d)\n", fexist( "/usr/bin/mpv" ) ); 
	printf( " Check sys /usr/bin/vlc (%d)\n", fexist( "/usr/bin/vlc" ) ); 

	char foocharo[PATH_MAX];
	printf( "URL: %s\n" , myfoourl ); 

	// mpv works on rpi4  buster 
	if ( fexist( "/usr/bin/mpv" ) == 1 ) 
	{
	    if  ( fexist( "/etc/apt/mpv-fs" ) == 1 ) 
	    {
	       printf( " > File /etc/apt/mpv-fs found.\n" ); 
               snprintf( foocharo , sizeof( foocharo ), " mpv --fs \"%s\"  " , myfoourl ); 
	    }
            else 
	    {
	       //printf( " > No file /etc/apt/mpv-fs found.\n" ); 
               snprintf( foocharo , sizeof( foocharo ), " mpv \"%s\"  " , myfoourl ); 
	    }

        }
	else if ( fexist( "/usr/bin/cvlc" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " cvlc  --play-and-exit    \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/vlc" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " vlc  --play-and-exit \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/mpv" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " mpv \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/cvlc" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " cvlc    \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/vlc" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " vlc \"%s\"  " , myfoourl ); 


	else if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " mplayer  \"%s\"  " , myfoourl ); 


	else if ( fexist( "/usr/bin/omxplayer" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " omxplayer  \"%s\"  " , myfoourl ); 


	else if ( fexist( "/usr/bin/parole" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " parole   \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/omxplayer" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " omxplayer  \"%s\"  " , myfoourl ); 

	else 
             snprintf( foocharo , sizeof( foocharo ), " vlc \"%s\"  " , myfoourl ); 
	nsystem( foocharo ); 
}









void procedure_video_player_fb(  char * myfoourl )
{
	printf( " ========================\n"  ); 
	printf( " PROCEDURE AUDIO PLAYER \n" ); 
	printf( " ========================\n"  ); 
	printf( " Check sys /usr/bin/mpv (%d)\n", fexist( "/usr/bin/mpv" ) ); 
	printf( " Check sys /usr/bin/vlc (%d)\n", fexist( "/usr/bin/vlc" ) ); 

	char foocharo[PATH_MAX];
	printf( "URL: %s\n" , myfoourl ); 
        //  cvlc --vout fb --fbdev=/dev/fb0   v4l2:///dev/video0  myvideofile.avi 
	// mpv works on rpi4  buster 
	if ( fexist( "/usr/bin/mpv" ) == 1 ) 
		snprintf( foocharo , sizeof( foocharo ), " mpv \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/omxplayer" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " omxplayer  \"%s\"  " , myfoourl ); 

	else 
		snprintf( foocharo , sizeof( foocharo ), "  cvlc --play-and-exit --vout fb --fbdev=/dev/fb0    \"%s\"  " , myfoourl ); 

	nsystem( foocharo ); 
}
















void procedure_xwebbrowser(  char * myfoourl )
{
	printf( " =====================\n"  ); 
	printf( " PROCEDURE WEB BROWSER \n" ); 
	printf( " =====================\n"  ); 

	char foocharo[PATH_MAX];
	printf( "URL: %s\n" , myfoourl ); 
	printf( " The Classic Mighty Links Graphics ...\n"); 
        snprintf( foocharo , sizeof( foocharo ), " links-gui -g  \"%s\"  " , myfoourl ); 
	nsystem( foocharo ); 
}







void procedure_console_webbrowser(  char * myfoourl )
{
	printf( " =====================\n"  ); 
	printf( " PROCEDURE WEB BROWSER \n" ); 
	printf( " =====================\n"  ); 
        char foocharo[PATH_MAX];

	if ( fexist( "/usr/pkg/bin/links" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/pkg/bin/links  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/local/bin/links" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/links  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/local/bin/cmlinks" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/cmlinks  \"%s\"  " , myfoourl ); 

         else
	   snprintf( foocharo , sizeof( foocharo ), " links  \"%s\"  " , myfoourl ); 


	printf( " System Command: (%s)\n", foocharo );
	nsystem(  foocharo ); 
}












void procedure_webbrowser_console(  char * myfoourl )
{
	printf( " ==============================\n"  ); 
	printf( " PROCEDURE WEB CONSOLE BROWSER \n" ); 
	printf( " ==============================\n"  ); 

	char foocharo[PATH_MAX];
	printf( "URL: %s\n" , myfoourl ); 

	if ( fexist( "/usr/bin/links" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/links   \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/links2" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/links2   \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/lynx" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " lynx  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/w3m" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " w3m  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/local/bin/cmlinks" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/cmlinks  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/local/bin/linksgfx" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/linksgfx  -g  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/local/bin/cmlinksg" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/cmlinksg  -g  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/local/bin/lynx" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/lynx \"%s\"  " , myfoourl ); 

	else 
	   snprintf( foocharo , sizeof( foocharo ), " links  \"%s\"  " , myfoourl ); 
	printf( " System Command: (%s)\n", foocharo );
	nsystem(  foocharo ); 
}












void procedure_webbrowser(  char * myfoourl )
{
	printf( " =====================\n"  ); 
	printf( " PROCEDURE WEB BROWSER \n" ); 
	printf( " =====================\n"  ); 
	printf( " Check sys /usr/bin/chromium (%d)\n", fexist( "/usr/bin/chromium" ) ); 
	printf( " Check sys /usr/bin/chromium-browser (%d)\n", fexist( "/usr/bin/chromium-browser" ) ); 
	printf( " Check sys /usr/bin/firefox (%d)\n", fexist( "/usr/bin/firefox" ) ); 

	char foocharo[PATH_MAX];
	printf( "URL: %s\n" , myfoourl ); 

	if ( ( fexist( "/home/pi" ) == 2 ) && ( fexist( "/usr/bin/chromium-browser" ) == 1 ) ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/chromium-browser --new-window  \"%s\"  " , myfoourl ); 


	else if ( fexist( "/usr/local/bin/firefox" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/firefox  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/firefox" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/firefox  -new-window  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/snap/bin/firefox" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " firefox  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/chromium-browser" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/chromium-browser --new-window  \"%s\"  " , myfoourl ); 


	else if ( fexist( "/usr/bin/chromium" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/chromium --new-window  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/pkg/bin/chromium" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/pkg/bin/chromium --new-window  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/pkg/bin/firefox" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/pkg/bin/firefox  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/pkg/bin/chromium-browser" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/pkg/bin/chromium-browser --new-window  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/pkg/bin/firefox52" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/pkg/bin/firefox52  \"%s\"  " , myfoourl ); 

        /// kde opensuse
	else if ( fexist( "/usr/bin/firefox" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/firefox  \"%s\"  " , myfoourl ); 

        /// kde devuan
	else if ( fexist( "/usr/bin/iceweasel" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/iceweasel  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/links2" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " links2 -g \"%s\"  " , myfoourl ); 

        // linux netsurf  
	else if ( fexist( "/usr/bin/netsurf" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/netsurf  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/dillo" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/bin/dillo  \"%s\"  " , myfoourl ); 


	else if ( fexist( "/usr/pkg/bin/dillo" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/pkg/bin/dillo  \"%s\"  " , myfoourl ); 


	else if ( fexist( "/usr/pkg/bin/netsurf-gtk3" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/pkg/bin/netsurf-gtk3  \"%s\"  " , myfoourl ); 


	else if ( fexist( "/usr/pkg/bin/netsurf" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/pkg/bin/netsurf  \"%s\"  " , myfoourl ); 


        /// lib x11 based
	else if ( fexist( "/usr/local/bin/linksgfx" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/linksgfx  -g  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/local/bin/xweb" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/xweb  -g  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/local/bin/cmlinksg" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/cmlinksg  -g  \"%s\"  " , myfoourl ); 



        /// textbased
	else if ( fexist( "/usr/bin/links" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " links  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/local/bin/links" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/pkg/bin/links  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/local/bin/cmlinks" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/local/bin/cmlinks  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/pkg/bin/links" ) == 1 ) 
	   snprintf( foocharo , sizeof( foocharo ), " /usr/pkg/bin/links  \"%s\"  " , myfoourl ); 

	else 
	   snprintf( foocharo , sizeof( foocharo ), " links  \"%s\"  " , myfoourl ); 


	printf( " System Command: (%s)\n", foocharo );
	nsystem(  foocharo ); 
}






















void listdir_origin(const char *name, int indent)
{
	DIR *dir;
	struct dirent *entry;

	if (!(dir = opendir(name)))
		return;

	while ((entry = readdir(dir)) != NULL) {
		if (entry->d_type == DT_DIR) {
			char path[1024];
			if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
				continue;
			snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
			printf("%*s[%s]\n", indent, "", entry->d_name);
			listdir_origin(path, indent + 2);
		} else {
			printf("%*s- %s\n", indent, "", entry->d_name);
		}
	}
	closedir(dir);
}







char *strfindmid(char *str, int fooit )
{  
      char ptr[2*strlen(str)+1];
      int i,j=0;
      int strfd = 0; 
      for(i=0; str[i]!='\0'; i++)
      {
          if ( str[i] == fooit ) 
	     strfd++; 

          if ( strfd == 1 ) 
	   if ( str[i] != ' ' )  
	     if ( str[i] != ':' )  
                ptr[j++]=str[i];
      } 
      ptr[j]='\0';
      size_t siz = sizeof ptr ; 
      char *r = malloc( sizeof ptr );
      return r ? memcpy(r, ptr, siz ) : NULL;
}










void dhclient_enx_wlx( char *filesource , char *sstring ) 
{
	FILE *source; 
	int c,j ;
	char lline[PATH_MAX]; 
	char mysstring[PATH_MAX]; 
	strncpy( mysstring, sstring,  PATH_MAX); int pcc=0;
	char fooenx[PATH_MAX]; 
	int begin;
	j = 0;
	printf( "FILENAME: %s\n" , filesource ); 
	source = fopen( filesource , "r");
	if ( fexist( filesource ) == 1 ) 
	{
		{    c = fgetc(source);
			while( c != EOF )
			{
				if ( c != '\n' ) 
					lline[j++] = c;

				begin = 0;
				if ( c == '\n' )
				{  
					begin = 1;
					lline[j]='\0';

					if ( lline[0] != '#' ) 
					if ( strstr( lline, " mtu " ) != 0 )
					if ( strstr( lline, mysstring ) != 0 )
					///if ( strstr( lline, ": enx" ) != 0 )
					{
						printf( "> FOUND: %s\n", lline );
	                                        strncpy( fooenx , strfindmid( lline, ':') ,  PATH_MAX ); 
						printf( "> ITEM '(%s)' \n", fooenx );  

						ncmdwith(  " ifup  " , fooenx );  
						ncmdwith(  " dhclient " , fooenx );  
					}
					j = 0;
					lline[j]='\0';
				}
				c = fgetc(source);
			}
			fclose(source);
		}
	}
}



char searchitem[PATH_MAX];
void listdir(const char *name, int indent)
{
	DIR *dir;
	struct dirent *entry;

	if (!(dir = opendir(name)))
		return;

	while ((entry = readdir(dir)) != NULL) 
	{
		if (entry->d_type == DT_DIR) 
		{
			char path[1024];

			if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
				continue;

			snprintf( path, sizeof(path), "%s/%s", name, entry->d_name);

			listdir( path, indent + 2);
		} 
		else 
		{
			if ( strstr( entry->d_name , searchitem ) != 0 ) 
			{
				printf("%s/%s\n", name , entry->d_name );
			}
		}
	}
	closedir(dir);
}








#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <termios.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <ctype.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>  
#include <time.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>

//int debug_mode = 0; 
int variable_bytes_received_max = 0; 
int variable_bytes_received = 0; 

char *strright(char *str)
{  
      char ptr[ 5* strlen(str)+1];
      int i,j=0;
      int fspace = 0; 
      int writ = 0; 
      for(i=0; str[i]!='\0'; i++)
      {
        if ( str[i] == '/' ) 
	{
	  if ( writ == 1 )  ptr[j++]=str[i];
          writ = 1;
	}
        else
	{
	  if ( writ == 1 ) 
             ptr[j++]=str[i];
	}
      } 
      ptr[j]='\0';
      size_t siz = 1 + sizeof ptr ; 
      char *r = malloc( 1 +  sizeof ptr );
      return r ? memcpy(r, ptr, siz ) : NULL;
}

char *strleft(char *str)
{  
      char ptr[ 5* strlen(str)+1];
      int i,j=0;
      int fspace = 0; 
      int writ = 1; 
      for(i=0; str[i]!='\0'; i++)
      {
        if ( str[i] == '/' ) 
	{
          writ = 0;
	}
        else
	{
	  if ( writ == 1 ) 
             ptr[j++]=str[i];
	}
      } 
      ptr[j]='\0';
      size_t siz = 1 + sizeof ptr ; 
      char *r = malloc( 1 +  sizeof ptr );
      return r ? memcpy(r, ptr, siz ) : NULL;
}

int ReadHttpStatus(int sock)
{
    char c;
    char buff[1024]="",*ptr=buff+1;
    int bytes_received, status;
    printf("Begin Response ..\n");
    //while(bytes_received = recv(sock, ptr, 1, 0))
    while(bytes_received == recv(sock, ptr, 1, 0))
    {
        if(bytes_received==-1){
            perror("ReadHttpStatus");
            exit(1);
        }

        if((ptr[-1]=='\r')  && (*ptr=='\n' )) break;
        ptr++;
    }
    *ptr=0;
    ptr=buff+1;

    sscanf(ptr,"%*s %d ", &status);

    printf("%s\n",ptr);
    printf("status=%d\n",status);
    printf("End Response ..\n");
    return (bytes_received>0)?status:0;

}

int ParseHeader(int sock)
{
    char c;
    char buff[1024]="",*ptr=buff+4;
    int bytes_received, status;
    printf("Begin HEADER ..\n");
    //while(bytes_received = recv(sock, ptr, 1, 0)){
    while(bytes_received == recv(sock, ptr, 1, 0))
    {
        if(bytes_received==-1){
            perror("Parse Header");
            exit(1);
        }

        if(
            (ptr[-3]=='\r')  && (ptr[-2]=='\n' ) &&
            (ptr[-1]=='\r')  && (*ptr=='\n' )
        ) break;
        ptr++;
    }

    *ptr=0;
    ptr=buff+4;

    if(bytes_received)
    {
        ptr=strstr(ptr,"Content-Length:");
        if(ptr){
            sscanf(ptr,"%*s %d",&bytes_received);

        }else
            bytes_received=-1; //unknown size

       printf("Content-Length (max): %d\n", bytes_received);
       variable_bytes_received_max = bytes_received; 
    }
    printf("End HEADER ..\n");
    return  bytes_received ;
}






void portable_httpwget( char *domain , char *path )
{
	//char domain[PATH_MAX];
	char filetarget[PATH_MAX];
	strncpy( filetarget, "test.png" , PATH_MAX ); 
	//char path[PATH_MAX];
	int retrmode = 1; 
        /*
	if ( argc >= 2 )
	{
		printf( "Arg-1: %s\n", argv[ 1 ] );
		strncpy( domain, strleft(  argv[ 1 ] ), PATH_MAX );  
		strncpy( path,   strright( argv[ 1 ] ), PATH_MAX ); 
	        strncpy( filetarget, fbasename( argv[ 1 ] ) , PATH_MAX ); 
	        fprintf( stderr, "Variables: (%s) (%s)\n", domain , path ); 
	        retrmode = 2; 
	}
	else 
	{
	        //char domain[] = "sstatic.net", path[]="stackexchange/img/logos/so/so-logo-med.png"; 
		strncpy( domain,  "sstatic.net" , PATH_MAX ); 
		strncpy( path,    "stackexchange/img/logos/so/so-logo-med.png" , PATH_MAX ); 
	        retrmode = 1; 
	}
	*/
	fprintf( stderr, "\nDOMAIN:(%s)\nPATH:(%s)\n", domain , path ); 

	int sock, bytes_received;  
	char send_data[1024],recv_data[1024], *p;
	struct sockaddr_in server_addr;
	struct hostent *he;



        printf( " > GETHOSTBYNAME ... \n" ); 
	/// warning: Using 'gethostbyname' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking
	/// warning: Using 'gethostbyname' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking
	/// warning: Using 'gethostbyname' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking
	/*
	he = gethostbyname( domain );
	if ( he == NULL ){
		herror( "gethostbyname");
		exit(1);
	}
	*/


	if ((sock = socket(AF_INET, SOCK_STREAM, 0))== -1){
		perror("Socket");
		exit(1);
	}
	server_addr.sin_family = AF_INET;     
	server_addr.sin_port = htons(80);
	server_addr.sin_addr = *((struct in_addr *)he->h_addr);
	bzero(&(server_addr.sin_zero),8); 

	printf("Connecting ...\n");
	if (connect(sock, (struct sockaddr *)&server_addr,sizeof(struct sockaddr)) == -1){
		perror("Connect");
		exit(1); 
	}

	printf("Sending data ...\n");


	snprintf(send_data, sizeof(send_data), "GET /%s HTTP/1.1\r\nHost: %s\r\n\r\n", path, domain);


	if(send(sock, send_data, strlen(send_data), 0)==-1){
		perror("send");
		exit(2); 
	}
	printf("Data sent.\n");  

	//fp=fopen("received_file","wb");
	printf("Receiving data...\n\n");

	int contentlengh;

	if(ReadHttpStatus(sock) && (contentlengh=ParseHeader(sock))){

		int bytes=0;

		//FILE* fd=fopen("test.png","wb");
		FILE* fd=fopen( filetarget ,"wb");
		printf("Saving data...\n\n");
		printf( "Start Downloading ...\n" ); 

		//while(bytes_received = recv(sock, recv_data, 1024, 0))
		while(bytes_received == recv(sock, recv_data, 1024, 0))
		{
			if(bytes_received==-1){
				perror("receive");
				exit(3);
			}

			fwrite(recv_data,1,bytes_received,fd);
			bytes+=bytes_received;

			if ( debug_mode == 1 ) 
			{
			   printf("Bytes received (debug): %d from %d\n",bytes,contentlengh);
			}
			else if ( debug_mode == 0 ) 
			{
                           variable_bytes_received = bytes;
			   //if ( variable_bytes_received == 1503000 ) 
			   //  printf("Bytes received (debug): %d from %d\n",bytes,contentlengh);
		        }

			if(bytes==contentlengh)
				break;
		}
		fclose(fd);
	}

	close(sock);

	printf("\n\nEND OF FILE.\n\n");
	fprintf( stderr, "\nRetrMode:%d\n" , retrmode ); 
	fprintf( stderr, "\nDOMAIN:(%s)\nPATH:(%s)\n", domain , path ); 
	fprintf( stderr, "URL:(%s/%s)\n", domain , path ); 
	fprintf( stderr, "Filetarget:(%s)\n", filetarget ); 
        printf( "Received Content-Length (max): %d\n", variable_bytes_received_max );
	printf("\n\n(Exited normally).\n\n");
	//return 0;
}




#include <time.h>
void nanosleep_time_loop()
{
    // usleep works under linux and most distros.
    // nanosleep is found in netbsd.
     int gameover = 0;
     struct timespec length; 
     length.tv_sec = 2 ; 
     while( gameover == 0 ) 
     {
	     printf( "===\n" );
	     printf("%d\n", (int)time(NULL));
	     nanosleep( &length , NULL);
	     printf( " nanosleep: hi! \n" );

             if ( MYOS == 1 ) 
		//usleep(   5 *   10 * 20 * 10000 );     // 5sec
		usleep(   10 * 20 * 10000 );     // 2sec
     }
}






void npkgmin( char *mycmd )
{
	char cmdi[PATH_MAX];
	if (  ( MYOS == 1 ) && ( fexist( "/etc/apt/sources.list" ) == 1 ) && ( fexist( "/usr/bin/apt-get" ) == 1 ) )
	{
	        printf( " DEBIAN \n" );  
		strncpy( cmdi , " apt-get install -y --no-install-recommends  " , PATH_MAX );
		strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
		strncat( cmdi , mycmd , PATH_MAX - strlen( cmdi ) -1 );
		strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
		if ( strcmp( mycmd, "" ) != 0 ) 
			nsystem( cmdi ); 
	}
	else
	{
	    npkg( mycmd ); 
	}
}



char *fbasenoext(char* mystr)
{
    char *retstr;
    char *lastdot;
    if (mystr == NULL)
         return NULL;
    if ((retstr = malloc (strlen (mystr) + 1)) == NULL)
        return NULL;
    strcpy (retstr, mystr);
    lastdot = strrchr (retstr, '.');
    if (lastdot != NULL)
        *lastdot = '\0';
    return retstr;
}



void lscount()
{
	double mycount = 0;
	DIR *dirp;
	struct dirent *dp;
	dirp = opendir( "." );
	while  ((dp = readdir( dirp )) != NULL ) 
	{    
	        //  printf( "%s\n", dp->d_name );
		mycount++;
	}
	printf( "list directory count: %g\n", mycount );
	closedir( dirp );
}





void linecount()
{
        double mycount = 0;
        int begin = 1;
        float charpos = 1 ; 
        int c ; 
        c = getchar();
        while( c != EOF )
        {
           begin = 0;
           if ( c == '\n' ) 
           { begin = 1; mycount++; }
           c = getchar();
        }
        printf( "Count: %g\n" , mycount );
}




void void_print_sources_pkg( ) 
{
	printf( "# https://www.devuan.org/os/packages\n" );
	printf( "deb http://archive.devuan.org/merged ascii          main\n" );
	printf( "deb http://archive.devuan.org/merged ascii-security main\n" );
	printf( "\n" ); 
}




char *strsedcutright( int myposition , char *str )
{  
      char ptr[ 5* strlen(str)+1];
      int i,j=0;
      int playit = 0; 
      for(i=0; str[i]!='\0'; i++)
      {
        if ( i == myposition ) 
        {
	  playit = 1; 
        }
        else
        {
	  if ( playit == 1 ) 
             ptr[j++]=str[i];
        }
      } 
      ptr[j]='\0';
      size_t siz = 1 + sizeof ptr ; 
      char *r = malloc( 1 +  sizeof ptr );
      return r ? memcpy(r, ptr, siz ) : NULL;
}




char *dirnameunix(char* fullpath) 
{
     char *e = strrchr( fullpath, '/');
     if(!e){
         char* buf = strdup(fullpath);
         return buf;
     }
     int index = (int)(e - fullpath);
     char* s = (char*) malloc(sizeof(char)*(index+1));
     strncpy(s, fullpath, index);
     s[index] = '\0';
     return s;
}





void music_player( char *musicurl )
{
		if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
	            ncmdwith( " /usr/bin/mplayer " , musicurl ); 

		else if ( fexist( "/usr/bin/mpv" ) == 1 ) 
	            ncmdwith( " /usr/bin/mpv " , musicurl ); 

		else if ( fexist( "/usr/bin/glurp" ) == 1 ) 
	            ncmdwith( " /usr/bin/glurp " , musicurl ); 

		else if ( fexist( "/usr/bin/parole" ) == 1 ) 
	            ncmdwith( " /usr/bin/parole " , musicurl ); 

		else if ( fexist( "/usr/bin/cvlc" ) == 1 ) 
	            ncmdwith( " /usr/bin/cvlc " , musicurl ); 

		else if ( fexist( "/usr/bin/mpv" ) == 1 ) 
	            ncmdwith( " /usr/bin/mpv " , musicurl ); 

		else if ( fexist( "/usr/bin/vlc" ) == 1 ) 
	            ncmdwith( " /usr/bin/vlc " , musicurl ); 

		else if ( fexist( "/usr/bin/mpg123" ) == 1 ) 
	            ncmdwith( " /usr/bin/mpg123 " , musicurl ); 

		else if ( fexist( "/usr/local/bin/ncrun" ) == 1 ) 
		   ncmdwith( " /usr/local/bin/ncrun  " ,  musicurl ); 

		else if ( fexist( "/usr/bin/ffplay" ) == 1 )  // freebsd 
	            ncmdwith( " /usr/bin/ffplay " , musicurl ); 

		else if ( fexist( "/usr/bin/aplay" ) == 1 )  // freebsd 
	            ncmdwith( " /usr/bin/aplay " , musicurl ); 

		else if ( fexist( "/usr/bin/omxplayer" ) == 1 )  // freebsd 
	            ncmdwith( " /usr/bin/omxplayer " , musicurl ); 

		else
		   ncmdwith( " mpg123  " ,  musicurl ); 
}










void procedure_audio_player(  char * myfoourl )
{
	printf( " ========================\n"  ); 
	printf( " PROCEDURE AUDIO PLAYER \n" ); 
	printf( " ========================\n"  ); 
	char foocharo[PATH_MAX];
	printf( "URL: %s\n" , myfoourl ); 
	if ( fexist( "/usr/bin/mpv" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " mpv --no-audio-display  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/vlc" ) == 1 )  // rpi0 
             snprintf( foocharo , sizeof( foocharo ), " vlc --no-video  --play-and-exit  \"%s\"  " , myfoourl ); 


        ///printf( " cat guitar4.wav | cvlc -q   --play-and-exit   -\n" );
	else if ( fexist( "/usr/bin/cvlc" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " cvlc --no-video  \"%s\"  " , myfoourl ); 


	else if ( fexist( "/usr/bin/mplayer" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " mplayer  -vo null \"%s\"  " , myfoourl ); 


	else if ( fexist( "/usr/bin/ffplay" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " ffplay  -nodisp  \"%s\"  " , myfoourl ); 


	else if ( fexist( "/usr/bin/smplayer2" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " /usr/bin/smplayer2  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/parole" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " parole -i \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/totem" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " totem  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/mpg123" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " mpg123  \"%s\"  " , myfoourl ); 

	else if ( fexist( "/usr/bin/omxplayer" ) == 1 ) 
             snprintf( foocharo , sizeof( foocharo ), " omxplayer  \"%s\"  " , myfoourl ); 

	else 
             snprintf( foocharo , sizeof( foocharo ), " mpg123  \"%s\"  " , myfoourl ); 

	nsystem( foocharo ); 
}









#include <stdio.h>
#include <unistd.h>
int main( int argc, char *argv[])
{

        pid_t pid = 0; int status;
	FILE *fpout;
        char foostr[PATH_MAX];   

        // os type, i.e.: 1: freebsd, 2:openbsd, 3:netbsd 
	if ( fexist( "/etc/freebsd-update.conf" ) == 1 )  // freebsd  1
		os_bsd_type = 1;
	else if ( fexist( "/etc/myname" ) == 1 )          // openbsd  2
		os_bsd_type = 2;
	else if ( fexist( "/etc/wscons.conf" ) == 1 )     // netbsd   3
		os_bsd_type = 3;




        int foocounter = 0; 
	char foocwd[PATH_MAX];
	char currentpath[PATH_MAX];
	char targetfile[PATH_MAX];
	char targetdir[PATH_MAX];

	char cmdi[PATH_MAX];
	char charo[PATH_MAX];
	char pathbefore[PATH_MAX];
	char foocmd[PATH_MAX];
	char cwd[PATH_MAX];
	int i; 

	int fookey, choix;




	if ( argc == 2 )
	if ( strcmp( argv[1] , "-yellow" ) ==  0 ) 
	{
		printf("%s", KYEL);
		printf("Hello Yellow Terminal\n");
		return 0;
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] , "-green" ) ==  0 )
	{
		printf("%s", KGRE);
		printf("Hello Terminal\n");
		return 0;
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] ,     "help" ) ==  0 ) 
	if ( strcmp( argv[2] ,     "zypper" ) ==  0 ) 
	{
		printf( "=====================================================\n" ); 
		printf( " Search: zypper se -v \n" ); 
		return 0; 
	}




	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "float" ) ==  0 ) || ( strcmp( argv[1] , "armel" ) ==  0 ) ) 
	{
            system( "  readelf -a /lib/libm.so.6  | grep FP " ); 
            system( "  readelf -a /lib/arm-linux-gnueabihf/libm.so.6  | grep FP " ); 
            return 0; 
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "bunny" ) ==  0 ) 
	{
                nsystem( " mconfig --mpv https://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4 " ); 
		return 0; 
	}





	if ( argc == 2 )
	if ( strcmp( argv[1] ,     "legacy" ) ==  0 ) 
	{
	        // will allow startx on linux 
		npkg(    " xserver-xorg-legacy " );
		return 0;
	}




	if ( argc >= 3 )
	if ( strcmp( argv[1] , "zypper" ) ==  0 ) 
	{
			strncpy( cmdi,  " ", PATH_MAX );
			if ( argc >= 2 )
			{
	                        strncpy( cmdi,  " zypper install ", PATH_MAX );
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else if ( i >= 3 )
					{

						strncat( cmdi , " \""  , PATH_MAX - strlen( cmdi ) -1 );
					        strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , "\" "  , PATH_MAX - strlen( cmdi ) -1 );
					}
				}
		                nsystem( cmdi );
			}
			return 0;
	}






	if ( argc >= 3)
	if ( strcmp( argv[1] , "search" ) ==  0 )  
	{
			strncpy( cmdi , "", PATH_MAX );
			// zypper se -v fltk
			if ( fexist( "/usr/bin/zypper" ) == 1 ) 
			{
				strncpy( cmdi , " zypper se -v " , PATH_MAX ); // check fix
			}
			else
				strncat( cmdi , "   apt-cache search  " , PATH_MAX - strlen( cmdi ) -1 );

			for( i = 2 ; i < argc ; i++) 
			{
				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
			}
			strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
			strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
			system( cmdi );
			return 0; 
	}







	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,   "find" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "--find" ) ==  0 )  
	|| ( strcmp( argv[1] ,   "-find" ) ==  0 ) ) 
	{
	        if  ( strcmp( argv[1] ,   "nfind" ) ==  0 ) printf( "== FIND FILE ==\n" ); 
		strncpy( searchitem, argv[ 2 ] , PATH_MAX );          
		listdir( ".", 0 );
		return 0; 
	}




	if ( argc == 3)
	if ( strcmp( argv[1] , "-cat" ) ==  0 ) 
	{
		ncat_static( argv[ 2 ] );
		return 0;
	}






	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "lscount" ) ==  0 ) || ( strcmp( argv[1] , "-lscount" ) ==  0 ) || ( strcmp( argv[1] , "--lscount" ) ==  0 ) )
	{
		lscount();
		return 0;
	}



	if ( argc >= 2 )
	if ( ( strcmp( argv[1] , "ls" ) ==  0 ) || ( strcmp( argv[1] , "-ls" ) ==  0 ) || ( strcmp( argv[1] , "--ls" ) ==  0 ) )
	{
		if ( argc >= 3 )
		   nls( argv[ 2 ] );
		else
		   // portable ls 
		   nls( "." ); 
		return 0;
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] , "mktmp" ) ==  0 ) 
	{
	        snprintf( charo , sizeof( charo ), " mkdir  tmpdir-%d  " , (int)time(NULL) );
		printf( "Command: %s\n", charo ); 
		nsystem( charo ); 
		return 0; 
	}




       if ( argc == 2)
       if ( strcmp( argv[1] , "-t" ) ==  0 ) 
       {
	       printf( "%d\n", (int)time(NULL));
	       return 0;
       }



       if ( argc == 2)
       if ( strcmp( argv[1] , "lsdir" ) ==  0 ) 
       {
	       printf( "%d\n", (int)time(NULL));
	       nlsdir( );
	       return 0;
       }





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "arec" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-arec" ) ==  0 ) 
	|| ( strcmp( argv[1] , "arecord" ) ==  0 ) )
	{
			printf( "Time: %d\n", (int)time(NULL));
			snprintf( charo , sizeof( charo ), "%d-audio-rec.wav", (int)time(NULL) );
			printf( "Command line: %s\n", charo );
			ncmdwith( " arecord -VV   ", charo ); 
			return 0; 
	}




	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "vrec" ) ==  0 ) || ( strcmp( argv[1] ,     "-vrec" ) ==  0 ) || ( strcmp( argv[1] ,     "--vrec" ) ==  0 ) )
	{
		if ( MYOS == 1 ) 
		if ( fexist( "/usr/bin/ffmpeg" ) == 0 )   
		{
		   npkg_update(); 
		   npkg( " ffmpeg " );
		}

	        snprintf( charo , sizeof( charo ), " ffmpeg  -f video4linux2 -i /dev/video0  %d-vrec-video0.avi    " , (int)time(NULL) );
		printf( "Command: %s\n", charo ); 
		nsystem( charo ); 
		return 0;
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "vrecshot" ) ==  0 ) || ( strcmp( argv[1] ,     "-vrecshot" ) ==  0 ) || ( strcmp( argv[1] ,     "--vrecshot" ) ==  0 ) )
	{
                //   -frames:v 1  
	        snprintf( charo , sizeof( charo ), " ffmpeg  -f video4linux2 -i /dev/video0      -frames:v 1    %d-vrec-video0.png    " , (int)time(NULL) );
		printf( "Command: %s\n", charo ); 
		nsystem( charo ); 
		return 0;
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "scrot" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-scrot" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--screenshot" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--scrot" ) ==  0 ) )
	{
			printf( "Time: %d\n", (int)time(NULL));
			snprintf( charo , sizeof( charo ), "%d-screenshot.png", (int)time(NULL) );
			printf( "Command line: %s\n", charo );
			ncmdwith( "  scrot   ", charo ); 
			return 0; 
	}









	if ( argc == 2 )
	if ( strcmp( argv[1] , "xxterm" ) ==  0 ) 
	{
			nsystem( " export DISPLAY=:0 ;  xterm -bg black -fg yellow     "  ); 
			return 0;
	}











	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) == 0 ) 
	if ( ( strcmp( argv[2] , "openpandora" ) ==  0 ) 
	|| ( strcmp( argv[2] , "pandora" ) ==  0 ) )
	{
		printf( "===== CPU =====  \n" ); 
		printf( "600\n" );
		printf( "400\n" );
		printf( "/proc/pandora$ \n" );
		printf( "cpu_mhz_max  cpu_opp_max  nub0  nub1  sys_mhz_max\n" );
		printf( "mbutton_delay  mbutton_threshold  mbutton_threshold_y  mode  mouse_sensitivity  scroll_rate  scrollx_sensitivity  scrolly_sensitivity\n" );

		printf( "===== PROC SYS =====  \n" ); 
		printf( "LCD Brightness \n" );
		/// Pan 
		printf( "   /usr/sbin/pdcmd LCDBright +    Increase the light of the lid  \n" );
		printf( "   /usr/sbin/pdcmd LCDBright -    Decrease the light of the lid  \n" );
		printf( "   /usr/sbin/pdcmd LCDBright 10   Set the Brightness of the lid to 10 \n" );


		printf( " ==== \n" );
		printf( " ==== \n" );
		printf( "# Some informations\n" );
		printf( "pndLCDBrightness=/sys/devices/platform/pandora-backlight/backlight/pandora-backlight/brightness\n" );
		printf( "pndLCDBrightnessMax=/sys/devices/platform/pandora-backlight/backlight/pandora-backlight/max_brightness\n" );
		printf( "pndLCDEnable=/sys/devices/omapdss/display0/enabled\n" );
		printf( " ==== \n" );
		printf( " ==== \n" );
		return 0;
	}




        // https://docs.gitlab.com/ee/ssh/index.html#generate-an-ssh-key-pair
	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "git" ) ==  0 ) 
	{
	        printf( "  git clone https://gitlab.com/username123/noname  \n" ); 
		return 0;
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "i915" ) ==  0 ) || ( strcmp( argv[2] , "intel" ) ==  0 ) )
	{
               printf( " mconfig textmode " ); 
               printf( "\n" ); 
	       return 0; 
        }




	if ( argc == 3 )
	if ( strcmp( argv[1] , "install" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "i915" ) ==  0 ) || ( strcmp( argv[2] , "intel" ) ==  0 ) )
	{
               //nsystem( " mconfig pkg libdrm drm-kmod drm-fbsd13-kmod " ); 
               nsystem( " mconfig pkg drm-kmod  " );  //only !
	       return 0; 
        }


	if ( argc == 3 )
	if ( strcmp( argv[1] , "install" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "i915-force" ) ==  0 ) || ( strcmp( argv[2] , "intel-force" ) ==  0 ) )
	{
               //nsystem( " mconfig pkg libdrm drm-kmod drm-fbsd13-kmod " ); 
               nsystem( " mconfig pkg libdrm drm-kmod drm-fbsd13-kmod " ); 
               nsystem( " mconfig pkg drm-kmod  " );  //only !
	       return 0; 
        }





	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "net" ) ==  0 )  
	|| ( strcmp( argv[2] , "NetworkManager" ) ==  0 )  
	|| ( strcmp( argv[2] , "networkmanager" ) ==  0 ) ) 
	{
		printf( " HELP NET \n" );
		//system( " whereis NetworkManager " ); 
		printf( " systemctl --no-pager status " ); 
		printf( " networkctl  ; ip addr  "     );  
		printf( " help: systemctl stop NetworkManager.service  \n" ); 
		printf( " help:  systemctl disable NetworkManager.service  \n"); 
                printf( " /usr/bin/nm-connection-editor is given by network-manager-gnome  \n" ); 
                printf( " iwlist  ---> into: wireless-tools  \n" ); 
		return 0; 
	}










        // nsystem( " firefox -new-window https://outlook.office365.com/mail " );   
	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,  "email" ) ==  0 ) 
	|| ( strcmp( argv[1] ,  "mail" ) ==  0 )  
	|| ( strcmp( argv[1] ,  "--email365" ) ==  0 )  
	|| ( strcmp( argv[1] ,  "--mail365" ) ==  0 ) )  // <- fltk 
	{
               nsystem( " mconfig --firefox  https://outlook.office.com/mail/    " ); 
	       return 0; 
        }

	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,  "cal" ) ==  0 )
	|| ( strcmp( argv[1] ,  "--cal365" ) ==  0 )  
	|| ( strcmp( argv[1] ,  "cal365" ) ==  0 ) ) 
	{
               nsystem( " mconfig --firefox https://outlook.office365.com/calendar/view/workweek" );   
	       return 0; 
        } 

	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "--compose" ) ==  0 ) 
	|| ( strcmp( argv[1] ,  "compose" ) ==  0 )   
	|| ( strcmp( argv[1] ,  "--compose-email" ) ==  0 ) )  
	{
                       nsystem( "  firefox -new-window 'https://outlook.office.com/mail/deeplink/compose'  " ); 
		       return 0; 
	}







	if  ( argc == 2)
	if ( strcmp( argv[1] , "dpms" ) ==  0 )
	{
		nsystem( " xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0  " );
		nsystem( " export DISPLAY=:0 ; xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0  " );
		nsystem( " xsetfork s off ;  xsetfork -dpms ;  xsetfork s noblank  ; setterm -blank 0  " );
		return 0;
	}



	if  ( argc == 2)
	if ( strcmp( argv[1] , "xdpms" ) ==  0 )
	{
		nsystem( " export DISPLAY=:0 ;  xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0  " );
		nsystem( " export DISPLAY=:0 ;  xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0  " );
		nsystem( " xsetfork s off ;  xsetfork -dpms ;  xsetfork s noblank  ; setterm -blank 0  " );
		return 0;
	}







	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "chroot+" ) ==  0 ) 
	|| ( strcmp( argv[1] , "chroot" ) ==  0 ) ) 
	{
	        // mtab added for manjaro 
	        nsystem( " xhost + ; mount -t proc /proc proc ; mount -t sysfs /sys sys ; mount -o bind /dev dev ; mount -t devpts pts dev/pts  ; rm etc/mtab ; cp /etc/mtab etc/mtab   ;  cp /etc/resolv.conf etc/resolv.conf ; chroot . "); 
		return 0;
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "mmcsys" ) ==  0 ) || ( strcmp( argv[1] , "mmc-sys" ) ==  0 ) ) 
	{
	        // openpandora ... using right mmc p2 
	        nsystem( " xhost + ;  mconfig mount /dev/mmcblk1p2 ; cd /media/mmcblk1p2 ; mount -t proc /proc proc ; mount -t sysfs /sys sys ; mount -o bind /dev dev ; mount -t devpts pts dev/pts  ; rm etc/mtab ; cp /etc/mtab etc/mtab   ;  cp /etc/resolv.conf etc/resolv.conf ; chroot . "); 
		return 0;
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "chroot" ) ==  0 ) 
	{
                if ( os_bsd_type == 1 ) 
		{
		   nsystem( " mount -t devfs dev dev " );
		}
		nsystem( "  cp /etc/resolv.conf etc/resolv.conf ; chroot . "); 
		return 0;
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "chroot+" ) ==  0 ) 
	{
                if ( os_bsd_type == 1 ) 
		{
		   nsystem( " mount -t devfs dev dev " );
		}
		nsystem( "  cp /etc/resolv.conf etc/resolv.conf ; chroot . "); 
		return 0;
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "--chroot-mnt" ) ==  0 )  
	{
		nsystem( "mount -t proc proc /mnt/proc  ;  mount -t sysfs sys /mnt/sys ; mount -o bind /dev /mnt/dev ; mount -t devpts pts /mnt/dev/pts  ; cp /etc/resolv.conf /mnt/etc/resolv.conf ; chroot /mnt  "); 
		return 0; 
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	{
		printf( "  == HELP == \n" ); 
		printf( " mconfig nmap     100    " ); 
		printf( " mconfig connect  100 10 " ); 
		printf( " openwrt :  4422  " ); 
		return 0;
	}












	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "diskspace" ) ==  0 ) 
	{
		printf( " part1: netbsd untar i386 9.1 /TEST# du -hs /TEST/\n");
		printf( "   934M    /TEST/ so 3GB needed.\n");
		printf( " part2: freebsd nomad  \n" ); 
		printf( "    2.1G to 3gb or 4gb \n "); 
		// 3 netbsd 
		// 4 nomadbsd freebsd  
		// 4 devuan amd64 - as rescue
		// 4 devuan i386 devuan presenter with win theme with mupdf and automount  
		//   newer kernel 
		return 0;
	}









	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "devuan" ) ==  0 ) 
	{
		printf( "\n" ); 
	        printf( " SID  : DEBIAN (newest, testing)\n" );  
	        printf( " CERES: DEVUAN (newest, testing)\n" );  
		printf( "\n" ); 
	        printf( " Example: debootstrap  --no-check-gpg    --include=debootstrap,wpasupplicant  ceres .   http://pkgmaster.devuan.org/merged   ; mkdir usr/src  " ); 
		printf( "\n" ); 
		printf( "\n" ); 
		printf( " =============== \n" ); 
		printf( " =============== \n" ); 
		printf( " = Releases = \n" ); 
		printf( " =============== \n" ); 
		printf( " =============== \n" ); 
		printf( "\n" ); 
		printf( "Devuan release codenames\n" );
		printf( "Stable releases\n" );
		printf( "Devuan release	Suite	Released	Planet nr.	Debian release	Status\n" );
		printf( "Chimaera	stable	2021-10-14	623	Bullseye	Maintained\n" );
		printf( "Beowulf	oldstable	2020-06-01	38086	Buster	Maintained\n" );
		printf( "ASCII	oldoldstable	2018-06-08	3568	Stretch	Maintained\n" );
		printf( "Jessie	oldoldoldstable	2017-05-25	10464	Jessie	Archived\n" );
		printf( "In development\n" );
		printf( "Devuan release	Suite	Released	Planet nr.	Debian release	Status\n" );
		printf( "Daedalus	in development	Not released	1864	Bookworm	Maintained\n" );
		printf( "Ceres	unstable	Not released	1	Sid	Maintained\n" );
		printf( " =============== \n" ); 
		printf( " =============== \n" ); 
		// https://minorplanetcenter.net
		return 0;
	}













	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "cc" ) ==  0 ) 
	{
	        printf( " cc -DVWM  -lm   -I/usr/X11R7/include/ -I/usr/pkg/include -I /usr/pkg/include/    -L/usr/pkg/lib  -L/usr/X11R7/lib -lX11 -I /usr/X11R7/include/    *.c      -o   x11application  " ); 
		printf( "\n" ); 
		return 0;
	}








       /*
           Reference: http://www.netbsd.org/docs/network/
           static IP: 
           First
           vi /etc/ifconfig.vr0:
           inet 192.168.0.49 netmask 255.255.255.0
           vi /etc/mygate
           put your gateway, only one ip, e.g. 192.168.0.1
           vi /etc/resolv.conf
           nameserver 8.8.8.8
           nameserver 8.8.4.4
           
           for me is enough
           
           afterward run: /etc/rc.d/network restart
        */

/*
 FreeBSD
 ifconfig_em0="DHCP"
 Replace it as follows:
 ifconfig_em0="inet 192.168.2.20 netmask 255.255.255.0"
*/
	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "network" ) ==  0 ) 
	{
		printf( "vi /etc/ifconfig.vr0:\n");
		printf( "inet 192.168.0.49 netmask 255.255.255.0\n");
		printf( "> NETBSD: vi /etc/ifconfig.vr0:\n");
		printf( "inet 192.168.0.49 netmask 255.255.255.0\n");
                printf( "> FREEBSD in /etc/rc.conf on ryzen:   ifconfig_re0=\"inet 192.168.2.20 netmask 255.255.255.0\" \n" ); 
                printf( "> FREEBSD in /etc/rc.conf on PI 3b:   ifconfig_ue0=\"inet 10.0.0.11 netmask 0xffffff00\"\n"); 
		printf( " \n" ); 
		printf( "/etc/resolv.conf:\n" ); 
		printf( "# Generated by resolvconf\n" ); 
		printf( "nameserver 10.0.0.238\n"); 
		printf( " \n" ); 
		printf( " static will need / defaultrouter in rc.conf on FreeBSD !\n" ); 
		printf( " \n" ); 

	        //nsystem( " systemctl status NetworkManager.service "); 
		printf( " help: systemctl stop NetworkManager.service  \n" ); 
		printf( " help:  systemctl disable NetworkManager.service  \n"); 
                printf( " /usr/bin/nm-connection-editor is given by network-manager-gnome  \n" ); 
                printf( " iwlist  ---> into: wireless-tools  \n" ); 
                printf( " ----\n" ); 
                printf( " ----\n" ); 
                printf( " Bookworm systemd: /etc/NetworkManager/system-connections/WIFI1234234.nmconnection  \n" ); 
		printf( "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n" );
		printf( "update_config=1\n" );
                printf( " ----\n" ); 
	        return 0; 
	}









	if ( argc == 2 )
	if (  ( strcmp( argv[1] , "networkmanager" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--start-daemon-network" ) ==  0 )      /// <--- fl   
	|| ( strcmp( argv[1] , "network" ) ==  0 )   ) 
	{
		nsystem( " systemctl enable NetworkManager.service" ); 
		nsystem( " systemctl start NetworkManager.service" ); 
		return 0; 
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "xnetwork" ) ==  0 ) 
	{
	        procedure_console_webbrowser( "http://www.netbsd.org/docs/network/" ); 
		return 0;
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "getty" ) ==  0 ) 
	{
	     printf( " /usr/libexec/getty std.9660  ttyE1 > /dev/ttyE1 " );
	     printf( "\n"); 
	     printf( " /usr/libexec/getty std.9660  ttyE1 > /dev/ttyE1 " );
	     printf( "It runs from 0 to ttyE7.\n"); 
	     printf( " /usr/libexec/getty std.9660 ttyE7 > /dev/ttyE7 " );
	     printf( "\n"); 
	     return 0;
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "cdrom" ) ==  0 ) 
	{
		printf( "	 mount -t cd9660 /dev/cd0a /mnt  \n" ); 
		return 0; 
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "cd73" ) ==  0 ) 
	{
		fetch_file_ftp( "https://cdn.openbsd.org/pub/OpenBSD/7.3/amd64/cd73.iso" ); 
		return 0; 
        }




	if ( argc == 2 )
	if ( strcmp( argv[1] , "us" ) ==  0 )
	{
			if ( MYOS == 1 ) 
			  nsystem( "  setxkbmap us " );
			else 
			  nsystem( "  wsconsctl  keyboard.encoding=us  " );
			return 0;
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] , "de" ) ==  0 )
	{
			//hhnsystem( "  setxkbmap de " );
			//hhnsystem( "  wsconsctl  keyboard.encoding=de  " );
			if ( MYOS == 1 ) 
			  nsystem( "  setxkbmap de " );
			else 
			  nsystem( "  wsconsctl keyboard.encoding=de " );
			return 0;
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "overscan" ) ==  0 ) 
	{
			printf( "# netbsd or linux \n" );
			printf( "#/boot/config.txt\n" );
			printf( "disable_overscan=1\n" );
			return 0;
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "overscan" ) ==  0 ) 
	{
			printf( "# netbsd \n" );
			printf( "#/boot/config.txt\n" );
			printf( "disable_overscan=1\n" );
			return 0;
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "keyb" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "de" ) ==  0 ) 
	{
		   nsystem( "echo 'encoding de'                >> /etc/wscons.conf " );
		  ///nsystem( "echo 'disable_overscan=1'         >> /boot/config.txt " );
                   printf( "  ");
		   return 0; 
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "dmi" ) ==  0 ) || ( strcmp( argv[1] ,   "machine" ) ==  0 ) )
	{
		system( " lscpu   " );   
		system( " dmesg | grep 'DMI:' " );
		system( " dmesg |   grep 'Rasp'  " );   
		return 0;
	}

	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "mem" ) ==  0 ) 
	|| ( strcmp( argv[1] , "memory" ) ==  0 ) || ( strcmp( argv[1] , "free" ) ==  0 ) )
	{
		system( " vmstat  " );
		system( " vmstat -S M " );
		system( " vmstat -S K " );
		system( " free     " );
		system( " free -h  " );
		return 0;
	}

	if ( argc == 3 )
	if ( strcmp( argv[1] , "get" ) ==  0 ) 
	if ( strcmp( argv[2] , "music" ) ==  0 ) 
	{
		fetch_file_ftp( "https://milkytracker.org/songs/Decibelter-Cosmic_'Wegian_Mamas.mp3" );
		fetch_file_ftp( "https://milkytracker.org/songs/Decibelter-Where's_Space.mp3" );
		return 0;
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "kernel" ) ==  0 ) 
	{
		   printf( "=====================\n" );
		   printf( " BSD Unix \n" );
		   printf( "=====================\n" );
                   printf( " sysctl kern.geom.debugflags=16 ");
                   printf( " mount -u -o rw /dev/ufs/FreeBSD_Install /   ");
                   printf( "  ");
                   printf( " boot: 0  (installed on ada0s2a)");
                   printf( " vbe set 0x17e " ); 
                   printf( " lsdev " );  
                   printf( " boot disk0s3:/boot/kernel/kernel \n" );    
                   printf( " multiboot disk0s3:/boot/kernel/kernel root=ada0s3 \n" );    
                   printf( "  \n");
		   printf( "=====================\n" );
                   printf( " pi3 pi srv: 4.14.98-v7+ stretch #1200 \n");
		   printf( " RetroPie-Setup version: 4.5.1 (f90600a2)\n" );
		   printf( " 573e75874b4b04c2138833f7ab29f007 cm  \n" ); 
		   printf( "System: Raspbian GNU/Linux 9.9 (stretch) - Linux retropie 4.14.98-v7+ #1200 SMP Tue Feb 12 20:27:48 GMT 2019 armv7l GNU/Linux\n" );
		   printf( "\n" );
		   printf( "=====================\n" );
		   printf( "= = = = = = = = = = = = = = = = = = = = =\n" );
		   printf( "=====================\n" );
                   printf( " pi3  \n");
                   printf( " Linux rpi0 buster retropie 5.10.103+ #1529 Tue Mar 8 12:19:18 GMT 2022 armv6l GNU/Linux \n" ); 
		   printf( "=====================\n" );
		   return 0; 
	}








	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "unix" ) ==  0 ) 
	{
             procedure_xwebbrowser( "https://homepage.cs.uri.edu/~thenry/resources/unix_art/ch01s06.html" ); 
	     return 0; 
        }








	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "irssi" ) ==  0 ) 
	{
                 printf( "cd src ; ./configure --prefix=/pkg --with-perl-lib=/pkg/lib/perl --with-modules --with-perl=module  ; make "); 
		 printf( "\n" ); 
		 return 0; 
        }



	if ( argc == 3 )
	if ( strcmp( argv[1] , "compile" ) ==  0 ) 
	if ( strcmp( argv[2] , "irssi" ) ==  0 ) 
	{
                 npkg( "libssl-dev" ); 
                 fetch_file_ftp( "http://deb.debian.org/debian/pool/main/i/irssi/irssi_1.0.7.orig.tar.xz");
                 nsystem( " tar xf irssi_1.0.7.orig.tar.xz");
                 nsystem( "cd irssi ; ./configure --prefix=/pkg --with-perl-lib=/pkg/lib/perl --with-modules --with-perl=module  ; make "); 
		 printf( "\n" ); 
		 return 0; 
        }









	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,      "install" ) ==  0  ) 
	|| ( strcmp( argv[1] ,    "opi" ) ==  0  ) 
	|| ( strcmp( argv[1] ,    "ins" ) ==  0  ) )
	if ( ( strcmp(   argv[2] ,    "codecs" ) ==  0  ) || ( strcmp(   argv[2] ,    "codec" ) ==  0  ) )
	{
                if ( MYOS == 1 ) 
		if ( fexist( "/usr/bin/zypper" ) == 1 )  // oh, man, opensuse... 
		{
			nsystem( "  zypper in opi " ); 
			nsystem( "  opi codecs " );   
			// opi codecs
		}
		printf( "\n" ); 
		return 0; 
        }




	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,    "install" ) ==  0  ) || ( strcmp( argv[1] ,    "ins" ) ==  0  ) )
	if ( strcmp(   argv[2] ,    "kde" ) ==  0  ) 
	{
	        npkg_update();  
		nsystem( " mconfig install xorg  " ); 
		nsystem( " apt-get install --download-only -y  kde-standard "); 
		nsystem( " apt-get install --download-only -y  xserver-xorg  "); 
		nsystem( " apt-get install --download-only -y  xserver-xorg xinit xterm  "); 
		npkg(    "  kde-standard " );
		npkg(    " xpaint        " );
		npkg(    " mupdf       " );
		npkg(    " xclip     " );
		npkg(    " less     " );
		npkg( " telnet "); 
		npkg( " ncftp  "); 
		npkg( "     libx11-dev  "  );     // for evilwm 
		npkg( "     libfltk1.3-dev  " );
		npkg( "     ssh    " );
		npkg( "     sshfs  " );
		npkg(    " xdemineur    " );
		return 0; 
        }







	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,    "install" ) ==  0  ) || ( strcmp( argv[1] ,    "ins" ) ==  0  ) )
	if ( strcmp(   argv[2] ,    "alpine-xfce" ) ==  0  ) 
	{
                 nsystem(  " apk update ; setup-xorg-base ; apk add xfce4 lightdm xterm xhost alsa-utils ; rc-service dbus start ; rc-update add dbus ; rc-update add lightdm ; rc-service lightdm start ");
                 nsystem( "  apk add clang gcc make libc-dev musl-dev " );   // alpine linux  , but path_max missing 
		 return 0; 
        }





	if ( argc == 2 )
	if ( strcmp( argv[1] ,    "cups" ) ==  0  ) 
	{
                nsystem( " links 127.0.0.1:631 " ); 
		return 0; 
        }




	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,    "install" ) ==  0  ) || ( strcmp( argv[1] ,    "ins" ) ==  0  ) )
	if ( strcmp(   argv[2] ,    "grub-loc-wd0" ) ==  0  ) 
	{
		nsystem( "  mkdir /media " ); 
		nsystem( "  mconfig mount wd0a " ); 
		nsystem( "  grub-install --no-floppy --root-directory=/media/wd0a  /dev/rwd0   " );
		nsystem( "  mconfig grub > /media/wd0a/grub/grub.cfg  " ); 
		nsystem( "  mconfig boot > /media/wd0a/boot.cfg  " ); 
		return 0; 
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] , "lilo" ) ==  0 ) 
	{
		printf( "######################################################\n" ); 
		printf( "### /etc/lilo.conf on Slackware 14.2 \n" ); 
		printf( "### Multiboot, BSD, Slackware and other Linux\n" ); 
		printf( "######################################################\n" ); 
		printf( "\n" ); 
		printf( "# LILO configuration file\n" ); 
		printf( "# generated by 'liloconfig'\n" ); 
		printf( "#\n" ); 
		printf( "# Start LILO global section\n" ); 
		printf( "lba32 # Allow booting past 1024th cylinder with a recent BIOS\n" ); 
		printf( "boot = /dev/sda\n" ); 
		printf( "#compact # faster, but won't work on all systems.\n" ); 
		printf( "\n" ); 
		printf( "# Boot BMP Image.\n" ); 
		printf( "# Bitmap in BMP format: 640x480x8\n" ); 
		printf( "bitmap = /boot/slack.bmp\n" ); 
		printf( "# Menu colors (foreground, background, shadow, highlighted\n" ); 
		printf( "# foreground, highlighted background, highlighted shadow):\n" ); 
		printf( "bmp-colors = 255,0,255,0,255,0\n" ); 
		printf( "# Location of the option table: location x, location y, number of\n" ); 
		printf( "# columns, lines per column (max 15), spill (this is how many\n" ); 
		printf( "# entries must be in the first column before the next begins to\n" ); 
		printf( "# be used. We don't specify it here, as there's just one column.\n" ); 
		printf( "bmp-table = 60,6,1,16\n" ); 
		printf( "# Timer location x, timer location y, foreground color,\n" ); 
		printf( "# background color, shadow color.\n" ); 
		printf( "bmp-timer = 65,27,0,255\n" ); 
		printf( "# Standard menu.\n" ); 
		printf( "# Or, you can comment out the bitmap menu above and\n" ); 
		printf( "# use a boot message with the standard menu:\n" ); 
		printf( "#message = /boot/boot_message.txt\n" ); 
		printf( "\n" ); 
		printf( "# Append any additional kernel parameters:\n" ); 
		///jhprintf( "append=" vt.default_utf8=0"\n" ); 
		printf( "prompt\n" ); 
		printf( "timeout = 300\n" ); 
		printf( "# Normal VGA console\n" ); 
		printf( "vga = normal\n" ); 
		printf( "# Ask for video mode at boot (time out to normal in 30s)\n" ); 
		printf( "#vga = ask\n" ); 
		printf( "# VESA framebuffer console @ 1024x768x64k\n" ); 
		printf( "# vga=791\n" ); 
		printf( "# VESA framebuffer console @ 1024x768x32k\n" ); 
		printf( "# vga=790\n" ); 
		printf( "# VESA framebuffer console @ 1024x768x256\n" ); 
		printf( "# vga=773\n" ); 
		printf( "# VESA framebuffer console @ 800x600x64k\n" ); 
		printf( "# vga=788\n" ); 
		printf( "# VESA framebuffer console @ 800x600x32k\n" ); 
		printf( "# vga=787\n" ); 
		printf( "# VESA framebuffer console @ 800x600x256\n" ); 
		printf( "# vga=771\n" ); 
		printf( "# VESA framebuffer console @ 640x480x64k\n" ); 
		printf( "# vga=785\n" ); 
		printf( "# VESA framebuffer console @ 640x480x32k\n" ); 
		printf( "# vga=784\n" ); 
		printf( "# VESA framebuffer console @ 640x480x256\n" ); 
		printf( "# vga=769\n" ); 
		printf( "# ramdisk = 0 # paranoia setting\n" ); 
		printf( "# End LILO global section\n" ); 
		printf( "# Linux bootable partition config begins\n" ); 
		printf( "# Linux bootable partition config ends\n" ); 
		printf( "# Linux bootable partition config begins\n" ); 
		printf( "\n" ); 
		//printf( "	linux	/boot/vmlinuz-6.1-x86_64 root=/dev/sda1  rw \n" );   
		//printf( "	initrd	/boot/initramfs-6.1-x86_64.img\n" );
		printf( "\n" ); 
		printf( "image = /boot/vmlinuz-6.1-x86_64\n" ); 
		printf( "initrd = /boot/initramfs-6.1-x86_64.img\n" ); 
		printf( "root = /dev/sda1\n" ); 
		printf( "label = manjaro \n" ); 
		printf( "read-only # Partitions should be mounted read-only for checking\n" ); 
		printf( "\n" ); 
		printf( "\n" ); 
		return 0; 
	}














	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "grub" ) ==  0 ) 
	{
		printf( " ======================= \n" );
		printf( "  mconfig mount wd0a " ); 
		printf( "  grub-install --no-floppy --root-directory=/media/wd0a  /dev/rwd0   \n" );
		printf( "  mconfig grub > /media/wd0a/grub/grub.cfg  \n" ); 
		printf( "  mconfig boot > /media/wd0a/boot.cfg  \n" ); 
		printf( " ======================= \n" );
                printf( " set root=(hd0,msdos3)   kfreebsd /boot/kernel/kernel  set kFreeBSD.vfs.root.mountfrom=ufs:/dev/ada0s3     set kFreeBSD.vfs.root.mountfrom.options=rw  \n" ); 
		printf( " ======================= \n" );

		printf( "	For syslinux, just changing the initrd to have a path to the ISO image should be enough. (Untested.)" );
		printf( "	label freebsd-install" );
		printf( "	menu label FreeBSD Install" );
		printf( "	    kernel memdisk" );
		printf( "	    initrd bootonly.iso" );
		printf( "	    append iso raw" );
		printf( " ======================= \n" );
		printf( "    mconfig install grub-loc-sd0   (usb pen)\n" );
		printf( "    mconfig install grub-loc-sd1   (usb pen)\n" );
		printf( "  ==== For a very much new disk: \n" );
		printf( "   disklabel -D wd0 \n" );
		printf( "   disklabel -a wd0 \n" );
		printf( "   newfs wd0a \n" );
		printf( "  mconfig install grub-loc-wd0   (main harddisk)\n" );
		printf( " ======================= \n" );
		return 0;
	}









	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "--hostname" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-hostname" ) ==  0 ) ) 
	{
		printf( " -- SET HOSTNAME -- \n" ); 
		if ( fexist( "/etc/hostname" ) == 1 ) 
		{
		  ncmdwith( " hostname ",  argv[ 2 ] );
		  snprintf( charo , sizeof( charo ), " echo '%s' > /etc/hostname ", argv[ 2 ] );   
		  nsystem( charo );
		}
		return 0;
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "man"  ) ==  0 ) 
	{
		if ( fexist( "/usr/bin/links2" ) == 1 ) 
			snprintf( charo , sizeof( charo ), "   /usr/bin/links2 \"https://linux.die.net/man/1/%s\" ",  argv[ 2 ] );
		else
			snprintf( charo , sizeof( charo ), "   links \"https://linux.die.net/man/1/%s\" ",  argv[ 2 ] );
		nsystem( charo );
		return 0; 
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "terminal" ) ==  0 ) 
	{
		nsystem( " xterm   -fa 'Liberation Mono'   -bg black -fg yellow -fs 19  " );
		return 0;
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "xbox" ) ==  0 ) 
	{
		nsystem( "  export DISPLAY=:0 ; xterm   -fa 'Liberation Mono'   -bg black -fg yellow -fs 19  " );
		return 0;
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "box" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "-box" ) ==  0 )  
	|| ( strcmp( argv[1] ,   "--box" ) ==  0 ) )  //flde --box 
	{
		///nsystem( "   xterm   -fa 'Liberation Mono'   -bg black -fg yellow -fs 19  " );
	        if ( fexist( "/usr/bin/xterm" ) == 1 ) 
		   nsystem( "   xterm   -fa 'Liberation Mono'   -bg black -fg yellow -fs 19  " );
		else 
		   nsystem( "  mconfig --konsole  " );
		return 0;
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "kmap" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-kmap" ) ==  0 ) 
	|| ( strcmp( argv[1] , "nub" ) ==  0 ) 
	|| ( strcmp( argv[1] , "nubs" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--kmap" ) ==  0 ) ) 
	{
                nsystem( "  loadkeys /etc/pandian/pandian.kmap   ; /etc/pandian/pandora-nubs start   " ); 
		return 0;
	}









	if ( argc == 2 )
	if ( strcmp( argv[1] , "netbase" ) ==  0 )
	{
		//printf( " == NETBASE == \n" ); 
		printf( " == CODE NCONFIG -- NETBASE \n" ); 
	        /// to be sure that it works on arm64 rpi4 !! 
		nsystem( " touch /boot/ssh "  ); 

		npkg_update(); 
		npkg( " subversion " ); 

		npkg( " screen  " ); 

		npkg( " make   " ); 
		npkg( " gcc    " ); 
		npkg( " ncftp  " ); 


		npkg( " netcat " ); 
		npkg( " wget " ); 

		nsystem( " touch /boot/ssh " ); 

		npkg( " subversion " ); 
		npkg( " make " ); 
		npkg( " ncftp " ); 

		if ( MYOS == 1 ) 
			if ( fexist( "/usr/bin/apt-get" ) == 1 ) 
				npkg_update(); 


		nsystem( " mkdir /usr/local/     " ); 
		nsystem( " mkdir /usr/local/bin/ " ); 
		nsystem( " mkdir /usr/src/ ; chmod 777 /usr/src/ " ); 

		if ( os_bsd_type == 1 ) 
		{
			printf( " FreeBSD \n" ); 
			nsystem( "  /etc/rc.d/sshd onestart " );
			// lzma ? 
		}

		if ( os_bsd_type == 3 ) //netbsd 
		{
			npkg( " screen "); 
		}

		if ( MYOS == 1 ) 
		{
			//npkg_update();
			//npkg( " wget less  " ); 
			//npkg( " subversion " ); 
			printf( "  Path: %s\n", getcwd( cwd , PATH_MAX ) );
			//printf( "  Package: net-tools gives /sbin/ifconfig\n" ); 
			//printf( "  Package: ifupdown  gives /sbin/ifup\n" ); 
			//npkg( " ifupdown    " ); 
			//npkg( " net-tools   " ); 
			//npkg( " screen      " );  // for fltk  
			//npkg( " subversion   " ); 
			//npkg( " links   " ); 
			//npkg( " sshfs   " ); 
			//npkg( " ssh   " ); 
			npkg( "  subversion " );
			//npkg( " openssh   " ); 
			//npkg( " openssh-server   " ); 
			//nsystem( " /etc/init.d/ssh start "); 
			//npkg( " ncurses-dev  " ); 
			//npkg( " dvtm " ); 
			npkg( " wget less  " ); 
			npkg( " subversion " );  // back !
		}


		if ( MYOS == 1 )
		{
			nsystem( " mconfig install gcc " ); 
			npkg( " make " );   
		}

		if ( MYOS == 1 )
		{
			// for rpi0 
			npkg( " links       " ); 
			npkg( " subversion  " ); 
		}

		if ( MYOS == 1 )
		if ( fexist( "/usr/bin/emulationstation" ) == 1 ) 
		{
				//	nsystem( "  ln -s /usr/bin/chromium-browser  /usr/bin/chromium " ); 
				nsystem( " touch /boot/ssh " ); 
		}

		if ( MYOS == 1 )
		{
			//npkg_update(); 
			//printf( " If you run sid and you need to reinstall on sda2 ..\n" ); 
			//npkg( " debootstrap  " ); 
			//npkg( " lzma  " ); 
		}


		if ( MYOS == 1 ) 
			if ( fexist( "/usr/bin/apt-get" ) == 1 ) 
			{
				/*
				   printf( "============\n" );
				   printf( " Debian or alike, checking the dhclient and wifi files.\n" );
				   printf( "============\n" );
				   npkg( " isc-dhcp-client " ); 
				   npkg( " wpasupplicant "   ); 
				   npkg( " less " ); 
				   npkg( " wget " ); 

				   npkg( " gcc " ); 
				   npkg( " make " ); 

				   printf( "============\n" );
				//npkg( " make " ); 
				 */

				printf( "============\n" );
				if ( fexist( "/usr/bin/apt-get" ) == 1 ) 
					nsystem( " apt-get remove --purge -y mandb  " ); 
				if ( fexist( "/usr/bin/apt-get" ) == 1 ) 
					nsystem( " apt-get remove --purge -y man  " ); 
				nsystem( " apt-get remove --purge -y usbmount  " ); 
				nsystem( " apt-get remove --purge -y man-db  " ); 
				nsystem( " apt-get remove --purge -y man  " ); 
				printf( "============\n" );
			}
		return 0; 
	}
	// end of netbase










	if ( argc == 2 )
	if ( strcmp( argv[1] , "xnetbase" ) ==  0   ) 
	{
	        npkg_update(  ); 
		nsystem( " touch /boot/ssh " ); 

		npkg( " screen    "  ); 
		npkg( "  ncftp    "  );  // find ip where ...   
		npkg( "  xterm    "  );  // find ip where ...   

		////npkg( " openssh-server "  ); 
		//nsystem( " /etc/init.d/ssh   start  " ); 
		//nsystem( " /etc/init.d/sshd  start  " ); 

		npkg( " subversion "  ); 

	        npkg( " xterm   "  ); 
	        npkg( " xclip   "  ); 
	        npkg( " screen   "  ); 

		npkg( " ncftp "  ); 
		//npkg( " sshfs "  ); 
		npkg( " screen "  ); 
		//npkg( " mpg123 "  ); 
	        //npkg( " gcc g++ "  ); 
	        npkg( " screen "  ); 
	        npkg( " xterm "  ); 

	        npkg( " libfltk1.3-dev "  ); 
	        npkg( " xclip   "  ); 
	        npkg( " wmctrl   "  ); 

	        npkg( " fluid  "  ); 
	        npkg( " fltk   "  ); 

	        npkg( " ncurses-dev   "  ); 
	        //npkg( " telnet  "  ); 

	        //npkg( " mpg123  "  ); 
	        npkg( " wmctrl  "  ); 
		npkg( " x11-xserver-utils " );   // xrandr  and maybe xset ???
	        npkg( " xterm  "  ); 
	        npkg( " xinit  "  ); 

		printf( "============\n" );
		// done 
		return 0;
		/// end of netbase
	}



























	if ( argc == 2 )
	if ( strcmp( argv[1] , "apache-start" ) ==  0 ) 
	{
	  nsystem( " /usr/local/etc/rc.d/apache24 onestart " ); 
	  return 0; 
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] , "enable" ) ==  0 ) 
	if ( strcmp( argv[2] , "sshd" ) ==  0 ) 
	{
	      // manjaro 
		nsystem( "    pacman -S openssh  " ); 
		nsystem( "    systemctl status sshd.service" ); 
		nsystem( "    cat  /etc/ssh/sshd_config" ); 
		nsystem( "    systemctl enable sshd.service" ); 
		nsystem( "    systemctl start sshd.service" ); 
		nsystem( "    echo  ssh tuxfixer@localhost " ); 
		return 0; 
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "ssh" ) ==  0 ) || ( strcmp( argv[1] , "sshd" ) ==  0 ) )
	{
	   if ( fexist( "/etc/init.d/ssh" ) == 1 ) 
	     nsystem( " /etc/init.d/ssh start  " ); 

	   if ( fexist( "/etc/init.d/sshd" ) == 1 ) 
	     nsystem( " /etc/init.d/sshd start  " ); 

	   nsystem( "  systemctl start  sshd.service " ); 
	   nsystem( "  systemctl start sshd ; systemctl enable sshd " ); 

	   nsystem( "  systemctl start  sshd.service " ); 
	   nsystem( "  systemctl enable sshd.service " ); 
	   return 0; 
        }







	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "service" ) ==  0 ) 
	{
		printf( "\n" ); 
		printf( "# sudo nano /lib/systemd/system/wpa_supplicant@wlan0.service\n" ); 
		printf( "\n" ); 
		printf( "[Unit]\n" ); 
		printf( "Description=WPA-Supplicant-Daemon (wlan0)\n" ); 
		printf( "Requires=sys-subsystem-net-devices-wlan0.device\n" ); 
		printf( "BindsTo=sys-subsystem-net-devices-wlan0.device\n" ); 
		printf( "After=sys-subsystem-net-devices-wlan0.device\n" ); 
		printf( "Before=network.target\n" ); 
		printf( "Wants=network.target\n" ); 
		printf( "\n" ); 
		printf( "[Service]\n" ); 
		printf( "Type=simple\n" ); 
		printf( "RemainAfterExit=yes\n" ); 
		printf( "ExecStart=/sbin/wpa_supplicant -qq -c/etc/wpa_supplicant.conf -Dnl80211 -iwlan0\n" ); 
		printf( "Restart=on-failure\n" ); 
		printf( "\n" ); 
		printf( "[Install]\n" ); 
		printf( "Alias=multi-user.target.wants/wpa_supplicant@wlan0.service\n" ); 
		printf( "\n" ); 
		printf( "# Speichern und schliessen mit Strg + O, Return, Strg + X.\n" ); 
		printf( "# Dann aktivieren und starten wir die neue systemd-Unit.\n" ); 
		printf( "\n" ); 
		printf( "# sudo systemctl enable wpa_supplicant@wlan0.service\n" ); 
		printf( "# sudo systemctl start wpa_supplicant@wlan0.service\n" ); 
		printf( "\n" ); 
		printf( "\n" ); 
		return 0; 
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] , "ssh" ) ==  0 ) 
	{
	   nsystem( " /etc/init.d/ssh start  " ); 
	   nsystem( " /etc/init.d/sshd start  " ); 
	   nsystem( " systemctl start  sshd.service " ); 
	   return 0; 
        }




	if ( argc == 3)
	if ( ( strcmp( argv[1] ,   "install" ) ==  0 ) || ( strcmp( argv[1] , "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] ,   "remote-ssh" ) ==  0 ) 
	|| ( strcmp( argv[2] ,   "openssh" ) ==  0 ) 
	|| ( strcmp( argv[2] ,   "sshd" ) ==  0 ) 
	|| ( strcmp( argv[2] ,   "ssh" ) ==  0 ) )
	{
		npkg( " ssh  " );
		npkg( " openssh-server  " );
		npkg( " openssh  " ); // manjaro 
		//npkg( " openssh  " );
		///npkg( " sshfs  " );
		///npkg( " fusefs-sshfs  " );
		///npkg( " sshfs  " );
		nsystem( " /etc/init.d/ssh  restart " ); 
		nsystem( " /etc/init.d/ssh  start " ); 
		nsystem( " /etc/init.d/sshd restart " ); 
		nsystem( " /etc/init.d/sshd start " ); 
		nsystem( " systemctl start sshd " );   
		return 0;
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "ubuntu" ) ==  0 ) 
	{
		printf( "\n" );
		printf( "deb http://us.archive.ubuntu.com/ubuntu/ jammy main restricted\n" );
		printf( "\n" );
		printf( "deb http://us.archive.ubuntu.com/ubuntu/ jammy-updates main restricted\n" );
		printf( "\n" );
		printf( "deb http://us.archive.ubuntu.com/ubuntu/ jammy universe\n" );
		printf( "deb http://us.archive.ubuntu.com/ubuntu/ jammy-updates universe\n" );
		printf( "\n" );
		printf( "deb http://us.archive.ubuntu.com/ubuntu/ jammy multiverse\n" );
		printf( "deb http://us.archive.ubuntu.com/ubuntu/ jammy-updates multiverse\n" );
		printf( "\n" );
		printf( "deb http://us.archive.ubuntu.com/ubuntu/ jammy-backports main restricted universe multiverse\n" );
		printf( "\n" );
		printf( "deb http://security.ubuntu.com/ubuntu jammy-security main restricted\n" );
		printf( "deb http://security.ubuntu.com/ubuntu jammy-security universe\n" );
		printf( "deb http://security.ubuntu.com/ubuntu jammy-security multiverse\n" );
		printf( "\n" );
		return 0; 
        }






	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "arch" ) ==  0 ) || ( strcmp( argv[1] , "architecture" ) ==  0 ) )
	{
	        if ( MYOS == 1 ) 
                   system( " dpkg --print-architecture " ); 
		return 0; 
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] , "--debootstrap" ) ==  0 ) 
	if ( strcmp( argv[2] , "ubuntu" ) ==  0 ) 
	{
           nsystem( " debootstrap --no-check-gpg  --include=netbase,debootstrap,gcc,less,vim,nano,make,login,passwd,wpasupplicant  lunar .  http://archive.ubuntu.com/ubuntu   "); 
	   return 0; 
        }




	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "mount" ) ==  0 ) 
	{
		printf( " === ===================================== \n " ); 
		printf( " === ===================================== \n " ); 
		printf( " === NetBSD ===  \n "); 
		printf( "   mount -u -o rw /dev/dk0 /media/dk0      \n " ); 
		printf( "   mount -u -o rw /dev/dk4 /media/dk4  (working)     \n " ); 
		printf( " === ===================================== \n " ); 
		printf( " === ===================================== \n " ); 
		printf( " # mount -t ufs -o ro,ufstype=ufs2 /dev/hda8 /mnt/freebsd/  \n" ); 
		printf( " === ===================================== \n " ); 
		return 0;
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] , "mount" ) ==  0 ) 
	if ( strcmp( argv[2] , "mmc" ) ==  0 ) 
	{
	        nsystem( " mconfig mount /dev/mmcblk* " );   
	        return 0; 
	}




	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "usb" ) ==  0 ) || ( strcmp( argv[1] , "pan" ) ==  0 ) )
	{
		printf(  "=> OpenPandora \n" );
		nsystem( " modprobe  ehci_hcd " );  // usb on 
		nsystem( " modprobe  wl1251      " );    // wifi 
		nsystem( " modprobe  wl1251_sdio " );    // wifi 
		return 0;
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] , "wifi1" ) ==  0 )
	{
             nsystem( "  nconfig bwpa wlan1 ; sleep 1 ; dhcpcd  wlan1 ; dhclient   wlan1  " ); 
             nsystem( "  ip addr " ); 
	     return 0; 
        }



	if ( strcmp( argv[1] ,   "wifi0" ) ==  0 )  
	{
             nsystem( "  nconfig bwpa wlan0 ; sleep 1 ; dhcpcd  wlan0 ; dhclient   wlan0   ; ip addr " ); 
	     return 0; 
        }




	if ( argc == 2 )
	if ( strcmp( argv[1] , "wifi" ) ==  0 ) 
	{
             //nsystem( "  nconfig bwpa wlan0 ; sleep 1 ; dhcpcd  wlan0 ; dhclient   wlan0   ; ip addr " ); 
             nsystem( "  mconfig bwpa wlan0 ; sleep 1 ; dhcpcd  wlan0 ; dhclient   wlan0   ; ip addr " ); 
             nsystem( "  mconfig bwpa wlan1 ; sleep 1 ; dhcpcd  wlan1 ; dhclient   wlan1  " ); 
             nsystem( "  ip addr " ); 
	     return 0; 
        }





	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "scan" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "wifi" ) ==  0 ) 
	{
	        if ( fexist( "/sbin/iwlist" ) == 0 ) 
		{
		    npkg( " wireless-tools ");  
		    npkg( " iwlist  ");  
		}
		nsystem( " iwlist scan " ); 
		nsystem( " iwlist scan | grep ESSID " ); 
		return 0;
	}



	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,    "install" ) ==  0 ) || ( strcmp( argv[1] ,    "ins" ) ==  0 )     )
	if ( ( strcmp( argv[2] ,    "wireless" ) ==  0 )  || ( strcmp( argv[2] ,    "wifi" ) ==  0 )  )
	{
           npkg( "   wireless-tools  " ); 
	   npkg( "   wpasupplicant    " ); 
	   return 0; 
        }





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "wlan" ) ==  0 ) 
	|| ( strcmp( argv[1] , "urtwn0" ) ==  0 ) 
	|| ( strcmp( argv[1] , "wlan0" ) ==  0 ) 
	|| ( strcmp( argv[1] , "wlan1" ) ==  0 ) 
	|| ( strcmp( argv[1] , "wlan2" ) ==  0 ) 
	|| ( strcmp( argv[1] , "wifi" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--wifi" ) ==  0 ) )
	{
	     if ( MYOS == 1 ) 
	     {
		     nsystem( "  /etc/init.d/networking start " ); 
		     nsystem( "  pkg wpa* ; ifdown --force wlan0 ; sleep 2 ; ifup wlan0 ; sleep 2 ; dhclient wlan0 " ); 
		     nsystem( "  pkg wpa* ; ifdown --force wlan1 ; sleep 2 ; ifup wlan1 ; sleep 2 ; dhclient wlan1 " ); 
		     nsystem( "  pkg wpa* ; ifdown --force wlan2 ; sleep 2 ; ifup wlan2 ; sleep 2 ; dhclient wlan2 " ); 
		     nsystem( "  pkg wpa* ; ifdown --force wlan3 ; sleep 2 ; ifup wlan3 ; sleep 2 ; dhclient wlan3 " ); 
		     nsystem( " dhclient wlan0 " ); 
		     nsystem( " dhclient wlan1 " ); 
		     nsystem( " dhcpcd wlan0 " ); 
		     nsystem( " dhcpcd wlan1 " ); 
	     }
	     else
	     {
		     nsystem( " dhcpcd urtwn0 " ); 
		     nsystem( " dhclient urtwn0 " ); 
	     }

	     nsystem( " dhcpcd eth0 " ); 
	     nsystem( " dhclient eth0 " ); 
	     return 0; 
        }





	/// Will fetch on any operating system (opensource)
	/// Quite handy util, right?
	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "--fetch" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--fetch-url" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--wget" ) ==  0 ) )
	{
			printf( "Current path: [%s]\n", getcwd( cmdi , PATH_MAX ) );
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else
					{
						printf( "> Fetch process.\n" );
						printf( "- File process (status: begin).\n" );
						printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
		                                if ( strcmp( argv[1] , "--fetch-sec" ) ==  0 ) 
							fetch_file_ftp(  argv[ i ] ); 
						else
						{
							fetch_file_ftp(     argv[ i ] ); 
						}
						printf( "- File process (status: completed).\n" );
					}
				}
			}
			return 0;
	}












	if ( argc == 2 )
	if ( strcmp( argv[1] , "vim" ) ==  0 ) 
	{

			chdir( getenv( "HOME") );
			chdir( getenv( "HOME") );
		        if ( MYOS == 1 ) 
		   	   npkg( " ncurses-dev" ); 

			nsystem( "   mconfig --fetch 'http://deb.debian.org/debian/pool/main/v/vim/vim_8.0.0197.orig.tar.gz'  " ); 
			nsystem( " tar xzf vim_8.0.0197.orig.tar.gz ; cd vim-8.0.0197 ; ./configure ; make ; make install " );  
			return 0;
	}













	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "compile" ) ==  0 ) || ( strcmp( argv[1] , "--compile" ) ==  0 ) )
	if ( strcmp( argv[2] , "vim" ) ==  0 ) 
	{
			printf( " File:  http://deb.debian.org/debian/pool/main/v/vim/vim_8.0.0197.orig.tar.gz \n" ); 
			printf( " cd ; tar xzf vim_8.0.0197.orig.tar.gz ; cd vim-8.0.0197 ; ./configure ; make " );  
			printf( "\n" ); 
			fetch_file_ftp( "http://deb.debian.org/debian/pool/main/v/vim/vim_8.0.0197.orig.tar.gz" ); 

                        fetch_file_ftp( "https://gitlab.com/openbsd98324/vim/-/raw/master/archive/vim_8.0.0197.orig.tar.gz" );   
			// needs gnu ftp ... 

			nsystem( " cd ; tar xzf vim_8.0.0197.orig.tar.gz ; cd vim-8.0.0197 ; ./configure ; make " );  
			return 0; 
	}

	if ( argc == 3 )
	if ( strcmp( argv[1] , "--install" ) ==  0 ) 
	if ( strcmp( argv[2] , "vim" ) ==  0 ) 
	{
			printf( " File:  http://deb.debian.org/debian/pool/main/v/vim/vim_8.0.0197.orig.tar.gz \n" ); 
			printf( " cd ; tar xzf vim_8.0.0197.orig.tar.gz ; cd vim-8.0.0197 ; ./configure ; make " );  
			printf( "\n" ); 
			fetch_file_ftp( "http://deb.debian.org/debian/pool/main/v/vim/vim_8.0.0197.orig.tar.gz" ); 
                        fetch_file_ftp( "https://gitlab.com/openbsd98324/vim/-/raw/master/archive/vim_8.0.0197.orig.tar.gz" );   
			// needs gnu ftp ... 

			nsystem( " cd ; tar xzf vim_8.0.0197.orig.tar.gz ; cd vim-8.0.0197 ; ./configure ; make ; make install " );  
			return 0; 
	}









	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "wpa" ) ==  0 ) 
	{
		printf( "   rtl_bt / rtl 8821a _ fw.bin "); 
		printf( " wpa_passphrase  \n" ); 
		printf( "wpa_supplicant -B -i urtwn0 -c /etc/wpa_supplicant.conf  " );
		printf( "\n" ); 
		printf( "wpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant.conf  " );
		printf( "\n" ); 
		printf( "wpa_supplicant -B -i urtwn0 -c /etc/wpa_supplicant.conf  " );
		printf( "\n" ); 
		printf( "    wpa_supplicant -B -i wlp1s0 -c /etc/wpa_supplicant.conf  " );
		printf( "\n" ); 
		printf( "  wpa_supplicant  -i wlp1s0 -c /etc/wpa_supplicant.conf  " );
		printf( "\n" ); 
		printf( "  wpa_supplicant  -i wlp1s0 -c /etc/wpa_supplicant.conf  " );
		printf( "\n" ); 
		printf( "\n" ); 
		printf( "-------------------------\n" ); 
		printf( "\n" ); 
		printf( "WPA2\n" ); 
		printf( " network={\n" ); 
		printf( "         ssid=Netzwerkname\n" ); 
		printf( "         scan_ssid=1\n" ); 
		printf( "         proto=RSN\n" ); 
		printf( "         key_mgmt=WPA-PSK\n" ); 
		printf( "         pairwise=CCMP\n" ); 
		printf( "         group=CCMP\n" ); 
		printf( "         psk=meinschluessel\n" ); 
		printf( " }\n" ); 
		printf( "\n" ); 
		printf( "veraltetem WPA1-TKIP verwendet wird\n" ); 
		printf( "\n" ); 
		printf( "         proto=RSN WPA\n" ); 
		printf( " ...\n" ); 
		printf( "         pairwise=CCMP\n" ); 
		printf( "         group=TKIP\n" ); 
		printf( "\n" ); 
		printf( "   oder reine WPA1-security:\n" ); 
		printf( "\n" ); 
		printf( "         proto=WPA\n" ); 
		printf( " ...\n" ); 
		printf( "         pairwise=TKIP\n" ); 
		printf( "         group=TKIP\n" ); 
		printf( "\n" ); 
		printf( "   oder Variationen\n" ); 
		printf( "\n" ); 
		printf( "         proto=RSN WPA\n" ); 
		printf( " ...\n" ); 
		printf( "         pairwise=CCMP TKIP\n" ); 
		printf( "         group=CCMP TKIP\n" ); 
		printf( "\n" ); 
		printf( "   security nach WPA1-TKIP beruht auf der veralteten WEP, ist rel. unsicher und sollte nicht\n" ); 
		printf( "   mehr verwendet werden.\n" ); 
		printf( "\n" ); 
		printf( "-------------------------\n" ); 
		printf( "\n" ); 
		return 0; 
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] , "bwpa"  ) ==  0 ) 
	{
	   // -B background 
	   snprintf( charo , sizeof( charo ), " wpa_supplicant -B -i %s -c /etc/wpa_supplicant.conf  " , argv[ 2 ] ); 
	   nsystem(   charo ); 
	   return 0; 
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] , "wpa"  ) ==  0 ) 
	{
	   snprintf( charo , sizeof( charo ), " wpa_supplicant  -i %s -c /etc/wpa_supplicant.conf  " , argv[ 2 ] ); 
	   nsystem(   charo ); 
	   return 0; 
	}






        // workaround 
	if ( argc == 3 )
	if ( strcmp( argv[1] , "wpa+"  ) ==  0 ) 
	{
  	   printf( "(R8188EU, worked) wpa_supplicant -i wlxd03745ec739b -D nl80211,wext -c /etc/wpa_supplicant.conf\n");
	   snprintf( charo , sizeof( charo ), " wpa_supplicant  -i %s -D nl80211,wext -c /etc/wpa_supplicant.conf " , argv[ 2 ] ); 
	   nsystem(   charo ); 
	   return 0; 
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) ==  0 )
	if ( ( strcmp( argv[2] , "wlan" ) ==  0 ) || ( strcmp( argv[2] , "wifi" ) ==  0 ) )
	{
		// 2: wifi router
		// 3: wifi pass
                printf( "  mconfig --rpi0-wifi wifirouter routerpass \n"); 
		return 0; 
	}








/*
printf( ""); 
printf( "# rpi0 wifi boot dir. "); 
printf( ""); 
printf( "country=us"); 
printf( "update_config=1"); 
printf( "ctrl_interface=/var/run/wpa_supplicant"); 
printf( ""); 
printf( "network={"); 
printf( " scan_ssid=1"); 
printf( " ssid="THEWIFIROUTER""); 
printf( " psk="THESUPERWPAKEY""); 
printf( "}"); 
printf( ""); 
 */







	if ( argc == 2 )
	if ( strcmp( argv[1] , "region" ) ==  0 ) 
	{
		//printf( "# rpi0 wifi boot dir. "); 
		printf( "\n" ); 
		printf( "\n" ); 
		printf( "country=us\n" ); 
		printf( "update_config=1\n"); 
		printf( "ctrl_interface=/var/run/wpa_supplicant\n"); 
		//printf( " ssid="THEWIFIROUTER""); 
		//printf( " psk="THESUPERWPAKEY""); 
		//printf( "}"); 
		//printf( ""); 
		printf( "\n" ); 
		return 0; 
	}











	if ( argc == 4 )
	if ( ( strcmp( argv[1] , "--rpi0-wifi" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--rpi0wifi" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--rpi0-wlan" ) ==  0 ) )
	// 2: wifi router
	// 3: wifi pass
	{
		printf( "\n" ); 
		printf( "# FILE: /boot/wpa_supplicant.conf \n" ); 
		printf( "\n" ); 
		printf( "country=us\n" ); 
		printf( "update_config=1\n" ); 
		printf( "ctrl_interface=/var/run/wpa_supplicant\n" ); 
		printf( "\n" ); 
		printf( "\n" ); 
		/*
		printf( "network={\n" ); 
		printf( " scan_ssid=1\n" ); 
		printf( " ssid=\"THEWIFIROUTER\"\n" ); 
		printf( " psk=\"THESUPERWPAKEY\"\n" ); 
		printf( "}\n" ); 
		*/
		snprintf( charo , sizeof( charo ), "  wpa_passphrase   %s  %s " ,  argv[ 2 ], argv[  3  ]   ); 
		system( charo ); 
		printf( "\n" ); 
		return 0; 
	}











	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "create" ) ==  0 ) 
	{
		printf( " FREEBSD:  mconfig create ada0s2a    (create on main harddisk)\n" ); 
		return 0;
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] , "create" ) ==  0 )
	if ( strcmp( argv[2] , "swapfile" ) ==  0 )
	{
		//nsystem( " dd if=/dev/zero of=/swapfile bs=1M count=4096 status=progress " ); 
		nsystem( " dd if=/dev/zero of=/swapfile bs=1M count=2096 status=progress " ); 
		return 0;
	}



	if ( argc == 5 )
	if ( ( strcmp( argv[1] ,   "set" ) ==  0 ) || ( strcmp( argv[1] ,   "create" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "configure" ) ==  0 ) ) if ( ( strcmp( argv[2] ,   "wpa" ) ==  0 )
	|| ( strcmp( argv[2] ,   "wifi" ) ==  0 ) )
	// 3 
	// 4
	// because... missing > key sometimes 
	{
		snprintf( charo , sizeof( charo ), "  wpa_passphrase   \"%s\"  \"%s\" > /etc/wpa_supplicant.conf " ,  argv[ 3 ], argv[  4  ]   ); 
		printf( "Command: %s\n", charo ); 
		nsystem( charo ); 
		return 0;
	}








	if ( argc == 5 )
	if ( strcmp( argv[1] ,   "set" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "wpa" ) ==  0 ) 
	// 3 
	// 4
	// because... missing > key sometimes 
	{
		snprintf( charo , sizeof( charo ), "  wpa_passphrase   %s  %s > /etc/wpa_supplicant.conf " ,  argv[ 3 ], argv[  4  ]   ); 
		printf( "Command: %s\n", charo ); 
		nsystem( charo ); 
		return 0;
	}






	if ( argc == 5 )
	if ( strcmp( argv[1] ,   "append" ) ==  0 ) 
	if ( ( strcmp( argv[2] ,   "wpa" ) ==  0 ) || ( strcmp( argv[2] ,   "wifi" ) ==  0 ) )
	// 3 
	// 4
	// because... missing > key sometimes 
	{
		snprintf( charo , sizeof( charo ), "  wpa_passphrase   %s  %s  >>  /etc/wpa_supplicant.conf " ,  argv[ 3 ], argv[  4  ]   ); 
		printf( "Command: %s\n", charo ); 
		nsystem( charo ); 
		return 0;
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "rwlan1" ) ==  0 ) || ( strcmp( argv[1] , "wlan1" ) ==  0 ) )
	{
			nsystem( " pkg wpa* ; ifdown --force wlan1 ; sleep 2 ; ifup wlan1 ; sleep 2 ; dhclient wlan1 " ); 
			return 0;
	}

	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "rwlan0" ) ==  0 ) || ( strcmp( argv[1] , "rwlan0" ) ==  0 ) 
	|| ( strcmp( argv[1] , "rwlan" ) ==  0 ) || ( strcmp( argv[1] , "wlan0" ) ==  0 ) )
	{
			nsystem( " pkg wpa* ; ifdown --force wlan0 ; sleep 2 ; ifup wlan0 ; sleep 2 ; dhclient wlan0 " ); 
			nsystem( "  ifconfig -a " ); 
			return 0;
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "rwlan1" ) ==  0 ) || ( strcmp( argv[1] , "rwlan1" ) ==  0 ) 
	  || ( strcmp( argv[1] , "--rwlan1" ) ==  0 )
	  || ( strcmp( argv[1] , "wlan1" ) ==  0 )
	  || ( strcmp( argv[1] , "--wlan1" ) ==  0 ) )
	{
                nsystem( " pkg wpa* ; ifdown --force wlan1 ; sleep 2 ; ifup wlan1 ; sleep 2 ; dhclient wlan1  "); 
		nsystem( "  ifconfig -a " ); 
		return 0;
	}




	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "rwlan0" ) ==  0 ) || ( strcmp( argv[1] , "rwlan0" ) ==  0 ) 
	  || ( strcmp( argv[1] , "--rwlan0" ) ==  0 )
	  || ( strcmp( argv[1] , "wlan0" ) ==  0 )
	  || ( strcmp( argv[1] , "--wlan0" ) ==  0 ) )
	{
		nsystem( "  pkg wpa* ; ifdown --force wlan0 ; sleep 2 ; ifup wlan0 ; sleep 2 ; dhclient wlan0 " ); 
		nsystem( "  ifconfig -a " ); 
		return 0;
	}


	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "rwlan3" ) ==  0 ) || ( strcmp( argv[1] , "rwlan3" ) ==  0 ) )
	{
			nsystem( " pkg wpa* ; ifdown --force wlan3 ; sleep 2 ; ifup wlan3 ; sleep 2 ; dhclient -v wlan3 " ); 
			nsystem( "  ifconfig -a " ); 
			return 0;
	}

	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "rwlan4" ) ==  0 ) || ( strcmp( argv[1] , "rwlan4" ) ==  0 ) )
	{
			nsystem( " pkg wpa* ; ifdown --force wlan4 ; sleep 2 ; ifup wlan4 ; sleep 2 ; dhclient -v wlan4 " ); 
			nsystem( "  ifconfig -a " ); 
			return 0;
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "rwlan1" ) ==  0 ) || ( strcmp( argv[1] , "rwlan1" ) ==  0 ) 
	  || ( strcmp( argv[1] , "--rwlan1" ) ==  0 ) || ( strcmp( argv[1] , "--wlan1" ) ==  0 ) )
	{
		nsystem( "  pkg wpa* ; ifdown --force wlan1 ; sleep 2 ; ifup wlan1 ; sleep 2 ; dhclient wlan1 " ); 
		nsystem( "  ifconfig -a " ); 
		return 0;
	}




        /// pandora 
	if  ( argc == 2 )
	if (  ( strcmp( argv[1] , "loadkeys" ) ==  0 ) 
	||  ( strcmp( argv[1] , "keys" ) ==  0 ) )
	{
		nsystem( "  loadkeys /etc/keymap-extension-2.6.map  " );
		nsystem( "  loadkeys /etc/keymap-extension.txt  " );
		nsystem( "  loadkeys /etc/keymap-extension  " );
		nsystem( "  loadkeys /etc/keymap  " );
		return 0;
	}



	if ( argc == 2 )
        if ( ( strcmp( argv[1] , "keymap-mini" ) ==  0 )
	|| ( strcmp( argv[1] , "keymini" ) ==  0 ) 
	|| ( strcmp( argv[1] , "keymapmini" ) ==  0 ) 
	|| ( strcmp( argv[1] , "keymaps-mini" ) ==  0 ) )
	{
		nsystem( "  loadkeys /etc/keymap-mini  " );
		nsystem( "  loadkeys /etc/keymap-mini.txt  " );
		return 0; 
        }








   if ( argc == 2 )
   if ( strcmp( argv[1] , "keymini" ) ==  0 ) 
   {
	nsystem( "  loadkeys /etc/keymap-mini.txt  " );
	return 0;
   }



	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 )
	if ( strcmp( argv[2] , "backup" ) ==  0 ) 
	{
		printf( " dd if=/dev/mmcblk0 | gzip > image-mmcblk0.gz     \n" ); 
		printf( " gunzip -c image-mmcblk0.gz | dd of=/dev/mmcblk0  \n" );
		return 0; 
	}





	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,   "--backup-dir" ) ==  0 ) 
	|| ( strcmp( argv[1] ,     "--backup-kernel" ) ==  0 ) 
	|| ( strcmp( argv[1] ,     "-bak" ) ==  0 ) 
	|| ( strcmp( argv[1] ,     "--bak"    ) ==  0 ) 
	|| ( strcmp( argv[1] ,     "--backup" ) ==  0 ) )
	{
	        if  ( strcmp( argv[1] ,     "--backup-kernel" ) ==  0 ) 
	          snprintf( charo , sizeof( charo ), " tar cvpfz \"%s\"-%d-$( uname -r )-kernel-bak.tar.gz  \"%s\"   " , argv[ 2 ],  (int)time(NULL), argv[ 2 ] ); 
		else 
	          snprintf( charo , sizeof( charo ), " tar cvpfz \"%s\"-%d.bak.tar.gz  \"%s\"   " , argv[ 2 ],  (int)time(NULL), argv[ 2 ] ); 
		printf( "Command: %s\n", charo ); 
		nsystem( charo ); 
		return 0; 
	}










	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "vdisk" ) ==  0 ) 
	{
		nsystem( " qemu-img create -f qcow2 vdisk.qcow2  25G \n" ); 
	        return 0; 
        }

	if ( argc == 3 )
	if ( strcmp( argv[1] , "new"   ) ==  0 )
	if ( strcmp( argv[2] , "vdisk" ) ==  0 ) 
	{
		nsystem( "  qemu-img create -f qcow2 vdisk.qcow2  25G " ); 
		return 0; 
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] , "new"   ) ==  0 )
	if ( strcmp( argv[2] , "ddisk" ) ==  0 ) 
	{
                nsystem( " qemu-img create -f raw  ddisk.img 8G " ); 
		return 0; 
	}










	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 )
	if ( strcmp( argv[2] , "qemu" ) ==  0 ) 
	{
		printf( " ============= \n" ); 
                printf( "qemu-system-i386   -machine type=pc   -curses  -cdrom debian-live-11.2.0-i386-standard+nonfree.iso  -hda vdisk.img.qcow2     -kernel /media/cdrom/live/vmlinuz-5.10.0-10-686  -initrd /media/cdrom/live/initrd.img-5.10.0-10-686    -append '  boot=live toram=filesystem.squashfs   live-media-path=live   bootfrom=/dev/sr0  console=ttyS0  '   \n" ); 
		printf( " ============= \n" ); 
		printf( " ============= \n" ); 
		printf( "  qemu-system-i386   -machine type=pc  -drive file=vdisk.qcow2,index=0,media=disk,format=qcow2  -drive file=FreeBSD-13.0-RELEASE-i386-memstick.img,index=1,media=disk,format=raw   -boot c  \n" ); 
		printf( " ============= \n" ); 
		printf( " ============= \n" ); 
		return 0; 
	}























	if ( argc == 4 )
	if ( strcmp( argv[1] , "backup" ) ==  0 )
	if ( strcmp( argv[2] , "mbr" ) ==  0 ) 
	/// 3 is location
	{
		snprintf( charo , sizeof( charo ), "    dd if=/dev/%s  of=mbr-backup-image-%s-mbr-5000.img  count=5000  " , fbasename( argv[ 3 ] ) ,  fbasename(  argv[ 3 ] ) ); 
		printf( "Command: %s\n", charo ); 
		nsystem( charo ); 
		return 0; 
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] , "config" ) ==  0 ) 
	if ( strcmp( argv[2] , "wpa" ) ==  0 ) 
	{
		printf( "\n" );
		printf( "\n" );
		printf( "# File for /dev/mmcblk0p1 (fat) rpi0\n" );
		printf( "\n" );
		printf( "\n" );
		printf( "\n" );
		printf( "country=us\n" );
		printf( "update_config=1\n" );
		printf( "ctrl_interface=/var/run/wpa_supplicant\n" );
		printf( "\n" );
		printf( "network={\n" );
		printf( " scan_ssid=1\n" );
		printf( " ssid=\"THEWIFIROUTER\"\n" );
		printf( " psk=\"THESUPERWPAKEY\"\n" );
		printf( "}\n" );
		printf( "\n" );
		printf( "\n" );
		return 0; 
	}

















	if ( argc == 2 )
	if ( strcmp( argv[1] , "adduser" ) ==  0 ) 
	{
		printf(  " >> ON BSD:  useradd -m  -G  wheel  -u 3010 netbsd \n" );

		nsystem( "  adduser --uid 3010 netbsd " ); 
		nsystem( "  passwd  netbsd " ); 

		nsystem( "  adduser netbsd audio " );
		nsystem( "  adduser netbsd video  " );
		nsystem( "  adduser netbsd scanner  " );
		nsystem( "  adduser netbsd printer  " );
		nsystem( "  adduser netbsd lp  " );
		nsystem( "  adduser netbsd lpadmin  " );
		nsystem( "  adduser netbsd scan  " );
		nsystem( "  adduser netbsd lp  " );
		nsystem( "  adduser netbsd lpadmin  " );

                if ( MYOS == 1 ) 
		{
		  // a little fix 
		  nsystem( "  mkdir /home/ " ); 
		  nsystem( "  mkdir /home/netbsd " ); 
		  nsystem( "  chown netbsd:netbsd /home/netbsd " ); 
		}
		return 0; 
	}













	if ( argc == 2 )
	if ( strcmp( argv[1] , "adduser-pi" ) ==  0 ) 
	{

		nsystem( "  adduser --uid 1000 pi " ); 
		//////nsystem( "  passwd  pi " ); 
		nsystem( "  adduser pi audio " );
		nsystem( "  adduser pi video  " );
		nsystem( "  adduser pi scanner  " );
		nsystem( "  adduser pi printer  " );
		nsystem( "  adduser pi lp  " );
		nsystem( "  adduser pi lpadmin  " );
		nsystem( "  adduser pi scan  " );
		nsystem( "  adduser pi lp  " );
		nsystem( "  adduser pi lpadmin  " );
                if ( MYOS == 1 ) 
		{
		  // a little fix 
		  nsystem( "  mkdir /home/ " ); 
		  nsystem( "  mkdir /home/pi " ); 
		  nsystem( "  chown pi:pi /home/pi " ); 
		}
		return 0; 
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "emu" ) ==  0 ) || ( strcmp( argv[1] , "emulationstation" ) ==  0 ) )
	{
		nsystem( " cd ; /opt/retropie/supplementary/emulationstation/emulationstation.sh " );
		return 0; 
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] , "--config" ) ==  0 ) 
	if ( strcmp( argv[2] , "emulationstation" ) ==  0 ) 
	{
		printf( "#!/bin/bash\n" );
		printf( "\n" );
		printf( "if [[ $(id -u) -eq 0 ]]; then\n" );
		printf( "    echo \"emulationstation should not be run as root. If you used 'sudo emulationstation' please run without sudo.\"\n" );
		printf( "    exit 1\n" );
		printf( "fi\n" );
		printf( "\n" );
		printf( "if [[ \"$(uname --machine)\" != *86* ]]; then\n" );
		printf( "    if [[ -n \"$(pidof X)\" ]]; then\n" );
		printf( "        echo \"X is running. Please shut down X in order to mitigate problems with losing keyboard input. For example, logout from LXDE.\"\n" );
		printf( "        exit 1\n" );
		printf( "    fi\n" );
		printf( "fi\n" );
		printf( "\n" );
		printf( "# save current tty/vt number for use with X so it can be launched on the correct tty\n" );
		printf( "TTY=$(tty)\n" );
		printf( "export TTY=\"${TTY:8:1}\"\n" );
		printf( "\n" );
		printf( "clear\n" );
		printf( "tput civis\n" );
		printf( "\n" );
		printf( " cd \n" );
		printf( " fbcp & \n" );
		printf( "\n" );
		printf( "\n" );
		printf( " sudo mconfig legacy  \n" );
		printf( "\n" );
		printf( " sudo -u pi   startx  \n" );
		printf( "\n" );
		printf( " exit\n" );
		printf( "\n" );
		printf( "\n" );
		return 0; 
	}



   


	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "pisetup" ) ==  0 ) 
	{
                nsystem(  "  cd ; sh /home/pi/RetroPie-Setup/retropie_setup.sh  "); 
	        return 0;
	}










        /// little kernel, lk on amd64 
	if ( argc == 2 )
	if ( strcmp( argv[1] , "toolchain" ) ==  0 )
	{
	   // lk 
	   fetch_file_ftp( "https://newos.org/toolchains/x86_64-elf-10.2.0-Linux-x86_64.tar.xz" ); 
	   return 0; 
	}















	if  ( argc == 2)
	if ( strcmp( argv[1] , "dpms" ) ==  0 )
	{
			nsystem( " xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0  " );
			nsystem( " export DISPLAY=:0 ; xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0  " );

			nsystem( " xsetfork s off ;  xsetfork -dpms ;  xsetfork s noblank  ; setterm -blank 0  " );
			return 0;
	}














	if ( argc == 3 )
	if ( strcmp( argv[1] , "install" ) ==  0 ) 
	if ( strcmp( argv[2] , "xclip" ) ==  0 ) 
	{
		npkg( " xclip "); 
		npkg( " xorg-xset  "); 
		npkg( " xorg-xclip "); 
		npkg( " xorg-xinit  " ); // manjaro 
		npkg( " xclip "); 
		return 0; 
	}








	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "xorg" ) ==  0 ) 
	{
	        printf( " --------- \n" ); 
	        printf( " Manjaro  \n" ); 
	        printf( " xorg xorg-xinit xterm screen " ); 
	        printf( "\n" ); 
	        printf( " --------- \n" ); 
		printf( " xfce  => /usr/lib/Xorg :0 -seat seat0 -auth /run/lightdm/root/:0 -nolisten tcp vt7 -novtswitch \n" );  
	        printf( " --------- \n" ); 
		printf( "  xorg: /usr/bin/xorg   (%d)\n", fexist( "/usr/bin/xorg" ) ); 
		printf( "  xorg: /usr/bin/Xorg   (%d)\n", fexist( "/usr/bin/Xorg" ) ); 
		printf( "  xorg: /usr/bin/X      (%d)\n", fexist( "/usr/bin/X" ) ); 
		printf( "  xorg: /usr/bin/startx (%d)\n", fexist( "/usr/bin/startx" ) ); 
	        printf( " --------- \n" ); 
                printf( " nconfig pkg xserver-xorg ctwm m4 libx11-dev  \n" ); 
	        printf( " --------- \n" ); 
		return 0;
	}








	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "install" ) ==  0 ) || ( strcmp( argv[1] , "ins" ) ==  0 ) )
	if ( strcmp( argv[2] ,  "xorg" ) ==  0 ) 
	{
	        npkg_update(); 
	        npkg( " libx11-dev  " ); 
	        npkg( " screen xserver-xorg xterm xinit " ); 
	        npkg( "  ctwm  m4 xclip " ); 
	        npkg( "  ctwm  m4 xclip " ); 
	        npkg( "  xserver-xorg " ); 
	        npkg( "  xterm  " ); 
		return 0; 
	}





	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "install" ) ==  0 ) || ( strcmp( argv[1] , "ins" ) ==  0 ) )
	if ( strcmp( argv[2] ,  "xorg+" ) ==  0 ) 
	{
                 npkg( "  xterm  " ); 
                 npkg( "  xinit   " ); 
                 npkg( "  screen  " ); 

                npkg( " xorgproto libx11 screen " );
                npkg( " libx11 screen " );

		npkg( " xterm  " ); // manjaro 
		npkg( " xinit  " ); 
		npkg( " screen  " ); // manjaro 

		npkg( " xorg-xset  "); 
		npkg( " xorg-xclip "); 


	        if ( os_bsd_type == 1 ) 
		{
		   printf( " FreeBSD \n" ); 
		   npkg_update(); 
		   npkg( " Xorg   " ); 
		   npkg( " xclip  " ); 
		}
	        else if ( os_bsd_type == 3 ) 
		{
		   printf( " NetBSD \n" ); 
		   //nsystem( " dvmixer  " ); 
		   printf( " Already installed normally \n" ); 
		}
		else if ( MYOS == 1 ) 
		{
			npkg( " libx11-dev  "  );     // for evilwm 
			npkg( " xserver-xorg  "  );   // for xterm  
			npkg( " xterm xinit  " );
			npkg( " xterm xinit  " );
			npkg( " screen   " );  // <-- for fltk 
			npkg(   " libfltk1.3-dev " ); 
			npkg(   " fltk  " ); 
			npkg(   " gcc g++ " ); 
			npkg(   " make " ); 
			if ( strcmp( argv[2] ,  "x11+" ) ==  0 ) npkg( " x11-xserver-utils ");
		}

                if ( MYOS == 1 ) 
		if ( fexist( "/usr/bin/zypper" ) == 1 )  // oh, man, opensuse... 
		{
		  npkg( " gcc-g++ " ); 
		  npkg( " gcc-c++ " ); 
		  npkg( " xorg-x11-devel " );   
		}


                 npkg( "  xterm  " ); 
                 npkg( "  xinit   " ); 
                 npkg( "  screen  " ); 


                  /// manjaro pbpro   
		  npkg( " xorg-server      " );   
		  npkg( " xorg-xinit      " );   
		return 0;
	}
	///
	/// x11 xserver utils for usr bin xsetroot !! 




       if ( argc == 2 )
       if ( strcmp( argv[1] , "manjaro-arm-20.12" ) ==  0 ) 
       {
               fetch_file_ftp( "https://ftp.halifax.rwth-aachen.de/osdn/storage/g/m/ma/manjaro-arm/pinebook/xfce/20.12/Manjaro-ARM-xfce-pinebook-20.12.img.xz" ); 
	       fetch_file_ftp( "https://osdn.dl.osdn.net/storage/g/m/ma/manjaro-arm/pinebook/xfce/20.12/Manjaro-ARM-xfce-pinebook-20.12.img.xz" ); 
	       return 0; 
        }












	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) == 0 ) 
	if ( ( strcmp( argv[2] , "fltk" ) == 0 ) || ( strcmp( argv[2] , "libfltk" ) == 0 ) ) 
	{
		printf( "\n" ); 
		printf( " = \n" ); 
		printf( " = FreeBSD:  cc  -lm   -I/usr/local/include/ -I/usr/local/include -I /usr/local/include/    -L/usr/local/lib  -L/usr/local/lib -lX11 -I /usr/local/include/    file.cxx  -o  file  " ); 
		printf( " = \n" ); 
		printf( "\n" ); 
		// on opensuse: fltk fltk-devel 
		return 0;
	}
/*
ii  fltk1.3-doc                           1.3.4-4                              all          Fast Light Toolkit - documentation
ii  libfltk-cairo1.3:armhf                1.3.4-4                              armhf        Fast Light Toolkit - Cairo rendering layer support
ii  libfltk-forms1.3:armhf                1.3.4-4                              armhf        Fast Light Toolkit - Forms compatibility layer support
ii  libfltk-gl1.3:armhf                   1.3.4-4                              armhf        Fast Light Toolkit - OpenGL rendering support
ii  libfltk-images1.3:armhf               1.3.4-4                              armhf        Fast Light Toolkit - image loading support
ii  libfltk1.3:armhf                      1.3.4-4                              armhf        Fast Light Toolkit - main shared library
ii  libfltk1.3-dev                        1.3.4-4                              armhf        Fast Light Toolkit - development files
*/










	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "install" ) ==  0 ) || ( strcmp( argv[1] , "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] , "fltk-devel" ) ==  0 ) 
	|| ( strcmp( argv[2] , "fltk-devel" ) ==  0 ) 
	|| ( strcmp( argv[2] , "fltk-dev" ) ==  0 ) 
	|| ( strcmp( argv[2] , "libfltk-dev" ) ==  0 ) )
	{
		printf( "   fltk ...   .. \n" ); 
		npkg( " libfltk1.3-dev " ); 
		npkg( " libfltk1.3 " ); 
		npkg( " fltk-devel " ); 
		npkg( " fltk " ); 
		npkg( " fluid  " );  // new 
		npkg( " make " ); 
		npkg( " g++ " ); 
		npkg( " screen " ); 
		return 0; 
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] , "--build-fltk" ) == 0 ) 
	{
		snprintf( charo , sizeof( charo ), "   cc  -lm   -I/usr/local/include/ -I/usr/local/include -I /usr/local/include/    -L/usr/local/lib  -L/usr/local/lib -lX11 -I /usr/local/include/   \"%s\"  -o  a.out  ",  argv[ 2 ] ); 
		nsystem(  charo );
		return 0; 
	}


        // manjaro, install update, through ... 
	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "install" ) ==  0 ) || ( strcmp( argv[1] , "ins" ) ==  0 ) )
	if ( strcmp( argv[2] , "xfltk" ) ==  0 ) 
	{
	        nsystem( " mconfig update " ); 
	        nsystem( " mconfig pkg fltk xterm screen " ); 
		printf( "   fltk ...   .. \n" ); 
		return 0; 
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "mixer" ) == 0 ) || ( strcmp( argv[1] ,   "sndstat" ) == 0 ) )
	{
		//printf( " == To change main device: sysctl hw.snd.default_unit=1 \n" );
		//printf( " == /etc/sysctl.conf : hw.snd.default_unit=1\n" ); 

	        if ( os_bsd_type == 1 ) 
		{
		   printf( " FreeBSD \n" ); 
		   nsystem( " mixer  " ); 
		}

	        else if ( os_bsd_type == 3 ) 
		{
		   printf( " NetBSD \n" ); 
		   nsystem( " dvmixer  " ); 
		}

		else  if (  MYOS == 1 ) 
		{
		   if ( fexist( "/usr/bin/pavucontrol" ) == 1 ) 
		     nsystem( " /usr/bin/pavucontrol " );
		   else
		     nsystem( " alsamixer " );
		}

		else 
		  nsystem(  " cat /dev/sndstat " );

		return 0;
	}












        // especially for pinebook 
	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "sound" ) ==  0 ) 
	|| ( strcmp( argv[1] , "unmute" ) ==  0 ) ) 
	{
              nsystem( "  amixer -c 0 sset DAC 75 ; amixer set Speaker,0 unmute ; amixer -c 0 sset DAC 75 ; amixer set Speaker,0 unmute " ); 
              nsystem( " amixer sset DAC 110 ; amixer sset Left Headphone Mixer Left DAC,0 unmute " ); 
	      return 0; 

	}







	if ( argc == 3 )
	if ( strcmp( argv[1] , "install" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "sound" ) ==  0 )
	  || ( strcmp( argv[2] , "alsa"  ) ==  0 ) 
	  || ( strcmp( argv[2] ,   "audio" ) ==  0 ) 
	  || ( strcmp( argv[2] ,   "mixer" )     ==  0 ) )
	{
		npkg(  " alsa-utils " );
		npkg(  " alsa-base " );
		npkg(  " alsa " );
		return 0;
	}


	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "ins" ) ==  0 ) || ( strcmp( argv[1] , "install" ) ==  0 ) )
	if ( ( strcmp( argv[2] , "alsa" ) ==  0 ) || ( strcmp( argv[2] , "alsamixer" ) ==  0 ) )
	{
		npkg( " alsa-base  " );
		npkg( " alsa-utils  " );
		return 0;
        }

	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "alsa" ) ==  0 ) || ( strcmp( argv[1] , "mixer" ) ==  0 ) )
	{
		if ( MYOS == 1 )   nsystem( " alsamixer " ); 
		return 0; 
	}









	if ( argc >= 2 )
	if ( ( strcmp( argv[1] , "--web-url" ) ==  0 ) ||  ( strcmp( argv[1] , "-web-url" ) ==  0 ) )
	{
                procedure_webbrowser( argv[ 2 ] ); 
		return 0;
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "xfirefox" ) ==  0 ) ||  ( strcmp( argv[1] , "xfire" ) ==  0 ) )
	{
                //procedure_webbrowser( "www.duckduckgo.com" ); 
		nsystem( " export DISPLAY=:0 ; firefox " ); 
		return 0;
	}





	if ( argc == 3)
	if ( ( strcmp( argv[1] ,  "install" ) ==  0 ) || ( strcmp( argv[1] ,  "ins" ) ==  0 ) )
	if (  strcmp( argv[2] , "dolphin" ) ==  0 ) 
	{
		nsystem( " apt-get update  " ); 
		npkg( " dolphin konsole " ); 
                npkg( " dbus-x11  "); // for dolphin on pi  
		printf( " =================== \n" ); 
		return 0; 
	}



	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "dolphin" ) ==  0 ) 
	|| ( strcmp( argv[1] , "xexplorer" ) ==  0 ) 
	|| ( strcmp( argv[1] , "explorer" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--explorer" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--nemo" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--dolphin" ) ==  0 ) 
	|| ( strcmp( argv[1] , "thunar" ) ==  0 ) 
	|| ( strcmp( argv[1] , "nemo" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--nemo" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-nemo" ) ==  0 ) 
	|| ( strcmp( argv[1] , "nautilus" ) ==  0 ) )
	{
	        if ( fexist( "/usr/bin/dolphin" ) == 1 ) 
		  snprintf( charo , sizeof( charo ),  " dolphin   --new-window --select \"%s\" ",   getcwd( foocwd, PATH_MAX ) ); 

		else if ( fexist( "/usr/bin/nautilus" ) == 1 ) 
		  snprintf( charo , sizeof( charo ),  "  nautilus --new-window \"%s\" ",   getcwd( foocwd, PATH_MAX ) ); 


		else if ( fexist( "/usr/bin/nemo" ) == 1 ) 
		  snprintf( charo , sizeof( charo ),  "  echo \"%s\" ; nemo . ",   getcwd( foocwd, PATH_MAX ) ); 


		else if ( fexist( "/usr/bin/thunar" ) == 1 ) 
		  snprintf( charo , sizeof( charo ),  "  thunar \"%s\" ",   getcwd( foocwd, PATH_MAX ) ); 

		else
		  snprintf( charo , sizeof( charo ),  " flm \"%s\" ",   getcwd( foocwd, PATH_MAX ) ); 

		nsystem( charo ); 
		return 0; 
	}




	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "cr" ) ==  0 ) ||  ( strcmp( argv[1] , "chromium" ) ==  0 ) )
	{
	        if ( fexist( "/usr/bin/chromium-browser" ) == 1 ) 
                  nsystem( " /usr/bin/chromium-browser --new-window  www.duckduckgo.com  " ); 
		else  
                  procedure_webbrowser( "www.duckduckgo.com" ); 
		return 0;
	}







	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,    "install" ) ==  0 ) || ( strcmp( argv[1] ,    "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] ,    "cr" ) ==  0 ) 
	|| ( strcmp( argv[2] ,      "chromium-browser" ) ==  0 )
	|| ( strcmp( argv[2] ,      "chromium" ) ==  0 ))
	{
		npkg_update();
		//if ( fexist( "/usr/bin/chromium-browser" ) == 1 ) 
		//	nsystem( " cp /usr/bin/chromium-browser      /usr/bin/chromium " );
		///nsystem( " ln -s /usr/bin/chromium-browser   /usr/bin/chromium " );
		////npkg( " links ");   // 2nd end links backup

		if ( fexist( "/etc/arch-release" ) == 1 )  // retroarch 
		{
			npkg( " chromium " );
		}
		else if ( fexist( "/usr/bin/apt-get" ) == 1 ) 
		{
			npkgmin( " chromium-browser " );
			npkgmin( " chromium " );
		}
		else 
		{
			npkg( " chromium " );
			npkg( " links " );
		}
		if ( fexist( "/usr/bin/emulationstation" ) == 1 ) 
		{
			nsystem( "  ln -s /usr/bin/chromium-browser  /usr/bin/chromium " ); 
		}

		if ( MYOS == 1 ) 
			npkg( " x11-xserver-utils ");

		//new 
		if ( MYOS == 1 )  
		{
			npkg(  " alsa-utils " );
			npkg(  " alsa-base " );
			npkg(  " alsa " );
		}

		if ( MYOS == 1 )  
			if ( fexist( "/usr/bin/apt-get" ) == 1 ) 
			{
				/// xsetroot for cursros curs 
				npkg( " x11-xserver-utils ");
				/// xsetroot for cursros curs 
				npkg(  " alsa-utils " );
				npkg(  " alsa-base " );
				npkg(  " alsa " );
			}
		return 0;
	}





	/////////////
	// old 
	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "xcr" ) ==  0 ) ||  ( strcmp( argv[1] , "xchromium" ) ==  0 ) )
	{
                nsystem( "  export DISPLAY=:0 ; mconfig cr " ); 
		return 0;
	}













	if ( argc == 2 )
	if ( strcmp( argv[1] , "cln" ) ==  0 ) 
	{
			nsystem( "  cd ; pkill chromium " );
			nsystem( "  cd ; pkill chromium-browser " );
			nsystem( "  cd ; pkill chromium " );
			nsystem( "  cd ; pkill firefox " );
			nsystem( "  cd ; pkill mozilla " );
			nsystem( "  cd ; pkill iceweasel " );
			nsystem( "  cd ; pkill qupzilla " );
			nsystem( "  cd ; pkill iceweasel  " );

			nsystem( "  sleep 1 " );
			nsystem( "  cd ; rm -rf .mozilla " ); 
			nsystem( "  cd ; rm -rf .cache ; rm -rf .config/chromium " ); 
			nsystem( "  cd ; rm -rf .mozilla " ); 
			nsystem( "  cd ; rm -rf .Mozilla " ); 
			nsystem( "  cd ; pkill -9 chromium " );
			nsystem( "  cd ; pkill -9 chromium-browser " );
			nsystem( "  cd ; rm -rf .cache ; rm -rf .config/chromium " ); 
			nsystem( "  cd ; rm -rf .mozilla " ); 
			nsystem( "  cd ; rm -rf .Mozilla " ); 

			nsystem( "  cd ; pkill -9 qupzilla " );
			nsystem( "  cd ; pkill -9 iceweasel  " );
			nsystem( "  cd ; pkill -9 qupzilla " );
			nsystem( "  cd ; pkill -9 iceweasel  " );
			nsystem( "  cd ; rm -rf  .config/qupzilla/  "); 
			nsystem( "  cd ; rm -rf  .config/qupzilla/  "); 
			nsystem( "  cd ; rm -rf  .config/iceweasel/ "); 
			return 0;   
	}


















       if ( argc == 2 )
       if ( strcmp( argv[1] ,    "upd" ) ==  0 ) 
       {
	       fetch_file_ftp( "https://gitlab.com/openbsd98324/mconfig/-/raw/master/src/mconfig.c" );   // working  from source, and compiled static 
	       printf( "Username: %s\n", getenv( "USER" ) ); 
	       nsystem( " rm mconfig.c " ); 

	       if ( strcmp( getenv( "USER" ), "root" ) == 0 ) 
	       {
		       nsystem( " mkdir /usr "); 
		       nsystem( " mkdir /usr/local/    "); 
		       nsystem( " mkdir /usr/local/bin "); 

		       printf( "==> Username is root.\n" ); 
		       nsystem( " cc -static mconfig.c -o /usr/local/bin/mconfig " ); 
	       }
	       else
	       {
		      printf( "==> Username is NOT root.\n" ); 
	              nsystem( "  mkdir ~/bin ;  mconfig --fetch 'https://gitlab.com/openbsd98324/mconfig/-/raw/master/src/mconfig.c'  ;   cc -static  mconfig.c -o ~/bin/mconfig " ); 
	       }

	       printf( " It is compiled with static to have no lib needed. \n" ); 
	       return 0; 
       }





















	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "-checkdisk" ) ==  0 ) || ( strcmp( argv[1] , "chkdsk" ) ==  0 ) || ( strcmp( argv[1] , "--checkdisk" ) ==  0 ) )
	{
		printf( "Automatic:\n" );
		if ( argc >= 2)
		{
			for( i = 1 ; i < argc ; i++) 
			{
				if ( i == 1 )
				{
					printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
				}
				else if ( i >= 2 )
				{
					printf( "> File process.\n" );
					printf( "- File process (status: begin).\n" );
					printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
					printf( "> Arg2 : %s\n",   argv[ 2 ]   );
					printf( "> Item : %s\n",   argv[ i ]   );
					printf( "> Item file type: %s (%d)\n",   argv[ i ]   ,  fexist(   argv[ i ] )   );

					strncpy( cmdi,  " ", PATH_MAX );
					strncpy( cmdi,  " ", PATH_MAX );
					strncat( cmdi , " dd if=/dev/" , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi ,  fbasename( argv[ i ] )  , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , "  of=/dev/zero  " , PATH_MAX - strlen( cmdi ) -1 );
					nsystem( cmdi );
				}
			}
		}
		return 0;
	}























   /// fsck on netbsd : fsck_ext2fs -y -f /dev/sd1e 
   /// this fsck will fix and clean up a way more than fsck.ext2fs on Linux! nota... that might help and good to know. 
	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "--force-fsck" ) ==  0 ) || ( strcmp( argv[1] , "ffsck" ) ==  0 ) ) 
	{
			printf( "Automatic processing ...:\n" );
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else if ( i >= 2 )
					{
						printf( "> File process.\n" );
						printf( "- File process (status: begin).\n" );
						printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
						printf( "> Arg2 : %s\n",   argv[ 2 ]   );
						printf( "> Item : %s\n",   argv[ i ]   );
						printf( "> Item file type: %s (%d)\n",   argv[ i ]   ,  fexist(   argv[ i ] )   );

						strncpy( cmdi,  " ", PATH_MAX );
						strncpy( cmdi,  " ", PATH_MAX );
	 				        strncat( cmdi , "  umount /dev/" , PATH_MAX - strlen( cmdi ) -1 );
					        strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination, new 
						strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
						nsystem( cmdi );

						strncpy( cmdi,  " ", PATH_MAX );
						strncpy( cmdi,  " ", PATH_MAX );
	 				        strncat( cmdi , "  fsck -f -y /dev/" , PATH_MAX - strlen( cmdi ) -1 );
					        strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination, new 
						strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
						nsystem( cmdi );


						printf( "- List process (status: completed).\n" );
					}
				}
			}
			return 0;
	}









	// c2 => 3   arg
	// c3 => 4   arg1 arg2
	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "umount" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "unmount" ) ==  0 )
	|| ( strcmp( argv[1] ,   "umount" ) ==  0 ))
	{
		printf( "=== LIST MEDIA DIRECTORY === \n" ); 
		system( "  mount | grep media "); 
		printf( "============================ \n" ); 
		printf( "Automatic:\n" );
		if ( argc >= 2)
		{
			for( i = 1 ; i < argc ; i++) 
			{
				if ( i == 1 )
				{
					printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
				}
				else if ( i >= 2 )
				{
					printf( "> File process.\n" );
					printf( "- File process (status: begin).\n" );
					printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
					printf( "> Arg2 : %s\n",   argv[ 2 ]   );
					printf( "> Item : %s\n",   argv[ i ]   );
					printf( "> Item file type: %s (%d)\n",   argv[ i ]   ,  fexist(   argv[ i ] )   );


					strncpy( cmdi,  " ", PATH_MAX );
					strncpy( cmdi,  " ", PATH_MAX );
					strncat( cmdi , " umount  /dev/" , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi ,  fbasename(  argv[ i ] )  , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
					strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
					nsystem( cmdi );

					printf( "- List process (status: completed).\n" );
				}
			}
		}
		printf( "=== LIST MEDIA DIRECTORY === \n" ); 
		system( "  mount | grep media "); 
		printf( "============================ \n" ); 
		return 0;
	}










	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "mount" ) ==  0 ) 
	{
			nsystem( " mount  "); 
			printf( "=== LIST MEDIA DIRECTORY === \n" ); 
			system( "  mount | grep media "); 
			printf( "============================ \n" ); 
			return 0;
	}





	/// needded  ntfsfix  /dev/sda2 !!!   <-- ok   fsck.ntfs  not there !, check su - if needed!
	//nsystem( " mount -t ntfs -o rw /dev/sda2 /mnt/  "); 
	if ( argc >= 3 )
	if ( strcmp( argv[1] , "mountntfs" ) ==  0 ) 
	{
		printf( " ============== \n "); 
		for( i = 2 ; i < argc ; i++) 
		{
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );

	                strncpy( cmdi , " ntfsfix /dev/" , PATH_MAX );
			strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );
			strncat( cmdi , "   " , PATH_MAX - strlen( cmdi ) -1 );
			nsystem( cmdi ); 

	                strncpy( cmdi , " mkdir " , PATH_MAX );
			strncat( cmdi , "  /media/" , PATH_MAX - strlen( cmdi ) -1 );
			strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );
			strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
			nsystem( cmdi ); 

	                strncpy( cmdi , " mount -t ntfs -o rw  /dev/" , PATH_MAX );
			strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );
			strncat( cmdi , "  /media/" , PATH_MAX - strlen( cmdi ) -1 );
			strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );
			strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
			nsystem( cmdi ); 

		        printf( " ============== \n "); 
		}
		return 0; 
        }








	if ( argc >= 3)
	if ( ( strcmp( argv[1] , "mountext2ro" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--mountext2fsro" ) ==  0 ) 
	|| ( strcmp( argv[1] , "mountext2fsro" ) ==  0 ) )
	{
		//printf(   "== NETBSD ==\n" ); 
		printf( " ============== \n "); 
		strncpy( cmdi , "  " , PATH_MAX );
		for( i = 2 ; i < argc ; i++) 
		{
			snprintf( charo , sizeof( charo ), " mkdir /media/%s  ", fbasename(  argv[ i ]  ) ); 
			nsystem(  charo );

			if ( MYOS == 1 ) 
			   snprintf( charo , sizeof( charo ), " mount  -o ro /dev/%s  /media/%s  ", fbasename(  argv[ i ]  )  , fbasename(  argv[ i ]  )  );
			else
			   snprintf( charo , sizeof( charo ), " mount  -t ext2fs -o ro /dev/%s  /media/%s  ", fbasename(  argv[ i ]  )  , fbasename(  argv[ i ]  )  );
			nsystem(  charo );
		}
		printf( " ============== \n "); 
		return 0; 
	}





	// c1 => 2   
	// c2 => 3   arg
	// c3 => 4   arg1 arg2
	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "-mount" ) ==  0 ) 
	|| ( strcmp( argv[1] , "mount" ) ==  0 ) 
	|| ( strcmp( argv[1] , "mountro" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--mount" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--mount" ) ==  0 ) )
	{
			printf( "Automatic Mount:\n" );
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else if ( i >= 2 )
					{
						printf( "> File process.\n" );
						printf( "- File process (status: begin).\n" );
						printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
						printf( "> Arg2 : %s\n",   argv[ 2 ]   );
						printf( "> Item : %s\n",   argv[ i ]   );
						printf( "> Item file type: %s (%d)\n",   argv[ i ]   ,  fexist(   argv[ i ] )   );

						//if ( ( fexist(  argv[ i ]      ) == 1 )  || ( fexist(  argv[ i ]     ) == 2 ) ) 
						{
							strncpy( cmdi,  " ", PATH_MAX );
							strncpy( cmdi,  " ", PATH_MAX );
							strncat( cmdi , " mkdir   /media ; mkdir /media/" , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi ,  fbasename( argv[ i ] )  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , "   " , PATH_MAX - strlen( cmdi ) -1 );
							nsystem( cmdi );


							if ( os_bsd_type == 1 ) 
							{
							        printf( "FreeBSD\n" ); 
							}

							strncpy( cmdi,  " ", PATH_MAX );
							strncpy( cmdi,  " ", PATH_MAX );
		                                        if ( strcmp( argv[1] , "mountro" ) ==  0 ) 
							{
							   if ( MYOS == 1 ) 
							     strncat( cmdi , "  mount -o ro  /dev/" , PATH_MAX - strlen( cmdi ) -1 );
							   else
							     // mount freebsd  running on netbsd 
							     // mount -o ro -t ffs /dev/dk0 /media/dk0 
							     strncat( cmdi , "  mount -o ro -t ffs  /dev/" , PATH_MAX - strlen( cmdi ) -1 );
							}
							else
							{
							     strncat( cmdi , "  mount /dev/" , PATH_MAX - strlen( cmdi ) -1 );
							}
							strncat( cmdi ,  fbasename(  argv[ i ] )  , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
							strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , "  /media/"  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi ,  fbasename(   argv[ i ] )  , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
							strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
							nsystem( cmdi );

							printf( "- List mount process (status: completed).\n" );
						}
					}
				}
			}
			printf( "=== LIST MEDIA DIRECTORY === \n" ); 
			system( "  mount | grep media "); 
			printf( "============================ \n" ); 
			return 0;
	}









        /// old type working for netbsd, i.e. ufstype=44bsd 
	if ( argc >= 2 )
	if ( strcmp( argv[1] , "mountbsd" ) ==  0 ) 
	{
			printf( "Automatic Mount:\n" );
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else if ( i >= 2 )
					{
						printf( "> File process.\n" );
						printf( "- File process (status: begin).\n" );
						printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
						printf( "> Arg2 : %s\n",   argv[ 2 ]   );
						printf( "> Item : %s\n",   argv[ i ]   );
						printf( "> Item file type: %s (%d)\n",   argv[ i ]   ,  fexist(   argv[ i ] )   );

						
							strncpy( cmdi,  " ", PATH_MAX );
							strncpy( cmdi,  " ", PATH_MAX );
							strncat( cmdi , " mkdir   /media ; mkdir /media/" , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , "   " , PATH_MAX - strlen( cmdi ) -1 );
							nsystem( cmdi );


							/// 1: 
							strncpy( cmdi,  " ", PATH_MAX );
							strncpy( cmdi,  " ", PATH_MAX );
							// lsmod gives here ufs 
							strncat( cmdi , "  mount -o ro  /dev/" , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
							strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , "  /media/"  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
							strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
							nsystem( cmdi );
							// 1.end 





							/// 2: 
							strncpy( cmdi,  " ", PATH_MAX );
							strncpy( cmdi,  " ", PATH_MAX );
							// lsmod gives here ufs 
							strncat( cmdi , "  mount -o ro  -t ext2fs  /dev/" , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
							strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , "  /media/"  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
							strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
							nsystem( cmdi );
							// 1.end 



							/// 2: bsd netbsd  
							if ( MYOS ==  1 )   // 1: linux
							{
								strncpy( cmdi,  " ", PATH_MAX );
								strncpy( cmdi,  " ", PATH_MAX );
								// lsmod gives here ufs 
								strncat( cmdi , "  mount    -t ufs -o ro,ufstype=44bsd  /dev/" , PATH_MAX - strlen( cmdi ) -1 );
								strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
								strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
								strncat( cmdi , "  /media/"  , PATH_MAX - strlen( cmdi ) -1 );
								strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
								strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
								nsystem( cmdi );
								// 1.end 
							}

							/// 2: ufs2 freebsd  
							if ( MYOS ==  1 )   // 1: linux
							{
								strncpy( cmdi,  " ", PATH_MAX );
								strncpy( cmdi,  " ", PATH_MAX );
								// lsmod gives here ufs 
								strncat( cmdi , "  mount    -t ufs -o ro,ufstype=ufs2 /dev/" , PATH_MAX - strlen( cmdi ) -1 );
								strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
								strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
								strncat( cmdi , "  /media/"  , PATH_MAX - strlen( cmdi ) -1 );
								strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
								strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
								nsystem( cmdi );
								// 1.end 
							}



							printf( "- List mount process (status: completed).\n" );
						
					}
				}
			}
			printf( "=== LIST MEDIA DIRECTORY === \n" ); 
			system( "  mount | grep media "); 
			printf( "============================ \n" ); 
			return 0;
	}











	if ( argc >= 3 )
	if ( strcmp( argv[1] , "newfs" ) ==  0 )
	{
			printf( "Automatic:\n" );
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else if ( i >= 2 )
					{
						printf( "> File process.\n" );
						printf( "- File process (status: begin).\n" );
						printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
						printf( "> Arg2 : %s\n",   argv[ 2 ]   );
						printf( "> Item : %s\n",   argv[ i ]   );
						printf( "> Item file type: %s (%d)\n",   argv[ i ]   ,  fexist(   argv[ i ] )   );

						if ( strcmp( argv[1] , "fnewfs" ) ==  0 )
						{
							snprintf( charo , sizeof( charo ), " umount /dev/%s  " , fbasename( argv[ i ] ) ); 
							printf( "Command to force umount: %s\n", charo ); 
							nsystem( charo ); 
						}

						strncpy( cmdi,  " ", PATH_MAX );
						strncpy( cmdi,  " ", PATH_MAX );
						if ( MYOS == 1 ) 
							strncat( cmdi , " mkfs.ext3            -F   /dev/" , PATH_MAX - strlen( cmdi ) -1 );
						else
							strncat( cmdi , "   newfs   /dev/" , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi ,  fbasename(  argv[ i ] )  , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
						strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
						nsystem( cmdi );

						printf( "- List process (status: completed).\n" );
					}
				}
			}
			//printf( "=== LIST MEDIA DIRECTORY === \n" ); 
			//system( "  mount | grep media "); 
			printf( "============================ \n" ); 
			return 0;
	}













	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "x" ) ==  0 ) 
	|| ( strcmp( argv[1] , "startx" ) ==  0 )
	|| ( strcmp( argv[1] , "X" ) ==  0 ) )
	{
		nsystem( " cd ; startx  ");
		return 0;
	}







	if ( argc == 2)
	if ( strcmp( argv[1] , "twm" ) ==  0 ) 
	{
			nsystem( " cd ; echo setxkbmap de >  .xinitrc  " );
			nsystem( " cd ; echo        >>       .xinitrc  " );
			nsystem( " cd ; echo twm  >>       .xinitrc  " );
			return 0;
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] , "lxde" ) ==  0 ) 
	{
			printf( "========================\n" );
			printf( "Classic version (clean) \n" );
			printf( "========================\n" );
			nsystem( " cd ; echo                >    .xinitrc  " );
			nsystem( " cd ; echo  cd            >>   .xinitrc  " );
			nsystem( " cd ; echo setxkbmap de   >>   .xinitrc  " );
			nsystem( " cd ; echo  ' xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    kicker &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo   startlxde                  >>       .xinitrc  " );
			printf( "============\n" );
			nsystem( " cd ; echo setxkbmap de          >        .xsession " );
			nsystem( " cd ; echo  'xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '    env TZ=Europe/Paris      kicker &  '        >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  startlxde                 >>       .xsession  " );
			printf( "============\n" );
			return 0;
	}



	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "install" ) ==  0 ) || ( strcmp( argv[1] , "ins" ) ==  0 ) )
	if ( strcmp( argv[2] , "lxde" ) ==  0 ) 
	{
		npkg( " lxde " ); 
		npkg( " gnome-screenshot " ); 
		npkg( " nautilus  " ); 

		npkg( " lxappearance " ); 
		npkg( " lxsession    " ); 
		npkg( " lxterminal    " ); 
		npkg( " pcmanfm      " ); 
		npkg( " mupdf       " ); 
		npkg( " lxpanel     " ); 
		npkg( " netcat      " ); 
		npkg( " mupdf    " ); 
		npkg( " xterm    " ); 
		npkg( " evince   " ); 
		npkg( " xpaint   " ); 
		return 0; 
	}









	if ( argc == 2 )
	if ( strcmp( argv[1] , "ctwm-us" ) ==  0 ) 
	{
			printf( "========================\n" );
			printf( "Classic version (clean) \n" );
			printf( "========================\n" );
			nsystem( " cd ; echo                >    .xinitrc  " );
			nsystem( " cd ; echo  cd              >>   .xinitrc  " );
			nsystem( " cd ; echo   setxkbmap us   >>   .xinitrc  " );
			nsystem( " cd ; echo  ' xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    kicker &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo ctwm                  >>       .xinitrc  " );
			printf( "============\n" );
			nsystem( " cd ; echo                       >    .xsession  " );
			nsystem( " cd ; echo  cd                   >>   .xsession  " );
			nsystem( " cd ; echo setxkbmap us          >>   .xsession " );
			nsystem( " cd ; echo  'xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '    env TZ=Europe/Paris      kicker &  '        >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  ctwm                 >>       .xsession  " );
			printf( "============\n" );
			/// nsystem( "    cd ; wget https://termbin.com/bp8h -O  .ctwmrc " );
			return 0;
	}









	if ( argc == 2)
	if ( ( strcmp( argv[1] , "Xterm" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "xterm-wm" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "wm-xterm" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "xterm-xinit" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "xinit-xterm" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "xterm-window-manager" ) ==  0 ) )
	{

			printf( "============\n" );
			nsystem( " cd ; echo                >    .xinitrc  " );
			nsystem( " cd ; echo  cd            >>   .xinitrc  " );
			nsystem( " cd ; echo setxkbmap de   >>   .xinitrc  " );
			nsystem( " cd ; echo               >> .xinitrc  " );
			nsystem( " cd ; echo  '     xsetroot -solid #3A6EA5 '        >>       .xinitrc  " );
			nsystem( " cd ; echo  '  xsetroot -cursor_name left_ptr '        >>       .xinitrc  " );
			nsystem( " cd ; echo  '    xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 '        >>       .xinitrc  " );
			nsystem( " cd ; echo  ' xsetfork s off ;  xsetfork -dpms ;  xsetfork s noblank  ; setterm -blank 0 ' >> .xinitrc  " );
			nsystem( " cd ; echo  '    '        >>       .xinitrc  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    kicker    &  '        >>       .xinitrc  " );
			///nsystem( " cd ; echo  '  env TZ=Europe/Paris    thunar    &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo xterm  >> .xinitrc  " );
			printf( "============\n" );

			nsystem( " cd ; echo                >    .xsession  " );
			nsystem( " cd ; echo  cd            >>   .xsession  " );
			nsystem( " cd ; echo setxkbmap de   >>   .xsession  " );
			nsystem( " cd ; echo  'xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    kicker    &  '        >>       .xsession  " );
			//nsystem( " cd ; echo  '  env TZ=Europe/Paris    thunar    &  '        >>       .xsession  " );
			nsystem( " cd ; echo             >> .xsession  " );
			nsystem( " cd ; echo xterm    >> .xsession  " );
			printf( "============\n" );
			printf( "============\n" );
			printf( "============\n" );
			return 0;
		}




	// for pibox 
	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "evilwm" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "wm" ) ==  0 ) 
	|| ( strcmp( argv[1] , "config-evilwm" ) ==  0 ) )
	{
			printf( "========================\n" );
			printf( "Classic version (clean) \n" );
			printf( "========================\n" );
			nsystem( " cd ; echo                >    .xinitrc  " );
			nsystem( " cd ; echo  cd            >>   .xinitrc  " );
			nsystem( " cd ; echo   setxkbmap de   >>   .xinitrc  " );
			nsystem( " cd ; echo  ' xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xinitrc  " );
			nsystem( " cd ; echo  ' xsetfork s off ;  xsetfork -dpms ;  xsetfork s noblank  ; setterm -blank 0 ' >> .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    kicker &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo   xrdb  .Xresources               >> .xinitrc  " );
			nsystem( " cd ; echo   xsetroot -cursor_name left_ptr  >> .xinitrc " ); 
			nsystem( " cd ; echo                        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo     mixerctl -w outputs.speaker=116      >> .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    evilwm  &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  '     xsetroot -solid #3A6EA5 '        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo    nloop              >>       .xinitrc  " );

			printf( "============\n" );

			nsystem( " cd ; echo setxkbmap de          >        .xsession " );
			nsystem( " cd ; echo  'xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xsession  " );
			nsystem( " cd ; echo   xrdb  .Xresources    >>      .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '    env TZ=Europe/Paris      kicker &  '        >>       .xsession  " );
			nsystem( " cd ; echo   xrdb  .Xresources               >> .xsession   " );
			nsystem( " cd ; echo   xsetroot -cursor_name left_ptr  >> .xsession   " ); 
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo     mixerctl -w outputs.speaker=116      >> .xsession   " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '    env TZ=Europe/Paris      evilwm &  '        >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '     xsetroot -solid #3A6EA5 '        >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo    nloop              >>       .xsession  " );
			printf( "============\n" );

			/// new: nsystem( "    mixerctl -w outputs.speaker=116   " ); // for netbsd, pi desktop after a reboot. 
			return 0;
	}







	// for openpandora pandora and pyra  
	if ( argc == 2 )
	if ( strcmp( argv[1] , "cmevilwm" ) ==  0 ) 
	{
			printf( "========================\n" );
			printf( "Classic version (clean) \n" );
			printf( "========================\n" );
			nsystem( " cd ; echo                >    .xinitrc  " );
			nsystem( " cd ; echo  cd            >>   .xinitrc  " );
			nsystem( " cd ; echo  ' xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xinitrc  " );
			nsystem( " cd ; echo  ' xsetfork s off ;  xsetfork -dpms ;  xsetfork s noblank  ; setterm -blank 0 ' >> .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    kicker &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo   xrdb  .Xresources               >> .xinitrc  " );
			nsystem( " cd ; echo   xsetroot -cursor_name left_ptr  >> .xinitrc " ); 
			nsystem( " cd ; echo                        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo     mixerctl -w outputs.speaker=116      >> .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    cmevilwm  &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  '     xsetroot -solid #3A6EA5 '        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo    nloop              >>       .xinitrc  " );

			printf( "============\n" );

			nsystem( " cd ; echo  'xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xsession  " );
			nsystem( " cd ; echo   xrdb  .Xresources    >>      .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '    env TZ=Europe/Paris      kicker &  '        >>       .xsession  " );
			nsystem( " cd ; echo   xrdb  .Xresources               >> .xsession   " );
			nsystem( " cd ; echo   xsetroot -cursor_name left_ptr  >> .xsession   " ); 
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo     mixerctl -w outputs.speaker=116      >> .xsession   " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '    env TZ=Europe/Paris      cmevilwm &  '        >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '     xsetroot -solid #3A6EA5 '        >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo    nloop              >>       .xsession  " );
			printf( "============\n" );

			/// new: nsystem( "    mixerctl -w outputs.speaker=116   " ); // for netbsd, pi desktop after a reboot. 
			return 0;
	}








	if ( argc == 2 )
	if ( strcmp( argv[1] , "ctwm" ) ==  0 ) 
	{
			printf( "========================\n" );
			printf( "Classic version (clean) \n" );
			printf( "========================\n" );
			nsystem( " cd ; echo                >    .xinitrc  " );
			nsystem( " cd ; echo  cd            >>   .xinitrc  " );
			nsystem( " cd ; echo setxkbmap de   >>   .xinitrc  " );
			nsystem( " cd ; echo  ' xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    kicker &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			///nsystem( " cd ; echo ctwm                  >>       .xinitrc  " );

			nsystem( " cd ; echo  '  env TZ=Europe/Paris    ctwm  &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  '     xsetroot -solid #3A6EA5 '        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo    nloop              >>       .xinitrc  " );

			printf( "============\n" );
			nsystem( " cd ; echo setxkbmap de          >        .xsession " );
			nsystem( " cd ; echo  'xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '    env TZ=Europe/Paris      kicker &  '        >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			//nsystem( " cd ; echo  ctwm                 >>       .xsession  " );

			nsystem( " cd ; echo  '  env TZ=Europe/Paris    ctwm  &  '        >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '     xsetroot -solid #3A6EA5 '        >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo    nloop              >>       .xsession  " );
			printf( "============\n" );
			/// nsystem( "    cd ; wget https://termbin.com/bp8h -O  .ctwmrc " );
			return 0;
	}









	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "arch" ) ==  0 ) || ( strcmp( argv[1] , "architecture" ) ==  0 ) )
	{
	        if ( MYOS == 1 ) 
		{
			printf( "\n" ); 
			//void_print_architecture();
                        system( " dpkg --print-architecture " ); 
			printf( "\n" ); 
		}
		else
		{
		    printf( "BSD systems you can use sysctl(3) to look at hw.machine_arch "); 
		    nsystem( " sysctl -a | grep arch " ); 
		}
		return 0; 
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] ,    "install" ) ==  0 )
	if ( ( strcmp( argv[2] ,   "archlinux" ) ==  0 )  || ( strcmp( argv[2] ,   "archlinux" ) ==  0 )  
	|| ( strcmp( argv[2] ,    "arch" ) ==  0 )  || ( strcmp( argv[2] ,    "archlinux" ) ==  0 )  )
	{
		nsystem( " dhclient      eth0      " ); /// archlinux
		nsystem( " dhcpcd        eth0  " ); /// archlinux 
		nsystem( " dhcpcd -v enp8s0 " ); /// ubuntu groovy  or bulleye
		nsystem( " dhclient enp8s0 " ); /// ubuntu groovy  or bulleye

		npkg_update(); 
		nsystem( " mkdir /usr/local/bin "); 
		nsystem( " mkdir /usr/src/      "); 
		npkg( " arch-install-scripts " ); 
		npkg( " xterm " );  
		npkg( " xinit " );  
		npkg( " screen " );  
		npkg( " clang " );  
		npkg( " gcc " );  
		npkg( " g++ " );  
		npkg( " make        " );  
		npkg( " nano        " );  
		npkg( " blackbox    " );  
		npkg( " dhcpcd      " );  
		npkg( " openssh        " );  
		npkg( " wpasupplicant  " );    // debian 
		npkg( " wpa_supplicant  " );   /// actually it is core/wpa_supplicant 2:2.9-8 
		npkg( " ifupdown       " );  
		npkg( " xorg-xrandr    " );  
		npkg( " xorg-xinit  xorg-server " ); 
		npkg( " links " );  
		npkg( " links2 " );  
		npkg( " xorg-xrandr " ); 
		npkg( " xorg-xclip " ); 
		npkg( " xorg-xinput " ); 
		npkg( " xorg-xinit  xorg-server " ); 

		npkg( " scrot " );  
		npkg( " xclip " );  
		return 0; 
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] , "--new" ) ==  0 ) 
        if ( strcmp( argv[2] , "vmdk" ) ==  0 )
	{
	       //nsystem( " qemu-img create -f raw -o size=1G sles-img-1g.raw   \n" ); 
	       nsystem( "  qemu-img create -f vmdk vdisk.vmdk 3G " ); 
	       return 0; 
        }




	// FreeBSD
	// sysctl kern.geom.debugflags=16 
	// syssrc  +amdgpu   <--modules
	if ( argc == 3 )
	if ( strcmp( argv[1] ,     "help" ) ==  0 ) 
	if ( strcmp( argv[2] ,     "freebsd" ) ==  0 ) 
	{
	   printf( " ============ \n" ); 
  //    https://download.freebsd.org/ftp/releases/arm64/aarch64/ISO-IMAGES/13.0/FreeBSD-13.0-RELEASE-arm64-aarch64-RPI.img.xz                   
	   printf( "  NomadBSD (FreeBSD):  \n" ); 
	   printf( " /usr/sbin/sysrc -f /etc/rc.conf kld_list+='cuse ig4 iicbus iichid utouch' >/dev/null 2>/dev/null  \n"); 
	   printf( "# kern.vt.fb.default_mode=0x17e\n" );
	   printf( " ========================== \n" ); 
	   printf( " ====   eeePC INSTALL  ====  \n" ); 
	   printf( " ========================== \n" ); 
	   printf( "   \n" ); 
	   printf( " > Memstick  (...)  \n" ); 
	   printf( "   vbe set 0x17e   \n" ); 
	   printf( "   bsdinstall    \n" ); 
	   printf( "     create adas1a (see help gpart)  \n" ); 
	   printf( "     newfs  adas1a (see help gpart)  \n" ); 
	   printf( "   bsdinstall  (...)  \n" ); 
	   printf( "   Esc  (...)  \n" ); 
	   printf( " > Boot  (...)  \n" ); 
	   printf( "   vbe set 0x17e   \n" ); 
	   printf( "   boot disk1s2a:/boot/kernel/kernel  \n" ); 
	   printf( " ========================== \n" ); 
	   return 0; 
        }

	if ( argc == 2 )
	if ( strcmp( argv[1] , "freebsd" ) ==  0 ) 
	{
		fetch_file_ftp(  "https://download.freebsd.org/ftp/releases/arm64/aarch64/ISO-IMAGES/12.2/FreeBSD-12.2-RELEASE-arm64-aarch64-RPI3.img.xz"   );
		//fetch_file_ftp(  "https://download.freebsd.org/ftp/releases/arm64/aarch64/ISO-IMAGES/12.1/FreeBSD-12.1-RELEASE-arm64-aarch64-RPI3.img.xz"   );
		return 0; 
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] , "freebsd" ) ==  0 ) 
	{
		//fetch_file_ftp( "http://ftp-archive.freebsd.org/pub/FreeBSD-Archive/old-releases/amd64/amd64/ISO-IMAGES/12.2/FreeBSD-12.2-RELEASE-amd64-disc1.iso" );
		//fetch_file_ftp( "http://ftp-archive.freebsd.org/pub/FreeBSD-Archive/old-releases/amd64/amd64/ISO-IMAGES/12.2/FreeBSD-12.2-RELEASE-amd64-memstick.img" );
		// qemu on PI rpi3b !
		fetch_file_ftp( "http://ftp-archive.freebsd.org/pub/FreeBSD-Archive/old-releases/i386/i386/ISO-IMAGES/12.2/FreeBSD-12.2-RELEASE-i386-disc1.iso" ); 
		return 0; 
	}





        ///  jitsi firefox 0.93 on jitsi on fedora efi fc35
	if ( argc == 2 )
	if ( strcmp( argv[1] , "rc-ntpd" ) ==  0 ) 
	{
		printf( "ntpd_enable=\"YES\"  \n" );
		printf( "ntpd_sync_on_start=\"YES\"  \n" );
		return 0; 
        }


  










	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "--mpv-webcam" ) ==  0 ) || ( strcmp( argv[1] , "--mpv-cam" ) ==  0 ) )
	{
                nsystem( " mpv av://v4l2:/dev/video0  "); 
		return 0; 
	}







	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "webshot" ) ==  0 ) 
	{
	        snprintf( charo , sizeof( charo ), " ffmpeg  -f video4linux2 -i /dev/video0      -frames:v 1    %d-vrec-video0.png    " , (int)time(NULL) );
		printf( "Command: %s\n", charo ); 
		nsystem( charo ); 
		return 0;
	}









 
	
	






	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "webcam" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--webcam-cvlc" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-webcam" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--webcam-mpv" ) ==  0 ) ) 
	{
	        if ( strcmp( argv[1] , "--cam-cvlc" ) ==  0 ) 
			nsystem( " cvlc /dev/video0  "); 

	        else if ( strcmp( argv[1] , "--cam-mpv" ) ==  0 ) 
                   nsystem( "  mpv av://v4l2:/dev/video0   " ); 

		else if ( fexist( "/usr/bin/mplayer" ) ) 
			nsystem( " sleep 1 ; mplayer tv:// " );
		else  if ( fexist( "/usr/bin/mpv" ) ) 
			nsystem( " mpv av://v4l2:/dev/video0  "); 
		else  if ( fexist( "/usr/bin/cvlc" ) ) 
			nsystem( " cvlc /dev/video0  "); 
		else  if ( fexist( "/usr/bin/vlc" ) ) 
			nsystem( " vlc /dev/video0  "); 
		// add vlc !
		return 0;
	}



	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "webcam" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--webcam" ) ==  0 )
	|| ( strcmp( argv[1] , "-webcam" ) ==  0 )
	|| ( strcmp( argv[1] , "cam" ) ==  0 ) )
	{
		if ( os_bsd_type == 1 ) 
		{
		   printf( " Likely freebsd \n" ); 
		   printf( " mplayer tv:// -cache 128 -tv driver=v4l2:width=640:height=480:device=/dev/video0 \n" ); 
		}
		nsystem( " sleep 1 ; mplayer tv:// " );
		nsystem( " cd ; xrandr ; cd ; xrdb .Xresources ; cd ; xrdb .Xresources ;   cd ; xsetroot -cursor_name left_ptr ;    cd ; xsetroot -cursor_name left_ptr ;   ");  
		return 0;
	}






















	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "vmm" ) ==  0 )
	{
	       printf( " =====================\n" ); 
	       printf( "   VMM / Hypervisor \n" ); 
	       printf( " =====================\n" ); 
	       printf( " =====================\n" ); 
	       printf( " vmctl create -s 20G username.qcow2 \n" ); 
	       printf( " or \n" ); 
	       printf( " qemu-img create -f qcow2 vdisk.qcow2  20G \n" ); 
	       printf( " =====================\n" ); 
	       printf( "\n"); 
	       return 0;
	}











	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "gpart" ) ==  0 ) 
	{
		printf( "    ==================== \n" ); 
		printf( "    ==================== \n" ); 
		/// freebsd
		printf( "    root@server:/home/username # gpart destroy -F da1\n" );
		printf( "    da1 destroyed\n" );
		printf( "    root@server:/home/username # gpart create -s MBR da0\n" );  //<--- better for lower size < tb
		printf( "    da1 created\n" );
		printf( "    root@server:/home/username # gpart add  -t freebsd da1 \n" );
		printf( "    da1s1 added\n" );
		printf( "    ==================== \n" ); 
		printf( "    ==================== \n" ); 
		printf( " > > gpart(8) to setup new systems with the following commands:\n" );
		printf( " > > # gpart create -s mbr ad4                 # Init the disk with an MBR\n" );
		printf( " > > # gpart add -t freebsd ad4                # Create a BSD container  \n" );
		printf( " > > # gpart create -s bsd ad4s1               # Init with a BSD scheme \n" );
		printf( " > > # gpart add -t freebsd-ufs  -s 1G ad4s1   # 1GB for /             \n" );
		printf( " > > # gpart add -t freebsd-swap -s 2G ad4s1   # 2GB for swap         \n" );
		printf( " > > # gpart add -t freebsd-ufs  -s 2G ad4s1   # 2GB for /var        \n" );
		printf( " > > # gpart add -t freebsd-ufs  -s 1G ad4s1   # 1GB for /tmp       \n" );
		printf( " > > # gpart add -t freebsd-ufs ad4s1          # all rest for /usr  \n" );
		printf( " > > # gpart set -a active -i 1 ad4                                \n" );
		printf( "    ==================== \n" ); 
		printf( "    ==================== \n" ); 
		printf( "    ==================== \n" ); 
		printf( "\n" );
		printf( " > Modify to linux-data to freebsd:\n" );
		printf( "\n" );
		printf( "   gpart modify -i 2 -t freebsd ada0 \n" );
		printf( "\n" );
		printf( "   gpart create -s bsd ada0s2   \n" );
		printf( "     > ada0s2 created \n" );
		printf( "\n" );
		printf( "   gpart add -t freebsd-ufs ada0s2 \n" );
		printf( "     > ada0s2a added \n" );
		printf( "    ==================== \n" ); 
		printf( "    ==================== \n" ); 
		return 0;
	}










	if ( argc == 3 )
	if ( strcmp( argv[1] , "minidisk" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "2G" ) ==  0 ) || ( strcmp( argv[2] , "2g" ) ==  0 ) )
	{
		nsystem( " qemu-img create -f vmdk minidisk.vmdk 2g " ); 
		nsystem( " qemu-img create -f vmdk minidisk.vmdk 2G " ); 
		return 0; 
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "ext3" ) ==  0 ) || ( strcmp( argv[2] , "ext2" ) ==  0 ) )
	{
		printf( " BSD:      e2fsprogs \n" ); 
		printf( " FREEBSD:  /boot/kernel/ext2fs.ko  \n" ); 
		printf( "           /usr/local/sbin/mkfs.ext3  /dev/ada0s3   <--- it works well \n" ); 
		return 0;
	}




	if ( argc == 3 )
	if ( strcmp( argv[1] , "create" ) ==  0 ) 
	if ( strcmp( argv[2] , "ada0s2a" ) ==  0 ) 
	{
		nsystem( " echo FreeBSD ; gpart modify -i 2 -t freebsd ada0  ; gpart create -s bsd ada0s2  ; gpart add -t freebsd-ufs ada0s2  "); 
		return 0;
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] , "create" ) ==  0 ) 
	if ( strcmp( argv[2] , "ada0s4a" ) ==  0 ) 
	{
		nsystem( " echo FreeBSD ; gpart modify -i 4 -t freebsd ada0  ; gpart create -s bsd ada0s4  ; gpart add -t freebsd-ufs ada0s4  "); 
		return 0;
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] , "create" ) ==  0 ) 
	if ( strcmp( argv[2] , "da0s2a" ) ==  0 ) 
	{
		nsystem( " echo FreeBSD ; gpart modify -i 2 -t freebsd da0  ; gpart create -s bsd da0s2  ; gpart add -t freebsd-ufs da0s2  "); 
		return 0;
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] , "create" ) ==  0 ) 
	if ( strcmp( argv[2] , "da0s3a" ) ==  0 ) 
	{
		nsystem( " echo FreeBSD ; gpart modify -i 3 -t freebsd da0  ; gpart create -s bsd da0s3  ; gpart add -t freebsd-ufs da0s3  "); 
		return 0;
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] , "create" ) ==  0 ) 
	if ( strcmp( argv[2] , "da1s1a" ) ==  0 ) 
	{
		nsystem( " echo FreeBSD ; gpart modify -i 1 -t freebsd da1  ; gpart create -s bsd da1s1  ; gpart add -t freebsd-ufs da1s1  "); 
		return 0;
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] , "create" ) ==  0 ) 
	if ( strcmp( argv[2] , "da0s4a" ) ==  0 ) 
	{
		nsystem( " echo FreeBSD ; gpart modify -i 4 -t freebsd da0  ; gpart create -s bsd da0s4  ; gpart add -t freebsd-ufs da0s4  "); 
		return 0;
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] , "create" ) ==  0 ) 
	if ( strcmp( argv[2] , "da1s2a" ) ==  0 ) 
	{
		nsystem( " echo FreeBSD ; gpart modify -i 2 -t freebsd da1  ; gpart create -s bsd da1s2  ; gpart add -t freebsd-ufs da1s2  "); 
		return 0;
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] , "create" ) ==  0 ) 
	if ( strcmp( argv[2] , "da1s4a" ) ==  0 ) 
	{
		nsystem( " echo FreeBSD ; gpart modify -i 2 -t freebsd da1  ; gpart create -s bsd da1s4  ; gpart add -t freebsd-ufs da1s4  "); 
		return 0;
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "disks"  ) ==  0 )
	{
		if ( MYOS == 1 ) 
			system( "  fdisk -l  "); 
		else  if ( os_bsd_type == 1 )  
		{
			printf( "=== LIST DRIVE  === \n" ); 
			printf( "> DRIVE\n" );
			system( "  sysctl -a | grep kern.disk "); 
			system( "  gpart show | grep da  "); 
		}
		else
			nsystem( " sysctl -a | grep 'diskname' " );
		return 0;
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] , "ramdisk" ) ==  0 ) 
	{
		 if ( MYOS == 1 ) 
		 {
                    nsystem( "   mkdir /ramdisk ; mount -t tmpfs -osize=1200M none   /ramdisk  ; mount ; mkdir 777 /ramdisk " ); 
		 }
		 else
		 {
	            printf( " FreeBSD:  mkdir /ramdisk ;   /sbin/mdmfs -M -S -o async -s 1500m md5 /ramdisk/  ; df -h | grep ramdisk \n" ); 
	            nsystem( " mkdir /ramdisk ;   /sbin/mdmfs -M -S -o async -s 1500m md5 /ramdisk/  ; df -h | grep ramdisk " ); 
		 }
		 return 0; 
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] ,    "ramdisk" ) ==  0 ) 
	if ( ( strcmp( argv[2] ,  "8G" ) ==  0 ) || ( strcmp( argv[2] ,     "8gb" ) ==  0 )  || ( strcmp( argv[2] ,  "8g" ) ==  0 ) )
	{
                  nsystem( "   mkdir /var/ramdisk ; mount -t tmpfs -osize=8500M none /var/ramdisk  ; mount | grep ramdisk ; chmod 777 /var/ramdisk " ); 
		  return 0; 
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] ,    "ramdisk" ) ==  0 ) 
	if ( ( strcmp( argv[2] ,  "14G" ) ==  0 ) || ( strcmp( argv[2] ,     "14gb" ) ==  0 )  || ( strcmp( argv[2] ,  "14g" ) ==  0 ) )
	{
                  nsystem( "   mkdir /var/ramdisk ; mount -t tmpfs -osize=14500M none /var/ramdisk  ; mount | grep ramdisk ; chmod 777 /var/ramdisk " ); 
		  return 0; 
	}


	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "minidisk" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "miniram" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "miniramdisk" ) ==  0 ) )
	{
		 if ( MYOS == 1 ) 
                    nsystem( "   mkdir /ramdisk ; mount -t tmpfs -osize=350M none   /ramdisk  ; mount  ; chmod 777 /ramdisk  " ); 
		 return 0; 
	}







	if ( argc == 2 )
	if ( strcmp( argv[1] ,     "ramdisk" ) ==  0 ) 
	{
		 if ( MYOS == 1 ) 
		 {
                    nsystem( "   mkdir /ramdisk ; mount -t tmpfs -osize=1200M none /ramdisk  ; mount  ; chmod 777 /ramdisk " ); 
		 }
		 else
		 {
	            printf( " FreeBSD:  mkdir /ramdisk ;   /sbin/mdmfs -M -S -o async -s 500m md5 /ramdisk/  ; df -h | grep ramdisk \n" ); 
	            nsystem( " mkdir /ramdisk ;   /sbin/mdmfs -M -S -o async -s 500m md5 /ramdisk/  ; df -h | grep ramdisk " ); 
		 }
		 return 0; 
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] ,     "ramdisk" ) ==  0 ) 
	if ( ( strcmp( argv[2] ,     "2G" ) ==  0 ) 
	|| ( strcmp( argv[2] ,     "2gb" ) ==  0 ) 
	|| ( strcmp( argv[2] ,     "2g" ) ==  0 ) )
	{
                  nsystem( "   mkdir /var/ramdisk ; mount -t tmpfs -osize=2200M none /var/ramdisk  ; mount | grep ramdisk " ); 
		  return 0; 
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] ,     "ramdisk" ) ==  0 ) 
	if ( ( strcmp( argv[2] ,     "4G" ) ==  0 ) || ( strcmp( argv[2] ,     "4g" ) ==  0 ) )
	{
                  nsystem( "   mkdir /var/ramdisk ; mount -t tmpfs -osize=4500M none /var/ramdisk  ; mount | grep ramdisk " ); 
		  return 0; 
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] ,     "rammnt" ) ==  0 ) 
	{
                 nsystem( " mount -t tmpfs -osize=2G none /mnt  ; mount " ); 
		 return 0; 
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] ,     "ramdisk" ) ==  0 ) 
	if ( ( strcmp( argv[2] ,     "4G" ) ==  0 ) || ( strcmp( argv[2] ,     "4g" ) ==  0 ) )
	{
                  nsystem( "   mkdir /var/ramdisk ; mount -t tmpfs -osize=4500M none /var/ramdisk  ; mount | grep ramdisk " ); 
		  return 0; 
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "rc.conf-intel" ) ==  0 ) 
	{
		printf( "allscreens_flags=\"green\"\n" );
		printf( "bsdstats_enable=\"NO\"\n" );
		printf( "clear_tmp_X=\"NO\"\n" );
		printf( "cupsd_enable=\"YES\"\n" );
		printf( "dbus_enable=\"YES\"\n" );
		printf( "devfs_system_ruleset=\"desktopuser\"\n" );
		printf( "dsbdriverd_enable=\"YES\"\n" );
		printf( "dsbmd_enable=\"YES\"\n" );
		printf( "entropy_file=\"NO\"\n" );
		printf( "hostname=\"NomadBSD\"\n" );
		printf( "kld_list=\"cuse4bsd /boot/modules/i915kms.ko\"\n" );
		printf( "ldconfig_insecure=\"YES\"\n" );
		printf( "lpd_enable=\"NO\"\n" );
		printf( "ntpd_enable=\"YES\"\n" );
		printf( "ntpd_sync_on_start=\"YES\"\n" );
		printf( "powerdxx_enable=\"YES\"\n" );
		printf( "savecore_enable=\"NO\"\n" );
		printf( "sendmail_enable=\"NO\"\n" );
		printf( "sendmail_msp_queue_enable=\"NO\"\n" );
		printf( "sendmail_outbound_enable=\"NO\"\n" );
		printf( "sendmail_submit_enable=\"NO\"\n" );
		printf( "setup_mouse_enable=\"YES\"\n" );
		printf( "syslogd_enable=\"YES\"\n" );
		printf( "update_motd=\"NO\"\n" );
		printf( "webcamd_enable=\"YES\"\n" );
		printf( "economy_cx_lowest=\"C2\"\n" );
		printf( "performance_cx_lowest=\"C2\"\n" );
		printf( "initgfx_enable=\"NO\"\n" );
		printf( "slim_enable=\"YES\"\n" );
		printf( "init_vbox_enable=\"YES\"\n" );
		printf( "avahi_daemon_enable=\"YES\"\n" );
		printf( "ipv6_activate_all_interfaces=\"YES\"\n" );
		printf( "ackfwl_enable=\"YES\"\n" );
		printf( "load_iichid_enable=\"YES\"\n" );
		printf( "keymap=\"de.kbd\"\n" );
		printf( "initgfx_kmods=\"/boot/modules/i915kms.ko\"\n" );
		printf( "ifconfig_re0=\"DHCP\"\n" );
		printf( "ifconfig_re0_ipv6=\"inet6 accept_rtadv\"\n" );
		printf( "wlans_rtwn0=\"wlan0\"\n" );
		printf( "create_args_wlan0=\"down country BE\"\n" );
		printf( "ifconfig_wlan0=\"up scan WPA DHCP\"\n" );
		printf( "ifconfig_wlan0_ipv6=\"inet6 accept_rtadv\"\n" );
		printf( "wlans_ath0=\"wlan1\"\n" );
		printf( "create_args_wlan1=\"down country BE\"\n" );
		printf( "ifconfig_wlan1=\"up scan WPA DHCP\"\n" );
		printf( "ifconfig_wlan1_ipv6=\"inet6 accept_rtadv\"\n" );
		return 0; 
	}













	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "nmap" ) ==  0 ) 
	{
		printf( " mconfig nmap 10   // with 10 the X in 192.168.X.... "); 
		printf( " mconfig cmping 8 2 4   // with 10 the X in 192.168.X.... "); 
		printf( "\n" ); 
		printf( " nmap -sP 192.168.1.2-100 \n" ); 
		printf( " mconfig nmap 1   // with 1 the X in 192.168.X.... "); 
		printf( "\n" ); 
		printf( " nmap -sP 10.0.0.2-254 may return 138, 201, 162 \n" );  
		return 0; 
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] , "nmap0" ) ==  0 ) 
	{
	    //if ( fexist( "/usr/bin/host" ) == 1 ) 
	    //  nsystem( "  seq 1 1 254 | while read -r i ; do  host 192.168.0.$i | grep -v 'not found'  ; done   " ); 
	    //else
	    nsystem( "  seq 1 1 254 | while read -r i ; do ping -c 1  192.168.0.$i ; done   " ); 
	    return 0; 
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] , "nmap-arpa" ) ==  0 ) 
	{
	    nsystem( "  seq 1 1 254 | while read -r i ; do   host 10.0.0.$i | grep -v 'not found'  ; done   " ); 
	    return 0; 
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] , "nmap" ) ==  0 ) 
	{
            nsystem( " seq 1 1 254 | while read -r i ; do ping -c1 192.168.1.$i   ; done   | grep ttl  "); 
	    printf( "\n" ); 
	    return 0; 
	}





	if ( argc == 2 )
	if ( strcmp( argv[1] , "nmap10" ) ==  0 ) 
	{
            nsystem( " seq 1 1 254 | while read -r i ; do ping -c 1 10.0.0.$i ; done  " ); 
	    return 0; 
	}



        /// this one ok
	if ( argc == 2)
	if ( strcmp( argv[1] , "nmap1" ) ==  0 ) 
	{
		for( i = 1 ; i <= 254 ; i++) 
		{
			strncpy( cmdi , " " , PATH_MAX );
			strncat( cmdi , " ping  -c 1 192.168.1." , PATH_MAX - strlen( cmdi ) -1 );
			snprintf( charo , sizeof(charo ), "%d",  i );
			strncat( cmdi , charo , PATH_MAX - strlen( cmdi ) -1 );
			system( cmdi );
		}
		return 0;
	}






	if ( argc >= 3 )
	if ( strcmp( argv[1] , "cmping" ) ==  0 ) 
	{
	    printf( "\n" ); 
	    for( i = 1 ; i <= 254 ; i++) 
	    {
		    //snprintf( charo , sizeof( charo ), "  ping -c 1  192.168.%s.%d  ", argv[ 2 ],  i  );
		    //strncpy( cmdi , " " , PATH_MAX );

		    snprintf( charo , sizeof( charo ), "  ping -c 1  192.168.%s.%d  | grep 192 | grep ttl  ", argv[ 2 ],  i  );
		    printf( " > Processing : %s \n" , charo ); 
		    //strncat( cmdi , charo , PATH_MAX - strlen( cmdi ) -1 );
		    //strncat( cmdi , "   | grep 192 | grep ttl   " , PATH_MAX - strlen( cmdi ) -1 );
		    system( charo );
	    }
	    return 0; 
	}






        /// this one ok
	if ( argc == 3 )
	if ( strcmp( argv[1] , "nmap" ) ==  0 ) 
	// 2 is the 3.id 
	{
		for( i = 1 ; i <= 254 ; i++) 
		{
				snprintf( charo , sizeof( charo ), "  ping -c 1  192.168.%s.%d  ", argv[ 2 ],  i  );
				printf( " > Processing : %s \n" , charo ); 

				strncpy( cmdi , " " , PATH_MAX );
				//strncat( cmdi , " ping  -c 1 192.168.1." , PATH_MAX - strlen( cmdi ) -1 );
				snprintf( charo , sizeof( charo ), "  ping -c 1  192.168.%s.%d  ", argv[ 2 ],  i  );
				strncat( cmdi , charo , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , " | grep 192 | grep ttl " , PATH_MAX - strlen( cmdi ) -1 );
				system( cmdi );
		}
		return 0;
	}








	if ( argc == 3 )
	if ( strcmp( argv[1] , "pissh" ) ==  0 ) 
	{
		snprintf( charo , sizeof( charo ), "  ssh -C  pi@192.168.1.%s ", argv[ 2 ] );
		nsystem( charo ); 
		return 0; 
	}
	if ( argc == 3 )
	if ( strcmp( argv[1] , "xpissh" ) ==  0 ) 
	{
		snprintf( charo , sizeof( charo ), "  ssh -C -X pi@192.168.1.%s ", argv[ 2 ] );
		nsystem( charo ); 
		return 0; 
	}






	///////////////////////////////////////////////////////
	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "-xssh" ) ==  0 ) || ( strcmp( argv[1] , "xssh" ) ==  0 ) )
	{
			strncpy( cmdi, "  ssh -C -X  -p  22  192.168.1.", PATH_MAX );
			strncat( cmdi , argv[2] , PATH_MAX - strlen( cmdi ) -1 );
			strncat( cmdi , "  "  , PATH_MAX - strlen( cmdi ) -1 );
			nsystem( cmdi );
			return 0;
	}



	if ( argc >= 4 )
	if ( ( strcmp( argv[1] , "connect" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-connect" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-piconnect" ) ==  0 ) 
	|| ( strcmp( argv[1] , "piconnect" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--connect" ) ==  0 ) )
	{
	        printf( "> SSH CONNECT ...\n" ); 
		printf( "Automatic:\n" );
		for( i = 3 ; i < argc ; i++ ) 
		{
			printf( " > Processing item number: %d/%d \n" , i , argc -1  ); 

			strncpy( cmdi , " " , PATH_MAX );

	                if ( strcmp( argv[1] , "piconnect" ) ==  0 ) 
			  snprintf( charo , sizeof( charo ), "  ssh -C  pi@192.168.%s.%s  ", argv[ 2 ],  argv[ i ] );

	                else if ( strcmp( argv[1] , "-piconnect" ) ==  0 ) 
			  snprintf( charo , sizeof( charo ), "  ssh -C  pi@192.168.%s.%s  ", argv[ 2 ],  argv[ i ] );
			else 
			  snprintf( charo , sizeof( charo ), "  ssh -C 192.168.%s.%s  ", argv[ 2 ],  argv[ i ] );
			strncat( cmdi , charo , PATH_MAX - strlen( cmdi ) -1 );
			nsystem( cmdi );
		}
		return 0;
	}












	if ( argc == 3 )
	if ( strcmp( argv[1] , "git" ) ==  0 ) 
	if ( strcmp( argv[2] , "push" ) ==  0 ) 
	{

		printf("  ===> git add foldername\n" );
		nsystem( " git add * ; git config --global user.email myusername@freefoss.org ; git config --global user.name myusername ; git status ; git commit -m added  ; git push   "); 
		return 0; 
	}
















	if ( argc == 2 )
	if ( strcmp( argv[1] , "fstab" ) ==  0 )
	{
		/*
		if ( MYOS == 1 ) 
		{
			printf( "\n" );
			printf( "\n" );
                        printf( "/dev/sda1  / ext4 rw\n" ); 
			printf( "\n" );
                        printf( "# /dev/sda1  /  ext3 rw\n" ); 
                        printf( "# /dev/mmcblk0p2  /  ext3 rw\n" ); 
			printf( "\n" );
			printf( "\n" );
		}
		*/
		printf( "\n" ); 
		printf( "/dev/sda2 / ext4 defaults,rw,noatime,commit=600,errors=remount-ro 0 1\n" ); 
		printf( "tmpfs /tmp tmpfs defaults,nosuid 0 0\n" ); 
		printf( "\n" ); 

		printf( "\n" ); 
		printf( "# proc            /proc           proc    defaults          0       0\n"); 
		printf( "# PARTUUID=6397c987-01  /boot           vfat    defaults          0       2\n"); 
		printf( "# /dev/mmcblk0p2  /               ext4    defaults,noatime  0       1\n"); 
		printf( "# a swapfile is not a swap partition, no line here\n"); 
		printf( "#   use  dphys-swapfile swap[on|off]  for that\n"); 
		printf( "\n" ); 
		return 0;
	}





	if ( argc == 2 )
	if ( strcmp( argv[1] , "fstab-old" ) ==  0 )
	{
		if ( MYOS == 1 ) 
		{
			printf( "\n" );
			printf( "\n" );
                        printf( "/dev/sda1  / ext4 rw\n" ); 
			printf( "\n" );
                        printf( "# /dev/sda1  /  ext3 rw\n" ); 
                        printf( "# /dev/mmcblk0p2  /  ext3 rw\n" ); 
			printf( "\n" );
			printf( "\n" );
		}
		else 
		{
			printf( "# NetBSD /etc/fstab\n" );
			printf( "# See /usr/share/examples/fstab/ for more examples.\n" );
			printf( "ROOT.a               /       ffs     rw               1 1\n" );
			printf( "ROOT.b               none    swap    sw,dp            0 0\n" );
			printf( "kernfs          /kern   kernfs  rw\n" );
			printf( "ptyfs           /dev/pts        ptyfs   rw\n" );
			printf( "procfs          /proc   procfs  rw\n" );
		}
		return 0; 
	}







	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "tuc" ) ==  0 ) || ( strcmp( argv[1] , "-tuc" ) ==  0 ) || ( strcmp( argv[1] , "--tuc" ) ==  0 ) )
	{
		// clean TeX content for PDF only
	        nsystem( " rm *.log *.dak  *.bak  *.blg  *.bbl *.bib  *.bmr *.tex *.out *.dat  *.toc *.snm *.aux *.nav *.bmr *.mrk *.aux *.snm         *.log *.aux " );
		return 0;
	}






	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "-tucdir" ) ==  0 ) || ( strcmp( argv[1] , "-tuc" ) ==  0 )
	|| ( strcmp( argv[1] , "tuc" ) ==  0 ) || ( strcmp( argv[1] , "-tuc" ) ==  0 ) || ( strcmp( argv[1] , "--tuc" ) ==  0 ) )
	{

			strncpy( currentpath , getcwd( cwd , PATH_MAX ) , PATH_MAX );
			printf( "path: %s\n", getcwd( cmdi , PATH_MAX ) );
			// mini -args 
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					chdir( currentpath ); 
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );

					}
					else
					{
						chdir( currentpath ); 
						printf( "%d/%d: %s\n", i, argc-1 ,  argv[ i ] );
						if ( fexist( argv[ i ] ) == 2 )
						{
							chdir( argv[ i ] ); 
							printf( "path: %s\n", getcwd( cwd , PATH_MAX ) );
							nsystem( " rm *.log *.dak  *.bak  *.blg  *.bbl *.bib  *.bmr *.tex *.out *.dat  *.toc *.snm *.aux *.nav *.bmr *.mrk *.aux *.snm         *.log *.aux " );
							chdir( currentpath ); 
						}
					}

				}
			}
			////
			return 0;
	}













	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "username" ) ==  0 ) || ( strcmp( argv[1] , "USERNAME" ) ==  0 ) )
	{
			printf( "Username: %s\n", getenv( "USER" ) ); 
			if ( strcmp( getenv( "USER" ), "root" ) == 0 ) 
			{
				printf( "Username is root.\n" ); 
			}
			else
			{
				printf( "Username is NOT root.\n" ); 
			}
			return 0;
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] , "bootloader" ) ==  0 ) 
	{
		printf( "# Tested Bootloader from SD/MMC (pbpro, pinebook pro) \n" ); 
		fetch_file_ftp( "https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-minimal-pbpro-23.02.img.xz" ); 
		return 0;
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] , "bootloader-github" ) ==  0 ) 
	{
		printf( "# Tested Bootloader from SD/MMC (pbpro, pinebook pro) \n" ); 
		fetch_file_ftp( "https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-minimal-pbpro-23.02.img.xz" ); 
		return 0;
	}




	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "boot" ) ==  0 ) 
	|| ( strcmp( argv[2] ,   "sysboot" ) ==  0 ) 
	|| ( strcmp( argv[2] ,   "dec" ) ==  0 ) )
	{
	       printf( " =========================== \n" ); 
	       printf( " ===== NETBSD  BOOT ======== \n" ); 
	       printf( " =========================== \n" ); 
               printf( " NETBSD NOTEBOOK: boot -2 didn't work. boot -1 didn't work. It appears boot -12 does work  \n"); 
	       printf( " sysboot  \n"); 
	       printf( " cp /usr/mdec/boot /mnt   " );
	       printf( " umount /mnt   " );
	       printf( " installboot -v -o timeout=20 /dev/rsd0a /usr/mdec/bootxx_ffsv2   " );
	       printf( " sync ; installboot -f -o console=pc,speed=9600 /dev/rsd0a /usr/mdec/bootxx_ffsv2  ");  

	       printf( " =========================== \n" ); 
	       printf( " ===== LINUX BOOT   ======== \n" ); 
	       printf( " =========================== \n" ); 
	       printf( " =========================== \n" ); 
	       printf( " =========================== \n" ); 
	       printf( "   FREEBSD BOOT LOADER - beastie: \n" );
	       printf( "   menu already gave you the correct value: \n" );
	       printf( "   OK> set currdev=\"disk0s2\" \n" );
	       printf( "   OK> set vfs.root.mountfrom=\"ufs:/dev/ada0s2a\" \n" );
	       printf( "   Followed by boot. \n" );
	       printf( " =========================== \n" ); 
	       printf( " ===== FREEBSD BOOT ======== \n" ); 
	       printf( " =========================== \n" ); 
	       printf( "  sysctl kern.geom.debugflags=16 ");
	       printf( "  mount -u -o rw /dev/ufs/FreeBSD_Install /   ");
	       printf( "  ");
	       printf( "  boot: 0  (installed on ada0s2a)");
	       printf( "  vbe set 0x17e " ); 
	       printf( "  lsdev " );  
	       printf( "  boot disk0s3:/boot/kernel/kernel \n" );    
	       printf( "  multiboot disk0s3:/boot/kernel/kernel root=ada0s3 \n" );    
	       printf( " =========================== \n" ); 
	       printf( " =========================== \n" ); 
	       printf( " ===== NOMADBSD BOOT ======= \n" ); 
	       printf( " =========================== \n" ); 
	       printf( "   select UEFI into the boot esc key \n" ); 
	       printf( " =========================== \n" ); 
	       return 0;
	}
















	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "jmtpfs" ) ==  0 ) 
	{
                nsystem( " cd ; pwd ; mkdir phone ; jmtpfs phone ; cd 'phone' ; xterm  " ); 
		return 0;
	}








	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "cpu" ) ==  0 ) 
	{
		nsystem( "   cat /proc/cpuinfo  ");
		nsystem( "   cat /proc/cpuinfo | grep model ");
		return 0;
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] , "check" ) ==  0 ) 
	if ( strcmp( argv[2] , "cpus" ) ==  0 ) 
	{
                nsystem( " cat /sys/devices/system/cpu/cpu0/*/* " );   
                nsystem( " cat /sys/devices/system/cpu/cpu0/cpufreq/scaling* " ); 
		return 0; 
	}

	if ( argc == 3 )
	if ( strcmp( argv[1] , "check" ) ==  0 ) 
	if ( strcmp( argv[2] , "cpu" ) ==  0 ) 
	{
		system( "  cat /proc/cpuinfo " ); 
		return 0; 
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "plot" ) ==  0 )
	{
		printf( " ===== \n" ); 
		printf( " plot1.csv plot2.csv  (from graphreader.com)\n" ); 
		printf( " ===== \n" ); 
		printf( "  PLT FILE with :\n" ); 
		printf( "    plot [x range and then y range ... \n" ); 
		printf( "    plot [-10:10] [-5:5]  x**2, x**3, x , 0 \n" ); 
		printf( "  gnuplot -p file.plt \n"); 
		printf( " ===== \n" ); 
		printf( "  gnuplot> set dataf sep \", \"\n" );
		printf( "  gnuplot> set yrange [0:300]\n" );
		printf( "  gnuplot> set xrange [-160:40]\n" );
		printf( "  gnuplot> plot \"/home/user/plot2.csv\" using 1:2 with lines\n" );
		printf( " ===== \n" ); 
		return 0; 
        }




	if ( argc >= 3 )
	if ( strcmp( argv[1] , "pkgsearch" ) ==  0 )  // it is like in BSD
	{
		if ( fexist( "/usr/bin/apt-get" ) == 1 ) 
		{
		      // https://packages.debian.org/search?searchon=contents&keywords=ubiattach&mode=exactfilename&suite=stable&arch=any   
		        if ( fexist( "/usr/bin/links" ) == 1 ) 
			   snprintf( charo , sizeof( charo ), " links  'https://packages.debian.org/search?searchon=contents&keywords=%s&mode=exactfilename&suite=stable&arch=any' ",  argv[ 2 ] );
		        else
			   snprintf( charo , sizeof( charo ), " cmlinks  'https://packages.debian.org/search?searchon=contents&keywords=%s&mode=exactfilename&suite=stable&arch=any' ",  argv[ 2 ] );
			printf( "Message :%s:\n", charo ); 
			nsystem( charo ); 
		}
		else
		{
			for( i = 2 ; i < argc ; i++) 
			{
				printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
				// as root 
				// eg. search for xclip into xclip 
				strncpy( cmdi , " pacman -Fy  " , PATH_MAX );
				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				nsystem( cmdi ) ; 
				printf( " ============== \n "); 
			}
			//printf( " List of packages: %s\n" , cmdi ); 
			printf( " ============== \n "); 
		}
		return 0; 
	}




	if ( argc == 3 )
	if ( strcmp( argv[1] , "set" ) ==  0 ) 
	if ( strcmp( argv[2] , "pkglst" ) ==  0 ) 
	{
              nsystem( "  echo \"deb http://archive.debian.org/debian stretch main \" > /etc/apt/sources.list ; touch /etc/apt/gpg "  ); 
	      printf( " ========================  \n" ); 
	      return 0; 
        }



	if ( argc >= 3 )
	if ( strcmp( argv[1] , "dpkg" ) ==  0 )  // for linux for now
	{
		printf( " ============== \n "); 
		for( i = 2 ; i < argc ; i++) 
		{
			printf(   "=> %d/%d %s \n", i , argc , argv[ i ] );
			if ( MYOS == 1 ) 
			{
			  snprintf( charo , sizeof( charo ), " dpkg -i %s  ",   fbasename(  argv[ i ]  )  );
			  nsystem(  charo );
			}
		}
		printf( " ============== \n "); 
		return 0; 
	}








	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "fswebcam" ) ==  0 ) 
	{
		//printf( " fswebcam --device /dev/video0   --no-timestamp --no-info  --no-shadow --no-underlay --no-overlay --no-banner  --png --save webcam-$( date +%s ).png  " ); 
		return 0; 
	}



	// on linux and openbsd too
	if ( argc == 2 )
	if ( strcmp( argv[1] , "fswebcam" ) ==  0 ) 
	{
		//if ( MYOS == 1 ) 
		if ( fexist( "/usr/bin/fswebcam" ) == 1 )   
		{
			//	npkg( " fswebcam " );
			//printf( "  fswebcam --device /dev/video0   --no-timestamp --no-info  --no-shadow --no-underlay --no-overlay --no-banner  --png --save webcam.png \n" ); 
			nsystem( "  fswebcam --device /dev/video0 --no-timestamp --no-info  --no-shadow --no-underlay --no-overlay --no-banner  --png --save      webcam-fs-$( mconfig -t).png " ); 
		}
		else
		{
		   printf( " FSWEBCAM not found\n" );  
		}
		return 0;
	}











	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "disktree" ) ==  0 ) || ( strcmp( argv[1] ,   "tree" ) ==  0 ) )
	{
		nsystem( "   drvctl -t -l  ; sysctl hw.disknames  " ); 
		return 0;
	}





















	/////////////////////
	if ( argc == 2)
	if ( ( strcmp( argv[1] , "megawad" ) ==  0 ) || ( strcmp( argv[1] ,  "megawads" ) ==  0 ) )
	{
		//nsystem( "  mconfig -links 'https://www.doomworld.com/idgames/levels/doom2/megawads/'  " ); 
	        procedure_console_webbrowser( "https://www.doomworld.com/idgames/levels/doom2/megawads/" ); 
		printf( " Visit too : https://www.wad-archive.com/ \n" ); 
		return 0;
	}











	if ( argc == 2 )
	if ( strcmp( argv[1] , "demonastery" ) ==  0 ) 
	{
	     printf( " Raspberry PI 4 : /opt/retropie/ports/lzdoom/lzdoom +set fullscreen 1 -iwad doom2.wad -file ZionV08_Code.pk3 -file ZionV08_Resources.pk3 -file Demonastery.wad " );    
	     nsystem( " /opt/retropie/ports/lzdoom/lzdoom +set fullscreen 1 -iwad doom2.wad -file ZionV08_Code.pk3 -file ZionV08_Resources.pk3 -file Demonastery.wad  " );    
	     nsystem( " /opt/retropie/ports/zdoom/zdoom +set fullscreen 1 -iwad doom2.wad -file ZionV08_Code.pk3 -file ZionV08_Resources.pk3 -file Demonastery.wad  " );    
	     return 0; 
       }







	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) ==  0 ) 
	if ( ( strcmp( argv[2] ,    "prboom" ) ==  0 ) || ( strcmp( argv[2] ,   "doom" ) ==  0 ) || ( strcmp( argv[2] ,   "boom" ) ==  0 ) )
	{
           printf( "  cd ;  cd wads ;  /opt/retropie/ports/zdoom/zdoom -iwad doom2.wad -file eviternity.wad  -file mymap3.wad \n" ); 
	   printf( " === \n" ); 
	   printf( " mconfig boom client \n" ); 
	   printf( " mconfig boom server \n" ); 
	   printf( " === \n" ); 
	   printf("%s", KMAG );
	   printf( " mconfig zdoom server Vanguard.wad \n" ); 
	   printf("%s", KGRE);
	   printf( " mconfig zdoom client 192.168.1.22 Vanguard.wad  \n" ); 
	   printf("%s", KNRM);
	   printf( " === \n" ); 
	   printf( " === \n" ); 
	   printf( " SERVER: cd ; cd wads ; /opt/retropie/ports/zdoom/zdoom -netmode 0 -port 5030 -host 2 -iwad doom2.wad -file Vanguard.wad  \n" ); 
	   printf( " CLIENT: cd ; cd wads ; /opt/retropie/ports/zdoom/zdoom +set fullscreen 1 -netmode 0 -join  192.168.....6  -port 5030 -iwad doom2.wad -file   Vanguard.wad  \n" ); 
	   printf( " === \n" ); 
           printf( "  cd ;  cd wads ;  /opt/retropie/ports/zdoom/zdoom -iwad doom2.wad -file eviternity.wad  -file mymap3.wad \n" ); 
	   printf( " === \n" ); 

	   printf( " === \n" ); 
	   printf( " === =========== HELP ============\n" ); 
	   printf( " === =========== HELP ============\n" ); 
	   printf( " === =========== HELP ============\n" ); 
	   printf("%s", KGRE);
	   printf("%s", KGRE);
	   printf( "> HELP In Short, type on retropie rpi3b: \n" );   
	   printf( "    mconfig zdoom server Vanguard.wad \n" ); 
	   printf( "    mconfig zdoom client 192.168.1.22 Vanguard.wad  \n" ); 
	   printf( "  \n" ); 
	   printf( "> If you run gzdoom (opensuse leap 15.3), then you can type:  \n" ); 
	   printf( "    gzdoom +set fullscreen 1 -netmode 0 -join 192.168.1.10 -port 5030 -iwad doom2.wad -file eviternity.wad  \n" ); 
	   printf( "> If you run prboom-plus (opensuse leap 15.3), then you can type:  \n" ); 
           printf( "    prboom-plus -iwad doom2.wad -net 192.168.1.10 -p 5030 -file eviternity.wad \n" ); 
	   printf( " === ===============================\n" ); 
	   printf( " === \n" ); 
	   printf("%s", KNRM);
	   printf( " === \n" ); 
	   return 0; 
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "zdoom" ) ==  0 ) || ( strcmp( argv[2] , "lzdoom" ) ==  0 ) )
	{
	   printf( " 1.) First, type on first PI:    mconfig zdoom server aaliens.wad               \n" ); 
	   printf( " 2.) Secondly, on next PI:       mconfig zdoom client 192.168.1.22 aaliens.wad  \n" ); 
	   return 0; 
	}


        /// cmzdoom or zdoom on retropie 
	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "zdoom" ) ==  0 ) || ( strcmp( argv[1] , "lzdoom" ) ==  0 ) )
	if ( ( strcmp( argv[2] , "client" ) ==  0 ) 
	|| ( strcmp( argv[2] , "-wadnet" ) ==  0 ) 
	|| ( strcmp( argv[2] , "start" ) ==  0 ) 
	|| ( strcmp( argv[2] , "server" ) ==  0 ) )
	{
		printf( " === \n" ); 
	        if ( ( strcmp( argv[1] , "lzdoom" ) ==  0 )  &&  ( strcmp( argv[2] ,  "server" ) ==  0 ) )
		{
		    ncmdwith( " /opt/retropie/ports/lzdoom/lzdoom -netmode 0 -port 5030 -host 2 -iwad doom2.wad -file " ,  argv[ 3 ] );
		}

	        else if ( ( strcmp( argv[1] , "lzdoom" ) ==  0 )  &&  ( strcmp( argv[2] ,     "client" ) ==  0 ) )
		{
			//ncmdwith( " /opt/retropie/ports/lzdoom/lzdoom -netmode 0 -port 5030 -host 2 -iwad doom2.wad -file " ,  argv[ 3 ] );
			snprintf( charo , sizeof( charo ), "   /opt/retropie/ports/lzdoom/lzdoom +set fullscreen 1 -netmode 0 -join  \"%s\"  -port 5030 -iwad doom2.wad -file   \"%s\"   ",   argv[ 3 ] , argv[ 4 ] );
			printf( "command : %s\n", charo );
			nsystem( charo );
		}

	        else if ( strcmp( argv[2] ,     "server" ) ==  0 ) 
		{
		    printf( " SERVER STARTED ... \n" );  
		    printf( " SERVER STARTED ... \n" );  
		    ncmdwith( " /opt/retropie/ports/zdoom/zdoom -netmode 0 -port 5030 -host 2 -iwad doom2.wad -file " ,  argv[ 3 ] );
		    printf( " SERVER DONE.\n" );  
		}

		else if ( ( strcmp( argv[2] , "client" ) ==  0 ) || ( strcmp( argv[2] , "-wadnet" ) ==  0 ) )
		{
		        // ncmdwith( "  /opt/retropie/ports/zdoom/zdoom +set fullscreen 1 -netmode 0 -join  %s  -port 5030 -iwad doom2.wad -file   %s " ,  argv[ 3 ] );
			snprintf( charo , sizeof( charo ), "   /opt/retropie/ports/zdoom/zdoom +set fullscreen 1 -netmode 0 -join  \"%s\"  -port 5030 -iwad doom2.wad -file   \"%s\"   ",   argv[ 3 ] , argv[ 4 ] );
			printf( "command : %s\n", charo );
			nsystem( charo );
		}
		printf( " === \n" ); 
		return 0;
	}









        /// prboom-plus on x11
	if ( argc >= 3 )
	if ( strcmp( argv[1] , "boom" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "client" ) ==  0 ) || ( strcmp( argv[2] , "server" ) ==  0 ) )
	{
	        if ( strcmp( argv[2] ,     "server" ) ==  0 ) 
		{
		   // level 10: nsystem( "  /usr/games/prboom-plus-game-server  -l 10 -n -a   -N 2  " ); 
                   nsystem( "  /usr/games/prboom-plus-game-server  -l 1  " ); 
		}
		else if ( strcmp( argv[2] , "client" ) ==  0 ) 
		{
		    if ( fexist( "doom2.wad" ) == 1 ) 
			ncmdwith( "    /usr/games/prboom-plus -iwad   doom2.wad     -net  " ,  argv[ 3 ] );
		    else if ( fexist( "freedoom2.wad" ) == 1 ) 
			ncmdwith( "    /usr/games/prboom-plus -iwad  freedoom2.wad  -net  " ,  argv[ 3 ] );
		    else
		        printf( "IWAD not found.\n" ); 
		}
		return 0;
	}













	if ( argc == 2 )
	if ( strcmp( argv[1] , "rpi3-config" ) ==  0 )
	{
		printf( "## \n" );
		printf( "## \n" );
		printf( "## ===========\n" );
		printf( "## rpi4-config \n" );
		printf( "## ===========\n" );
		printf( "## \n" );
		printf( "dtparam=audio=on\n" );
		printf( "gpu_mem_256=128\n" );
		printf( "gpu_mem_512=256\n" );
		printf( "gpu_mem_1024=256\n" );
		printf( "overscan_scale=1\n" );
		printf( "disable_overscan=1\n" );
		printf( "disable_overscan=1\n" );
		printf( "hdmi_force_hotplug=1\n" );
		printf( "hdmi_group=2\n" );
		printf( "hdmi_mode=87\n" );
		printf( "hdmi_cvt=640 480 60 1 0 0 0\n" );
		printf( "framebuffer_width=640\n" );
		printf( "framebuffer_height=480\n" );
		printf( "disable_overscan=1\n" );
		printf( "## \n" );
		printf( "## \n" );
		return 0;
	}








	if ( argc == 2 )
	if ( strcmp( argv[1] , "rpi4-config" ) ==  0 )
	{
		printf( "## \n" );
		printf( "## \n" );
		printf( "## ===========\n" );
		printf( "## rpi4-config \n" );
		printf( "## ===========\n" );
		printf( "## \n" );
		printf( "dtparam=audio=on\n" );
		printf( "gpu_mem_256=128\n" );
		printf( "gpu_mem_512=256\n" );
		printf( "gpu_mem_1024=256\n" );
		printf( "overscan_scale=1\n" );
		printf( "disable_overscan=1\n" );
		printf( "disable_overscan=1\n" );
		printf( "hdmi_force_hotplug=1\n" );
		printf( "hdmi_group=2\n" );
		printf( "hdmi_mode=87\n" );
		printf( "hdmi_cvt=1024 768 60 1 0 0 0\n" );
		printf( "framebuffer_width=1024\n" );
		printf( "framebuffer_height=768\n" );
		printf( "disable_overscan=1\n" );
		printf( "## \n" );
		printf( "## \n" );
		return 0;
	}






	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "-workspace" ) ==  0 )   || ( strcmp( argv[1] , "--workspace" ) ==  0 ) ) 
	{
		snprintf( charo , sizeof( charo ), " wmctrl -s  %d ",   atoi( argv[ 2 ] ) );
		printf( "command : %s\n", charo );
		nsystem( charo );
		return 0; 
        }











	if ( argc == 3)
	if ( ( strcmp( argv[1] , "install" ) ==  0 ) || ( strcmp( argv[1] , "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] , "ncurses" ) ==  0 ) 
	|| ( strcmp( argv[2] , "ncurses-devel" ) ==  0 )  //fl <--- keep
	|| ( strcmp( argv[2] , "ncurse" ) ==  0 ) 
	|| ( strcmp( argv[2] , "curses" ) ==  0 ) 
	|| ( strcmp( argv[2] , "curse" ) ==  0 ) )
	{
		npkg( "   ncurses-dev " );

		if ( fexist( "/usr/bin/zypper" ) == 1 ) 
			npkg( "   ncurses-devel  " );
		else if ( fexist( "/usr/bin/apt-get" ) == 1 ) 
			npkg( "   ncurses-dev " );
		else 
			npkg( "   ncurses  " );
		return 0;
	}








	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,   "install" ) ==  0 ) || ( strcmp( argv[1] ,   "ins" ) ==  0 )  )
	if ( strcmp( argv[2] ,   "clang" ) ==  0 ) 
	{
		npkg( " gcc5-libs gcc5 " );
		npkg( " make " ); 
		if ( fexist( "/usr/bin/zypper" ) == 1 )  // oh, man, opensuse... 
		{
		  npkg( " gcc-g++ " ); 
		  npkg( " gcc-c++ " ); 
		}
		if ( fexist( "/usr/bin/apt-get" ) == 1 ) 
		  npkg( " clang gcc " );
		else 
		  npkg( " gcc " );

	        npkg( " gcc   " );
	        npkg( " clang " );
		npkg( " make " );
		return 0;
	}




	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,   "install" ) ==  0 ) || ( strcmp( argv[1] ,   "ins" ) ==  0 )  )
	if ( ( strcmp( argv[2] ,   "cc" ) ==  0 ) || ( strcmp( argv[2] ,   "gcc" ) ==  0 ) )
	{
		npkg( " gcc5-libs gcc5 " );
		npkg( " make " ); 
		if ( fexist( "/usr/bin/zypper" ) == 1 )  // oh, man, opensuse... 
		{
		  npkg( " gcc-g++ " ); 
		  npkg( " gcc-c++ " ); 
		}
		if ( fexist( "/usr/bin/apt-get" ) == 1 ) 
		  npkg( " clang gcc " );
		else 
		  npkg( " gcc " );

		npkg( " make " );
		return 0;
	}






	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,   "install" ) ==  0 ) || ( strcmp( argv[1] ,   "ins" ) ==  0 )  )
	if ( ( strcmp( argv[2] ,   "c++" ) ==  0 ) || ( strcmp( argv[2] ,   "g++" ) ==  0 ) )
	{
		npkg( " g++ " );
		/// if only UBUNTU: npkg( " clang " );
		if ( fexist( "/usr/bin/zypper" ) == 1 )  // oh, man, opensuse... 
		{
		  npkg( " gcc-g++ " ); 
		  npkg( " gcc-c++ " ); 
		}
		return 0;
	}








	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "install" ) ==  0 ) || ( strcmp( argv[1] , "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] , "fltk" ) ==  0 ) 
	|| ( strcmp( argv[2] , "fltk-dev" ) ==  0 )  ) 
	{
		npkg_update(); 

		npkg(   " make  " ); 
		npkg(   " gcc   " ); 
		npkg(   " g++ " ); 

		npkg(   " libfltk1.3-dev " ); 
		npkg(   " screen   " ); 
		npkg(   " xterm    " ); 
		//npkg(   " libfltk1.1 " ); 
		//npkg(   " libfltk1.1-dev " ); 

		if ( MYOS == 1 ) 
		{
			npkg(   " libfltk1.3-dev " ); 
			npkg(   " libfltk-dev " ); 

			if ( fexist( "/usr/bin/apt-get" ) == 1 )  // oh, man, opensuse... 
			{
			  npkg(   " gcc g++ " ); 
			  npkg(   " make " ); 
			}
			if ( fexist( "/usr/bin/zypper" ) == 1 )  // oh, man, opensuse... 
			{
			      npkg( " fltk " ); 
			      npkg( " fltk-devel  " ); 
			}
		}
	        else 
		{
		   npkg(   " fltk " ); 
		}

		npkg( " screen " ); 
		return 0;
	}











	if ( argc == 3)
	if ( ( strcmp( argv[1] ,   "install" ) ==  0 ) || ( strcmp( argv[1] ,   "ins" ) ==  0 )  )
	if ( ( strcmp( argv[2] ,   "cc" ) ==  0 ) || ( strcmp( argv[2] ,   "gcc" ) ==  0 ) )
	{
		npkg( " clang gcc " );
		npkg( " gcc5-libs gcc5 " );
		npkg( " make " ); 
		if ( MYOS == 1 ) 
		{
			nsystem( "  apk add clang gcc make libc-dev musl-dev " );   // alpine linux  , but path_max missing 
			npkg( " make gcc " ); 
		}
		return 0;
	}





	if ( argc == 3)
	if ( ( strcmp( argv[1] ,   "install" ) ==  0 ) || ( strcmp( argv[1] ,   "ins" ) ==  0 )  )
	if ( ( strcmp( argv[2] ,   "c++" ) ==  0 ) || ( strcmp( argv[2] ,   "g++" ) ==  0 ) )
	{
		npkg( " g++ " );
		npkg( " clang " );
		if ( fexist( "/usr/bin/zypper" ) == 1 )  // oh, man, opensuse... 
		{
		  npkg( " gcc-g++ " ); 
		  npkg( " gcc-c++ " ); 
		}
		return 0;
	}






















	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "q3a-gl1" ) ==  0 ) 
	|| ( strcmp( argv[1] , "ioquake3gl1" ) ==  0 ) 
	|| ( strcmp( argv[1] , "ioquake3-gl1" ) ==  0 ) )
	{
                nsystem( " cd ;  cd /usr/lib/ioquake3 ; ./ioquake3 +set cl_renderer opengl1 " ); 
		return 0; 
	}





        ///// 
	// this runs on rpi3b
        ///// 
	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "q3a" ) ==  0 ) 
	|| ( strcmp( argv[1] , "quake3" ) ==  0 )
	|| ( strcmp( argv[1] , "io3" ) ==  0 ) 
	|| ( strcmp( argv[1] , "ioquake3" ) ==  0 ) )
	{

                if ( fexist( "/opt/retropie/ports/ioquake3/ioquake3.arm" ) == 1 ) 
                  system( "  cd ;  /opt/retropie/ports/ioquake3/ioquake3.arm  +set cl_renderer opengl1 " );  

		else if ( fexist( "/usr/lib/ioquake3/ioquake3" ) == 1 ) 
		    nsystem( "/usr/lib/ioquake3/ioquake3" ); 

		else if ( fexist( "/usr/lib/ioquake3/ioquake3" ) == 1 ) 
                  nsystem( " cd ; cd /usr/lib/ioquake3 ;   screen -d -m  ./ioquake3 +set cl_renderer opengl1  " ); 

		else
		   nsystem( " /opt/retropie/ports/quake3/ioquake3.arm " ); 
		return 0;
	}


	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "q3a-gl1" ) ==  0 ) || ( strcmp( argv[1] , "ioquake3-gl1" ) ==  0 ) )
	{
                nsystem( " cd ;  cd /usr/lib/ioquake3 ; ./ioquake3 +set cl_renderer opengl1 " ); 
		return 0; 
	}







        ///// 
	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "get" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "maps" ) ==  0 ) || ( strcmp( argv[2] , "q3a-maps" ) ==  0 ) )
	{
	      nsystem( " mkdir ~/.q3a " );   
	      printf( "HOME: %s\n", getenv( "HOME" ));
	      chdir( getenv( "HOME") );
	      chdir( ".q3a" ); 

	      printf( "Current path: %s\n", getcwd( cmdi , PATH_MAX ) );
              fetch_file_ftp( "http://files.retropie.org.uk/archives/Q3DemoPaks.zip" ); 
	      return 0; 
        }






	// x11vnc -auth guess -forever -loop -noxdamage -repeat -rfbauth /etc/x11vnc.pwd -rfbport 3029 -shared
	if ( argc == 2)
	if ( strcmp( argv[1] , "x11vnc" ) ==  0 ) 
	{
		//nsystem( " while : ; do     export DISPLAY=:0 ; x11vnc -shared ; echo x11vnc  ; sleep 3 ; done  " );
		if ( MYOS == 1 )
		if ( fexist( "/usr/bin/x11vnc" ) == 0 ) 
		{
			///npkg( " x11vnc " );
			npkg( " x11vnc  "  ); 
		}
		nsystem( " while : ; do  echo x11vnc ;  x11vnc  -shared  ; echo x11vnc  ; sleep 3 ; done  " );
		return 0;
	}



	if ( argc == 2)
	if ( strcmp( argv[1] , "xx11vnc" ) ==  0 ) 
	{
		nsystem( "  export DISPLAY=:0 ; echo ;  while : ; do x11vnc -shared ; echo sleep-x11vnc  ; echo port-5900-default ; sleep 3 ; done  " );
		return 0;
	}










	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "moresongs" ) ==  0 ) 
	{
                procedure_audio_player(  "https://milkytracker.org/songs/MilkyTracker-MoreSongs.pls" ); 
	        return 0; 
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "--firmware-intel" ) ==  0 ) || ( strcmp( argv[1] , "intel" ) ==  0 ) )
	{
                printf( "> Firmware for wifi on i5:  firmware-iwlwifi_20190114-2_all.deb     \n" ); 
		fetch_file_ftp( "http://ftp.uk.debian.org/debian/pool/non-free/f/firmware-nonfree/firmware-iwlwifi_20190114-2_all.deb"  ); 
		return 0; 
        }









   /*
	if ( argc == 3)
	if ( strcmp( argv[1] , "wget" ) ==  0 ) 
	if ( strcmp( argv[2] , "fltk" ) ==  0 ) 
	{
				if ( fexist( "/usr/bin/fetch" ) == 1 ) 
				{
					nsystem( " fetch -R  https://www.fltk.org/pub/fltk/1.3.5/fltk-1.3.5-source.tar.gz  " );
				}
				else
				{
					nsystem( " wget -c  https://www.fltk.org/pub/fltk/1.3.5/fltk-1.3.5-source.tar.gz  " );
					nsystem( " tar xvpfz fltk-1.3.5-source.tar.gz  " );
				}

				printf( " \n" );
	*/

	if ( argc == 3)
	if ( strcmp( argv[1] , "--compile" ) ==  0 ) 
	if ( strcmp( argv[2] , "fltk" ) ==  0 ) 
	{
		fetch_file_ftp( "https://www.fltk.org/pub/fltk/1.3.5/fltk-1.3.5-source.tar.gz" );
		nsystem( " tar xvpfz fltk-1.3.5-source.tar.gz  " );
		nsystem( " cd fltk-1.3.5-source ; ls " );
		return 0; 
	}









	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "--compile-fltk" ) ==  0 ) || ( strcmp( argv[1] , "--gcc-fltk" ) ==  0 ) )
	// 2 ... item 
	// from location to source/bin/  !!  
	{
		if ( argc >= 2)
		{
			for( i = 1 ; i < argc ; i++) 
			{
				if ( i == 1 )
				{
				}
				else if ( i >= 2 )
				{
					nsystem( " mkdir source ; mkdir source/bin " );   
					strncpy( targetfile, fbasenoext( argv[ i ] ) , PATH_MAX );
					if ( MYOS == 1 )
					{
						printf( "=> linux\n" ); 
						// see if debian 
						snprintf(  charo , sizeof( charo ),  " c++   \"%s\"  -lX11 -lfltk -o   source/bin/\"%s\"  " ,  argv[ i ] ,  fbasename( targetfile ) ); 
					}
					else if ( os_bsd_type == 1 ) //freebsd  
					{
						printf( "=> Unix/BSD FreeBSD \n" ); 
						snprintf(  charo , sizeof( charo ),  " c++ -DVWM  -lm   -I/usr/local/include/ -I/usr/local/include -I /usr/local/include/    -L/usr/local/lib  -L/usr/local/lib -lX11 -lfltk   -I /usr/local/include/   \"%s\" -o   source/bin/\"%s\"  " ,  argv[ i ] ,  fbasename( targetfile ) ); 
					}
					else if ( os_bsd_type == 3 ) //netbsd  
					{
						printf( "=> Unix/BSD NetBSD \n" ); 
	                                        // pwd ; c++ -lm   -I"/usr/X11R7/include/" -I"/usr/pkg/include" -I /usr/pkg/include/    -L"/usr/pkg/lib"  -L/usr/X11R7/lib -lX11 -I /usr/X11R7/include/   -lfltk   source/fltk/kicker.cxx          -o   source/bin/kicker     
						snprintf(  charo , sizeof( charo ),  " c++ -lm   -I/usr/X11R7/include/ -I/usr/pkg/include -I /usr/pkg/include/    -L/usr/pkg/lib  -L/usr/X11R7/lib -lX11 -I /usr/X11R7/include/   -lfltk   \"%s\" -o  source/bin/\"%s\"  " ,  argv[ i ] ,  fbasename( targetfile ) ); 
					}
					printf( "Command : %s\n" , charo ); 
					nsystem( charo ); 
				}
			}
		}
		return 0; 
	}










	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "xboxmux" ) ==  0 )   
	{
	       //nsystem( "  export DISPLAY=:0 ;  xterm   -fa 'Liberation Mono'   -bg black -fg yellow -fs 19  " );
               nsystem( " export DISPLAY=:0 ; xterm     -fa 'Liberation Mono'   -bg black -fg yellow -fs 19   -geometry 310x80+0+0  -e tmux attach "); 
	       return 0;
	}


	if ( argc == 2 )
        if ( ( strcmp( argv[1] ,   "20g" ) ==  0 ) || ( strcmp( argv[3] ,   "20G" ) ==  0 ))
	{
	       nsystem( " qemu-img create -f qcow2 vdisk.qcow2  20G " ); 
	       return 0;
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "base" ) ==  0 ) 
	{
	   if ( MYOS == 1 ) 
	      npkg( " subversion " ); 
	   else if ( os_bsd_type == 1 ) 
		   npkg( "  subversion " );
	   else
		   npkg( "  subversion-base " );
	   npkg( " screen " ); 
	   return 0; 
	}







	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,   "install" ) ==  0 ) || ( strcmp( argv[1] ,   "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] ,   "partclone" ) ==  0 ) || ( strcmp( argv[2] ,   "partimage" ) ==  0 ) )
	{
		//npkg_update(); 
		npkg( " partclone ");  

		//npkg_update(); 
		//npkg( " partclone  " );
		npkg( " ntfs-3g    " ); 
		npkg( " ntfsprogs  " ); 
		npkg( " dosfstools  "  );   // for fdisk and fs, mount,...
		return 0;
	}




	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "vbox" ) ==  0 ) 
	if ( strcmp( argv[2] , "sda" ) ==  0  ) 
	{
	     //printf( "\n" ); 
	     //printf( " VBoxManage clonehd --format VDI server1-disk1.vmdk   server2-disk1.vdi \n" ); 
	     nsystem( "   VBoxManage  internalcommands createrawvmdk -filename          disksda.vmdk -rawdisk        /dev/sda  " );
	     nsystem( " chmod 777 /dev/sda* " ); 
	     nsystem( " chmod 777 disksda.vmdk  "); 
	     return 0; 
	}



        ////// MAIN with --clone   !!! 
        /// new windows 8 on partition 4, in order to backup the nokia phone kaios
	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,   "partclone.ntfs" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "-partclone.ntfs" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "--partclone.ntfs" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "--partclone-ntfs" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "--partclone-new-ntfs" ) ==  0 ) )
	if ( ( strcmp( argv[2] ,   "sda" ) ==  0 ) 
	|| ( strcmp( argv[2] ,   "sda1" ) ==  0 ) 
	|| ( strcmp( argv[2] ,   "sda2" ) ==  0 ) 
	|| ( strcmp( argv[2] ,   "sda3" ) ==  0 ) 
	|| ( strcmp( argv[2] ,   "sda4" ) ==  0 ) )
	{
		printf( " =============================================== \n" ); 
		printf( " \n" ); 
		printf( " TO RESTORE Windows:  partclone.ntfs -r -d -s partclone.sdb2.ntfs.img -o /dev/sda2  " ); 
		printf( " TO RESTORE Windows,  Force:  partclone.ntfs -r -d -C -F  -s partclone.sdb2.ntfs.img -o /dev/sda2  " ); 
		printf( " \n" ); 
		printf( " =============================================== \n" ); 

		//npkg_update(); 

		npkg( " partclone  " );
		npkg( " ntfs-3g    " ); 
		npkg( " ntfsprogs  " ); 

		printf( "== PARTCLONE NTFS part sda3 or sda4 for ntfs ==\n" );
		printf("Time: %d\n", (int)time(NULL));


		snprintf( charo , sizeof( charo ), " fdisk -l > image-mbr-partclone-ntfs-%d.fdisk.log  ", (int)time(NULL));
		nsystem( charo ); 

		snprintf( charo , sizeof( charo ), " dd if=/dev/sda of=image-mbr-partclone-ntfs-%d.mbr   count=9999  ", (int)time(NULL));
		nsystem( charo ); 


	        if ( strcmp( argv[1] ,   "--partclone-new-ntfs" ) ==  0 ) 
		{
			snprintf( charo , sizeof( charo ), " partclone.ntfs  -c -d -s /dev/%s  -o partclone.%s.new.ntfs.img  ", argv[ 2 ], argv[ 2 ] );
			nsystem(  charo ); 
		}

		else if ( strcmp( argv[2] ,   "sda" ) ==  0 ) 
		{
			nsystem( "  partclone.ntfs  -c -d -s /dev/sda  -o partclone.sda.ntfs.img  " );
		}

		else if ( strcmp( argv[2] ,   "sda1" ) ==  0 ) 
		{
			nsystem( "  partclone.ntfs  -c -d -s /dev/sda1  -o partclone.sda1.ntfs.img  " );
		}

		else if ( strcmp( argv[2] ,   "sda2" ) ==  0 ) 
		{
			nsystem( "  partclone.ntfs  -c -d -s /dev/sda2  -o partclone.sda2.ntfs.img  " );
		}

		else if ( strcmp( argv[2] ,   "sda3" ) ==  0 ) 
		{
			nsystem( "  partclone.ntfs  -c -d -s /dev/sda3  -o partclone.sda3.ntfs.img  " );
		}
		else if ( strcmp( argv[2] ,   "sda4" ) ==  0 ) 
		{
			nsystem( "  partclone.ntfs  -c -d -s /dev/sda4  -o partclone.sda4.ntfs.img  " );
		}

		nsystem( "  sync  " );
		nsystem( "  fdisk -l  >> partclone.fdisk.log " ); 
		nsystem( "  md5sum *  >> partclone.fdisk.log " ); 
		printf("%d\n", (int)time(NULL));
		printf( "\n" );

		nsystem( " mkdir /media " ); 
		return 0;
	}





	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "-ssh" ) ==  0 ) || ( strcmp( argv[1] , "ssh" ) ==  0 ) )
	{
			printf( " -- FOR SSH -- \n" ); 
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else
					{
						printf( "%d/%d: %s\n", i, argc-1 ,  argv[ i ] );   
						strncpy( charo , " ssh -C 192.168.1." , PATH_MAX ); 
						strncat( charo , argv[ i ]  , PATH_MAX - strlen( charo ) -1 );
						strncat( charo , " "  , PATH_MAX - strlen( charo ) -1 );
						nsystem( charo ); 
					}
				}
			}
			return 0;
	}














	if ( argc >= 2 )
	if ( strcmp( argv[1] , "ssh0" ) ==  0 ) 
	{
		for( i = 2 ; i < argc ; i++) 
		{
			printf( " ============== \n "); 
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
			snprintf( charo , sizeof( charo ), "      ssh -X -C         pi@192.168.0.%s  ",  argv[ i ] );
			printf( "<CMD: %s>\n", charo );
			nsystem( charo );
		}
		printf( " ============== \n "); 
		return 0; 
	}














	if ( argc == 2 )
	if ( strcmp( argv[1] , "vol+" ) ==  0 ) 
	{
		system( " mixerctl -w outputs.master+=1 " ); 
		return 0;
	}
	if ( argc == 2 )
	if ( strcmp( argv[1] , "vol-" ) ==  0 ) 
	{
		system( " mixerctl -w outputs.master-=1 " ); 
		return 0;
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] , "edit" ) ==  0 ) 
	if ( strcmp( argv[2] , "repository" ) ==  0 ) 
	{
              nsystem( " vim /usr/pkg/etc/pkgin/repositories.conf " );   
	      return 0; 
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] , "panclock" ) ==  0 ) 
	{
	     // 800 x 480 
		nsystem( " xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0  ; sleep 1 ;  env  LD_LIBRARY_PATH='/usr/X11R7/lib:/usr/pkg/lib'  TZ=Europe/Amsterdam dclock -bg Black -bg Black -fg red -led_off Black -nofade   -geometry 790x470+0+0  " ); 
		return 0;
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "dclock" ) ==  0 ) || ( strcmp( argv[1] ,   "--dclock" ) ==  0 ) )
	{
		if ( fexist( "/usr/bin/dclock" ) == 1 ) 
			nsystem( "   env  LD_LIBRARY_PATH='/usr/X11R7/lib:/usr/pkg/lib'  TZ=Europe/Amsterdam dclock -bg Black -bg Black -fg red -led_off Black -nofade   -geometry 320x240+0+0  " ); 
		else 
			nsystem( "   TZ=Europe/Amsterdam flclock " ); 

		return 0;
	}








	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,   "install" ) ==  0 ) || ( strcmp( argv[1] ,   "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] ,   "writer" ) ==  0 ) || ( strcmp( argv[2] ,     "word" ) ==  0 ) )
	{
			        npkg_update();
				npkgmin( " libreoffice-writer  "); 
				return 0;
	}


	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,   "install" ) ==  0 ) || ( strcmp( argv[1] ,   "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] ,     "excel" ) ==  0 ) || ( strcmp( argv[2] ,   "libreoffice-calc" ) ==  0 ) ) 
	{
                // libreoffice - office productivity suite (metapackage)
		// libreoffice-calc - office productivity suite -- spreadsheet
		npkg_update();
		npkgmin( " libreoffice-calc " ); 
		return 0; 
	}



	if ( argc == 3)
	if ( ( strcmp( argv[1] , "install" ) ==  0 ) || ( strcmp( argv[1] , "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] ,   "vnc" ) ==  0 ) 
	|| ( strcmp( argv[2] ,   "vnc+" ) ==  0 ) 
	|| ( strcmp( argv[2] ,   "vnc" ) ==  0 ) || ( strcmp( argv[2] ,   "xvnc" ) ==  0 ) )
	{
				//npkg( " x11vnc  "  ); 

				npkg( " x11vnc  "  ); 
				npkg( " xtightvncviewer  "  ); 

				if ( MYOS == 1 )
				{
					///npkg( " x11vnc " );
					npkg( " xtightvncviewer  "  ); 
				        // npkg( " xtightvncviewer  "  ); 
				}

				else if ( os_bsd_type == 1 )
				{
				    npkg( " tigervnc-viewer " ); 
				    npkg( " tightvnc  " ); 
				    npkg( " tightvncviewer  " ); 
				}

				else if ( os_bsd_type == 3 )
				{
					if ( fexist( "/etc/wscons.conf" ) == 1 )     // netbsd 
					{
						npkg( " tightvncviewer   "  ); 
						npkg( " vncviewer   "  ); 
					}
					else
						// openbsd
						//npkg( " x11vnc " );
						npkg( " ssvnc-viewer  "  ); 
				}

	                        if ( strcmp( argv[2] ,   "vnc+" ) ==  0 ) 
				   npkg( " x11vnc xtightvncviewer "); 

		                if ( fexist( "/usr/bin/zypper" ) == 1 ) 
				{
				   npkg( " tigervnc "); 
				   npkg( " x11vnc  "); 
				   npkg( " xtightvncviewer  "); 
				}
				return 0;
	}




	if ( argc == 3  )
	if ( ( strcmp( argv[1] , "install" ) ==  0 ) || ( strcmp( argv[1] , "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] ,   "vnc" ) ==  0 ) 
	|| ( strcmp( argv[2] ,   "vnc+" ) ==  0 ) 
	|| ( strcmp( argv[2] ,   "vnc++" ) ==  0 ) 
	|| ( strcmp( argv[2] ,   "vnc" ) ==  0 ) || ( strcmp( argv[2] ,   "xvnc" ) ==  0 ) )
	{
		//npkg( " x11vnc  "  ); 
		npkg( " x11vnc  "  );
		npkg( " xtightvncviewer  "  );   
		return 0;
	}








	if ( argc == 3 )
	if ( strcmp( argv[1] , "list" ) ==  0 ) 
	if ( strcmp( argv[2] , "games" ) ==  0 ) 
	{
	        printf( " ================    \n" ); 
	        printf( "  List of Games      \n" ); 
	        printf( " ================    \n" ); 
	        printf( "   (based on OpenSuse)  \n" ); 
	        printf( " > Racing:              \n" ); 
	        printf( "     supertuxkart    \n" ); 
	        printf( " > Emulators:        \n" ); 
	        printf( "     mednafen     \n" ); 
	        printf( "     dolphin-emu  \n" ); 
	        printf( " > Strategy:      \n" ); 
	        printf( "     0ad \n" ); 
		return 0;
	}










        // build is netbsd 
	// maybe on linux ? 
	if ( argc == 2 )
	if ( strcmp( argv[1] , "autocompile" ) ==  0 ) 
	{
		if ( MYOS == 1 ) npkg( " xutils-dev " );  // xmkmf  <- debian  
		nsystem( " mconfig install g++ " ); 
		nsystem( "  xmkmf ;  aclocal ; autoconf ; automake " ); 
		nsystem( " ./configure ; make  " ); 
		printf( " > completed.\n" );
		return 0;
	}




	if ( argc == 3 )
	if ( strcmp( argv[1] ,  "help"       ) ==  0 ) 
	if ( strcmp( argv[2] ,  "irc"  ) ==  0 ) 
	{
                  printf( " On DEVUAN/DEBIAN: apt-get install charybdis \n" ); 
		  return 0; 
	}



	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,  "irc" ) ==  0 ) || ( strcmp( argv[1] ,  "ircnow"       ) ==  0 ) )
	{
                  nsystem( " mconfig --cr 'https://kiwiirc.com/nextclient/irc.ircnow.org/?channels=#ircnow' " ); 
		  return 0; 
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] , "--ip-api" ) ==  0 ) 
	{
		printf( "HOME: %s\n", getenv( "HOME" ));
		procedure_webbrowser_console( "https://api.ipify.org" );
		return 0;
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] , "--check-system" ) ==  0 ) 
	{
	     printf( " == CHECK == \n" ); 
             printf( " > /usr/local/bin/mconfig, file check: (%d)\n" , fexist( "/usr/local/bin/mconfig" ) ); 
             printf( " > /usr/local/bin/mconfig, file check: (%d)\n" , fexist( "/usr/local/bin/mconfig" ) ); 
             printf( " > /usr/bin/links, file check: (%d)\n" , fexist( "/usr/bin/links" ) ); 
             printf( " > /usr/local/bin/links, file check: (%d)\n" , fexist( "/usr/local/bin/links" ) ); 
             printf( " > /usr/pkg/bin/wget, file check: (%d)\n" , fexist( "/usr/pkg/bin/wget" ) ); 
             printf( " > /usr/bin/subversion, file check: (%d)\n" , fexist( "/usr/bin/subversion" ) ); 
	     return 0; 
        }




	if ( argc == 3 )
	if ( strcmp( argv[1] , "confs" ) ==  0 ) 
	{
		snprintf( charo , sizeof( charo ), " cd ; mkdir sshfs ; fusermount -u sshfs ; sshfs 192.168.10.%s:/  sshfs ",  argv[ 2 ] );
		nsystem(  charo ); 
		return 0; 
        }



	if ( argc == 2)
	if ( ( strcmp( argv[1] , "xx11vnc" ) ==  0 ) || ( strcmp( argv[1] , "xx" ) ==  0 ) )
	{
		nsystem( "  export DISPLAY=:0 ; echo ;   while : ; do x11vnc -shared ; echo sleep-x11vnc  ; echo port-5900-default ; sleep 3 ; done  " );
		return 0;
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] , "winlock" ) ==  0 ) 
	{
		printf( "HOME: %s\n", getenv( "HOME" ));
		chdir( getenv( "HOME") );
		//nsystem( "   cd ; sleep 1 ; i3lock -i .wallpaper.jpg "); 
		nsystem( "   cd ; sleep 1 ; i3lock -i .wallpaper.png ");   // should be png 
		return 0;
	}




	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "--zypper" ) ==  0 ) || ( strcmp( argv[1] , "-zypper" ) ==  0 ) )
	if ( strcmp( argv[2] , "teams" ) ==  0 ) 
	{
	       printf( " > System OpenSUSE \n" ); 
               nsystem( " cd ; pwd ; rpm --import https://packages.microsoft.com/keys/microsoft.asc ; zypper ar https://packages.microsoft.com/yumrepos/ms-teams/ ms-teams ; zypper refresh ; zypper install teams " ); 
	       return 0; 
         }







	if ( argc == 3 )
	if ( strcmp( argv[1] , "create" ) ==  0 )  
	if ( strcmp( argv[2] , "tmuxrc" ) ==  0 )  
	{
		nsystem( "echo 'bind-key -n F5 previous-window' > .tmuxrc.conf   "); 
		nsystem( "echo 'bind-key -n F6 next-window'     >> .tmuxrc.conf  "); 
		nsystem( "echo 'bind-key -n F7 new-window'      >> .tmuxrc.conf  "); 
		return 0;
	}





        // vbox 5. 2. 44 for win32 bits 
	// keep it for remote assistance   
	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,  "runme" ) ==  0 ) 
	|| ( strcmp( argv[1] ,    "--runme" ) ==  0 ) 
	|| ( strcmp( argv[1] ,    "-runme" ) ==  0 ) 
	|| ( strcmp( argv[1] ,    "runme.sh" ) ==  0 ) )
	{
	        if ( fexist( "/opt/runme.sh" ) == 1 ) 
	           nsystem( " cd ; sh /opt/runme.sh " );    
		else 
	           nsystem( " cd ; sh runme.sh " );    
		return 0;
	}
	

	if ( argc == 2 )
	if ( strcmp( argv[1] , "bgr" ) ==  0 ) 
	{
		nsystem(  "  xtightvncviewer -bgr233 localhost:0 "  );
		return 0;
	}




	///////////////////////////////////////////////
	if ( argc == 2 )
	if ( strcmp( argv[1] , "blue" ) ==  0 ) 
	{
			nsystem( " xsetroot      -solid '#3A6EA5' " );
			nsystem( " xsetrootfork  -solid '#3A6EA5' " );
			return 0;
	}





	if ( argc == 2)
	if ( ( strcmp( argv[1] , "slideshow" ) ==  0 ) || ( strcmp( argv[1] , "slideshow" ) ==  0 ) )
	{
	        if ( fexist( "/usr/bin/gpicview" ) == 1 ) 
		  nsystem( "  gpicview --slideshow * " );
		else 
	 	  nsystem( "  feh -F -D 2 --cycle-once *.jpg *.png  *.JPG " );
		return 0;
	}






	if ( argc >= 2 )
	if ( ( strcmp( argv[1] , "--rotate" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-rotate" ) ==  0 ) )
	{
		printf( "\n" );
		strncpy( cmdi , "  " , PATH_MAX );
		for( i = 2 ; i < argc ; i++) 
		{
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
		        snprintf( charo , sizeof( charo ), " convert  \"%s\" -rotate 180 \"%s%s\"   ", argv[ i ] , fbasenoext( argv[ i ]  ), "-rotated.jpg"  ); 
			nsystem( charo );  
		}
		return 0; 
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] , "ncftp" ) ==  0 ) 
	{
            //fetch_file_ftp( "ftp://ftp.ncftp.com/ncftp/ncftp-3.2.6-src.tar.gz" ); 
	    //nsystem( "  tar xvpfz ncftp-3.2.6-src.tar.gz ;  cd ncftp-3.2.6 ; ./configure ; make ; cp bin/ncftp /usr/local/bin/ncftp  ;  cp bin/ncftpput  /usr/local/bin/ncftpput ; cp bin/ncftpget  /usr/local/bin/ncftpget  " ); 
	    return 0; 
        }




	if ( argc >= 2 )
	if ( strcmp( argv[1] , "--batcmd" ) ==  0 ) 
	{
		for( i = 2 ; i < argc ; i++) 
		{
		        printf( " ============== \n "); 
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
                        if ( i >= 3 ) 
			{
				strncpy( cmdi , " " , PATH_MAX );
				strncat( cmdi , argv[ 2 ] , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "   " , PATH_MAX - strlen( cmdi ) -1 );
				nsystem( cmdi );
			}
		}
		printf( " ============== \n "); 
		return 0; 
	}





	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "firmware" ) ==  0 ) 
	{
		fetch_file_ftp( "http://ftp.debian.org/debian/pool/non-free/f/firmware-nonfree/firmware-nonfree_20161130.orig.tar.xz" ); 
		return 0; 
	}





	/// freebsd 
	if ( argc >= 2 )
	if ( ( strcmp( argv[1] , "--wavplay" ) ==  0  ) || ( strcmp( argv[1] , "-wavplay" ) ==  0  ) )
	{
			strncpy( cmdi , "  " , PATH_MAX );
			for( i = 2 ; i < argc ; i++) 
			{
				printf( "=> %d/%d %s \n", i , argc , argv[ i ] );

				strncpy( cmdi , " wavplay " , PATH_MAX );  /// freebsd 
				strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				nsystem( cmdi );
			}
			printf( " ============== \n "); 
			return 0; 
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] , "rpi4" ) ==  0 ) 
	{
			printf( " ============== \n "); 
                        fetch_file_ftp( "https://files.retropie.org.uk/images/weekly/retropie-buster-4.8-rpi4_400.img.gz" ); 
			printf( " ============== \n "); 
			return 0; 
	}





	///////////////////////////////////////////////
	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "debug" ) ==  0 ) || ( strcmp( argv[2] , "gdb" ) ==  0 ) )
	{
		printf( " ================= \n"); 
		printf( "  g++ -Wall -std=c++14  -O2 -o a.out   prog.cpp \n");
		printf( "  Then add -ggdb3 and run it in gdb\n" );
		printf( "  g++ -Wall -ggdb3 -std=c++14  -O2 -o a.out   prog.cpp \n");
		printf( " ================= \n"); 
		return 0;
	}


	///////////////////////////////////////////////
	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,   "install" ) ==  0 ) || ( strcmp( argv[1] ,   "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] , "debug" ) ==  0 ) || ( strcmp( argv[2] ,   "gdb" ) ==  0 ) )
	{
		npkg( " gcc gdb " ); 
		return 0;
	}





	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "chtheme" ) ==  0 ) 
	{
		nsystem( " gtk-chtheme " );
		return 0; 
        }







	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "clock" ) ==  0 ) 
	{
		if ( fexist( "/usr/bin/dclock" ) == 1 ) 
			nsystem( "  env  LD_LIBRARY_PATH='/usr/X11R7/lib:/usr/pkg/lib'  TZ=Europe/Amsterdam dclock -bg Black -bg Black -fg red -led_off Black -nofade   -geometry 320x240+0+0  " ); 
		else 
			nsystem( "   TZ=Europe/Amsterdam flclock " ); 
		return 0;
	}







	// https://packages.debian.org/de/python-numpy 
	// https://packages.debian.org/search?keywords=python-matplotlib
	if ( argc == 3 )
	if ( strcmp( argv[1] ,     "install" ) ==  0 ) 
	if ( ( strcmp( argv[2] ,   "numpy" ) ==  0 ) || ( strcmp( argv[2] ,   "matplot" ) ==  0 ) )
	{
	         npkg( " python " ); 
	         npkg( " python-numpy " ); 
	         npkg( " python-matplotlib " ); 
	         npkg( " python-is-python3 " );
	         npkg( " python3 " ); 
	         npkg( " python3-numpy " ); 
	         npkg( " python3-matplotlib " ); 
                 // ubuntu 20.10  ii  python-is-python3                          3.9.2-2                                  all          symlinks /usr/bin/python to python3
		 return 0; 
	}








	if ( argc == 2 )                              
	if ( strcmp( argv[1] , "notepad" ) ==  0 ) 
	{
		// gnome-text-editor 
		if ( fexist( "/usr/local/bin/flnotepad"  ) == 1 ) 
		   nsystem( " /usr/local/bin/flnotepad " ); 

		else if ( fexist( "/usr/bin/leafpad"  ) == 1 ) 
		   nsystem( " leafpad " ); 

		else if ( fexist( "/usr/bin/gnome-text-editor"  ) == 1 ) 
		  nsystem( " gnome-text-editor " ); 

		else if ( fexist( "/usr/bin/geany"  ) == 1 ) 
		   nsystem( " geany " ); 
		else
		  nsystem( " notepad " ); 
		return 0;
	}









	if ( argc == 2 )
	if ( strcmp( argv[1] , "gdm" ) ==  0 ) 
	{
	        nsystem( " /etc/init.d/slim stop " ); 
	        nsystem( " /etc/init.d/slim stop " ); 
	        nsystem( " /etc/init.d/gdm  start " ); 
	        nsystem( " /etc/init.d/gdm3 start " ); 
		return 0;
	}

	if ( argc == 2 )
	if ( strcmp( argv[1] , "slim" ) ==  0 ) 
	{
	        nsystem( " /etc/init.d/gdm  stop " ); 
	        nsystem( " /etc/init.d/gdm3 stop " ); 
	        nsystem( " /etc/init.d/slim start " ); 
		return 0;
	}






	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,   "mount" ) ==  0 ) || ( strcmp( argv[1] ,   "attach" ) ==  0 ) ) 
	if ( ( strcmp( argv[2] ,   "nand" ) ==  0 )  || ( strcmp( argv[2] ,   "ubifs" ) ==  0 ) )
	{
                nsystem( "  mkdir /mnt/nand ; ubiattach /dev/ubi_ctrl -m 4 ; mount -t ubifs ubi0:rootfs /mnt/nand  " ); 
		return 0;
	}




	if ( argc == 3 )
	if ( strcmp( argv[1] , "install" ) ==  0 ) 
	if ( strcmp( argv[2] , "ubiattach" ) ==  0 ) 
	{
	        //npkg( " ubiattach "); 
	        npkg( " mtd-utils "); 
		return 0;
	}





	if ( argc == 2 )
	if ( strcmp( argv[1] , "ubifs" ) ==  0 ) 
	{
                nsystem( "  mkdir /mnt/nand ; ubiattach /dev/ubi_ctrl -m 4 ; mount -t ubifs ubi0:rootfs /mnt/nand  " ); 
		return 0;
	}




	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "ins" ) ==  0 ) || ( strcmp( argv[1] , "install" ) ==  0 ) )
	if ( strcmp( argv[2] ,  "kodak" ) ==  0 ) 
	{
		npkg_update(); 
		npkg( "   mpg123 " );    
		npkg( " mplayer " ); 
		npkg( " feh " ); 
		npkg( " rox-filer " ); 
	        return 0; 
	}





	if ( argc == 2 )
	if ( strcmp( argv[1] ,     "termbin" ) ==  0 ) 
	{
		nsystem( "  xclip -o | nc termbin.com 9999 " ); 
		return 0; 
        }



	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "gnu" ) ==  0 ) 
	{
		nsystem( " ncftp ftp.gnu.org " ); 
		return 0; 
        }



	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "media" ) ==  0 ) 
	{
			printf( "\n" ); 
			if (  os_bsd_type == 1 )  
			{
				printf( "=== LIST DRIVE  === \n" ); 
				printf( "> DRIVE\n" );
				system( "  sysctl -a | grep kern.disk "); 
				system( "  gpart show | grep da  "); 
			}
			printf( "=== LIST MEDIA DIRECTORY === \n" ); 
			printf( "> MEDIAs\n" );
			system( "  mount  | grep media "); 
			printf("> Filesystem      Size  Used Avail Use/pct. Mounted on\n"); 
			system( "  df -h  | grep media "); 
			printf( "============================ \n" ); 
			return 0;
	}







	if ( argc == 3 + 1  )
	if ( strcmp( argv[1] ,   "findtxt" ) ==  0 ) 
	// 2 
	// 3 
	// +1 
	{
		snprintf( cmdi , sizeof( cmdi ), "  filegrep2 \"%s\" \"%s\"  *.txt  ", argv[ 2 ] , argv[ 3 ] ); 
		nsystem( cmdi );   
		return 0; 
	}




	if ( argc >= 2 )
	if ( strcmp( argv[1] , "--notepad" ) ==  0 )
	{
		strncpy( cmdi , "  " , PATH_MAX );
		for( i = 2 ; i < argc ; i++) 
		{
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
			if ( fexist( "/usr/bin/leafpad" ) == 1 ) 
			   ncmdwith( "   leafpad  " ,  argv[ i ]  );
			else
			   ncmdwith( "   flnotepad  " ,  argv[ i ]  );
		}
		printf( " ============== \n "); 
		return 0; 
	}




	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "connect" ) ==  0 ) || ( strcmp( argv[1] , "con" ) ==  0 ) )
	{
	       strncpy( cmdi , " " , PATH_MAX );
	       snprintf( charo , sizeof( charo ), "  ssh -C 192.168.%s.%s  ", "10" ,  argv[ 2 ] );
	       strncat( cmdi , charo , PATH_MAX - strlen( cmdi ) -1 );
	       nsystem( cmdi );
	       return 0; 
	}

	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "xconnect" ) ==  0 ) || ( strcmp( argv[1] , "xcon" ) ==  0 ) )
	{
	       strncpy( cmdi , " " , PATH_MAX );
	       snprintf( charo , sizeof( charo ), "  ssh -C -X 192.168.%s.%s  ", "10" ,  argv[ 2 ] );
	       strncat( cmdi , charo , PATH_MAX - strlen( cmdi ) -1 );
	       nsystem( cmdi );
	       return 0; 
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] , "help"    ) ==  0 ) 
	if ( strcmp( argv[2] , "port" ) ==  0 ) 
	{
		nsystem( " netstat -tpln | egrep '(Proto|ssh)' " ); 
		nsystem( "  netstat -lntu " ); 
		return 0; 
	}



	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "port"    ) ==  0 ) || ( strcmp( argv[1] , "ports"    ) ==  0 ) )
	{
		//nsystem( "  netstat -tpln | egrep '(Proto|ssh)' " ); 
		if ( os_bsd_type == 3 ) 
		  nsystem( " netstat -nat | grep 127.0.0.1 " ); 
		else
		  nsystem( "  netstat -lntu " ); 
		return 0; 
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] , "create" ) ==  0 ) 
	if ( strcmp( argv[2] , "aes" ) ==  0 ) 
	{
		nsystem( "echo 'Host *'  >>   ~/.ssh/config    " ); 
		nsystem( "echo '  SendEnv LANG LC_*'   >>   ~/.ssh/config   " ); 
		nsystem( "echo '  Ciphers +aes256-cbc' >>   ~/.ssh/config   " ); 
		return 0;
	}









	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "--vol-max" ) ==  0 ) || ( strcmp( argv[1] ,   "--volume-max" ) ==  0 ) )
	{
		printf( " ============ \n" ); 
		nsystem( " amixer -c 0 sset MASTER 255+  ");
		nsystem( " amixer -c 0 sset Master 255+  ");
		nsystem( " amixer -c 0 sset Headphone 255+  ");
		nsystem( " amixer -c 0 sset Speaker 255+  ");
		nsystem( " amixer -c 0 sset PCM 255+  ");
		nsystem( " amixer -c 0 sset Capture 255+  ");
		nsystem( " amixer -c 0 sset Digital 255+  ");
		nsystem( " amixer -c 0 sset MASTER 255+  ");

		nsystem( " amixer -c 0 sset MASTER 255+  unmute ");
		nsystem( " amixer -c 0 sset Master 255+   unmute");
		nsystem( " amixer -c 0 sset Headphone 255+  unmute ");
		nsystem( " amixer -c 0 sset Speaker 255+  unmute ");
		nsystem( " amixer -c 0 sset PCM 255+  unmute ");
		nsystem( " amixer -c 0 sset Capture 255+  unmute ");
		nsystem( " amixer -c 0 sset Digital 255+  unmute ");
		nsystem( " amixer -c 0 sset MASTER 255+  unmute ");

		nsystem( " amixer -c 0 sset Capture  cap  ");

		nsystem( " amixer -c 0 sset 'Mic Boost' 0 ");
		nsystem( " amixer -c 0 sset 'Internal Mic Boost' 0 ");
		printf( " ============ \n" ); 
		//  system( "  amixer -c 0 sset HDMI 25+ " );  // for tv pi 
		//system( "  amixer -c 0 sset PCM 2+ " );  // for tv pi 
		//nsystem( " amixer -c 0 sset Master 2-  " ); 
		return 0;
	}










	if ( argc == 2)  // default
	if ( strcmp( argv[1] , "asoundrc1" ) ==  0 ) 
	{
			printf( "pcm.!default { \n" );
			printf( "          type asym \n" );
			printf( "          playback.pcm { \n" );
			printf( "                  type plug \n" );
			printf( "                  slave.pcm \"hw:1,0\" \n" );
			printf( "          } \n" );
			printf( "          capture.pcm { \n" );
			printf( "                  type plug \n" );
			printf( "                  slave.pcm \"hw:1,0\" \n" );
			printf( "          }  \n" );
			printf( " } \n" );
			return 0;
	}




	if ( argc == 2)  // default
	if ( strcmp( argv[1] , "asoundrc2" ) ==  0 ) 
	{
			printf( "pcm.!default { \n" );
			printf( "          type asym \n" );
			printf( "          playback.pcm { \n" );
			printf( "                  type plug \n" );
			printf( "                  slave.pcm \"hw:2,0\" \n" );
			printf( "          } \n" );
			printf( "          capture.pcm { \n" );
			printf( "                  type plug \n" );
			printf( "                  slave.pcm \"hw:2,0\" \n" );
			printf( "          }  \n" );
			printf( " } \n" );
			return 0;
	}




	if ( argc == 3)  // default
	if ( strcmp( argv[1] , "create" ) ==  0 ) 
	if ( strcmp( argv[2] , "asoundrc1" ) ==  0 ) 
	{
		nsystem( " cd ; mconfig asoundrc1 > .asoundrc " ); 
		return 0;
	}

	if ( argc == 3 )  
	if ( strcmp( argv[1] , "soundcard" ) ==  0 ) 
	if ( strcmp( argv[2] , "1" ) ==  0 ) 
	{
		nsystem( " cd ; mconfig asoundrc1 > .asoundrc " ); 
		return 0;
	}

	if ( argc == 3 )  
	if ( strcmp( argv[1] , "soundcard" ) ==  0 ) 
	if ( strcmp( argv[2] , "2" ) ==  0 ) 
	{
		nsystem( " cd ; mconfig asoundrc2 > .asoundrc " ); 
		return 0;
	}





	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "copymntlocalbin" ) ==  0 ) 
	if ( MYOS == 1 ) 
	{
		// pandora 
		nsystem( " mkdir -p /usr/local/bin ;  cp /mnt/usr/local/bin/*  /usr/local/bin/  " );
		return 0;
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "df" ) ==  0 ) 
	{
			nsystem( "   df -h " );
			return 0;
	}


	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "--attach" ) ==  0 ) || ( strcmp( argv[1] , "attach" ) ==  0 ) )
	if ( strcmp( argv[2] , "mmc1" ) ==  0 ) 
	{
		nsystem( " mconfig mount /dev/mmcblk1p1 " ); 
		nsystem( " mconfig mount /dev/mmcblk1p2 " ); 
		return 0; 
	}





	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "ins" ) ==  0 ) || ( strcmp( argv[1] , "install" ) ==  0 ) )
	if ( ( strcmp( argv[2] ,  "xz" ) ==  0 ) || ( strcmp( argv[2] ,  "xz-utils" ) ==  0 ) )
	{
		npkg(  " xz-utils " ); 
	        return 0; 
	}





	if ( argc == 2 )
	if (  ( strcmp( argv[1] , "--base" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--base" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-base" ) ==  0 ) )
	{
		nsystem( " pacstrap -i  . base nano wpa_supplicant dhcpcd  wget  " );  
		return 0;
	}






	/// fetch base of manjaro !
	if ( argc == 3 )
	if ( strcmp( argv[1] , "--base" ) ==  0 ) 
	if ( strcmp( argv[2] , "manjaro" ) ==  0 )
	{
		// manjaro - tools  <- nope 
		printf( "== BASE Manjaro \n" ); 
		npkg( " arch-install-scripts " ); 
		printf( "\n" ); 
		printf(  "  basestrap .  base linux dhcpcd  " );   
		// about wpa_supplicant ????
		printf( "\n" ); 
		return 0;
	}




	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "pacstrap" ) ==  0 ) || ( strcmp( argv[2] , "basestrap" ) ==  0 ) )
	{
		printf( " manjaro:   pacstrap \n" ); 
		printf( " archlinux: basestrap \n" ); 
		printf( " arch-install-scripts " ); 
		printf( " pacstrap -i  . base dhcpcd \n" ); 
		printf( "\n" ); 
		printf( " pacstrap -i  . base dhcpcd linux nano linux-firmware gcc make " ); 
		printf( "\n" ); 
		printf( "\n" ); 
		printf( "\n" ); 
		printf( " pacstrap -i  . base dhcpcd linux nano linux-firmware gcc make " ); 
		printf( "\n" ); 
		printf( " pacstrap -i  . base dhcpcd \n" ); 
		printf( " pacstrap -i  . base dhcpcd wpa_supplicant \n" ); 
		printf( " pacstrap -i  . base dhcpcd wpa_supplicant subversion gcc make \n" ); 
		printf( " ========================  \n" ); 
		printf( "\n" ); 
		printf( " pacstrap -i  . base nano wpa_supplicant dhcpcd  texlive-bin  subversion  wget  " );  
		printf( "\n" ); 
		return 0;
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] , "--bring" ) ==  0 ) 
	if ( strcmp( argv[2] , "texlive" ) ==  0 ) 
	{
	         /// ready for debian armbian, pbpro 
                 npkg( " texlive-base  texlive-binaries               texlive-font-utils             texlive-fonts-recommended  texlive-latex-base       texlive-latex-extra    texlive-latex-recommended texlive-pictures     " ); 
		 return 0; 
	}







      if ( argc == 2 )
      if ( ( strcmp( argv[1] , "armbian" ) ==  0 ) || ( strcmp( argv[1] , "Armbian" ) ==  0 ) )
      { 
		printf( " === Best Choice hw support =============== \n" ); 
                fetch_file_ftp( "https://imola.armbian.com/dl/pinebook-pro/archive/Armbian_23.11.1_Pinebook-pro_bookworm_current_6.1.63_cinnamon_desktop.img.xz" ); 
		fetch_file_ftp( "https://fi.mirror.armbian.de/archive/pinebook-pro/archive/Armbian_23.11.1_Pinebook-pro_bookworm_current_6.1.63_cinnamon_desktop.img.xz" ); 
		fetch_file_ftp( "https://fi.mirror.armbian.de/archive/pinebook-pro/archive/Armbian_23.11.1_Pinebook-pro_bookworm_current_6.1.63_cinnamon_desktop.img.xz.sha" ); 
		printf( " =============== \n" ); 
		return 0; 
      }



      if ( argc == 3 )
      if ( strcmp( argv[1] ,  "help" ) ==  0 ) 
      if ( ( strcmp( argv[2] , "armbian-2" ) ==  0 ) || ( strcmp( argv[2] , "Armbian-2" ) ==  0 ) )
      { 
		printf( "\n" ); 
		printf( " =============== \n" ); 
                printf( " 439a5e00c6e6be4eb358640f46ab6e09  Armbian_23.11.1_Pinebook-pro_bookworm_current_6.1.63_cinnamon_desktop.img.xz \n" ); 
		printf( " =============== \n" ); 
		printf( "https://fi.mirror.armbian.de/archive/pinebook-pro/archive/Armbian_23.11.1_Pinebook-pro_bookworm_current_6.1.63_cinnamon_desktop.img.xz\n" ); 
		printf( "https://fi.mirror.armbian.de/archive/pinebook-pro/archive/Armbian_23.11.1_Pinebook-pro_bookworm_current_6.1.63_cinnamon_desktop.img.xz.sha\n" ); 
		printf( "Armbian 23.11.1 Bookworm  \n" ); 
		printf( " =============== \n" ); 
		printf( "\n" ); 
		return 0; 
      }








	/// put base of manjaro !
	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "install" ) ==  0 ) || ( strcmp( argv[1] , "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] , "pacstrap" ) ==  0 ) || ( strcmp( argv[2] , "basestrap" ) ==  0 ) )
	{
		printf( " Manjaro \n" ); 
		// manjaro - tools  <- nope 
		npkg( " arch-install-scripts " ); 
		printf( "\n" ); 
		printf(  "   basestrap .  base linux dhcpcd  " );   
		printf( "\n" ); 
		return 0;
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] , "pacstrap" ) ==  0 ) 
	{
		//nsystem( " pacstrap -i  . base nano wpa_supplicant dhcpcd  texlive-bin  subversion  wget  " );  
                nsystem( " pacstrap -i  . base wmctrl wget links wpa_supplicant net-tools pipewire subversion  nm-connection-editor networkmanager ncftp openssh vim nano arch-install-scripts pipewire kbd alsa-utils mupdf mpg123 feh screen fltk ctwm m4 texlive-basic texlive-bin texlive-latex texlive-latexextra texlive-latexrecommended texlive-pictures debootstrap gcc make wget xorg-xinit xorg-xinput xorg-xkbcomp xorg-xkbevd xorg-xkbutils xorg-xkill xorg-xlsatoms xorg-xlsclients xorg-xmodmap xorg-xpr xorg-xprop xorg-xrandr xorg-xrdb xorg-xrefresh xorg-xset xorg-xsetroot xorg-xvinfo xorg-xwayland xorg-xwd xorg-xwininfo xorg-xwud xterm screen  dillo vim  " );   
		return 0;
	}











	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "xterm" ) ==  0 ) || ( strcmp( argv[1] , "terminal" ) ==  0 ) ) 
	{
	        if ( fexist( "/usr/bin/xfce4-terminal" ) == 1 ) 
		        nsystem( " xfce4-terminal " ); 
	        else if ( fexist( "/usr/bin/konsole" ) == 1 ) 
		        nsystem( " konsole " ); 
	        else if ( fexist( "/usr/bin/lxterm" ) == 1 ) 
		        nsystem( " lxterm " ); 
	        else if ( fexist( "/usr/bin/lxterminal" ) == 1 ) 
		        nsystem( " lxterminal " ); 
	        else if ( fexist( "/usr/bin/xterm" ) == 1 ) 
		{
			strncpy( cmdi, " ", PATH_MAX );
			strncat( cmdi , " xterm -bg black -fg yellow     " , PATH_MAX - strlen( cmdi ) -1 );
			nsystem( cmdi );
		}
		else 
		        nsystem( " xterm " ); 
		return 0;
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] , "--rootfs" ) ==  0 ) 
	if ( strcmp( argv[2] , "devuan-armhf" ) ==  0 ) 
	{
               fetch_file_ftp( "http://devuan-cd.sedf.de/devuan_ascii/embedded/devuan_ascii_2.0.0_armhf_raspi2.tar.gz" );
	       nsystem( "  tar xvpfz devuan_ascii_2.0.0_armhf_raspi2.tar.gz " );
	       return 0; 
        }







	if ( argc == 3 )
	if ( strcmp( argv[1] , "get" ) ==  0 ) 
	if ( strcmp( argv[2] , "22.06" ) ==  0 )   
	{
           nsystem( " wget --limit-rate=900k -c --no-check-certificate https://github.com/manjaro-arm/pbpro-images/releases/download/22.06/Manjaro-ARM-kde-plasma-pbpro-22.06.img.xz " ); 
	   return 0;
	}





	if ( argc == 2 )
	if ( strcmp( argv[1] , "22.06" ) ==  0 )
	{
		fetch_file_ftp( "https://github.com/manjaro-arm/pbpro-images/releases/download/22.06/Manjaro-ARM-kde-plasma-pbpro-22.06.img.xz" ); 
                // fetch_file_ftp( "https://github.com/manjaro-arm/pbpro-images/releases/download/22.06/Manjaro-ARM-kde-plasma-pbpro-22.06.img.xz" );  
		return 0;
	}





	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "ins" ) ==  0 ) || ( strcmp( argv[1] , "install" ) ==  0 ) )
	if ( strcmp( argv[2] , "console" ) ==  0 ) 
	{
		npkg(  " console-setup " );   
		return 0;
	}





        // ttf-bitstream-vera
	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,   "install" ) == 0 ) || ( strcmp( argv[1] ,   "ins" ) == 0 ) )
	if ( ( strcmp( argv[2] ,   "fonts" ) == 0 ) || ( strcmp( argv[2] ,   "xfonts" ) == 0 ) )
	{
	        // manjaro !!
		npkg( " ttf-bitstream-vera " );
		return 0;
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "wipe" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "sda" ) ==  0 ) 
	{
				fookey = ckeypress();
				if ( fookey == '1' ) 
					nsystem( "  dd if=/dev/zero of=/dev/sda  count=9999 " );
				return 0;
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] , "create" ) ==  0  ) 
	if ( ( strcmp( argv[2] , "sources" ) ==  0  ) || ( strcmp( argv[2] , "repository" ) ==  0 ) )  
	{
		//npkg_update(); 
		//deb http://archive.devuan.org/merged ascii          main
		//deb http://archive.devuan.org/merged ascii-security main
		nsystem( "  echo 'deb http://archive.devuan.org/merged ascii          main'  > /etc/apt/sources.list " );  
		return 0;
	}







	if ( argc == 2 )
	if ( strcmp( argv[1] , "cleanup" ) ==  0 ) 
	{
		if ( MYOS == 1 )
		{
			nsystem( " apt-get remove --purge -y usbmount  " ); 
			nsystem( " apt-get remove --purge -y man-db  " ); 
		}
		return 0; 
	}









        if ( argc == 2 )
        if ( strcmp( argv[1] , "mbat" ) ==  0 ) 
        {
	    nsystem( " cat /sys/class/power_supply/bq27500-0/capacity" ); 
	    nsystem( " cat /sys/class/power_supply/bq27500-0/status " ); 
	    nsystem( " cat /sys/class/power_supply/bq27500-0/* " ); 
	    return 0; 
	}




	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "panbat" ) ==  0 ) 
	|| ( strcmp( argv[1] , "pbat" ) ==  0 ) 
	|| ( strcmp( argv[1] , "pandora-battery" ) ==  0 ) 
	|| ( strcmp( argv[1] , "pbat" ) ==  0 ) ) 
	{
	        system( " cat /sys/class/power_supply/bq27500-0/capacity" ); 
		return 0; 
	}




	if ( argc >= 3 )
	if ( strcmp( argv[1] , "--catfile" ) ==  0 ) 
	if ( strcmp( argv[2] , "" ) !=  0 ) 
	{
		for( i = 2 ; i < argc ; i++) 
		{
		   if ( fexist( argv[ i ] ) == 1 ) 
		   {
		        printf( " ============== \n "); 
			printf( "[FILE] => %d/%d, File: (%s): \n", i , argc , argv[ i ] );
		        strncpy( cmdi , "  " , PATH_MAX );
			snprintf( charo , sizeof( charo ), " cat \"%s\" ",  argv[ i ] );
			nsystem(  charo );
		        printf( " ============== \n "); 
	           }
		}
		return 0; 
	}








	if ( argc == 3 + 1  )
	if ( ( strcmp( argv[1] ,   "findlst" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "-findlst" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "--findlst" ) ==  0 ) )
	{
		snprintf( cmdi , sizeof( cmdi ), "  filegrep2  \"%s\" \"%s\"  *.lst  ", argv[ 2 ] , argv[ 3 ] ); 
		nsystem( cmdi );   
		return 0; 
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "bright" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "br" ) ==  0 ) 
	|| ( strcmp( argv[1] , "brightness" ) ==  0 ) )
	{
                nsystem( " echo 4095 > /sys/class/backlight/edp-backlight/brightness   " );  
		return 0; 
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) ==  0 ) 
	if ( ( strcmp( argv[2] ,   "wallpaper" ) ==  0 ) 
	|| ( strcmp( argv[2] ,   "wallpapers" ) ==  0 ) )
	{
	    printf( "\n" ); 
	    printf( " Eg: manjaro-pine64,  manjaro-pine64-2b \n" );  
	    printf( " /usr/share/wallpapers/manjaro-arm/generic/manjaro-pine64-2b.png" ); 
	    printf( "\n" ); 
	    return 0; 
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] , "wlan0loop" ) ==  0 ) 
	{
		nsystem( "  while : ;  do   dhclient -v wlan0 ; sleep 5 ; done " ); 
		return 0; 
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "tmux" ) ==  0 )   /// <- bug workaround !
	{
			nsystem( " export TERM=linux ; export LC_CTYPE=C.UTF-8  ; tmux   " );
			return 0;
	}

	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "tmux" ) ==  0 )   /// <- bug workaround !
	if ( strcmp( argv[2] ,   "attach" ) ==  0 )   /// <- bug workaround !
	{
			nsystem( " export TERM=linux ; export LC_CTYPE=C.UTF-8  ; tmux  attach " );
			return 0;
	}






	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "pinebat" ) ==  0 ) || ( strcmp( argv[1] , "batpine" ) ==  0 ) )
	{
                // pinebook 
                system( " cat /sys/devices/platform/ff3d0000.i2c/i2c-4/4-0062/power_supply/cw2015-battery/capacity " );  
		return 0; 
	}



	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "dateloop" ) ==  0 ) || ( strcmp( argv[1] ,   "timeloop" ) ==  0 ) )
	{
			while( 1 ) 
			{
				printf( "===\n" );
				nsystem( " date ; sleep 5 " ); 
			}
			return 0;
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] ,     "ramdisk" ) ==  0 ) 
	if ( ( strcmp( argv[2] ,     "2G" ) ==  0 ) || ( strcmp( argv[2] ,     "2gb" ) ==  0 ) || ( strcmp( argv[2] ,     "2g" ) ==  0 ) )
	{
                  nsystem( "   mkdir /var/ramdisk ; mount -t tmpfs -osize=2200M none /var/ramdisk  ; mount | grep ramdisk " ); 
		  return 0; 
	}











	if ( argc == 2 )
	if ( strcmp( argv[1] , "savekernel" ) ==  0 ) 
	{
		/// printf( "wlans_ath0=\"wlan0\"\n" );
                nsystem( " uname -a > kernel.txt  ; lsmod  >> kernel.txt ; ls -1 /lib/modules >>  kernel.txt  ; ls -1 /lib/firmware/ >> kernel.txt      " );   
		return 0; 
        }




	if ( argc == 2 )
	if  ( ( strcmp( argv[1] , "root-blue" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--root-blue" ) ==  0 ) )
	{
			nsystem( " xsetroot      -solid '#3A6EA5' " );
			nsystem( " xsetrootfork  -solid '#3A6EA5' " );
			return 0;
	}

	if ( argc == 2 )
	if ( strcmp( argv[1] , "--panel" ) ==  0 )
	{
		return 0;
	}

	if ( argc == 2 )
	if ( strcmp( argv[1] , "--desktop" ) ==  0 )
	{
		return 0;
	}



	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "minidisk" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "miniram" ) ==  0 ) )
	{
		 if ( MYOS == 1 ) 
                    nsystem( "   mkdir /ramdisk ; mount -t tmpfs -osize=350M none   /ramdisk  ; mount " ); 
		 return 0; 
	}









	if ( argc == 2 )
	if ( strcmp( argv[1] , "stop-default" ) ==  0 ) 
	{
		nsystem( " pkill feh       "); 
		nsystem( " pkill alsamixer   "); 
		nsystem( " pkill mupdf     "); 
		nsystem( " pkill gpicview  "); 
		nsystem( " pkill mpv  "); 
		nsystem( " pkill vlc  "); 
		nsystem( " pkill cvlc "); 
		nsystem( " pkill mpg123  "); 
		nsystem( " pkill vlc  "); 
		nsystem( " pkill mplayer "); 
		nsystem( " pkill feh     "); 
                // new 
		nsystem( " pkill ffmpeg     "); 
		nsystem( " pkill retroarch     "); 
                return 0; 
	}



        // flmenu --kodak --jukebox --stop 
	// stream tuner  
	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "stop" ) ==  0 ) || ( strcmp( argv[1] ,   "--stop" ) ==  0 ) )
	{
		nsystem( " pkill feh         "); 
		nsystem( " pkill alsamixer   "); 
		nsystem( " pkill mupdf     "); 
		nsystem( " pkill gpicview  "); 
		nsystem( " pkill mpv  "); 
		nsystem( " pkill vlc  "); 
		nsystem( " pkill cvlc "); 
		nsystem( " pkill mpg123  "); 
		nsystem( " pkill vlc  "); 
		nsystem( " pkill mplayer "); 
		nsystem( " pkill feh     "); 
		nsystem( " pkill ffplay     "); 
		nsystem( " pkill parole     "); 

		nsystem( " pkill -9 ffplay     ");  // manjaro bug fix on pbpro  
		nsystem( " pkill -9 mpg123     ");  // manjaro bug fix on pbpro  

                // new 
		///nsystem( " pkill ffmpeg     "); 
		nsystem( " pkill retroarch     "); 
                return 0; 
	}







	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "22.06" ) ==  0 ) || ( strcmp( argv[1] , "2206" ) ==  0 ) )  
	{
		fetch_file_ftp( "https://github.com/manjaro-arm/pbpro-images/releases/download/22.06/Manjaro-ARM-kde-plasma-pbpro-22.06.img.xz" ); 
		return 0;
	}










        //strncpy( cwd , "   setxkbmap us ; mednafen  -vdriver sdl     " , PATH_MAX );
	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "--mednafen" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-mednafen" ) ==  0 ) )
	{
	        // manjaro 
                //    MEDNAFEN_ALLOWMULTI=1   mednafen   anguna.gba  
		if ( fexist( "/usr/games/mednafen" ) == 1 ) 
		    ncmdwith( "   /usr/games/mednafen -video.driver sdl " ,  argv[ 2 ] );

		else if ( fexist( "/usr/bin/mednafen" ) == 1 ) 
		    ncmdwith( "   /usr/bin/mednafen -video.driver sdl " ,  argv[ 2 ] );

		else if ( fexist( "/usr/bin/mednafen" ) == 1 ) 
		    ncmdwith( " MEDNAFEN_ALLOWMULTI=1  /usr/bin/mednafen -video.driver sdl " ,  argv[ 2 ] );

		else 
		    ncmdwith( " mednafen -video.driver sdl " ,  argv[ 2 ] );
		return 0;
	}









	if ( argc == 2 )
	if ( strcmp( argv[1] , "ascii" ) ==  0 ) 
	{
		for( fookey = 1 ; fookey < 255 ; fookey++) 
		{
		   printf( " Char (d) %d, Char (c) %c \n", fookey , fookey ); 
		}
		return 0; 
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "--home" )    ==  0 ) 
	{
		printf( "home: %s\n", getenv( "home" ));  // amd64 as grub2 !!
		chdir(  getenv( "home" ));
		printf( "Current dir: %s\n", getcwd( cmdi , PATH_MAX ) );
		return 0; 
        }

	if ( argc == 2 )
	if ( strcmp( argv[1] , "--cwd" ) ==  0 ) 
	{
		printf( "%s\n", getcwd( cmdi , PATH_MAX ) );
		return 0;
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "service" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "sshd" ) ==  0 ) 
	{
		nsystem( " /etc/init.d/ssh  start " ); 
	        nsystem( " systemctl status sshd.service ; systemctl enable sshd.service ;  systemctl start sshd.service " ); 
	 	printf( "\n" ); 
		return 0; 
	}





      if ( argc == 4 )
      if ( strcmp( argv[1] ,  "--grep" ) ==  0 ) 
      {
               // h | gr
	       snprintf( foostr , sizeof( foostr ), " cat '%s' | grep \"%s\" " , argv[ 2 ] , argv[ 3 ] ); 
	       system( foostr ); 
	       return 0; 
      }




      if ( argc == 3 )
      if ( strcmp( argv[1] ,  "--grephttp" ) ==  0 ) 
      {
	       //printf( " grep -Po '(?<=href=\")[^\"]*'  \"%s\"  ", argv[ 2 ] ); 
	       snprintf( foostr , sizeof( foostr ), " grep -Po '(?<=href=\")[^\"]*' \"%s\" " , argv[ 2 ] ); 
	       system( foostr ); 
	       return 0; 
      }

      if ( argc == 3 )
      if ( strcmp( argv[1] ,  "--GREPHTTP" ) ==  0 ) 
      {
	       //printf( " grep -Po '(?<=href=\")[^\"]*'  \"%s\"  ", argv[ 2 ] ); 
	       snprintf( foostr , sizeof( foostr ), " grep -Po '(?<=HREF=\")[^\"]*' \"%s\" " , argv[ 2 ] ); 
	       system( foostr ); 
	       return 0; 
      }





	if ( argc == 3 )
	if ( strcmp( argv[1] , "wmctrl" ) ==  0 ) 
	if ( strcmp( argv[2] , "close" ) ==  0 ) 
	{
               // wmctrl -r :ACTIVE: -b toggle,maximized_vert,maximized_horz 
		nsystem( " wmctrl -c :ACTIVE: " ); 
		return 0;
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "system" ) ==  0 ) 
	{
	       system( " uname -a " );  
               system( " nconfig --right 15 \"` dmesg | grep DMI:  `\" " ); 
	       system( " firefox --version " );  
	       system( " vmstat " );  
	       system( "  chromium --version ; uname -a ;  cat /etc/apt/sources.list | grep 'deb http' | grep -v '#' "); 
	       system( "  uname -a " );  
	       system( "  ls -1 /var/cache/apt/archives/ | tail -n 1  " ); 
	       system( "  whereis vim  " );  
	       system( "  whereis ncftp    " );  
	       system( "  whereis svn      " );  
	       system( "  whereis mpg123   " );  
	       system( "  whereis xterm    " );  
	       system( "  whereis pdflatex    " );  
	       system( "  whereis vim    " );  
	       return 0; 
	}


	if ( argc == 2 )
	if (  ( strcmp( argv[1] , "fastreboot" ) ==  0 ) ||  ( strcmp( argv[1] , "forcereboot" ) ==  0 ) ) 
	{
           nsystem( " echo s > /proc/sysrq-trigger ; echo u > /proc/sysrq-trigger ; echo b > /proc/sysrq-trigger " ); 
	   return 0; 
	}




	if ( argc >= 3 )
	if ( strcmp( argv[1] , "--unzip2dir" ) ==  0 ) 
	{
		foocounter = 1; 
		if ( fexist( "/usr/bin/unzip" ) == 1 ) 
		{
			for( i = 2 ; i < argc ; i++) 
			{
				printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
				snprintf( charo , sizeof( charo ), "dir-%d-%d", (int)time(NULL), foocounter );
				strncpy( targetdir, charo , PATH_MAX );  // dir actually 

				snprintf( charo , sizeof( charo ), " mkdir \"%s\" ", targetdir );
				nsystem( charo ); 

				printf( "Component file: %s\n", argv[ i ] );

				strncpy( cmdi , " unzip -o " , PATH_MAX );
				strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "\" "  , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , " -d " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , targetdir , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "\" "  , PATH_MAX - strlen( cmdi ) -1 );
				nsystem( cmdi );

				printf( " ============== \n "); 
				foocounter = foocounter +1; // classic code
				printf( " ============== \n "); 
			}
		}
	   return 0; 
	}




	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "-atari2600" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--a26" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--atari2600" ) ==  0 ) )
	{
	    printf( " ATARI 2600 emu ...\n" ); 
	    snprintf( charo , sizeof( charo ), "    /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-stella2014/stella2014_libretro.so --config /opt/retropie/configs/atari2600/retroarch.cfg   \"%s\" --appendconfig /dev/shm/retroarch.cfg  " , argv[ 2 ] ); 
	    nsystem( charo ); 
	    return 0; 
        }





        /*
              printf( " /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-mame2003/mame2003_libretro.so --config /opt/retropie/configs/mame-libretro/retroarch.cfg   solomon.zip     --appendconfig   /dev/shm/retroarch.cfg " ); 
	      printf(  "\n" ); 
              printf( " /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-mame2003/mame2003_libretro.so --config /opt/retropie/configs/mame-libretro/retroarch.cfg   solomon.zip     --appendconfig   /dev/shm/retroarch.cfg " ); 
	      printf(  "\n" ); 
        */
	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "--fb-psx" ) ==  0  ) 
	|| ( strcmp( argv[1] , "-fbpsx" ) ==  0  )  
	|| ( strcmp( argv[1] , "-fb-psx" ) ==  0  ) ) 
	{
			strncpy( cmdi , "  " , PATH_MAX );
			for( i = 2 ; i < argc ; i++) 
			{
				printf( "=> %d/%d %s \n", i , argc , argv[ i ] );

	                        if ( ( strcmp( argv[1] , "--xpsx" ) ==  0  ) || ( strcmp( argv[1] , "-xpsx" ) ==  0  ) )
				   strncpy( cmdi , "  export DISPLAY=:0 ; /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-pcsx-rearmed/pcsx_rearmed_libretro.so --config /opt/retropie/configs/psx/retroarch.cfg         " , PATH_MAX );
			        else 
				   strncpy( cmdi , "    /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-pcsx-rearmed/pcsx_rearmed_libretro.so --config /opt/retropie/configs/psx/retroarch.cfg         " , PATH_MAX );

				strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "    --appendconfig   /dev/shm/retroarch.cfg   " , PATH_MAX - strlen( cmdi ) -1 );
				nsystem( cmdi );

			}
			printf( " ============== \n "); 
			return 0; 
	}









	if ( argc >= 3 )
	if ( strcmp( argv[1] , "--fb-atari2600" ) ==  0  )
	{
			strncpy( cmdi , "  " , PATH_MAX );
			for( i = 2 ; i < argc ; i++) 
			{
				printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
		                strncpy( cmdi , "  /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-stella2014/stella2014_libretro.so --config /opt/retropie/configs/atari2600/retroarch.cfg  " , PATH_MAX );
				strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "    --appendconfig   /dev/shm/retroarch.cfg   " , PATH_MAX - strlen( cmdi ) -1 );
				nsystem( cmdi );
			}
			printf( " ============== \n "); 
			return 0; 
	}







	if ( argc >= 2 )
	if ( ( strcmp( argv[1] , "--fb-nes" ) ==  0 )  || ( strcmp( argv[1] , "--fbnes" ) ==  0 )  ) 
	{
		for( i = 2 ; i < argc ; i++) 
		{
		        printf( " ============== \n "); 
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
			strncpy( cmdi , "     /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-fceumm/fceumm_libretro.so --config /opt/retropie/configs/nes/retroarch.cfg   " , PATH_MAX );
			strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
			strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
			strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
			strncat( cmdi , "     --appendconfig /dev/shm/retroarch.cfg   " , PATH_MAX - strlen( cmdi ) -1 );
			nsystem( cmdi );
		}
		printf( " ============== \n "); 
		return 0; 
	}






	if ( argc >= 3 )
	if ( strcmp( argv[1] , "--fb-sms" ) ==  0  )
	{
			strncpy( cmdi , "  " , PATH_MAX );
			for( i = 2 ; i < argc ; i++) 
			{
				printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
				strncpy( cmdi , " /opt/retropie/emulators/retroarch/bin/retroarch -L /opt/retropie/libretrocores/lr-genesis-plus-gx/genesis_plus_gx_libretro.so --config /opt/retropie/configs/mastersystem/retroarch.cfg  " , PATH_MAX );
				strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "    --appendconfig   /dev/shm/retroarch.cfg   " , PATH_MAX - strlen( cmdi ) -1 );
				nsystem( cmdi );


			}
			printf( " ============== \n "); 
			return 0; 
	}









	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "west" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-desktop-west" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--desktop-west" ) ==  0 ) )
	{
		printf( " > File at: head -n 1 ~/.x2vnc.west \n" ); 
		snprintf( charo , sizeof( charo ), " x2vnc -west $( head -n 1 ~/.x2vnc.west ):0 " );  
		nsystem( charo ); 
		return 0;
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] , "--url" ) ==  0 ) 
	if ( strcmp( argv[2] , "rpi4" ) ==  0 ) 
	{
                printf( "https://gitlab.com/openbsd98324/retropie-rpi4/-/raw/main/archives/retropie-buster-4.8-rpi4_400.img.gz"  ); 
		return 0; 
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] , "stretch" ) ==  0 ) 
	{
               printf( "deb http://archive.debian.org/debian stretch main \n" ); 
	       return 0; 
	}




	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "xfce4" ) ==  0 ) 
	|| ( strcmp( argv[1] , "xfce" ) ==  0 ) 
	|| ( strcmp( argv[1] , "wm-xfce" ) ==  0 ) 
	|| ( strcmp( argv[1] , "wm-xfce4" ) ==  0 ) ) 
	{
			printf( "========================\n" );
			printf( "Classic version (clean) \n" );
			printf( "========================\n" );
			nsystem( " cd ; echo                >    .xinitrc  " );
			nsystem( " cd ; echo  cd            >>   .xinitrc  " );
			nsystem( " cd ; echo setxkbmap de   >>   .xinitrc  " );
			nsystem( " cd ; echo  ' xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    kicker &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo   xrdb  .Xresources               >> .xinitrc  " );
			nsystem( " cd ; echo   xsetroot -cursor_name left_ptr  >> .xinitrc " ); 
			nsystem( " cd ; echo                        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  xfce4-session        >>       .xinitrc  " );
			printf( "============\n" );
			nsystem( " cd ; echo setxkbmap de          >        .xsession " );
			nsystem( " cd ; echo  'xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xsession  " );
			nsystem( " cd ; echo   xrdb  .Xresources    >>      .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '    env TZ=Europe/Paris      kicker &  '        >>       .xsession  " );
			nsystem( " cd ; echo   xrdb  .Xresources               >> .xsession   " );
			nsystem( " cd ; echo   xsetroot -cursor_name left_ptr  >> .xsession   " ); 
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  xfce4-session        >>       .xsession  " );
			printf( "============\n" );
			return 0;
	}









	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "konsole" ) ==  0 ) 
	|| ( strcmp( argv[1] , "gnome-terminal" ) ==  0 ) 
	|| ( strcmp( argv[1] , "terminal" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--konsole" ) ==  0 ) 
	|| ( strcmp( argv[1] , "gnome-terminal" ) ==  0 ) 
	|| ( strcmp( argv[1] , "lxterm" ) ==  0 ) 
	|| ( strcmp( argv[1] , "lxterminal" ) ==  0 ) 
	|| ( strcmp( argv[1] , "lxterm" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-lxterm" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--lxterminal" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-lxterminal" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--nemo" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--konsole" ) ==  0 ) 
	|| ( strcmp( argv[1] , "console" ) ==  0 ) 
	|| ( strcmp( argv[1] , "terminal" ) ==  0 ) 
	|| ( strcmp( argv[1] , "TERMINAL" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-konsole" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--gnome-terminal" ) ==  0 ) 
	|| ( strcmp( argv[1] , "xfce4-terminal" ) ==  0 ) ) 
	{
		if ( fexist( "/usr/bin/konsole" ) == 1 )
			nsystem( "konsole" );
		else if ( fexist( "/usr/bin/xfce4-terminal" ) == 1 )
			nsystem( "xfce4-terminal" );
		else if ( fexist( "/usr/bin/lxterminal" ) == 1 )
			nsystem( "lxterminal" );
		else if ( fexist( "/usr/bin/gnome-terminal" ) == 1 )
			nsystem( "gnome-terminal" );
		else if ( fexist( "/usr/bin/nemo" ) == 1 )
			nsystem( "nemo" );
		else if ( fexist( "/usr/bin/eterm" ) == 1 )
			nsystem( "eterm" );
		else 
			nsystem( "  xterm " );
		return 0;
	}





	if ( argc == 2 )
	if ( strcmp( argv[1] , "src" ) ==  0 ) 
	{
	        printf( " deb http://archive.debian.org/debian stretch main \n" );   
		return 0;
	}






	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "dolphin" ) ==  0 ) 
	|| ( strcmp( argv[1] , "xexplorer" ) ==  0 ) 
	|| ( strcmp( argv[1] , "explorer" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--dolphin" ) ==  0 ) 
	|| ( strcmp( argv[1] , "thunar" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-thunar" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--thunar" ) ==  0 ) 
	|| ( strcmp( argv[1] , "pcmanfm" ) ==  0 ) 
	|| ( strcmp( argv[1] , "pc" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--pcmanfm" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--nautilus" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--rox" ) ==  0 ) 
	|| ( strcmp( argv[1] , "nautilus" ) ==  0 ) )
	{
	        if ( fexist( "/usr/bin/nautilus" ) == 1 ) 
		  snprintf( charo , sizeof( charo ),  "  nautilus --new-window \"%s\" ",   getcwd( foocwd, PATH_MAX ) ); 

	        else if ( fexist( "/usr/bin/dolphin" ) == 1 ) 
		  snprintf( charo , sizeof( charo ),  " dolphin   --new-window --select \"%s\" ",   getcwd( foocwd, PATH_MAX ) ); 

		else if ( fexist( "/usr/bin/nautilus" ) == 1 ) 
		  snprintf( charo , sizeof( charo ),  "  nautilus --new-window \"%s\" ",   getcwd( foocwd, PATH_MAX ) ); 

		else if ( fexist( "/usr/bin/thunar" ) == 1 ) 
		  snprintf( charo , sizeof( charo ),  "  thunar \"%s\" ",   getcwd( foocwd, PATH_MAX ) ); 

		else if ( fexist( "/usr/bin/pcmanfm" ) == 1 ) 
		  snprintf( charo , sizeof( charo ),  "  pcmanfm \"%s\" ",   getcwd( foocwd, PATH_MAX ) ); 

		// no screen for rox: 
		//else if ( fexist( "/usr/bin/rox" ) == 1 ) 
		//  snprintf( charo , sizeof( charo ),  "  rox \"%s\" ",   getcwd( foocwd, PATH_MAX ) ); 

		else if ( fexist( "/usr/bin/konqueror" ) == 1 ) 
		  snprintf( charo , sizeof( charo ),  "  konqueror  \"%s\" ",   getcwd( foocwd, PATH_MAX ) ); 

		else if ( fexist( "/usr/bin/tuxcmd" ) == 1 ) 
		  snprintf( charo , sizeof( charo ),  "  tuxcmd \"%s\" ",   getcwd( foocwd, PATH_MAX ) ); 

		else
		  snprintf( charo , sizeof( charo ),  " flm \"%s\" ",   getcwd( foocwd, PATH_MAX ) ); 

		nsystem( charo ); 
		return 0; 
	}









	if ( argc == 3 )
	if ( strcmp( argv[1] , "pissh0" ) ==  0 ) 
	{
		snprintf( charo , sizeof( charo ), "  ssh pi@192.168.0.%s  ",  argv[ 2 ] );
		nsystem(  charo ); 
		return 0; 
        }








	if ( argc == 3 )
	if ( strcmp( argv[1] , "sources.list" ) ==  0 ) 
	if ( strcmp( argv[2] , "stretch" ) ==  0 ) 
	{
		printf( "# deb http://raspbian.raspberrypi.org/raspbian/   bullseye main contrib non-free rpi\n" ); 
		printf( "\n" ); 
		printf( "deb http://archive.debian.org/debian stretch main \n" ); 
		printf( "\n" ); 
		return 0;
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[1] , "vision" ) ==  0 ) 
	{
	   printf( " /usr/bin/blackbox (%d)\n" , fexist( "/usr/bin/blackbox" ) ); 
	   printf( " /usr/bin/x11vnc (%d)\n"   , fexist( "/usr/bin/x11vnc" ) ); 
	   printf( " /usr/bin/Xvfb (%d)\n"     , fexist( "/usr/bin/Xvfb" ) ); 
	   return 0; 
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] , "vision" ) ==  0 ) 
	{
		 printf( " /usr/bin/blackbox (%d)\n" , fexist( "/usr/bin/blackbox" ) ); 
		 printf( " /usr/bin/x11vnc (%d)\n"   , fexist( "/usr/bin/x11vnc" ) ); 

	         if ( strcmp( argv[1] , "vision" ) ==  0 ) 
		 {
		    if ( fexist( "/usr/bin/x11vnc" ) == 0 ) 
		    {
		       npkg_update(); 
		       npkg( " x11vnc " ); 
		    }
		    if ( fexist( "/usr/bin/blackbox" ) == 0 ) 
		    {
		       npkg_update(); 
		       npkg( " blackbox " ); 
		    }
		 }
	         ///printf( " Teamviewer or free tvviewer over ssh ?\n" ); 
                 ///nsystem( " mconfig  xv1920 & sleep 1  ; mconfig blackbox1  &   sleep 3 ;  nconfig x11vnc1 " ); 
		 /// to me completed!!!
		 return 0; 
	}










	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "install" ) ==  0 ) || ( strcmp( argv[1] , "ins" ) ==  0 ) )
	if ( strcmp( argv[2] , "gnome" ) ==  0 ) 
	{

		// nsystem( " pacman -Syu ; pacman -S gnome   " ); 
		nsystem( " pacman -Syu ; pacman -Sy  --noconfirm gnome   " ); 

		npkg( " lxappearance " ); 
		npkg( " lxsession    " ); 
		npkg( " lxterminal    " ); 

		npkg( " lxsession    " ); 
		npkg( " lxterminal    " ); 
		npkg( " pcmanfm      " ); 
		npkg( " mupdf       " ); 
		npkg( " lxpanel     " ); 
		npkg( " netcat      " ); 
		npkg( " mupdf    " ); 
		npkg( " xterm    " ); 
		npkg( " evince   " ); 
		npkg( " xpaint   " ); 

		npkg( " gnome " ); 
		npkg( " gwenview " ); 
		npkg( " mousepad " ); 
		npkg( " eog      " ); 
		npkg( " mpv    " ); 
		npkg( " wmctrl    " ); 

		npkg( " gdm3   " ); 
		return 0; 
	}




	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,  "--xscreensaver" ) ==  0 ) || ( strcmp( argv[1] ,  "--xscreensaver-lock" ) ==  0 ) )
	{
		nsystem( " xscreensaver-command  -lock " ); 
		return 0; 
	}



        // the bar of gnome tweak 
	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "tweaks" )     ==  0 ) 
	|| ( strcmp( argv[1] ,   "-tweak" )     ==  0 )
	|| ( strcmp( argv[1] ,   "--tweak" )     ==  0 ) 
	|| ( strcmp( argv[1] ,   "tweak" )     ==  0 ) )
	{
		nsystem( " cd ; gnome-tweaks  " );  // 20.10 ubuntu  to change the icons 
		return 0;
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] , "23.02-gnome" ) ==  0 ) 
	{
                fetch_file_ftp( "https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-gnome-pbpro-23.02.img.xz" );
		return 0; 
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] , "23.02-gnome" ) ==  0 ) 
	{
	        fetch_file_ftp( "https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-gnome-pbpro-23.02.img.xz" ); 
		return 0; 
	}






	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "xscreensaver" ) ==  0 )  
	|| ( strcmp( argv[1] , "lock" ) ==  0 )  )
	{
		npkg( " xscreensaver  " );  
		nsystem( " screen -d -m  xscreensaver  " );  
		nsystem( " screen -d -m  xscreensaver-demo  " );  
		nsystem( " xscreensaver-command  -lock " ); 
		return 0; 
	}



        // # Generated by resolv.conf
	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "resolv" ) ==  0 ) || ( strcmp( argv[1] ,   "resolv.conf" ) ==  0 ) )
	{
              printf( "nameserver 192.168.1.1\n" ); 
              return 0; 
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] , "--gnome-settings" )     ==  0 ) 
	{
              if(  fexist( "/usr/bin/gnome-control-center" ) == 1 )  
                nsystem( " cd ; gnome-control-center " ); 
	      else
                nsystem( " cd ; flsetting " ); 
	      return 0; 
        }





	if ( argc == 2 )
	if ( strcmp( argv[1] , "benchdisk" ) ==  0 )
	{
                printf(  "\n" ); 
		nsystem( " dd if=/dev/zero of=benchdisk bs=1M count=2096 status=progress " );   // 2GB benchdisk  
                printf(  "\n" ); 
		return 0;
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 )
	if ( strcmp( argv[2] , "swap" ) ==  0 )
	{
		// mkswap 
		//nsystem( " dd if=/dev/zero of=/swapfile bs=1M count=4096 status=progress " ); 
                printf(  "\n" ); 
                printf(  "\n" ); 
                printf(  "/swapfile none swap defaults 0 0 " ); 
                printf(  "\n" ); 
                printf(  "\n" ); 
		return 0;
	}







	if ( argc == 2 )
	if ( strcmp( argv[1] , "mkswap" ) ==  0 )
	{
		// mkswap 
		// 4gb 
		///nsystem( " dd if=/dev/zero of=/swapfile bs=1M count=4096 status=progress " ); 
		// 2gb 
		nsystem( " dd if=/dev/zero of=/swapfile bs=1M count=2096 " );    /// on 6 openpandora 
		//nsystem( " dd if=/dev/zero of=/swapfile bs=1M count=2096 status=progress " ); 
		// then swapon 
                nsystem( " chmod  0600 /swapfile ; mkswap  /swapfile  ; swapon /swapfile  " ); 
		return 0;
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "swapon" ) ==  0 )
	|| ( strcmp( argv[1] , "swapfile" ) ==  0 )
	|| ( strcmp( argv[1] , "swap" ) ==  0 ) ) 
	{
		// then swapon 
                nsystem( " chmod  0600 /swapfile ; mkswap  /swapfile  ; swapon /swapfile  " ); 
		return 0;
	}













	if ( argc >= 3 )
	if ( strcmp( argv[1] , "--fbi" ) ==  0  ) 
	{
			printf( " ============== \n "); 
			for( i = 2 ; i < argc ; i++) 
			{
				printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
				strncpy( cmdi , "  fbi  -vt 1 -d /dev/fb0   " , PATH_MAX );
				strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "   " , PATH_MAX - strlen( cmdi ) -1 );
				nsystem( cmdi );
			}
			printf( " ============== \n "); 
			return 0; 
	}


	if ( argc >= 3 )
	if ( strcmp( argv[1] , "--fbi-std" ) ==  0  ) 
	{
			printf( " ============== \n "); 
			for( i = 2 ; i < argc ; i++) 
			{
				printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
				strncpy( cmdi , "  fbi   " , PATH_MAX );
				strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "   " , PATH_MAX - strlen( cmdi ) -1 );
				nsystem( cmdi );
			}
			printf( " ============== \n "); 
			return 0; 
	}




        
	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "aurora" ) ==  0 ) 
	{
		nsystem( " cd ; fbi  -vt 1 -d /dev/fb0  /usr/share/rpd-wallpaper/aurora.jpg  " ); 
		return 0; 
	}




	if ( argc == 3 )
	if ( strcmp( argv[1] ,  "--check-or-install-application" ) ==  0 ) 
	{
		   snprintf( foostr , sizeof( foostr ), "/usr/bin/%s" , argv[ 2 ] ); 
		   if ( fexist( foostr ) == 0 ) 
		   {
		     npkg_update();   
		     npkg( argv[ 2 ] ); 
		   }
		   return 0; 
         }





        // new 
	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "--update-pkg" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--upkg" ) ==  0 )  
	|| ( strcmp( argv[1] , "-upkg" ) ==  0 )  
	|| ( strcmp( argv[1] , "upkg" ) ==  0 ) ) 
	{
			npkg_update();   

			strncpy( cmdi , " apt-get install -f -y  " , PATH_MAX );
			for( i = 2 ; i < argc ; i++) 
			{
				printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
				strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "\" " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , "   " , PATH_MAX - strlen( cmdi ) -1 );
			}
			nsystem( cmdi );
			printf( " ============== \n "); 
			return 0; 
	}








 
	if ( argc == 2 )
	if ( strcmp( argv[1] , "pandrive" ) ==  0 ) 
	{
	    printf( " = PANDRIVE LEVEL = \n" ); 
	    return 0; 
	}



	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "nl" ) ==  0 ) || ( strcmp( argv[1] ,   "nluug" ) ==  0 ) )
	{
		nsystem( "  ncftp ftp.nluug.nl  " ); 
	        ///nsystem( " ncftp  https://ftp.nluug.nl/os/NetBSD/NetBSD-7.1.1/i386/  "); 
		return 0;
	}



        /// time  dd if=/dev/zero bs=4096 count=9048576 > /dev/null  
	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "--benchmark" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--testdisk" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--minibenchmark" ) ==  0 ) )
	{
                nsystem( " dd if=/dev/zero bs=4096 count=9048576 > /dev/null   " ); 
		return 0; 
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] , "panbootstrap" ) == 0 ) 
	{
	        ///printf( "   debootstrap  --include=debootstrap,less,kbd,gcc,clang,busybox   squeeze   .    http://archive.debian.org/debian  "); 
	        nsystem( "   debootstrap  --include=debootstrap,less,kbd,gcc,mtd-utils,gcc,make,screen,mpg123,links,alsa-utils,wpasupplicant,wireless-tools,tmux,screen,sshfs,ncftp,unzip,dosfstools   jessie   .    http://archive.debian.org/debian  "); 
		return 0; 
	}





        if ( argc == 2 )
        if ( strcmp( argv[1] , "revserver" ) == 0 )  
        {
	      strncpy( cmdi,  " ssh -C -R 2222:localhost:22  pi@", PATH_MAX );
	      strncat( cmdi , "10.0.0.100" , PATH_MAX - strlen( cmdi ) -1 );
	      strncat( cmdi , " -p 22 "  , PATH_MAX - strlen( cmdi ) -1 );
	      nsystem( cmdi );
	      return 0;
	}





	if ( argc == 2 )
	if ( strcmp( argv[1] , "logo" ) ==  0 ) 
	{
		fetch_file_ftp( "http://netbsd.org/images/NetBSD-smaller-tb.png" );
		nsystem( " cp NetBSD-smaller-tb.png logo.png " );
		return( 0 );
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "root" ) ==  0 ) 
	{
	      system( "  mount | grep '/ ' " ); 
              return 0; 
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "charging" ) ==  0 ) 
	{
            printf( " cat   '/sys/devices/platform/ff3d0000.i2c/i2c-4/4-0062/power_supply/cw2015-battery/voltage_now' " ); 
            printf( "\n" ); 
            printf( "\n" ); 
            printf( " Power supply usb, 5.1v,3.0A  15.3w of rpi4:    3856000 \n" ); 
            printf( " XO output 5v,2.4A                   3814000 \n" ); 
            printf( " Samsung NO output 5v,2.0A           3822000 \n" ); 
            printf( "\n" ); 
            printf( " USB Acer usb3.0:                    3787000 \n" ); 
            printf( " USB Acer usb2.0:                    3723000 \n" ); 
            printf( "\n" ); 
            printf( " USB rpi3b:                          3439000 \n" ); 
            printf( "\n" ); 
            printf( " usb zero, nothing:                  3439000    \n" ); 
            printf( " white power rpi3b, is output 5.1v,2.5A,12.75w  \n" ); 
            printf( "\n" ); 
	    return 0; 
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] , "screenfontsize" ) ==  0 ) 
	{
                   nsystem( " setfont /usr/share/consolefonts/Lat15-TerminusBold14.psf.gz " ); 
	  	   return 0;
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] , "--hostname" ) ==  0 ) 
	{
		if ( fexist( "/etc/hostname" ) == 1 ) 
		{
		  ncmdwith( " hostname ",  argv[ 2 ] );
		  snprintf( charo , sizeof( charo ), " echo '%s' > /etc/hostname ", argv[ 2 ] );   
		  nsystem( charo );
		}
		return 0;
	}


	if ( argc == 2)
	if ( strcmp( argv[1] , "max" ) ==  0 ) 
	{
		nsystem( "  wmctrl -r :ACTIVE: -b toggle,maximized_vert,maximized_horz   " );
		return 0;
	}

	if ( argc == 2)
	if ( strcmp( argv[1] , "cmmax" ) ==  0 ) 
	{
		nsystem( "  cmwmctrl -r :ACTIVE: -b toggle,maximized_vert,maximized_horz   " );
		return 0;
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "vlc" ) ==  0 )  
	{
                printf( " mconfig --audio-player *.mp4\n" );
                printf( " cat guitar4.wav | cvlc -q --play-and-exit -\n" );
                printf( " cvlc --no-video  -q --play-and-exit   *.mp4     <-- non working \n" );
		return 0; 
	}





	if ( argc >= 3 )
	if (  ( strcmp( argv[1] , "mpv-playlist" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-mpv-playlist" ) ==  0  )  
	|| ( strcmp( argv[1] , "--mpv-playlist" ) ==  0 )  )  
	{
		snprintf( charo , sizeof( charo ), " cat  \"%s\"  | while read -r i  ; do xterm -e  mpv --fs \"$i\" ; sleep 1 ; done  " ,  argv[ 2 ] ); 
		nsystem( charo );
		return 0;
	}











	if ( argc == 3 )
	if ( strcmp( argv[1] ,  "--log" ) ==  0 ) 
	{
		i = 2; 
		snprintf( charo , sizeof( charo ), " echo \"%s\" >> logfile.log ",  argv[ i ] );
		nsystem( charo );
		return 0; 
        }




	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,  "--log-ip" ) ==  0 ) 
	|| ( strcmp( argv[1] ,  "-logfile" ) ==  0 ) 
	|| ( strcmp( argv[1] ,  "--log-ip-txt" ) ==  0 ) 
	|| ( strcmp( argv[1] ,  "--logfile-append" ) ==  0 )  
	|| ( strcmp( argv[1] ,  "--logfile" ) ==  0 ) ) 
	{
		i = 2; 
		snprintf( charo , sizeof( charo ), " echo \"%s\" >> logfile.txt ",  argv[ i ] );
		nsystem( charo );
		return 0; 
        }








	if ( argc == 2 )
	if ( strcmp( argv[1] , "--finddu" ) ==  0 ) 
	{
               //nsystem( " find . -type d -name '*' -print0 | xargs -0 du -hcs " ); 
               nsystem( " find . -name '*' -print0 | xargs -0 du  " ); 
	       return 0; 
        }


	if ( argc == 3 )
	if ( strcmp( argv[1] , "mount" ) ==  0 ) 
	if ( strcmp( argv[2] , "mmc" ) ==  0 ) 
	{
	        nsystem( " mconfig mount /dev/mmcblk* " );   
	        return 0; 
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "fdisk" ) ==  0 ) 
	{
		nsystem( "  fdisk -l "); 
		return 0;
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] , "show" ) ==  0 ) 
	if ( strcmp( argv[2] , "sources" ) ==  0 ) 
	{
	         system( " cat /etc/apt/sources.list " ); 
		 return 0; 
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] , "--dialog-ip" ) ==  0 ) 
	{
              nsystem( " dialog --title 'MessageBox' --msgbox \"Ubuntu rocks! ` ip addr ` \" 20 75  " ); 
	      return 0;
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "amount" ) ==  0 ) 
	{
	        nsystem( " mconfig --mount /dev/mmcblk* ; ls -1 /media/mmcblk1p2/ " ); 
		return 0;
	}




	if ( argc == 3 )
	if ( strcmp( argv[1] ,  "--cleanup" ) ==  0 ) 
	if ( strcmp( argv[2] ,  "sources.list" ) ==  0 ) 
	{
	nsystem( " rm /etc/apt/apt.conf.d/* /etc/apt/trusted.gpg.d/*  /etc/apt/sources.list.d/*  /etc/apt/preferences.d/*    " ); 
	nsystem( " rm /etc/apt/gpg  /etc/apt/gpt  /etc/apt/listchanges.conf  /etc/apt/sources.list  /etc/apt/trusted.gpg  " ); 
		return 0; 
        }




	if ( argc == 3 )
	if ( strcmp( argv[1] , "sources.list" ) == 0 ) 
	if ( strcmp( argv[2] , "jessie" ) ==  0 ) 
	{
		printf( "\n" ); 
		printf( "# release 6\n" ); 
		printf( "\n" ); 
		printf( "deb http://archive.debian.org/debian/ jessie main \n" );
		printf( "\n" ); 
		return 0; 
        }




	if ( argc == 3 )
	if ( strcmp( argv[1] , "show" ) ==  0 ) 
	if ( strcmp( argv[2] , "wpa" ) ==  0 ) 
	{
                nsystem( " cat /etc/wpa_supplicant.conf " ); 
		return 0;
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] , "mac" ) ==  0 ) 
	{
              system( "  ip addr | grep -B1 ether  " ); 
	      return 0; 
        }








	if ( argc == 3)
	if ( ( strcmp( argv[1] , "install" ) ==  0 ) || ( strcmp( argv[1] , "ins" ) ==  0 ) )
	if ( strcmp( argv[2] , "beamer" ) ==  0 ) 
	{
				/*
				nsystem( " apt-get update " );
				nsystem( " apt-get install --no-install-recommends  -y texlive    " );
				nsystem( " apt-get update ; apt-get install --no-install-recommends  -y texlive  ; apt-get install -y --no-install-recommends  texlive texlive-latex-extra texlive-font-utils texlive-font-utils netpbm texlive-font-utils netpbm             screen mupdf " );
				printf( " To make sure that it is installed, we can run this ...\n" );
				nsystem( " apt-get update " );
				nsystem( " apt-get install --no-install-recommends  -y texlive  mupdf screen   " );
				nsystem( " apt-get install -y --no-install-recommends  texlive texlive-latex-extra texlive-font-utils " );
				nsystem( " apt-get install --no-install-recommends  -y netpbm  " );
				npkg( " sshfs " );
				npkg( " mupdf " );
				npkg( " screen " );
				*/
				nsystem( " apt-get update " );
				nsystem( " apt-get install --no-install-recommends  -y texlive    " );
				nsystem( " apt-get update ; apt-get install --no-install-recommends  -y texlive  ; apt-get install -y --no-install-recommends  texlive texlive-latex-extra texlive-font-utils texlive-font-utils netpbm texlive-font-utils netpbm             screen mupdf " );
				printf( " To make sure that it is installed, we can run this ...\n" );
				nsystem( " apt-get update " );
				nsystem( " apt-get install --no-install-recommends  -y texlive  mupdf screen   " );
				nsystem( " apt-get install -y --no-install-recommends  texlive texlive-latex-extra texlive-font-utils " );
				nsystem( " apt-get install --no-install-recommends  -y netpbm  " );
				npkg( " texlive-plaingeneric " ); //soul.sty
				///npkg( " sshfs " );
				npkg( " texlive-latexextra " ); // wallpaper.sty for manjaro 
				npkg( " texlive-bin " );
				npkg( " mupdf " );
				npkg( " screen " );
				npkg( " screen texlive  texlive texlive-latex-extra texlive-font-utils texlive-font-utils netpbm texlive-font-utils netpbm  screen mupdf texlive texlive-latex-extra texlive-font-utils netpbm  " ); //soul.sty
				//printf( " /usr/bin/vim VIM (%d)\n", fexist( "/usr/bin/vim" ) ); 
				//printf( " /usr/local/bin/vim VIM (%d)\n", fexist( "/usr/local/bin/vim" ) ); 
				//printf( "\n" ); 
				//printf( " => /usr/bin/pdflatex (%d)\n", fexist( "/usr/bin/pdflatex" ) ); 
				return 0;
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] , "nosapt" ) ==  0 ) 
	if ( strcmp( argv[2] , "beamer" ) ==  0 ) 
	{
		// nsystem( " apt-get install --no-install-recommends  tmux screen texlive  texlive texlive-latex-extra texlive-font-utils texlive-font-utils netpbm texlive-font-utils netpbm  screen mupdf texlive texlive-latex-extra texlive-font-utils netpbm texlive-plaingeneric " ); //soul.sty
		nsystem( " apt-get install --no-install-recommends  tmux screen texlive  texlive texlive-latex-extra texlive-font-utils texlive-font-utils netpbm texlive-font-utils netpbm  screen mupdf texlive texlive-latex-extra texlive-font-utils netpbm  " ); //soul.sty
		return 0;
	}





	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "psauxgrep" ) ==  0 ) 
	|| ( strcmp( argv[1] , "greppsaux" ) ==  0 ) ) 
	{
			ncmdwith( "  ps aux | grep   " ,   argv[ 2 ]  );
			return 0; 
	}



      if ( argc == 4 )
      if ( strcmp( argv[1] ,  "--grep" ) ==  0 ) 
      {
               // h | gr
	       snprintf( foostr , sizeof( foostr ), " cat '%s' | grep \"%s\" " , argv[ 2 ] , argv[ 3 ] ); 
	       system( foostr ); 
	       return 0; 
      }



	if ( argc == 2 )
	if ( strcmp( argv[1] , "alsa"  ) ==  0 ) 
	{
		nsystem( " alsamixer " ); 
		return 0; 
        }




	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "bogosum" ) ==  0 ) 
	{
		system( "  awk -F: '/[Bb]ogo[Mm][Ii][Pp][Ss]/ {sum+=$2} END {print sum}' /proc/cpuinfo  " );    
		system( "  cat /proc/cpuinfo | awk -F: '/bogomips/ {sum+=$2} END {print sum}' " ); 
		return 0;
	}




	if ( argc >= 3 )
	if ( ( strcmp( argv[1] ,  "--play" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--video-player" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--video-play" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "-video-play" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--play-video" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--video" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "-play-video" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--video-play" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "videoplay" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "videoplayer" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "-videoplayer" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--videoplayer" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--videoplayer-fs" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--mpv-video" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--mpv" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--mpv-nv" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--mpv-no-video" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--mpv-fs" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--mpvfs" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--mpv-no-audio" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--mpv-loop" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--player" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--video-player" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--mplayer" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--vlc-hw1" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--vlc-hw0" )    ==  0 ) 

	|| ( strcmp( argv[1] ,  "--mpv-vox11" )    ==  0 ) 
        // mconfig --mpv-vox11 
	|| ( strcmp( argv[1] ,  "--mpv-x" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--mpv-x11" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--player-video" )    ==  0 ) 
	|| ( strcmp( argv[1] ,  "--movieplayer" )    ==  0 ) )
	{
		/*
		mpv: 
		   --no-audio        do not play sound
		   --no-video        do not play video
		*/

		printf( "   mpv: %d \n",  fexist(  "/usr/bin/mpv"  ) );
		printf( "   vlc: %d \n",  fexist(  "/usr/bin/vlc"  ) );

		for( i = 2 ; i < argc ; i++) 
		{
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );

	                if ( strcmp( argv[1] ,  "--vlc-hw1" )    ==  0 ) 
			   ncmdwith( "  cvlc -q --play-and-exit  --alsa-audio-device=plughw:1,0  " , argv[ i ]  );

	                else if ( strcmp( argv[1] ,  "--vlc-hw0" )    ==  0 ) 
			   ncmdwith( "  cvlc -q --play-and-exit  --alsa-audio-device=plughw:0,0  " , argv[ i ]  );

	                else if ( strcmp( argv[1] ,  "--mpv-nv" )    ==  0 ) 
			   // no video 
			   ncmdwith( " /usr/bin/mpv --no-video " , argv[ i ]  );

	                else if ( strcmp( argv[1] ,  "--mpv-no-video" )    ==  0 ) 
			   // no video 
			   ncmdwith( " /usr/bin/mpv --no-video " , argv[ i ]  );


	                else if ( ( strcmp( argv[1] ,  "--mpv-fs" )  == 0 ) && ( fexist( "/usr/bin/mpv" ) == 1 ) ) 
			   ncmdwith( " /usr/bin/mpv --fs " , argv[ i ]  );

	                else if ( ( strcmp( argv[1] ,  "--mpvfs" )  == 0 ) && ( fexist( "/usr/bin/mpv" ) == 1 ) ) 
			   ncmdwith( " /usr/bin/mpv --fs " , argv[ i ]  );

	                //|| ( strcmp( argv[1] ,  "--mpv-no-audio" )    ==  0 ) 
	                else if ( ( strcmp( argv[1] ,  "--mpv-no-audio" )  == 0 ) && ( fexist( "/usr/bin/mpv" ) == 1 ) ) 
			   ncmdwith( " /usr/bin/mpv --fs --no-audio " , argv[ i ]  );

	                else if ( ( strcmp( argv[1] ,  "--mpv-fs" )  == 0 ) && ( fexist( "/usr/bin/cvlc" ) == 1 ) ) 
			   ncmdwith( " /usr/bin/cvlc  --fullscreen " , argv[ i ]  );

	                else if ( ( strcmp( argv[1] ,  "--mpv-vox11" )  == 0 ) && ( fexist( "/usr/bin/mpv" ) == 1 ) ) 
			   ncmdwith( " /usr/bin/mpv   --vo=x11 --fs " , argv[ i ]  );

	                else if ( ( strcmp( argv[1] ,  "--mpv-loop" )  == 0 ) && ( fexist( "/usr/bin/mpv" ) == 1 ) ) 
			   ncmdwith( " /usr/bin/mpv   --loop  " , argv[ i ]  );

	                else if ( ( strcmp( argv[1] ,  "--mpv-loop" )  == 0 ) && ( fexist( "/usr/bin/cvlc" ) == 1 ) ) 
			   ncmdwith( "   mconfig --vlc  " , argv[ i ]  );

	                else if ( ( strcmp( argv[1] ,  "--mpv-x" )  == 0 ) && ( fexist( "/usr/bin/mpv" ) == 1 ) ) 
			   ncmdwith( " /usr/bin/mpv   --vo=x11 --fs " , argv[ i ]  );

	                else if ( ( strcmp( argv[1] ,  "--mpv-x11" )  == 0 ) && ( fexist( "/usr/bin/mpv" ) == 1 ) ) 
			   ncmdwith( " /usr/bin/mpv   --vo=x11 --fs " , argv[ i ]  );

	                else if ( ( strcmp( argv[1] ,  "--mpv" )  == 0 ) && ( fexist( "/usr/bin/mpv" ) == 1 ) ) 
			{
			   ncmdwith( " /usr/bin/mpv  " , argv[ i ]  );
			}
			else 
			{
		           procedure_video_player( argv[ i ] );   
			}
		}
		printf( " ============== \n "); 
		return 0;
	}







	if ( argc >= 3 )
	if ( strcmp( argv[1] , "--fexist" ) ==  0 ) 
	if ( strcmp( argv[2] , "" ) !=  0 ) 
	{
		for( i = 2 ; i < argc ; i++) 
		{
		   ///if ( fexist( argv[ i ] ) == 1 ) 
		   {
		        printf( " ============== \n "); 
			printf( "[FILE] => %d/%d, File: (%s): \n", i , argc , argv[ i ] );
		        //strncpy( cmdi , "  " , PATH_MAX );
			//snprintf( charo , sizeof( charo ), " cat \"%s\" ",  argv[ i ] );
			//nsystem(  charo );
		        printf( " ============== \n "); 
	           }
		}
		return 0; 
	}








	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "player" ) ==  0 ) || ( strcmp( argv[2] , "players" ) ==  0 ) ) 
	{
		// v4l2
		printf( " nconfig --vidplay video.mov \n" ); 

		printf( " ============================================== \n" ); 
		printf( " > System: Available Video Player and software.\n" );
		printf( " ============================================== \n" );   

		printf( "   xterm: %d \n", fexist( "/usr/bin/xterm"    ) );
		printf( "   screen: %d \n", fexist( "/usr/bin/screen"  ) ); 

		printf( "   wget: %d \n", fexist( "/usr/bin/wget"  ) );
		printf( "   curl: %d \n", fexist( "/usr/bin/curl"  ) );

		printf( "   mpv: %d \n",  fexist(  "/usr/bin/mpv"  ) );
		printf( "   vlc: %d \n",  fexist(  "/usr/bin/vlc"  ) );

		printf( "   cvlc: %d \n",  fexist(  "/usr/bin/cvlc"  ) );

		printf( "   parole: %d \n",  fexist(  "/usr/bin/parole"  ) );  // parole -i 

		printf( "   mpv: %d \n",  fexist( "/usr/bin/mpv"  ) );
		printf( "   mplayer: %d \n", fexist( "/usr/bin/mplayer"  ) );

		printf( "   ffplay: %d \n", fexist( "/usr/bin/ffplay"  ) );
		printf( "   ffmpeg: %d \n", fexist( "/usr/bin/ffmpeg"  ) );


		printf( "   python: %d \n",  fexist( "/usr/bin/python"  ) );
		printf( "   mpg123: %d \n", fexist( "/usr/bin/mpg123"  ) );
		printf( "   alsamixer: %d \n", fexist( "/usr/bin/alsamixer"  ) );

	        printf( " BSD freebsd: wavplay    \n" ); 
		// openbsd 
	        printf( " BSD netbsd:  audioplay  \n" ); 

		printf( " /usr/bin/totem (%d)\n", fexist( "/usr/bin/totem" ) ); 
		printf( " /usr/bin/ffmpeg (%d)\n", fexist( "/usr/bin/ffmpeg" ) ); 
		return 0; 
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] , "--play-audio" ) ==  0 ) 
	{
	      procedure_audio_player( argv[ 2 ] );   
	      return 0; 
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] , "acer-rc" ) ==  0 ) 
	{
		printf( "wlans_ath0=\"wlan0\"\n" );
		printf( "ifconfig_wlan0=\"WPA DHCP\"\n" );
		printf( "ifconfig_wlan0_ipv6=\"inet6 accept_rtadv\"\n" );
		printf( "sshd_enable=\"YES\"\n" );
		printf( "# Set dumpdev to \"AUTO\" to enable crash dumps, \"NO\" to disable\n" );
		printf( "dumpdev=\"AUTO\"\n" );
		printf( "keymap=de \n" );
		printf( "ntpd_enable=\"YES\"  \n" );
		printf( "ntpd_sync_on_start=\"YES\"  \n" );
		printf( "sshd_enable=\"YES\"  \n" );
		printf( "fusefs_enable=\"YES\"  \n" );
		printf( "# allscreens_flags=\"-f terminus-b32\"  \n" );
		printf( "#apache24_enable=\"YES\"  \n" );
		printf( "fsck_y_enable=\"YES\"  \n");
		return 0; 
        }




	if ( argc == 3)
	if ( ( strcmp( argv[1] ,   "install" ) ==  0 ) || ( strcmp( argv[1] ,   "ins" ) ==  0 ) )
	if ( strcmp( argv[2] ,   "xfs" ) ==  0 ) 
	{
	        npkg( " xfsprogs " ); 
		return 0; 
        }


	if ( argc == 3)
	if ( ( strcmp( argv[1] ,   "install" ) ==  0 ) || ( strcmp( argv[1] ,   "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] ,   "scanner" ) ==  0 ) || ( strcmp( argv[2] ,   "scanner+" ) ==  0 ) )
	{
				nsystem( " apt-get update " ); 
				//nsystem( "  apt-get install -y gpicview  " );
				///nsystem( "  apt-get install -y rox-filer    " );
				nsystem( "  apt-get install -y --no-install-recommends sane " );
				npkg(    "   sane " );
				npkg(    "   xsane " );
				nsystem( "   apt-get install -y sane-utils  " );
				npkg(    "  netpbm  " ); 

				npkg(    " sane      " );
				npkg(    " scanimage " );
				npkg(    " xpaint    " );
				npkg(    " mupdf     " );

				printf(  "==================================\n" ); 
				printf(  "HELP\n" ); 
				printf(  "   scanimage --mode color                    >> image-scan-LD-img.ppm  " ); 
				printf(  "\n" ); 
				printf(  "   scanimage --mode color  --resolution 900  >> image-scan-HD-img.ppm  " );
				printf(  "\n" ); 
				printf(  "==================================\n" ); 
				if ( strcmp( argv[2] ,   "scanner+" ) ==  0 ) 
				{
				    npkg( " rox-filer mupdf netpbm  " ); 
				    npkg( " xsane " ); 
				    npkg( " xpaint  " ); 

				    npkg( " gimp " ); // from gimp  
				    npkg( " sane  " ); // how to scan with same size, gimp, create/new and select xsane -> devices 
				}
				nsystem( "  apt-get install -y xpaint    " );
				return 0;
	}







        ///////////
	if ( argc == 3)
	if ( strcmp( argv[1] , "set" ) ==  0 ) 
	if ( strcmp( argv[2] , "time" ) ==  0 ) 
	{
		if ( MYOS == 1 )  nsystem( " /usr/sbin/ntpdate-debian " ); 
		if ( MYOS == 4 ) 
			nsystem( "  pkg install -y openntpd ; tzsetup ; service ntpd restart  " );
		else 
			nsystem( " dpkg-reconfigure tzdata " );
		if ( MYOS == 1 )  npkg( " ntpdate " );

		//new 
		if ( MYOS == 1 )  nsystem( " /usr/sbin/ntpdate-debian " ); 
		return 0;
	}







	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "date" ) ==  0 ) || ( strcmp( argv[1] ,   "time" ) ==  0 ) ) 
	{
	       system( " export TZ=Europe/Amsterdam ; date " );
	       return 0; 
        }









	if ( argc == 3)
	if ( strcmp( argv[1] , "set" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "locales" ) ==  0 ) || ( strcmp( argv[2] , "locale" ) ==  0 ) 
	|| ( strcmp( argv[2] , "utf8" ) ==  0 ) || ( strcmp( argv[2] , "utf" ) ==  0 ) )
	{
				//nsystem( " apt-get install -f -y console-data locales" );
				if ( MYOS == 1 ) 
				{
					nsystem( " dpkg-reconfigure locales " );
					nsystem( " locale " );
					nsystem( " echo LANG=C.UTF-8 > /etc/default/locale  " );
					//void_system_set_locale( );
				}
				return 0;
	}











	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "autox11" ) ==  0 ) || ( strcmp( argv[1] , "autoxorg" ) ==  0 ) )
	{
	     printf( " >> 1.) FIX and prepare installation ...\n" ); 
	     nsystem( " mkdir /usr/local/     " ); 
	     nsystem( " mkdir /usr/local/bin/ " ); 
	     nsystem( " mkdir /usr/src/ ; chmod 777 /usr/src/ " ); 
	     nsystem( " mconfig cure " ); 

	     printf( " >> 2.) update packages ...\n" ); 
	     npkg_update( ); 

	     printf( " >> 3.) installation ...\n" ); 
	     nsystem( " mconfig install xorg        " ); 
	     nsystem( " mconfig xnetbase " ); 
	     nsystem( " mconfig install audio       " ); 
	     npkg( " ctwm  " ); 

	     nsystem( " mconfig install fltk  " ); 
             npkg( " screen " ); 

	     nsystem( " mconfig install xset " );   
	     return 0; 
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] , "install123" ) ==  0 ) 
	{
	     nsystem( "  mconfig netbase   " ); 
	     nsystem( "  mconfig xnetbase   " ); 
	     return 0; 
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] , "autoinstall" ) ==  0 ) 
	{
	     printf( " >> 1.) FIX and prepare installation ...\n" ); 
	     nsystem( " mkdir /usr/local/     " ); 
	     nsystem( " mkdir /usr/local/bin/ " ); 
	     nsystem( " mkdir /usr/src/ ; chmod 777 /usr/src/ " ); 
	     nsystem( " mconfig cure " ); 

	     printf( " >> 2.) update packages ...\n" ); 
	     npkg_update( ); 

	     nsystem( " mconfig netbase " ); 
	     nsystem( " mconfig install xorg        " ); 
	     nsystem( " mconfig install xorg        " ); 

	     printf( " >> 3.) installation ...\n" ); 
	     nsystem( " mconfig netbase " ); 
	     nsystem( " mconfig xnetbase " ); 

	     nsystem( " mconfig install xorg        " ); 

	     nsystem( " mconfig install audio       " ); 
	     npkg( " mupdf    " ); 
	     npkg( " mpg123   " ); 
	     npkg( " links   " ); 
	     nsystem( " mconfig install svn  " ); 
	     nsystem( " mconfig install fltk  " ); 
             npkg( " screen " ); 
	     nsystem( " mconfig install xset " );   
	     if ( MYOS == 1 )
	     if ( fexist( "/usr/bin/emulationstation" ) == 1 ) 
	     if ( fexist( "/usr/bin/apt-get" ) == 1 ) 
	     {
	          nsystem( " mconfig autologin >   /usr/bin/emulationstation " ); 
	          nsystem( " mconfig overscan  >>  /boot/config.txt  " ); 
		  nsystem( " mconfig install xset " );   // xset  

		  printf( "   echo 'disable_overscan=1' >> /boot/config.txt ");
		  printf( "   echo LANG=C.UTF-8 > /etc/default/locale  ");

		  nsystem( " screen -d -m  sudo        mconfig legacy   "); 
		  nsystem( " screen -d -m  sudo -u pi  mconfig evilwm   "); 

		  printf( "   chmod 777   /home/pi/.xinitrc       ");
		  printf( "   chmod 777   /home/pi/.xsession       ");

		  printf(  ">> Settings: timezone to Paris\n" );
		  nsystem( " echo  'Europe/Paris'   >  /etc/timezone  " );
	     }
	     printf(  ">> Installation Completed.\n" );
	     return 0; 
	}





	if ( argc == 2 )
	if ( strcmp( argv[1] , "autologin" ) ==  0 ) 
	{
		printf( "#!/bin/bash\n" );
		printf( "\n" );
		printf( "if [[ $(id -u) -eq 0 ]]; then\n" );
		printf( "    echo \"emulationstation should not be run as root. If you used 'sudo emulationstation' please run without sudo.\"\n" );
		printf( "    exit 1\n" );
		printf( "fi\n" );
		printf( "\n" );
		printf( "if [[ \"$(uname --machine)\" != *86* ]]; then\n" );
		printf( "    if [[ -n \"$(pidof X)\" ]]; then\n" );
		printf( "        echo \"X is running. Please shut down X in order to mitigate problems with losing keyboard input. For example, logout from LXDE.\"\n" );
		printf( "        exit 1\n" );
		printf( "    fi\n" );
		printf( "fi\n" );
		printf( "\n" );
		printf( "# save current tty/vt number for use with X so it can be launched on the correct tty\n" );
		printf( "TTY=$(tty)\n" );
		printf( "export TTY=\"${TTY:8:1}\"\n" );
		printf( "\n" );
		printf( "clear\n" );
		printf( "tput civis\n" );
		printf( "\n" );
		printf( "sudo mconfig legacy\n" );
		printf( "\n" );
		printf( "sudo -u pi  startx\n" );
		printf( "\n" );
		printf( " cd ; /opt/retropie/supplementary/emulationstation/emulationstation.sh " );    
		printf( "\n" );
		printf( "\n" );
		printf( "\n" );
		return 0; 
	}











	if ( argc == 2)
	if ( strcmp( argv[1] , "gg" ) ==  0 ) 
	{
	    if ( fexist(  "/usr/bin/links" ) == 1 ) 
		nsystem( " links  http://www.google.com " );
	    else if ( fexist(  "/usr/local/bin/links" ) == 1 ) 
		nsystem( " links  http://www.google.com " );
	    else if ( fexist(  "/usr/local/bin/cmlinks" ) == 1 ) 
		nsystem( " cmlinks http://www.google.com " ); /// no ssl on linux pandora, handshake 
	    else
		nsystem( " links http://www.google.com " );
 	    return 0;
	}






/*
You have searched for files named soul.sty in suite bullseye, all sections, and all architectures. Found 2 results.
File	Packages
/usr/share/texlive/texmf-dist/tex/generic/soul/soul.sty	texlive-plain-generic [not mips]
/usr/share/texlive/texmf-dist/tex/latex/soul/soul.sty	texlive-latex-extra [mips]
*/
	if ( argc == 3)
	if ( strcmp( argv[1] , "install" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "soul" ) ==  0 ) 
	{
	   npkg(  " texlive-plain-generic " );  // soul.sty 
	   return 0; 
	}


	if ( argc == 3)
	if ( strcmp( argv[1] , "install" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "tex" ) ==  0 ) 
	{
				/// wallpaper.sty and titling.sty are not installed.!!!!!
				if ( MYOS == 1 )  // linux 
				{
					nsystem( " apt-get update ; apt-get install -y --no-install-recommends texlive " );
					npkg( " mupdf  " );  
					npkg( " screen " );  
				}
				else 
				{
					npkg_update(); 
					npkg( "  mupdf " );  
					npkg( "  tex-latex-bin " );
					npkg( "  tex-caption " ); // for mrk docs
					npkg( "  tex-pdfpages " ); // for mrk docs
					npkg( "  screen " );  
					// gs for eps??
				}
				return 0;
	}










	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "--adb" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "dcim" ) ==  0 ) 
	{
		printf( "===\n" );
		printf("%d\n", (int)time(NULL));
		//usleep(   10 * 20 * 10000 );     // 2sec
		//nsystem( " dhclient -v wlan0 " ); 
		nsystem( " adb pull /sdcard/DCIM " ); 
		nsystem( " adb pull '/sdcard/Voice Recorder'   " ); 
		return 0;
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "--loop" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "dhclient" ) ==  0 ) 
	{
			while( 1 ) 
			{
				printf( "===\n" );
				printf("%d\n", (int)time(NULL));
				usleep(   10 * 20 * 10000 );     // 2sec
				nsystem( " dhclient -v wlan0 " ); 
			}
			return 0;
	}



	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "watch" ) ==  0 ) || ( strcmp( argv[1] , "-watch" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--watch" ) ==  0 ) )
	{

			if ( argc >= 2)
			{
				strncpy( cmdi, " ", PATH_MAX );
				strncpy( cmdi, " ", PATH_MAX );
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip) (command)\n", i, argc-1 ,  argv[ i ] );
					}
					else if ( i >= 2 )
					{
						printf( "=====================================================.\n" );
						printf( "%d/%d: %s (add to arguments) (command line).\n", i, argc-1 ,  argv[ i ] );
						printf( "=-=-=-=\n" ); 
						strncat( cmdi , "  " , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , "  " , PATH_MAX - strlen( cmdi ) -1 );
						printf( "=====================================================.\n" );
					}
				}

				while( 1 ) 
				{
					printf( "===\n" );
					printf("%d\n", (int)time(NULL));
					nsystem( cmdi );
					system( " sleep 3 " ); 
					usleep(   10 * 20 * 10000 * 10 );     // 2sec * 10
				}
			}
			return 0;
		}










	if ( argc >= 3 )
	if ( strcmp( argv[1] , "--linewatch" ) ==  0 ) 
	{

			if ( argc >= 2)
			{
				strncpy( cmdi, " ", PATH_MAX );
				strncpy( cmdi, " ", PATH_MAX );
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip) (command)\n", i, argc-1 ,  argv[ i ] );
					}
					else if ( i >= 2 )
					{
						printf( "=====================================================.\n" );
						printf( "%d/%d: %s (add to arguments) (command line).\n", i, argc-1 ,  argv[ i ] );
						printf( "=-=-=-=\n" ); 
						strncat( cmdi , "  " , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , "  " , PATH_MAX - strlen( cmdi ) -1 );
						printf( "=====================================================.\n" );
					}
				}

				while( 1 ) 
				{
					printf( "===\n" );
					printf("%d\n", (int)time(NULL));
					system( cmdi );
					system( " sleep 3 " ); 
					usleep(   10 * 20 * 10000 * 1 );     // 2sec * 10
				}
			}
			return 0;
		}




























	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "loader" ) ==  0 ) || ( strcmp( argv[1] , "loader.conf" ) ==  0 ) )
	{
		printf( "\n" );
		printf( "# screen.depth=16\n" );
		printf( "# screen.font=14x28\n" );
		printf( "# screen.width=1366\n" );
		printf( "# screen.height=768\n" );
		printf( "# screen.textmode=1\n" ); //  <--- it should be one in order to get intel??? !
		printf( "# kern.vt.fb.default_mode=0x17e\n" );
		printf( "kern.vt.fb.default_mode=0x17e\n" );
		printf( "\n" );
		printf( "# Use new virtual terminal driver\n" );
		printf( "# Recommended for Intel i915drm\n"); 
		printf( "# hw.vga.textmode=1\n" );
		printf( "\n" );
		printf( "kern.geom.debugflags=16\n" );
		printf( "\n" );
                return 0; 
	}











	if ( argc == 2 )
	if ( strcmp( argv[1] , "0x17e" ) ==  0 ) 
	{
		printf( "\n" );
		printf( "\n" );
		printf( "#########################\n" );
		printf( "## it will force fb, to get text on fb, but startx X wont work.\n" );
		printf( "kern.vt.fb.default_mode=0x17e\n" );
		printf( "\n" );
		printf( "\n" );
                return 0; 
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] , "textmode" ) ==  0 ) 
	{
		printf( "\n" );
		printf( "#########################\n" );
		printf( "# it will force fb, to get text on fb, but startx X wont work.\n" );
		printf( "screen.textmode=0\n" ); //  <--- it should be one in order to get intel??? !
		printf( "# kern.vt.fb.default_mode=0x17e\n" );
		printf( "kern.vt.fb.default_mode=0x17e\n" );
		printf( "\n" );
		printf( "kern.geom.debugflags=16\n" );
		printf( "\n" );
                return 0; 
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] , "set" ) ==  0 ) 
	if ( strcmp( argv[2] , "textmode" ) ==  0 ) 
	{
		if (  os_bsd_type == 1 ) // freebsd, it can do devs 
		{
		    nsystem( " mconfig textmode >> /boot/loader.conf " ); 
		}
                return 0; 
	}













	if ( argc == 3 )
	if ( strcmp( argv[1] ,     "menu" ) ==  0 ) 
	if ( strcmp( argv[2] ,     "freebsd" ) ==  0 ) 
	{
			printf(      "1.> AARCH64 (Raspberry Pi,...).\n" ); 
			printf(      "m.> memstick i386 (Intel, ...).\n" ); 
			printf(      "l.> memstick (with links) i386 (Intel, i386,...).\n" ); 
			printf(      "o.> cdrom iso i386 (Intel,...).\n" ); 
			printf(      "d.> dvd iso i386 (Intel, i386,...).\n" ); 
			printf(      "w.> qcow2 i386 freebsd 13 (Intel, i386,...).\n" ); 

			printf(      "5.> freebsd 13 iso disc1 i386 \n" ); 
			printf(      "    namely: https://download.FreeBSD.org/releases/i386/i386/ISO-IMAGES/13.0/FreeBSD-13.0-RELEASE-i386-disc1.iso "); 

			printf( "i.> Abort \n" ); 
			printf( "=> Choice [?]\n" ); 
			fookey = ckeypress();

			if ( fookey == '1' ) 
			{
				fetch_file_ftp( "https://download.freebsd.org/ftp/releases/arm64/aarch64/ISO-IMAGES/12.1/FreeBSD-12.1-RELEASE-arm64-aarch64-RPI3.img.xz" );
                                fetch_file_ftp( "https://download.freebsd.org/ftp/releases/arm64/aarch64/ISO-IMAGES/13.0/FreeBSD-13.0-RELEASE-arm64-aarch64-RPI.img.xz" ); 
			}
			else if ( fookey == '5' ) 
			     fetch_file_ftp( "https://download.FreeBSD.org/releases/i386/i386/ISO-IMAGES/13.0/FreeBSD-13.0-RELEASE-i386-disc1.iso" ); 

			else if ( fookey == 'w' ) 
                             fetch_file_ftp( "https://download.freebsd.org/ftp/releases/VM-IMAGES/13.0-RELEASE/i386/Latest/FreeBSD-13.0-RELEASE-i386.qcow2.xz" ); 

			else if ( fookey == 'm' ) 
		  	     fetch_file_ftp( "https://download.freebsd.org/ftp/releases/i386/i386/ISO-IMAGES/13.0/FreeBSD-13.0-RELEASE-i386-memstick.img.xz" ); 

			else if ( fookey == 'l' ) 
			{
			    if ( fexist( "/usr/local/bin/cmlinks" ) == 1 )   
				nsystem( "  /usr/local/bin/cmlinks  https://download.freebsd.org/ftp/releases/i386/i386/ISO-IMAGES/13.0/FreeBSD-13.0-RELEASE-i386-memstick.img.xz " ); 
		 	    else
				nsystem( "  links  https://download.freebsd.org/ftp/releases/i386/i386/ISO-IMAGES/13.0/FreeBSD-13.0-RELEASE-i386-memstick.img.xz " ); 
			}


			else if ( fookey == 'o' ) 
				fetch_file_ftp( "https://download.freebsd.org/ftp/releases/i386/i386/ISO-IMAGES/13.0/FreeBSD-13.0-RELEASE-i386-disc1.iso" );


			else if ( fookey == 'd' ) 
                                fetch_file_ftp( "https://download.freebsd.org/releases/i386/i386/ISO-IMAGES/13.0/FreeBSD-13.0-RELEASE-i386-dvd1.iso" ); 


			return 0;
	}























	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "pidesktop" ) ==  0 ) 
	{
		printf( "https://downloads.raspberrypi.org/raspbian/images/raspbian-2017-08-17/2017-08-16-raspbian-stretch.zip" ); 
		printf( "\n" ); 
		printf( "https://downloads.raspberrypi.org/raspios_full_armhf/images/raspios_full_armhf-2022-01-28/2022-01-28-raspios-bullseye-armhf-full.zip" ); 
		printf( "\n" ); 
		/// fetch_file_ftp( "https://downloads.raspberrypi.com/raspios_full_armhf/images/raspios_full_armhf-2023-10-10/2023-10-10-raspios-bookworm-armhf-full.img.xz" ); 
		return 0; 
	}




	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "raspbian-desktop-full" ) ==  0 ) 
	|| ( strcmp( argv[1] , "raspbian-desktop-2017" ) ==  0 ) 
	|| ( strcmp( argv[1] , "raspbian2017" ) ==  0 ) 
	|| ( strcmp( argv[1] , "pidesktop" ) ==  0 ) 
	|| ( strcmp( argv[1] , "pi-desktop" ) ==  0 ) 
	|| ( strcmp( argv[1] , "raspbian-desktop" ) ==  0 ) )
	{
		//fetch_file_ftp( "https://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2017-12-01/2017-11-29-raspbian-stretch-lite.zip" ); 
                //printf( " e7b86f2f0e8654ae47d0f051cdb5bdfe  2017-11-29-raspbian-stretch.zip \n" ); 
                fetch_file_ftp( "https://downloads.raspberrypi.org/raspbian/images/raspbian-2017-12-01/2017-11-29-raspbian-stretch.zip" ); 
                printf( " e7b86f2f0e8654ae47d0f051cdb5bdfe  2017-11-29-raspbian-stretch.zip \n" ); 
		return 0; 
	}

	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "raspbian-stretch" ) ==  0 ) 
	{
             fetch_file_ftp( "https://downloads.raspberrypi.org/raspbian/images/raspbian-2017-08-17/2017-08-16-raspbian-stretch.zip" ); 
	     return 0; 
        }





	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "raspios-full" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "raspios" ) ==  0 ) ) 
	{
		printf( " --- \n" ); 
		printf( "working: \n" ); 
		printf( "xzcat  2024-07-04-raspios-bookworm-armhf-full.img.xz  >  /dev/sda\n" ); 
		printf( "working: \n" ); 
		printf( "3b7ef085e6d660f8380b6dbfd5029d83  2024-07-04-raspios-bookworm-armhf-full.img.xz\n" ); 
		fetch_file_ftp( "https://downloads.raspberrypi.com/raspios_full_armhf/images/raspios_full_armhf-2024-07-04/2024-07-04-raspios-bookworm-armhf-full.img.xz" );   // kernel is  ...  
		return 0; 
	}





		//  fetch_file_ftp( "https://downloads.raspberrypi.com/raspios_armhf/images/raspios_armhf-2023-12-06/2023-12-05-raspios-bookworm-armhf.img.xz"  ); 
		// fetch_file_ftp( "https://downloads.raspberrypi.org/raspios_full_armhf/images/raspios_full_armhf-2023-05-03/2023-05-03-raspios-bullseye-armhf-full.img.xz" ); 
		//fetch_file_ftp( "https://downloads.raspberrypi.org/raspios_full_armhf/images/raspios_full_armhf-2023-05-03/2023-05-03-raspios-bullseye-armhf-full.img.xz" );   // kernel is ...  
		///fetch_file_ftp( "https://downloads.raspberrypi.org/raspios_armhf/images/raspios_armhf-2023-05-03/2023-05-03-raspios-bullseye-armhf.img.xz" );     
		///fetch_file_ftp( "https://downloads.raspberrypi.com/raspios_lite_armhf/images/raspios_lite_armhf-2024-07-04/2024-07-04-raspios-bookworm-armhf-lite.img.xz" ); 
		///fetch_file_ftp( "https://downloads.raspberrypi.com/raspios_armhf/images/raspios_armhf-2024-07-04/2024-07-04-raspios-bookworm-armhf.img.xz" );   // working with hp printer and brlaser !!! bookworm !! with desktop 





	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "raspios-2024" ) ==  0 ) 
	{
		///fetch_file_ftp( "https://downloads.raspberrypi.com/raspios_lite_armhf/images/raspios_lite_armhf-2024-07-04/2024-07-04-raspios-bookworm-armhf-lite.img.xz" ); 
		fetch_file_ftp( "https://downloads.raspberrypi.com/raspios_armhf/images/raspios_armhf-2024-07-04/2024-07-04-raspios-bookworm-armhf.img.xz" );   // working with hp printer and brlaser !!! bookworm !! with desktop 
		return 0; 
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "raspios4all" ) ==  0 )     
	{ 
	    fetch_file_ftp( "https://downloads.raspberrypi.com/raspios_armhf/images/raspios_armhf-2024-03-15/2024-03-15-raspios-bookworm-armhf.img.xz" );  // desktop and recommended software, for all 
            ///fetch_file_ftp( "https://downloads.raspberrypi.com/raspios_arm64/images/raspios_arm64-2023-10-10/2023-10-10-raspios-bookworm-arm64.img.xz" ); 
	    return 0; 
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "raspios-desktop" ) ==  0 ) 
	{
                fetch_file_ftp( "https://downloads.raspberrypi.org/raspios_armhf/images/raspios_armhf-2023-05-03/2023-05-03-raspios-bullseye-armhf.img.xz" );     
		return 0; 
	}












	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "raspios-lite" ) ==  0 ) 
	{
             fetch_file_ftp( "https://downloads.raspberrypi.org/raspios_armhf/images/raspios_armhf-2023-05-03/2023-05-03-raspios-bullseye-armhf.img.xz" );     
	     return 0; 
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] , "raspios-desktop-full" )    ==  0 ) 
	{
             fetch_file_ftp( "https://downloads.raspberrypi.org/raspios_full_armhf/images/raspios_full_armhf-2021-11-08/2021-10-30-raspios-bullseye-armhf-full.zip" );
	     return 0; 
        }










	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,   "cat" ) ==  0 ) || ( strcmp( argv[1] ,   "view" ) ==  0 ) )
	if ( strcmp( argv[2] ,   "pirouter" ) ==  0 ) 
	{
	     printf(  " ======== \n" ); 
             nsystem( " cat  /etc/hostapd/hostapd.conf  "); 
	     printf(  " ======== \n" ); 
	     return 0; 
        }


	if ( argc == 4 )
	if ( strcmp( argv[1] ,   "pirouter" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "forward" ) ==  0 ) 
	if ( strcmp( argv[3] ,   "on" ) ==  0 ) 
	{
           nsystem( "     modprobe   br_netfilter  ;    iptables -F FORWARD "); 
	   return 0; 
	}



	if ( argc == 4 )
	if ( strcmp( argv[1] ,   "pirouter" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "forward" ) ==  0 ) 
	if ( strcmp( argv[3] ,   "off" ) ==  0 ) 
	{
           nsystem( "    modprobe   br_netfilter ;  iptables    -I FORWARD -m physdev --physdev-in wlan0 -j DROP  " ); 
	   return 0; 
	}









	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "boot" ) ==  0 ) 
	{
               printf( " NETBSD NOTEBOOK: boot -2 didn't work. boot -1 didn't work. It appears boot -12 does work  \n"); 
	       printf( " sysboot  \n"); 
	       printf( " cp /usr/mdec/boot /mnt   " );
	       printf( " umount /mnt   " );
	       printf( " installboot -v -o timeout=20 /dev/rsd0a /usr/mdec/bootxx_ffsv2   " );
	       printf( " sync ; installboot -f -o console=pc,speed=9600 /dev/rsd0a /usr/mdec/bootxx_ffsv2  ");  


	       printf( " =========================== \n" ); 
	       printf( " =========================== \n" ); 
	       printf( " FREEBSD BOOT LOADER - beastie: \n" );
	       printf( "menu already gave you the correct value: \n" );
	       printf( "OK> set currdev=\"disk0s2\" \n" );
	       printf( "OK> set vfs.root.mountfrom=\"ufs:/dev/ada0s2a\" \n" );
	       printf( "Followed by boot. \n" );
	       printf( " =========================== \n" ); 
	       printf( " =========================== \n" ); 
	       return 0;
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "netbsd" ) ==  0 ) 
	{
		printf( " Unlike as in Linux, in NetBSD please note /dev/rsd0d represents full disk\n"); 
		printf( " Multiboot: kernel (fd0)/netbsd.gz -c console=pc root=wd0e\n"); 
		printf( " Multiboot: boot hd0a:netbsd   -c console=pc root=wd0a\n"); 
		printf( " installboot -f -o console=pc,speed=9600 /dev/rsd0a /usr/mdec/bootxx_ffsv2  ");  
                printf( "  older netbsd,   grub-install /dev/rwd0d   \n" ); 
                printf( "  ================= \n" ); 
		printf( "   => ifconfig_axen0=dhcp\n" );
		printf( "   => ifconfig_usmsc0=dhcp\n" );  //  on RPI3b  (classic)
		printf( "   => ifconfig_mue0=dhcp\n" );    //  on RPI3b+ (model plus) 
                printf( "  ================= \n" ); 
                printf( "  NetBSD 7.1.1., i386, filesystem wd0a 1.3G, used:731MB, available:537MB \n" ); 
                printf( "  NetBSD 5.1.1., i386, filesystem wd0a 1.3G, used:483MB, available:785MB \n" ); 
                printf( "  ================= \n" ); 
		printf( "    NetBSD-7.1.1-i386\n");
		printf( "    140M    base/\n");
		printf( "    239M    comp/\n");
                printf( "  ================= \n" ); 
		printf( " DRIVERS: https://ftp.netbsd.org/pub/NetBSD/misc/ddwg/NetBSD-driver_writing-1.0.1e.pdf \n" ); 
                printf( "  ================= \n" ); 
		printf( " Image : http://cdn.netbsd.org/pub/NetBSD/NetBSD-9.1/images/NetBSD-9.1-i386-install.img.gz  \n" ); 
		return 0;
	}









	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "netbsd86" ) ==  0 ) 
	|| ( strcmp( argv[1] , "netbsd-memstick" ) ==  0 ) )
	{
		printf( " Image : http://cdn.netbsd.org/pub/NetBSD/NetBSD-9.1/images/NetBSD-9.1-i386-install.img.gz  \n" ); 
	  fetch_file_ftp( "http://cdn.netbsd.org/pub/NetBSD/NetBSD-9.1/images/NetBSD-9.1-i386-install.img.gz" ); 
		return 0;
	}









	if ( argc == 2 )
	if ( strcmp( argv[1] , "7.1.1" ) ==  0 ) 
	{
                  fetch_file_ftp( "http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-7.1.1/i386/binary/kernel/netbsd-GENERIC.gz" ); 
                  fetch_file_ftp( "http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-7.1.1/i386/binary/kernel/netbsd-INSTALL.gz" ); 
                  fetch_file_ftp( "http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-7.1.1/i386/binary/sets/base.tgz" ); 
                  fetch_file_ftp( "http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-7.1.1/i386/binary/sets/comp.tgz" ); 
                  fetch_file_ftp( "http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-7.1.1/i386/binary/sets/etc.tgz" ); 
                  fetch_file_ftp( "http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-7.1.1/i386/binary/sets/modules.tgz" ); 
		  return 0; 
	}






	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "touchpad" ) ==  0 ) || ( strcmp( argv[1] , "pad" ) ==  0 ) )
	{
	     nsystem( " kldload ig4 " ); 
	     return 0;
	}

	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "no-touchpad" ) ==  0 ) || ( strcmp( argv[1] , "nopad" ) ==  0 ) )
	{
	     nsystem( " kldunload ig4 " ); 
	     return 0;
	}






















	if ( argc == 2 )
	if ( strcmp( argv[1] , "openbsd.iso" ) ==  0 ) 
	{
           //fetch_file_ftp( "https://ftp.openbsd.org/pub/OpenBSD/7.0/amd64/cd70.iso" ); 
	   fetch_file_ftp( "https://cdn.openbsd.org/pub/OpenBSD/7.1/amd64/install71.iso" );
	   return 0; 
	}

	if ( argc == 2 )
	if ( strcmp( argv[1] , "netbsd.iso" ) ==  0 ) 
	{
           //fetch_file_ftp( "https://ftp.openbsd.org/pub/OpenBSD/7.0/amd64/cd70.iso" ); 
	   //fetch_file_ftp( "https://cdn.openbsd.org/pub/OpenBSD/7.1/amd64/install71.iso" );
	   fetch_file_ftp(  "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.0/images/NetBSD-9.0-amd64.iso" );
	   fetch_file_ftp(  "https://cdn.netbsd.org/pub/NetBSD/NetBSD-9.2/iso/NetBSD-9.2-amd64.iso" ); 
	   return 0; 
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] ,     "menu" ) ==  0 ) 
	if ( strcmp( argv[2] ,     "netbsd" ) ==  0 ) 
	{
		printf( "|------------------------------|\n" );
		printf( "|   *Select*                   |\n" );
		printf( "| 1: ISO     NetBSD 9.1 AMD64  |\n" );
		printf( "| 2: USB/MMC NetBSD 9.1 AMD64  |\n" );
		printf( "| 3: 7.1.1   i386 iso          |\n" ); 
		printf( "| 4: 7.1.1   i386 mem          |\n" ); 
		printf( "| o: ISO     NetBSD 9.1 i386   |\n" );
		printf( "| f: (ftp) USB/MMC NetBSD 9.1 i386   |\n" );
		printf( "| m: (cdn) USB/MMC NetBSD 9.1 i386   |\n" );
		printf( "|------------------------------|\n" );
		printf( " > Choice ?\n"      );
		choix = getchar();
		if ( choix == '1' ) 
			fetch_file_ftp(  "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/images/NetBSD-9.1-amd64.iso" );  
		else if ( choix == '2' ) 
			fetch_file_ftp(  "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/images/NetBSD-9.1-amd64-install.img.gz"  );
		else if ( choix == '3' ) 
			fetch_file_ftp( "http://archive.netbsd.org/pub/NetBSD-archive/images/7.1.1/NetBSD-7.1.1-i386.iso" );                       
		//fetch_file_ftp( "http://archive.netbsd.org/pub/NetBSD-archive/images/7.1/NetBSD-7.1-amd64-install.img.gz" ); 

		else if ( choix == '4' ) 
			//fetch_file_ftp( "http://archive.netbsd.org/pub/NetBSD-archive/images/7.1/NetBSD-7.1-amd64.iso" );
		        ///http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-7.1.1/images/NetBSD-7.1.1-i386-install.img.gz
			fetch_file_ftp( "http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-7.1.1/images/NetBSD-7.1.1-i386-install.img.gz" );    

		else if ( choix == 'o' ) 
			fetch_file_ftp(  "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/images/NetBSD-9.1-i386.iso" );  
		else if ( choix == 'f' ) 
			fetch_file_ftp(  "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/images/NetBSD-9.1-i386-install.img.gz"  );
		else if ( choix == 'm' ) 
			fetch_file_ftp(  "http://cdn.netbsd.org/pub/NetBSD/NetBSD-9.1/images/NetBSD-9.1-i386-install.img.gz"  );
		return 0; 
	}










	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "armv7" ) ==  0 ) 
	{
		printf( "The default 9.1, working on armv7 rpi3b \n" ); 
		fetch_file_ftp( "http://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/evbarm-earmv7hf/binary/gzimg/armv7.img.gz" ); 
		return 0;
	}









	if ( argc == 3 )
	if ( strcmp( argv[1] , "cvs" ) ==  0 ) 
	if ( strcmp( argv[2] , "netbsd" ) ==  0 ) 
	{
                nsystem( " export CVSROOT='anoncvs@anoncvs.NetBSD.org:/cvsroot' ; export CVS_RSH='ssh' ; cvs checkout -A -P src  " ); 
		return 0;
	}








	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0  ) 
	if ( ( strcmp( argv[2] , "slackware" ) ==  0  ) || ( strcmp( argv[2] , "slack" ) ==  0  ) )
	{
		printf( "\n" );
		printf( "\n" );
		// virtualbox may crash with this:
		printf( "menuentry 'RAMDISK Slackboot Slackware-64 Rescue, from NetBSD Rescue (hd0,msdos1)' {\n" );
		printf( "   set root='hd0,msdos1'\n" );
		printf( "   linux  /slackboot/EFI/BOOT/huge.s\n" );
		printf( "   initrd /slackboot/EFI/BOOT/initrd.img\n" );
		printf( "}\n" );
		printf( "\n" );
		return 0; 
        }




	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "slackware" )    ==  0 ) || ( strcmp( argv[1] ,   "slack" )    ==  0 ) )
	{
		printf( " => 522db1d2845aaab22078530c67f858c1  slackware64-14.2-install-dvd.iso \n" );
		printf( "|-----------------------------------|\n" );
		printf( "|   *Select*                        |\n" );
		printf( "| 2:12 4:14 5:15 \n" ); 
		printf( "|-----------------------------------|\n" );
		printf( " > Choice ?\n"      );
		choix = getchar();
		if ( choix == '4' ) 
		 fetch_file_ftp( "https://mirrors.slackware.com/slackware/slackware-iso/slackware64-14.2-iso/slackware64-14.2-install-dvd.iso" );
		if ( choix == '5' ) 
	 	 fetch_file_ftp( "https://mirrors.slackware.com/slackware/slackware-iso/slackware64-15.0-iso/slackware64-15.0-install-dvd.iso" );  
		if ( choix == '2' ) 
                 fetch_file_ftp( "https://mirrors.slackware.com/slackware/slackware-iso/slackware-12.0-iso/slackware-12.0-install-dvd.iso"     ); 
		printf( " => 522db1d2845aaab22078530c67f858c1  slackware64-14.2-install-dvd.iso \n" );
		return 0;
	}










	if ( argc == 3 )
	if ( strcmp( argv[1] , "debootstrap" ) ==  0 ) 
	if ( strcmp( argv[2] , "debian" ) ==  0 )  /// stable
	{
		nsystem( " debootstrap --no-check-gpg  --include=wpasupplicant  stable . http://ftp.debian.org/debian/    " );  
		return 0;
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] , "debian" ) ==  0 ) 
	{
		printf( "|-----------------------------------|\n" );
		printf( "|   *Select*                        |\n" );
		printf( "| 1: debian  11.2.0, i386, netinst  |\n" ); 
		printf( "| 2: debian  11.2.0, i386, live     |\n" ); 
		printf( "| 3: debian   3.1.x, i386, sarge    |\n" ); 
		printf( "| 4: debian wheezy, i386, inst. iso |\n" ); 
		printf( "|-----------------------------------|\n" );
		printf( " > Choice ?\n"      );
		choix = getchar();
		if ( choix == '1' ) 
		{
                   fetch_file_ftp( "https://cdimage.debian.org/debian-cd/current/i386/iso-cd/debian-11.2.0-i386-netinst.iso" ); 
                   printf(         " -> Installation: qemu-system-x86_64 -machine type=pc -curses -cdrom debian-11.2.0-i386-netinst.iso -kernel /home/user/debian/install.386/vmlinuz -initrd /home/user/debian/install.386/initrd.gz -m 1024 -append ' console=tty0 priority=low '  \n" ); 
		}
		else if ( choix == '2' ) 
	           fetch_file_ftp( "https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current-live/i386/iso-hybrid/debian-live-11.2.0-i386-standard+nonfree.iso" ); 

		else if ( choix == '3' ) 
		   fetch_file_ftp( "https://archive.org/download/debian_3.1r8_i386/debian-31r8-i386-binary-1.iso" );

		else if ( choix == '4' ) 
	              fetch_file_ftp( "https://cdimage.debian.org/mirror/cdimage/archive/7.11.0-live/i386/iso-hybrid/debian-live-7.11.0-i386-standard.iso" ); 
		return 0; 
	}











	if ( argc == 2)
	if ( ( strcmp( argv[1] , "blackbox" ) ==  0 ) || ( strcmp( argv[1] ,   "xblackbox" ) ==  0 ) )
	{

			printf( "============\n" );
			nsystem( " cd ; echo                >    .xinitrc  " );
			nsystem( " cd ; echo  cd            >>   .xinitrc  " );
			nsystem( " cd ; echo setxkbmap de   >>   .xinitrc  " );
			nsystem( " cd ; echo               >> .xinitrc  " );
			nsystem( " cd ; echo  '     xsetroot -solid #3A6EA5 '        >>       .xinitrc  " );
			nsystem( " cd ; echo  '  xsetroot -cursor_name left_ptr '        >>       .xinitrc  " );
			nsystem( " cd ; echo  '    xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 '        >>       .xinitrc  " );
			nsystem( " cd ; echo  ' xsetfork s off ;  xsetfork -dpms ;  xsetfork s noblank  ; setterm -blank 0 ' >> .xinitrc  " );
			nsystem( " cd ; echo  '    '        >>       .xinitrc  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    kicker    &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    xterm      &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    thunar    &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo blackbox    >> .xinitrc  " );
			printf( "============\n" );
		        ///nsystem( "    xrandr -s 800x600  " ); 

			nsystem( " cd ; echo                >    .xsession  " );
			nsystem( " cd ; echo  cd            >>   .xsession  " );
			nsystem( " cd ; echo setxkbmap de   >>   .xsession  " );
			nsystem( " cd ; echo  'xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    kicker    &  '        >>       .xsession  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    xterm      &  '        >>       .xsession  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    thunar    &  '        >>       .xsession  " );
			nsystem( " cd ; echo             >> .xsession  " );
			nsystem( " cd ; echo blackbox    >> .xsession  " );
			printf( "============\n" );
			printf( "============\n" );
			printf( "============\n" );
	                if ( strcmp( argv[1] ,   "xblackbox" ) ==  0 ) 
			{
			   nsystem( "  cd ; startx  "); 
			}
			return 0;
	}


















        if ( argc >= 2 )
	if ( strcmp( argv[1] ,    "getbase" ) ==  0 ) 
	// 2: the release
	// 3: the arch/port
	{
	        if ( argc == 4 )
		{
			printf( " ========= \n" ); 
			snprintf( charo , sizeof( charo ),  "http://cdn.netbsd.org/pub/NetBSD/NetBSD-%s/%s/binary/kernel/netbsd-INSTALL.gz"  , argv[ 2 ]  , argv[ 3 ]   );
			fetch_file_ftp( charo ); 
			snprintf( charo , sizeof( charo ),  "http://cdn.netbsd.org/pub/NetBSD/NetBSD-%s/%s/binary/kernel/netbsd-GENERIC.gz" , argv[ 2 ]  , argv[ 3 ] );
			fetch_file_ftp( charo ); 
			snprintf( charo , sizeof( charo ),  "http://cdn.netbsd.org/pub/NetBSD/NetBSD-%s/%s/binary/sets/base.tar.xz" , argv[ 2 ] , argv[ 3 ] );
			fetch_file_ftp( charo ); 
			snprintf( charo , sizeof( charo ),  "http://cdn.netbsd.org/pub/NetBSD/NetBSD-%s/%s/binary/sets/comp.tar.xz" , argv[ 2 ], argv[ 3 ] );
			fetch_file_ftp( charo ); 
			snprintf( charo , sizeof( charo ),  "http://cdn.netbsd.org/pub/NetBSD/NetBSD-%s/%s/binary/sets/etc.tar.xz" , argv[ 2 ], argv[ 3 ] );
			fetch_file_ftp( charo ); 
			snprintf( charo , sizeof( charo ),  "http://cdn.netbsd.org/pub/NetBSD/NetBSD-%s/%s/binary/sets/kern-GENERIC.tar.xz" , argv[ 2 ] , argv[ 3 ]  );
			fetch_file_ftp( charo ); 
			snprintf( charo , sizeof( charo ),  "http://cdn.netbsd.org/pub/NetBSD/NetBSD-%s/%s/binary/sets/modules.tar.xz" , argv[ 2 ] , argv[ 3 ]  );
			fetch_file_ftp( charo ); 
		}
		return 0; 
	}













	if ( argc == 2 )
	if ( strcmp( argv[1] , "speakers" )    ==  0 ) 
	{

			if ( fexist( "/etc/myname" ) == 1 ) 
			{
				nsystem( "  mixerctl -w  inputs.mic=255 " );
				nsystem( "  mixerctl -w  inputs.mic1=255 " );
				nsystem( "  mixerctl -w  inputs.mic2=255 " );
				nsystem( "  aucat  -f snd/1 -o -  | aucat -i -   " ); 
			}
			else 
			    nsystem( " arecord -D plughw:1 - | aplay -   " );
			return 0;
	}






       if ( argc == 3 )
       if ( strcmp( argv[1] ,    "help" )   ==  0   )
       if ( strcmp( argv[2] ,    "pi" ) ==  0 ) 
       {
                printf( " RPI rpi3b  - older gen cpu -  usmsc0: flags=0x8843<UP,BROADCAST,RUNNING,SIMPLEX,MULTICAST> mtu 1500 \n" ); 
                printf( " RPI rpi3b+ - newer gen cpu -  mue0  (working)\n" ); 
                printf( " https://mrrooster.tumblr.com/post/62694672/netbsd-wpa-wireless-ap \n" ); 
		return 0;
       }





       if ( argc == 3 )
       if ( strcmp( argv[1] ,    "help" )   ==  0   )
       if ( strcmp( argv[2] ,    "wpa" ) ==  0 ) 
       {
                printf( " RPI rpi3b  - older gen cpu -  usmsc0: flags=0x8843<UP,BROADCAST,RUNNING,SIMPLEX,MULTICAST> mtu 1500 \n" ); 
                printf( " RPI rpi3b+ - newer gen cpu -  mue0  (working)\n" ); 
		printf( "   rtl_bt / rtl 8821a _ fw.bin "); 
		printf( "\n" ); 
		printf( "wpa_supplicant -B -i urtwn0 -c /etc/wpa_supplicant.conf  " );
		printf( "\n" ); 
		printf( "wpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant.conf  " );
		printf( "\n" ); 
		printf( "wpa_supplicant -B -i urtwn0 -c /etc/wpa_supplicant.conf  " );
		printf( "\n" ); 
		printf( "    wpa_supplicant -B -i wlp1s0 -c /etc/wpa_supplicant.conf  " );
		printf( "\n" ); 
		printf( "  wpa_supplicant  -i wlp1s0 -c /etc/wpa_supplicant.conf  " );
		printf( "\n" ); 
                procedure_xwebbrowser( "https://mrrooster.tumblr.com/post/62694672/netbsd-wpa-wireless-ap" ); 
		return 0;
       }









	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "wifi" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "scanner" ) ==  0 ) 
	{
	        if ( MYOS == 1 ) 
		   nsystem( "  cd ; while : ; do  iwlist scan | grep ESSID ; sleep 5 ; done " ); 
		else
		   nsystem( " ifconfig urtwn0 list scan " ); 
		return 0;
	}











	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "scanplus" ) ==  0 ) 
	{
	        if ( MYOS == 1 ) 
		   nsystem( " iwlist scan " ); 
		else
		   nsystem( " ifconfig urtwn0 list scan " ); 
		return 0;
	}

	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "scan" ) ==  0 ) 
	{
		//nsystem( " iwlist scan " ); 
		nsystem( " iwlist scan | grep ESSID " ); 
		return 0;
	}

	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "iwlist" ) ==  0 ) 
	{
		nsystem( " iwlist scan " ); 
		nsystem( " iwlist scan | grep ESSID " ); 
		return 0;
	}






/*
  localhost$ cd sys/arch/amd64/compile/MYKERNEL/
  localhost$ pwd
  /usr/src/sys/arch/amd64/compile/MYKERNEL
  localhost$ ls netbsd
  netbsd
  localhost$ du -hs netbsd
  21M     netbsd
*/
       if ( argc == 3 )
       if ( strcmp( argv[1] ,    "help" )   ==  0   )
       if ( strcmp( argv[2] ,    "build" ) ==  0 ) 
       {
	       printf( " full installation, mk.conf, mkdir /usr/src/obj ; build.sh -O /usr/src/obj tools obj  "); 
	       printf(  "  CROSS: bash build.sh -U -u -O /root/obj -m amd64 -a x86_64 tools obj  \n" ); 
	       printf( "  /usr/obj/sys/arch/evbarm/compile/MYPI4   <--- there is there netbsd.img \n ");
	       printf( "  Your kernel shall appear in: /usr/src/sys/arch/amd64/compile/obj/GENERIC/netbsd \n" ); 

	       printf(  " == PKGSRC == \n" ); 
	       printf( "\n"); 
	       printf( "\n"); 
	       printf( "  echo 'PKG_DBDIR=/var/db/pkg' >> /etc/mk.conf  " ); 
	       printf( "\n"); 
	       printf( "\n"); 
	       printf(  " == DIR ought to be /usr/src/  == \n" ); 
	       printf( "\n"); 
	       printf( "  mkdir obj ; ./build.sh  -O obj tools obj  "); 
	       printf( "\n"); 
	       printf( "  cp sys/arch/amd64/conf/GENERIC sys/arch/amd64/conf/MYKERNEL ");  
	       printf( "\n"); 
	       printf( "  cd sys/arch/amd64/conf/ ; config MYKERNEL " ); 
	       printf( "\n"); 
	       printf( "  cd sys/arch/amd64/compile/MYKERNEL ; make depend ; make  " ); 
	       printf( "\n"); 
	       printf( "  cd / ;  rm *.tgz ; mconfig source 9.1 ; mconfig --untar *.tgz ; cd /usr/src ; mconfig build xxx "); 
	       printf( "\n"); 
	       return 0; 
       }



















       if ( argc >= 2 )
       if ( strcmp( argv[1] ,    "build" )   ==  0   )
       // amd64 i386
       {
	       printf(  " == == == == == \n" ); 
	       printf(  " BUILD KERNEL \n" ); 
	       printf(  " == == == == == \n" ); 
	       printf(  " ==> Usage : cd /usr/src ; mconfig build amd64 \n" ); 
	       printf(  " == DIR ought to be /usr/src/  == \n" ); 
	       if ( argc >= 3 )
	       {
	               nsystem( " echo 'PKG_DBDIR=/var/db/pkg' >> /etc/mk.conf  " ); 
		       // ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/syssrc.tgz      
		       // ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/src.tgz      
		       // ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/xsrc.tgz      
		       // ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/sharesrc.tgz      
		       if ( fexist( "/usr/src/src.tgz" ) == 0 ) 
		       {
			       // no time... this part needs a fix (naturally)... 
			       nsystem( " mkdir /usr/ ; mkdir /usr/src/ " ); 
			       chdir( "/" ); 
			       fetch_file_ftp( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/syssrc.tgz" );
			       fetch_file_ftp( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/src.tgz" );
			       fetch_file_ftp( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/xsrc.tgz" );
			       fetch_file_ftp( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/sharesrc.tgz" );
			       nsystem( " cp /src.tgz /usr/src/src.tgz " ); 
			       nsystem( " tar xpfz syssrc.tgz " ); 
			       nsystem( " tar xpfz src.tgz " ); 
			       nsystem( " tar xpfz xsrc.tgz " ); 
			       nsystem( " tar xpfz sharesrc.tgz " ); 
		       }
		       chdir( "/usr/src/" ); 

		       chdir( "/usr/src/" ); 
		       nsystem( "  mkdir obj ; ./build.sh  -O obj tools obj  "); 
		       //nsystem( "  cp sys/arch/amd64/conf/GENERIC sys/arch/amd64/conf/MYKERNEL ");  
		       ///  ("make clean CLEANDEPENDS=YES")
		       snprintf( charo , sizeof( charo ), "  cp sys/arch/%s/conf/GENERIC sys/arch/%s/conf/MYKERNEL ",  argv[ 2 ] , argv[ 2 ] );
		       nsystem( charo ); 
		       //nsystem( "  cd sys/arch/amd64/conf/ ; config MYKERNEL " ); 
		       snprintf( charo , sizeof( charo ), "  cd sys/arch/%s/conf/ ; config MYKERNEL  " , argv[ 2 ] );
		       nsystem( charo ); 
		       //nsystem( "  cd sys/arch/amd64/compile/MYKERNEL ; make depend ; make  " ); 
		       snprintf( charo , sizeof( charo ),  "   cd sys/arch/%s/compile/MYKERNEL ; make depend ; make     " , argv[ 2 ] );
		       nsystem( charo ); 
	       }
	       return 0; 
       }











	if ( argc == 3 )
	if ( strcmp( argv[1] ,  "install" ) ==  0 ) 
	if ( strcmp( argv[2] ,   "desktop" ) ==  0 ) 
	{
		npkg_update();

		npkg( " xserver-xorg  "  );   // for xterm  
		npkg( " xterm xinit  " );
		npkg( " xterm xinit  " );
		npkg( " screen   " );  // <-- for fltk 

		npkg( "  libx11-dev  "  );     // for evilwm !!!
		npkg( "  libfltk1.3-dev " ); 
		npkg( "  gcc g++ " ); 
		npkg( "  make " ); 

		npkg( "    xinit xterm xserver-xorg blackbox "); 
		npkg( "    libfltk1.3-dev " ); 
		npkg( "    tmux screen " ); 
		npkg( "    tmux " ); 
		npkg( "    less  " ); 

		npkg(   " libfltk1.3-dev " ); 

		npkg(   " dillo " ); 
		npkg(   " mpg123 " ); // for fltk radio and streamtuner 

		nsystem( " mkdir /usr/src/       " ) ;

		if ( MYOS == 1 ) 
		{
			nsystem( " apt-get remove -y usbmount " );
			nsystem( " apt-get remove --purge -y usbmount  " ); 
		}
		return 0; 
	}













	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "installer" ) ==  0 ) 
	{
		    fetch_file_ftp( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/i386/binary/kernel/netbsd-INSTALL.gz" ); 
		    return 0; 
	}









        // newfs : -V2 -O2 
	// arg tar.gz 
	// destination sdb2 
	// destination sdc2 
	/// ... 
	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "--deploy-tarball" ) ==  0 ) || ( strcmp( argv[1] ,   "-deploy" ) ==  0 )  
	|| ( strcmp( argv[1] ,   "--deploy-ext2fs" ) ==  0 )  
	|| ( strcmp( argv[1] ,   "--deploy" ) ==  0 )  )
	{
			//printf( "-- Automatic deploy target tar.gz -- will format.\n" );
			///jjnsystem( "mkdir /root/tmp" );
			//printf( "HOME: %s\n", getenv( "HOME" ));
			//chdir( getenv( "HOME") );
			//printf( "%s\n", getenv( "HOME") );
			printf( "===\n" );
			printf( "PATH: %s\n", getcwd( cmdi , PATH_MAX ) );
			printf( "===\n" );

			printf( " Argument #2: (%s)\n" ,      argv[ 2 ] );
			printf( " fexist     : (%d)\n" ,      fexist( argv[ 2 ] ) );


			//nsystem( " fdisk -l " );
			//printf( " The format will be:  mkfs.ext3 -F xxx\n" );
			//printf( " The format will be:  mkfs.ext3 -F %s\n", argv[ i ] );
			///             strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
			if ( fexist( argv[ 2 ] ) == 0 )
			{
				//printf("%s", KRED);
				printf("Hello There is no Image!!\n");
				////printf("EXIT\n");
				////exit( 0 ); 
			}

			if ( fexist( argv[ 2 ] ) == 1 )
			{
				//printf("%s", KGRE);
				printf("Hello There is The Image!!\n");
				//nsystem( " mkdir /media  ");   // new for small systems without /media directory !! 
				nsystem( " mkdir /target  ");   // new for small systems without /media directory !! 
			}

			printf( " Arg 2: (%s), make sure full path\n" ,      argv[ 2 ] );
			printf( " Arg fexist: (%d), make sure full path\n" , fexist( argv[ 2 ] ) );


			printf( "HOME: %s\n", getenv( "HOME" ));
			printf( "Continue? \n" ); 
			//fookey = ckeypress();
			//if ( fookey == '1' ) 
			{
				//nsystem( " echo sleep ; echo sleep and go ; sleep 25 " );
				nsystem( " echo sleep ; echo sleep and go ; sleep 5 " );
				//fookey = ckeypress();
				//if ( fookey == '1' ) 
				if ( argc >= 2 )
				{
					nsystem( " mkdir /target  ");  
					nsystem( " umount /target  ");  

					for( i = 1 ; i < argc ; i++) 
					{
						if ( i == 1 )
						{
							printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
						}
						else if ( i == 2 )
						{
							printf( "%d/%d: %s (skip source)\n", i, argc-1 ,  argv[ i ] );
						}
						else if ( i >= 3 )
						{

							printf( "> File process.\n" );
							printf( "- File process (status: begin).\n" );
							printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
							printf( "> Source: %s\n", argv[ 2 ] );
							printf( "> Target: %s\n", argv[ i ] );


							strncpy( cmdi,  " ", PATH_MAX );
							strncpy( cmdi,  " ", PATH_MAX );
							strncat( cmdi , " cd ; umount  " , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , "     /dev/"  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi ,  fbasename( argv[ i ] )  , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
							strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
							nsystem( cmdi );



							nsystem( " umount /target "  );

							strncpy( cmdi,  " ", PATH_MAX );
							strncpy( cmdi,  " ", PATH_MAX );
							strncat( cmdi , "  umount /target ;  cd / ;  " , PATH_MAX - strlen( cmdi ) -1 );



							if (  os_bsd_type == 1 ) // freebsd, it can do devs 
							   strncat( cmdi , "  newfs  /dev/"  , PATH_MAX - strlen( cmdi ) -1 );

	                                                else if ( strcmp( argv[1] ,   "--deploy-ext2fs" ) ==  0 )  
							   strncat( cmdi , "  mkfs.ext2 -F  /dev/"  , PATH_MAX - strlen( cmdi ) -1 );
							else if ( MYOS == 1 ) 
							   strncat( cmdi , "  mkfs.ext3 -F  /dev/"  , PATH_MAX - strlen( cmdi ) -1 );
							else
							   strncat( cmdi , "  newfs_ext2fs  /dev/"  , PATH_MAX - strlen( cmdi ) -1 );

							strncat( cmdi ,  fbasename( argv[ i ] )  , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
							strncat( cmdi , "  ; sync ; sync ; sleep 1   "  , PATH_MAX - strlen( cmdi ) -1 );
							///nsystem( cmdi );  <-- bug on devuan amd64 with mkfs.!!!
							nsystem( cmdi );  

							strncpy( cmdi,  " ", PATH_MAX );
							strncpy( cmdi,  " ", PATH_MAX );
							strncat( cmdi , " mkdir /target ; umount /target ; mount    " , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , "  "  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , "     /dev/"  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi ,  fbasename(  argv[ i ] )  , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
							strncat( cmdi , "   "  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , " /target ;   cd /target ;  tar xvpfz    \"" , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , argv[ 2 ] , PATH_MAX - strlen( cmdi ) -1 );   //// file tarball 
							strncat( cmdi , "\" ;  cd / ; cd / ; umount /target ; umount /target ; cd ; sync  ; sync   " , PATH_MAX - strlen( cmdi ) -1 );
							nsystem( cmdi );
							printf( "- File process (status: completed).\n" );
						}
					}
				}
			}
			return 0;
		}










	if ( argc == 2 )
	if (  ( strcmp( argv[1] , "reboot" ) ==  0 ) 
	||  ( strcmp( argv[1] ,   "re" ) ==  0 )
	||  ( strcmp( argv[1] ,   "RE" ) ==  0 )  // for handheld 
	||  ( strcmp( argv[1] , "REBOOT" ) ==  0 ) )
	{
                        if ( os_bsd_type == 3 )
			{
			   nsystem( " shutdown -r now  " ); 
			}
			printf( " NCONFIG REBOOT \n " ); 
			printf( " ...  \n " ); 
			nsystem( " sleep 1 " );
			nsystem( "  sync  " );
			nsystem( " sleep 1 " );
			nsystem( "  sync  " );
			printf( "  NCONFIG REBOOT \n " ); 
			nsystem( " shutdown -r now " );
			printf( " NCONFIG REBOOT \n " ); 
			nsystem( " /sbin/shutdown -r now " );                    // sometimes needed
			nsystem( " screen -d -m sudo /sbin/shutdown -r now " );  // sometimes needed
			printf( " ...  \n " ); 
			return 0;
	}





	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "install" ) ==  0 ) || ( strcmp( argv[1] , "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] , "netsurf" )     ==  0 ) || ( strcmp( argv[2] , "surf" )     ==  0 ) )
	{
		npkgmin( " netsurf      " );
		npkgmin( " netsurf-gtk  " );
		//npkgmin( " netsurf  " );
		return 0;
	}







	if ( argc == 2 )
	if  ( strcmp( argv[1] , "halt" ) ==  0 ) 
	{
		nsystem( " sleep 2 " );
		nsystem( " sync ; sync " );
		if ( fexist( "/etc/wscons.conf" ) == 1 )     // netbsd 
		{
		        printf( " == NetBSD == \n" ); 
			nsystem( " /sbin/shutdown -p now " ); // sometimes needed
		}
		else
		{
			nsystem( " shutdown -h now " );
			nsystem( " /sbin/shutdown -h now " ); // sometimes needed
		}
		return 0;
	}



	if ( argc == 2 )
	if  ( strcmp( argv[1] , "halt" ) ==  0 ) 
	{
		nsystem( " sleep 2 " );
		nsystem( " sync ; sync " );
		if ( fexist( "/etc/wscons.conf" ) == 1 )     // netbsd 
		{
		        printf( " == NetBSD == \n" ); 
			nsystem( " /sbin/shutdown -p now " ); // sometimes needed
		}
		else
		{
			nsystem( " shutdown -h now " );
			nsystem( " /sbin/shutdown -h now " ); // sometimes needed
		}
		return 0;
	}






	if  ( argc == 2 )
	if  ( strcmp( argv[1] , "--root-halt" ) ==  0 ) 
	{
		printf( "Username: %s\n", getenv( "USER" ) ); 
		if ( strcmp( getenv( "USER" ), "root" ) == 0 ) 
		{
			printf( "Username is root.\n" ); 
			if ( MYOS == 1 ) 
				nsystem( " halt " ); 
			else 
				nsystem( " shutdown -p now "  ); 
		}
		// check and force halt 
		return 0;
	}





	if ( argc == 2 )
	if ( strcmp( argv[1] , "--username" ) ==  0 ) 
	{
			printf( "Username: %s\n", getenv( "USER" ) ); 
			if ( strcmp( getenv( "USER" ), "root" ) == 0 ) 
			{
				printf( "Username is root.\n" ); 
			}
			else
			{
				printf( "Username is NOT root.\n" ); 
			}
			return 0;
	}




















	if ( argc >= 3)
	if ( ( strcmp( argv[1] , "store" ) ==  0 ) 
	|| ( strcmp( argv[1] , "store-links" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--store-links" ) ==  0 ) )
	{
		printf( " ============== \n "); 
	        printf( " - Current Path: %s -\n", getcwd( charo , PATH_MAX ) );
		printf( " ============== \n "); 
		strncpy( cmdi , "  " , PATH_MAX );
		for( i = 2 ; i < argc ; i++) 
		{
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
			printf( "== \n" ); 

	                if ( ( strcmp( argv[1] , "store-links" ) ==  0 ) 
	                || ( strcmp( argv[1] , "--store-links" ) ==  0 ) )
			{
			   snprintf( charo , sizeof( charo ),  "  /usr/local/bin/cmlinks  https://gitlab.com/openbsd98324/%s/-/archive/main/%s-main.tar.gz" , argv[ i ] , argv[ i ] );
			   nsystem( charo );

			   snprintf( charo , sizeof( charo ),  "https://gitlab.com/openbsd98324/%s/-/archive/master/%s-master.tar.gz" , argv[ i ] , argv[ i ] );
			   ncmdwith( "  links  " ,  charo );
			   //fetch_file_ftp( charo );
			   printf( "== \n" ); 
			}
			else
			{
				snprintf( charo , sizeof( charo ),  "https://gitlab.com/openbsd98324/%s/-/archive/main/%s-main.tar.gz" , argv[ i ] , argv[ i ] );
				fetch_file_ftp( charo );
				printf( "== \n" ); 
				snprintf(  charo , sizeof( charo ),  "  tar xvpfz %s-main.tar.gz" , argv[ i ] ); 
				nsystem( charo ); 


				// NEW... 
				snprintf( charo , sizeof( charo ),  "%s-main.tar.gz" , argv[ i ] );
				if ( fexist( charo ) == 0 ) 
				{
					printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
					printf( "== (...) However, try with the master repository.\n" ); 
					snprintf(      charo , sizeof( charo ),  "https://gitlab.com/openbsd98324/%s/-/archive/master/%s-master.tar.gz" , argv[ i ] , argv[ i ] );
					fetch_file_ftp( charo );
					printf( "== \n" ); 
					snprintf(  charo , sizeof( charo ),  "  tar xvpfz %s-master.tar.gz" , argv[ i ] ); 
					nsystem( charo ); 
				}
			}
		}
		printf( " ============== \n "); 
		return 0; 
	}
























	if ( argc == 3 )
	if ( strcmp( argv[1] , "clone" ) ==  0 )  
	if ( ( strcmp( argv[2] , "modules" ) ==  0 )  
	|| ( strcmp( argv[2] , "modules+" ) ==  0 ) )
	{
	     nsystem( " mkdir lib/ ; mkdir lib/modules ;  cp -a  -v   /lib/modules/$( uname -r )/  lib/modules   " );
	     if ( strcmp( argv[2] , "modules+" ) ==  0 ) 
	        nsystem( " mkdir lib/ ;  cp -a  -v   /lib/firmware/ lib/firmware  " );
	     return 0; 
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] , "clone" ) ==  0 )  
	if ( strcmp( argv[2] , "firmware" ) ==  0 )  
	{
	     nsystem( " mkdir lib/ ;  cp -a  -v   /lib/firmware/ lib/firmware  " );
	     return 0; 
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] , "clone" ) ==  0 )  
	if ( strcmp( argv[2] , "setup" ) ==  0 )  
	{
		printf( "===============\n" ); 
		printf( "===============\n" ); 
		nsystem( " cp /usr/local/bin/* usr/local/bin/ " ); 
		//nsystem( " cp /etc/wpa_supplicant.conf etc/wpa_supplicant.conf  " ); 
		return 0; 
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 )  
	if ( strcmp( argv[2] , "pacman" ) ==  0 )  
	{
		printf( "===============\n" ); 
		printf( "     dhcpcd enp8s0       \n"); 
		printf( "     pacman -Syy xterm   \n"); 
		printf( "===============\n" ); 
		printf( "   Arch Linux: pacman -S arch-install-scripts   \n");
		printf( "   Debian: apt-get install arch-install-scripts \n");
		printf( "   Ubuntu: apt-get install arch-install-scripts \n");
		printf( "   Alpine: apk add arch-install-scripts         \n");
		printf( "===============\n" ); 
		printf( " pacstrap -i  . base dhcpcd linux nano linux-firmware gcc make \n" ); 
		printf( "===============\n" ); 
		return 0; 
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] , "pacman-upgrade" ) ==  0 )  
	{
		nsystem( " pacman -Syu ; pacman -Syyu " ); 
		return 0; 
	}




	if ( argc == 3)
	if ( ( strcmp( argv[1] ,   "install" ) ==  0 ) || ( strcmp( argv[1] ,   "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] ,   "subversion" ) ==  0 ) || ( strcmp( argv[2] ,   "svn" ) ==  0 ) )
	{

                npkg_update();  // for sure it will work?

		if ( MYOS == 1 ) 
			npkg( "  subversion " );

		else if ( os_bsd_type == 1 ) 
			npkg( "  subversion " );
		else
			npkg( "  subversion-base " );
		return 0; 
	}









	if ( argc == 3 )
	if ( strcmp( argv[1] , "fstab" ) ==  0 ) 
	{
	   if ( strcmp( argv[2] , "freebsd" ) ==  0 ) 
	   {
		printf( "# Device        Mountpoint      FStype  Options Dump    Pass# \n" ); 
		printf( "/dev/ada0s2a    /               ufs     rw      1       1 \n"); 
	   }
	   return 0; 
	}







	if ( argc == 2 )
	if ( strcmp( argv[1] ,    "minsets" ) ==  0 ) 
	{
	    // or: base comp etc kern-Gen modules ... tgz 
            if (  os_bsd_type == 1 ) 
	    {
	      printf( " MANIFEST base kernel-dbg kernel ports src tests \n"); 
	    }
            else if (  os_bsd_type == 3 ) 
	    {
		    printf( " ========= \n" ); 
		    printf( " minsets: \n" ); 
		    printf( " 9.1 Amd64: \n" ); 
		    printf( " ========= \n" ); 
		    printf( " base.tar.xz \n" ); 
		    printf( " comp.tar.xz \n" ); 
		    printf( " etc.tar.xz \n" ); 
		    printf( " kern-GENERIC.tar.xz  \n" ); 
		    printf( " modules.tar.xz \n" ); 
		    printf( " ========= \n" ); 
		    printf( "  tgz for i386 \n" ); 
		    printf( " ========= \n" ); 
	    }
            return 0; 
	}














	if ( argc == 3 )
	if ( strcmp( argv[1] , "bsdstrap" ) ==  0 )  
	{

	       printf( " == HELP == \n");  
	       printf( " > Help BSDSTRAP, example: bsdstrap netbsd         \n" ); 
	       printf( "        BSDSTRAP, example: bsdstrap freebsd        \n" ); 
	       printf( "        BSDSTRAP, example: bsdstrap freebsd-arm64  \n" ); 
	       printf( " == == ==\n");  
	       printf( "      freebsd: /usr/freebsd-dist \n"); 
	       printf( " == == ==\n");  

	       if ( strcmp( argv[2] , "freebsd-arm64" ) ==  0 )  
	       {
		       nsystem( " links https://download.freebsd.org/ftp/releases/arm64/ " ); 
	       }
	       else if ( strcmp( argv[2] , "netbsd" ) ==  0 )  
	       {
	            printf( " BSD: NetBSD \n" ); 
                    fetch_file_ftp( "https://cdn.netbsd.org/pub/NetBSD/NetBSD-9.1/i386/binary/sets/base.tgz" ); 
                    fetch_file_ftp( "https://cdn.netbsd.org/pub/NetBSD/NetBSD-9.1/i386/binary/sets/comp.tgz" ); 
                    fetch_file_ftp( "https://cdn.netbsd.org/pub/NetBSD/NetBSD-9.1/i386/binary/sets/etc.tgz" ); 
                    fetch_file_ftp( "https://cdn.netbsd.org/pub/NetBSD/NetBSD-9.1/i386/binary/sets/kern-GENERIC.tgz" ); 
                    fetch_file_ftp( "https://cdn.netbsd.org/pub/NetBSD/NetBSD-9.1/i386/binary/sets/modules.tgz" ); 
	       }

	       else if ( strcmp( argv[2] , "freebsd" ) ==  0 ) 
	       {
	               printf( " BSD: FreeBSD \n" ); 
	               printf( " MANIFEST base kernel-dbg kernel ports src tests \n"); 
		       //fetch_file_ftp( "https://download.freebsd.org/ftp/releases/i386/i386/13.0-RELEASE/src.txz" ); 
		       fetch_file_ftp( "https://download.freebsd.org/ftp/releases/i386/i386/13.0-RELEASE/MANIFEST" ); 
		       fetch_file_ftp( "https://download.freebsd.org/ftp/releases/i386/i386/13.0-RELEASE/base.txz" ); 
		       fetch_file_ftp( "https://download.freebsd.org/ftp/releases/i386/i386/13.0-RELEASE/kernel-dbg.txz" ); 
		       fetch_file_ftp( "https://download.freebsd.org/ftp/releases/i386/i386/13.0-RELEASE/kernel.txz" ); 
		       fetch_file_ftp( "https://download.freebsd.org/ftp/releases/i386/i386/13.0-RELEASE/ports.txz" ); 
		       fetch_file_ftp( "https://download.freebsd.org/ftp/releases/i386/i386/13.0-RELEASE/src.txz" ); 
		       fetch_file_ftp( "https://download.freebsd.org/ftp/releases/i386/i386/13.0-RELEASE/tests.txz" ); 
	       }
	       return 0; 
	}















	/// put base of archlinux !
	if ( argc == 2 )
	if ( strcmp( argv[1] , "pacstrap" ) ==  0 )  
	{
		nsystem( " pacstrap -i  . base dhcpcd linux nano linux-firmware gcc make " ); 
		nsystem( " mkdir usr/local/bin ");  
		nsystem( " mkdir usr/src/      ");  
		return 0;
	}






	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "profile" ) ==  0 ) || ( strcmp( argv[1] , "profile-netbsd" ) ==  0 ) )
	{
		printf( "# NETBSD FILE etc/profile \n" );
		printf( "export LANG=\"en_US.UTF-8\"\n" );
		printf( "export LC_CTYPE=\"en_US.UTF-8\"\n" );
		printf( "export LC_ALL=\"\"\n" );
		printf( "export TZ=Europe/Amsterdam\n" );
		return 0; 
	}









	if ( argc == 3 )
	if ( strcmp( argv[1] , "--record-tube" ) ==  0 )   
	{
		snprintf( charo , sizeof( charo ), "  ffmpeg -i  `  /usr/bin/yt-dlp  -g   '%s' `   $( mconfig -t )-videocallback.mp4 " ,  argv[ 2 ] ); 
		nsystem( charo ); 
		return 0; 
	}

	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "--tube" ) ==  0 )   
	|| ( strcmp( argv[1] , "-tube" ) ==  0 ) ) 
	{
		chdir( getenv( "HOME") );

		if ( fexist( "yt-dlp/yt-dlp" ) == 1 ) 
		{
		  snprintf( charo , sizeof( charo ), "  mconfig --mpv  ` yt-dlp/yt-dlp -f best   -g '%s'  ` " ,  argv[ 2 ] ); 
		  nsystem( charo ); 
		}
		else
		{
		  snprintf( charo , sizeof( charo ), "  mconfig --mpv  ` yt-dlp  -f best  -g '%s'  ` " ,  argv[ 2 ] ); 
		  nsystem( charo ); 
		}
		return 0; 
	}





	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "--tube-fs" ) ==  0 )  
	|| ( strcmp( argv[1] , "-tube-fs" ) ==  0 )   
	|| ( strcmp( argv[1] , "--tubefs" ) ==  0 )  ) 
	{
		chdir( getenv( "HOME") );
		if ( fexist( "yt-dlp/yt-dlp" ) == 1 ) 
		{
		    snprintf( charo , sizeof( charo ), "  mconfig --mpv-fs  ` yt-dlp/yt-dlp  -f best  -g '%s'  ` " ,  argv[ 2 ] ); 
		    nsystem( charo ); 
		}
		else
		{
		  snprintf( charo , sizeof( charo ), "  mconfig --mpv-fs  ` yt-dlp -f best  -g '%s'  ` " ,  argv[ 2 ] ); 
		  nsystem( charo ); 
		}
		return 0; 
	}








	if ( argc == 3 )
	if ( strcmp( argv[1] , "--curl-mirror" ) ==  0 ) 
	{
		printf("Time: %d\n", (int)time(NULL));
		if ( fexist( "/usr/bin/curl" ) == 1 ) 
		  snprintf( charo , sizeof( charo ), "  /usr/bin/curl -k --insecure -L '%s' -o '%d-%s'  ", argv[ 2 ], (int)time(NULL), fbasename( argv[ 2 ] )  );
		else if ( fexist( "/usr/pkg/bin/curl" ) == 1 ) 
		  snprintf( charo , sizeof( charo ), "  /usr/pkg/bin/curl -k --insecure -L '%s' -o '%d-%s'  ", argv[ 2 ], (int)time(NULL), fbasename( argv[ 2 ] )  );

		else 
		  snprintf( charo , sizeof( charo ), "  wget -c --no-check-certificate '%s' -O '%d-%s'  ", argv[ 2 ], (int)time(NULL), fbasename( argv[ 2 ] )  );
		nsystem( charo ); 
		printf(" Command: %s\n", charo );
		return 0; 
	}















        // cat   devices/LNXSYSTM:00/LNXSYBUS:00/PNP0A08:00/PNP0C0A:00/power_supply/BAT0/capacity
	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "bat" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--battery-level" ) ==  0 )
	|| ( strcmp( argv[1] , "battery" ) ==  0 ) )
	{
                nsystem( " cat  '/sys/devices/LNXSYSTM:00/LNXSYBUS:00/PNP0A08:00/PNP0C0A:00/power_supply/BAT0/capacity'  " );  
		// pinebook 
		if ( MYOS == 1 ) 
		  nsystem( " cat /sys/devices/platform/ff3d0000.i2c/i2c-4/4-0062/power_supply/cw2015-battery/capacity ; cat /sys/devices/platform/ff3d0000.i2c/i2c-4/4-0062/power_supply/cw2015-battery/status " ); 

		if ( os_bsd_type == 1 ) 
			nsystem( " sysctl hw.acpi.battery " ); 
		else if ( os_bsd_type == 3 ) 
			nsystem( " envstat -d acpibat0 " ); 

		// pinebook 
		////nsystem( " cat /sys/devices/platform/ff3d0000.i2c/i2c-4/4-0062/power_supply/cw2015-battery/capacity ; cat /sys/devices/platform/ff3d0000.i2c/i2c-4/4-0062/power_supply/cw2015-battery/status " ); 
		return 0; 
        }











	if ( argc == 2 )
	if ( strcmp( argv[1] , "manjaro" ) ==  0 ) 
	{
	      /// aachen !
               //  fetch_file_ftp( "https://ftp.halifax.rwth-aachen.de/osdn/storage/g/m/ma/manjaro-arm/pinebook/xfce/20.12/Manjaro-ARM-xfce-pinebook-20.12.img.xz" ); 
	       ///
               // https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-kde-plasma-pbpro-23.02.img.xz 
                 fetch_file_ftp( "https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-kde-plasma-pbpro-23.02.img.xz" ); 
		 return 0; 
        }


	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "manjaro" ) ==  0 ) 
	|| ( strcmp( argv[1] , "manjaro-xfce" ) ==  0 )   
	|| ( strcmp( argv[1] , "aachen" ) ==  0 )   
	|| ( strcmp( argv[1] , "manjaro-aachen" ) ==  0 ) )  
	{
	        /// aachen !  tested, it works !!!
	        //  + it needs lib. 
                fetch_file_ftp( "https://ftp.halifax.rwth-aachen.de/osdn/storage/g/m/ma/manjaro-arm/pinebook/xfce/20.12/Manjaro-ARM-xfce-pinebook-20.12.img.xz" ); 
	        return 0; 
        }








	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "ip" ) ==  0 ) || ( strcmp( argv[2] , "lan" ) ==  0 ) )
	{
			printf( " ip link set eth0 up " );  
			return 0; 
	}




	if ( argc == 2)
	if ( ( strcmp( argv[1] , "lan" ) ==  0 ) || ( strcmp( argv[1] , "net" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--lan" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-lan" ) ==  0 ) )
	{
		printf("[%d] \n", (int)time(NULL));
		printf(" == Hello Lan ! == \n" ); 

		nsystem( " dhcpcd enp1s0  " ); // acer 

		nsystem( " dhclient enp1s0 ");
		nsystem( " dhcpcd enp1s0 ");

                // machine pbpro with ethernet usb 
		nsystem( " dhclient enu1 ");
		nsystem( " dhcpcd enu1 ");

		nsystem( " dhcpcd enu1ue2  " ); // pine hm  
		// pc wk 
                // enu1u2: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
		// usb to ethernet manjaro pb
                nsystem( " dhcpcd enu1u2  " ); 
                nsystem( " dhcpcd enu1  " );   // on BASEPLUS with usb to ethernet cable  

                // manjaro 
		nsystem( " dhcpcd enu1ue2  " ); // pine hm  
		// pc wk 
                // enu1u2: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
		// usb to ethernet manjaro pb
                nsystem( " dhcpcd enu1u2  " ); 

		nsystem( " dhcpcd enu1 " ); 
                // manjaro 
		nsystem( " dhcpcd enu1ue2  " ); // pine hm  
		// pc wk 
                // enu1u2: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
                nsystem( " dhcpcd enu1u2  " ); 

	        if ( ( os_bsd_type == 1 ) && ( MYOS != 1 ) ) 
		{
		    printf( "FreeBSD ...\n" ); 
		    nsystem( " dhclient ue0 " ); 
		    nsystem( " dhclient re0 " ); 
		    nsystem( " dhclient me0 " ); 
		}

		if ( MYOS == 1 ) 
		{
		        printf(  " > System LINUX \n" ); 
			nsystem( " dhcpcd   enp8s0   " ); 
			nsystem( " dhclient enp8s0   " ); 
			nsystem( " dhcpcd    enp3s0f2   " ); 
			nsystem( " dhclient  enp0s31f6  " );  //ubuntu   
			nsystem( " dhclient eth0 " ); 
			nsystem( " dhclient eth1 " ); 
		}
		else
		{
		        printf( "[ BSD ]\n" ); 
			nsystem( "  dhcpcd  re0  " ); 
			nsystem( "  dhcpcd  axen0  " ); 
		}

                if ( MYOS == 1 ) 
		        nsystem( " ip link set eth0 up  "); 

		nsystem( " dhcpcd enp8s0 " ); 
		nsystem( " dhclient eth0 " ); 
		nsystem( " dhclient eth1 " ); 

		nsystem( " dhcpcd enp8s0   " ); 
		nsystem( " dhcpcd enp3s0f2 " ); 

		nsystem( " dhcpcd enp3f0s2 " ); 
		nsystem( " dhcpcd enp3s0f2 " ); 

                nsystem( "  dhcpcd eth0 ;  dhcpcd enp8s0  ; dhcpcd enp0s20u3  ; dhcpcd enp8s0     ; dhcpcd enp9s0f3u3  "  ); 

		nsystem( " ifconfig -a " ); 


		if ( MYOS == 1 ) 
		{
			if ( fexist( "/usr/bin/systemctl" ) == 1 ) 
			{
				nsystem( " ip link set eth0 up  "); 
			}
		}
		return 0;
	}











	//if ( argc >= 2 )
	if ( argc == 4 )
	if ( strcmp( argv[1] ,   "config.txt" ) ==  0 ) 
	{ 
		printf( "\n");
		printf( "\n");
		printf( "# CONFIG.TXT\n");
		printf( "dtparam=audio=on\n" );
		printf( "gpu_mem_256=128\n" );
		printf( "gpu_mem_512=256\n" );
		printf( "gpu_mem_1024=256\n" );
		printf( "overscan_scale=1\n" );
		printf( "disable_overscan=1\n" );
		printf( "disable_overscan=1\n" );
		printf( "hdmi_force_hotplug=1\n" );
		printf( "hdmi_group=2\n" );
		printf( "hdmi_mode=87\n" );
		printf( "hdmi_cvt=%s %s 60 1 0 0 0\n", argv[2] ,  argv[3]    );
		printf( "framebuffer_width=%s\n" ,   argv[2]   );
		printf( "framebuffer_height=%s\n" ,  argv[3]  );
		printf( "disable_overscan=1\n" );
		printf( "\n");
		printf( "\n");
                return 0; 
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "config.txt" ) ==  0 ) 
	// retropie rpi4  4GB !! 
	{ 
		printf( "# Enable audio (loads snd_bcm2835)\n" );
		printf( "dtparam=audio=on\n" );
		printf( "\n" );
		printf( "[pi4]\n" );
		printf( "# Enable DRM VC4 V3D driver on top of the dispmanx display stack\n" );
		printf( "dtoverlay=vc4-fkms-v3d\n" );
		printf( "max_framebuffers=2\n" );
		printf( "\n" );
		printf( "[all]\n" );
		printf( "#dtoverlay=vc4-fkms-v3d\n" );
		printf( "overscan_scale=1\n" );
		printf( "\n" );
		printf( "dtparam=audio=on\n" );
		printf( "gpu_mem_256=128\n" );
		printf( "gpu_mem_512=256\n" );
		printf( "gpu_mem_1024=256\n" );
		printf( "overscan_scale=1\n" );
		printf( "disable_overscan=1\n" );
		printf( "disable_overscan=1\n" );
		printf( "hdmi_force_hotplug=1\n" );
		printf( "hdmi_group=2\n" );
		printf( "hdmi_mode=87\n" );
		printf( "hdmi_cvt=1024 768 60 1 0 0 0\n" );
		printf( "framebuffer_width=1024\n" );
		printf( "framebuffer_height=768\n" );
		printf( "disable_overscan=1\n" );
		return 0; 
	}






        /*
	if ( argc == 4 )
	if ( strcmp( argv[1] ,   "config.txt" ) ==  0 ) 
	{ 
		printf( "\n");
		printf( "\n");
		printf( "# CONFIG.TXT\n");
		printf( "dtparam=audio=on\n" );
		printf( "gpu_mem_256=128\n" );
		printf( "gpu_mem_512=256\n" );
		printf( "gpu_mem_1024=256\n" );
		printf( "overscan_scale=1\n" );
		printf( "disable_overscan=1\n" );
		printf( "disable_overscan=1\n" );
		printf( "hdmi_force_hotplug=1\n" );
		printf( "hdmi_group=2\n" );
		printf( "hdmi_mode=87\n" );
		printf( "hdmi_cvt=%s %s 60 1 0 0 0\n", argv[2] ,  argv[3]    );
		printf( "framebuffer_width=%s\n" ,   argv[2]   );
		printf( "framebuffer_height=%s\n" ,  argv[3]  );
		printf( "disable_overscan=1\n" );
		printf( "\n");
		printf( "\n");
                return 0; 
	}
	*/







	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "font" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "ft" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "fonts" ) ==  0 ) 
	|| ( strcmp( argv[1] , "setfont" ) ==  0 ) )
	{
		if ( MYOS == 1 )
			nsystem( " setfont  /usr/share/consolefonts/Lat15-TerminusBold32x16.psf.gz    " );
		else if ( fexist( "/usr/bin/fetch" ) == 1 )          // freebsd
			nsystem( " vidfont " );
		else
			nsystem( " setfont  /usr/share/consolefonts/Lat15-TerminusBold32x16.psf.gz    " );
		return 0;
	}







	if ( argc >= 3 )
	if ( strcmp( argv[1] , "badblock" ) ==  0 ) 
	{
		printf( " ============== \n "); 
		printf( " Using dd to check the disk (read-only).\n "); 
		printf( " ============== \n "); 
		strncpy( cmdi , "  " , PATH_MAX );
		for( i = 2 ; i < argc ; i++) 
		{
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
			snprintf( charo , sizeof( charo ), "  dd if=/dev/%s   of=/dev/null  ",   fbasename(  argv[ i ]  )  );
			nsystem(  charo );
		}
		printf( " ============== \n "); 
		return 0; 
	}






	if ( argc >= 3)
	if ( strcmp( argv[1] , "diskinfo" ) ==  0 ) 
	{
		strncpy( cmdi , "  " , PATH_MAX );
		for( i = 2 ; i < argc ; i++) 
		{
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
			ncmdwith( "  fdisk        " ,  fbasename(  argv[ i ]  ) );
			ncmdwith( "  disklabel    " ,  fbasename(  argv[ i ]  )   );
			snprintf( charo , sizeof( charo ), " dmesg  | grep  %s ",   fbasename(  argv[ i ]  )  );
			nsystem(  charo );
		}
		return 0; 
	}
























        // for the panel 
	if ( argc >= 3 )
	if ( strcmp( argv[1] , "--crfox" ) ==  0 ) 
	{
		printf( " ============== \n "); 
		strncpy( cmdi , "  " , PATH_MAX );
		for( i = 2 ; i < argc ; i++) 
		{
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
			if ( fexist( "/usr/bin/chromium" ) == 1 ) 
			    ncmdwith( " /usr/bin/chromium --new-window " , argv[ i ]  );
			else if ( fexist( "/usr/bin/chromium-browser" ) == 1 ) 
			    ncmdwith( " /usr/bin/chromium-browser --new-window " , argv[ i ]  );
			else if ( fexist( "/usr/bin/firefox" ) == 1 ) 
			    ncmdwith( " /usr/bin/firefox " , argv[ i ]  );
			else 
			    ncmdwith( " dillo  " , argv[ i ]  );
		}
		printf( " ============== \n "); 
		return 0; 
	}




	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "--cr" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-cr" ) ==  0 ) )
	{
		printf( " ============== \n "); 
		strncpy( cmdi , "  " , PATH_MAX );
		for( i = 2 ; i < argc ; i++) 
		{
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
			procedure_webbrowser( argv[ i ]  );
		}
		printf( " ============== \n "); 
		return 0; 
	}











	if ( argc >= 3)
	if ( strcmp( argv[1] , "pkgmin" ) ==  0 )  // it is like in BSD
	if ( strcmp( argv[2] , "" ) !=  0 ) 
	{
				strncpy( cmdi , "  " , PATH_MAX );
				for( i = 2 ; i < argc ; i++) 
				{
					printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
					strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				}
				printf( " ============== \n "); 
				printf( " List of packages: %s\n" , cmdi ); 
				npkgmin( cmdi );
				printf( " ============== \n "); 
				return 0; 
	}









	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,   "install" ) ==  0 ) || ( strcmp( argv[1] , "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] ,   "irc-server" ) ==  0 ) || ( strcmp( argv[2] ,   "ircserver" ) ==  0 ) || ( strcmp( argv[2] ,   "charybdis" ) ==  0 ) )
	{
		printf( " How to install a server: charybdis  \n" ); 
		//npkg_update( ); 
		npkg( " charybdis " ); 
		npkg( " irssi     " ); 
		return 0;
	}





	if ( argc >= 3 )
	if ( strcmp( argv[1] , "apt" ) ==  0 )  // it is like in debian
	if ( strcmp( argv[2] , "" ) !=  0 ) 
	{
				if ( mode_gpg_allow_unauthenticated == 1 ) 
					strncpy( cmdi , " apt-get install --allow-unauthenticated  " , PATH_MAX );

			        else if ( fexist( "/usr/bin/zypper" ) == 1 ) 
				    nsystem( " /usr/bin/zypper  install " ); 

			        else if ( fexist( "/usr/bin/pacman" ) == 1 )  //  manjaro pine aach64
					strncpy( cmdi , " pacman -S " , PATH_MAX );

				else
					strncpy( cmdi , " apt-get install " , PATH_MAX );

				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				for( i = 2 ; i < argc ; i++) 
				{
					printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
					strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				}
				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				nsystem( cmdi );
				return 0; 
	}







	if ( argc >= 3)
	if ( strcmp( argv[1] , "apt" ) ==  0 )  // it is like in debian
	if ( strcmp( argv[2] , "" ) !=  0 ) 
	{
				if ( mode_gpg_allow_unauthenticated == 1 ) 
					strncpy( cmdi , " apt-get install --allow-unauthenticated  " , PATH_MAX );
				else
					strncpy( cmdi , " apt-get install " , PATH_MAX );

				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				for( i = 2 ; i < argc ; i++) 
				{
					printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
					strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				}
				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				nsystem( cmdi );
				return 0; 
	}



	if ( argc >= 3)
	if ( strcmp( argv[1] , "--apt" ) ==  0 )  // it is like in debian
	if ( strcmp( argv[2] , "" ) !=  0 ) 
	{
				if ( mode_gpg_allow_unauthenticated == 1 ) 
					strncpy( cmdi , " apt-get install --allow-unauthenticated  " , PATH_MAX );
				else
					strncpy( cmdi , " apt-get install " , PATH_MAX );

				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				for( i = 2 ; i < argc ; i++) 
				{
					printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
					strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				}
				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				nsystem( cmdi );
				return 0; 
	}











	if ( argc >= 3)
	if ( ( strcmp( argv[1] , "--pkg" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-pkg" ) ==  0 ) 
	|| ( strcmp( argv[1] , "pkg" ) ==  0 ) ) // it is like in BSD, --pkg used still. 
	if ( strcmp( argv[2] , "" ) !=  0 ) 
	{
				strncpy( cmdi , "  " , PATH_MAX );
				for( i = 2 ; i < argc ; i++) 
				{
					printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
					//strncat( cmdi , " apt-get install  -y " , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				}
				printf( " ============== \n "); 
				printf( " List of packages: %s\n" , cmdi ); 
				npkg( cmdi ) ; 
				printf( " ============== \n "); 
				return 0; 
	}











	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "ti84" ) ==  0   ) 
	|| ( strcmp( argv[1] , "-ti84" ) ==  0   ) 
	|| ( strcmp( argv[1] , "--ticalc" ) ==  0   ) 
	|| ( strcmp( argv[1] , "ticalc" ) ==  0   ) 
	|| ( strcmp( argv[1] , "--ti84" ) ==  0   ) )
	{
		chdir( getenv( "HOME") );
		printf( "HOME: %s\n", getenv( "HOME" ));

		nsystem( " mkdir .ti84 " ); 
		chdir( ".ti84" ); 
		printf( "Current path: %s\n", getcwd( cmdi , PATH_MAX ) );
	        fetch_file_ftp( "https://tiroms.weebly.com/uploads/1/1/0/5/110560031/ti84plus.rom" ); 
		printf( " default: be820cf0b7b18d91f42a7d5c27f99654  .ti84plus.rom  \n" );   
                nsystem( "   cd ; tilem2 --rom .ti84/ti84plus.rom     " ); 
		return 0; 
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "x48" ) ==  0 ) 
	if ( MYOS == 1 ) 
	{
		printf( " No Warranty, your own use \n" ); 
		printf( " No Warranty, your own use \n" ); 
		chdir( getenv( "HOME") );
		printf( "HOME: %s\n", getenv( "HOME" ));
		if ( fexist( ".hp48" ) == 0 ) 
		   nsystem( "  cd ; wget -c http://www.linuxfocus.org/common/src/article319/x48-gxrom-r.tar.gz    ; tar xvpfz x48-gxrom-r.tar.gz  ; cd            ;   x48   " );
		if ( MYOS == 1 ) 
		{
	          printf( " == LINUX == \n" ); 
		  nsystem( " xlsfonts ; x48 -connFont -misc-fixed-medium-r-semicondensed--0-0-75-75-c-0-iso8859-1     -smallFont -misc-fixed-medium-r-semicondensed--0-0-75-75-c-0-iso8859-1   -mediumFont -misc-fixed-medium-r-semicondensed--13-100-100-100-c-60-iso8859-1  -largeFont -misc-fixed-medium-r-semicondensed--13-120-75-75-c-60-iso8859-1     " );
		}
		else 
		  nsystem( " x48 " ); // BSD 
		return 0;
	}












	if ( argc >= 3 )
	if ( strcmp( argv[1] , "pkginstall" ) ==  0 ) 
	if ( strcmp( argv[2] , "7" ) ==  0 ) 
	{
	   printf( "# === \n" ); 
	   printf( "# Please wait... \n" ); 
           nsystem( "  echo choix ;  export PKG_PATH=\"http://archive.netbsd.org/pub/pkgsrc-archive/packages/NetBSD/i386/7.1.1/All/\" ; pkg_add -v pkgin " ); 
	   printf( "# === \n" ); 
	   printf( "# File /usr/pkg/etc/pkgin/ ... and add: \n" ); 
           printf( "# http://archive.netbsd.org/pub/pkgsrc-archive/packages/NetBSD/i386/7.1.1/All/\n" ); 

	   nsystem( " echo '## http://archive.netbsd.org/pub/pkgsrc-archive/packages/NetBSD/i386/7.1.1/All/' >> /usr/pkg/etc/pkgin/repositories.conf " );
	   nsystem( " echo >> /usr/pkg/etc/pkgin/repositories.conf " );
	   return 0; 
	}










	if ( argc == 3 )
	if ( strcmp( argv[1] , "pkginstall" ) ==  0 ) 
	if ( strcmp( argv[2] , "armv7" ) ==  0 ) 
	{
		printf( "| 5: uname earmv7hf  9.1 <----- PI3 working |\n" );
		nsystem( "  echo choix ;  export PKG_PATH=\"http://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/earmv7hf/9.1/All/\" ;   pkg_add -v pkgin ; pkgin update ; pkgin update " ) ; 
		return 0; 
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] , "pkginstall" ) ==  0 ) 
	if ( strcmp( argv[2] , "9" ) ==  0 ) 
	{
				printf( "  Path: %s \n", getcwd( cwd , PATH_MAX ) );
				printf( "  pkgin: %d\n", fexist(  "/usr/pkg/bin/pkgin" ) );
				system( "  uname -a " ); 
				printf( "|------------------------------|\n" );
				printf( "|   *Select*                   |\n" );
				printf( "| 1: uname AMD64 9.0           |\n" );
				printf( "| 2: uname i386 9.0            |\n" );
				printf( "| 3: uname AMD64 9.1           |\n" );
				printf( "| 4: uname i386  9.1           |\n" );
			        printf( "| 5: uname earmv7hf  9.1 <----- PI3 working |\n" );
			        printf( "| 6: uname earmv7hf  9.2       |\n" );
				printf( "| 7: uname AMD64 9.2 (default) |\n" );
				printf( "| 8: uname i386  9.1 (default) <-- Grub PC  |\n" );
				printf( "|------------------------------|\n" );
				printf( " > Choice ?\n"      );
				choix = getchar();

				if ( choix == '1' ) 
					nsystem( "  echo choix ;  export PKG_PATH=\"http://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/amd64/9.0/All/\" ;   pkg_add -v pkgin " ); 
				else if ( choix == '2' ) 
					nsystem( "  echo choix ;  export PKG_PATH=\"http://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/i386/9.0/All/\" ;   pkg_add -v pkgin " ); 

				else if ( choix == '3' ) 
					nsystem( "  echo choix ;  export PKG_PATH=\"http://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/amd64/9.1/All/\" ;   pkg_add -v pkgin " ); 

				else if ( choix == '4' ) 
					nsystem( "  echo choix ;  export PKG_PATH=\"http://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/i386/9.1/All/\" ;   pkg_add -v pkgin " ); 

				else if ( choix == '5' ) 
					nsystem( "  echo choix ;  export PKG_PATH=\"http://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/earmv7hf/9.1/All/\" ;   pkg_add -v pkgin ; pkgin update ; pkgin update " ) ; 
				else if ( choix == '6' ) 
					nsystem( "  echo choix ;  export PKG_PATH=\"http://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/earmv7hf/9.2/All/\" ;   pkg_add -v pkgin ; pkgin update ; pkgin update " ) ; 
				else if ( choix == '7' ) 
					nsystem( "  echo choix ;  export PKG_PATH=\"http://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/amd64/9.2/All/\" ;   pkg_add -v pkgin " ); 

				else if ( choix == '8' ) 
					nsystem( "  echo choix ;  export PKG_PATH=\"http://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/i386/9.1/All/\" ;   pkg_add -v pkgin " ); 

				else 
				{
					printf( "Cancelled, nothing carried out.\n");
				}
				return 0;
	}









	if ( argc >= 2 )
	if ( ( strcmp( argv[1] , "--fork" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-fork" ) ==  0 ) 
	|| ( strcmp( argv[1] , "fork" ) ==  0 ) )
	{
		printf( "HELP : Fork usage, mconfig --fork /usr/bin/chromium "); 
		pid = fork();
		if (pid == 0) 
		{
			printf("I am the child.\n");
			strncpy( foocmd , argv[ 2 ], PATH_MAX ); 
			printf( "Command : %s \n", foocmd ); 
			//int ret = execl( foocmd , foocmd , "--debug", (char *)0);
			//int ret = execl( "/sbin/ls" , "ls", "-1", (char *)0);
			int ret = execl( foocmd , foocmd , "", (char *)0);
			printf("I am the child, 10 seconds later.\n");
		}

		if (pid > 0) 
		{
			printf("I am the parent, the child is %d.\n", pid);
			pid = wait(&status);
			printf("End of process %d: ", pid);
			if (WIFEXITED(status)) 
			{
				printf("The process ended with exit(%d).\n", WEXITSTATUS(status));
			}
			if (WIFSIGNALED(status)) 
			{
				printf("The process ended with kill -%d.\n", WTERMSIG(status));
			}
		}
		return 0; 
	}









	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "ip" ) ==  0 ) || ( strcmp( argv[1] , "IP" ) ==  0 ) )
	{
			if ( MYOS == 1 ) nsystem( "  ip addr " );
			nsystem( "  /sbin/ifconfig -a " );
			nsystem( "  ifconfig -a " );

			if ( MYOS == 1 ) 
				nsystem( "  ip addr | grep 192 " );

			nsystem( "  /sbin/ifconfig -a | grep 192 " );
			return 0; 
	}





	// NETBSD:  this operation: wsconsctl -k -w map+='keycode 56 = Cmd Alt_L' 
	// OPENBSD: this openbsd: wsconsctl keyboard.map+="keycode 56 = Cmd Alt_L"     <-- this is useful. 
	// "https://archive.fosdem.org/2020/schedule/event/fbdev/attachments/slides/3595/export/events/attachments/fbdev/slides/3595/fosdem_2020_nicolas_caramelli_linux_framebuffer.pdf"
	if ( argc == 2 )
		if ( strcmp( argv[1] ,   "alt" ) ==  0 ) 
		{
			printf( " ==> The command is:   wsconsctl -k -w map+='keycode 56 = Cmd Alt_L'  \n" );
			if ( fexist( "/etc/freebsd-update.conf" ) == 1 )  // freebsd 
			{
				printf( "FreeBSD\n" );
			}
			else if ( fexist( "/etc/myname" ) == 1 )          // openbsd
			{
				printf( "OpenBSD\n" );
				nsystem( "wsconsctl keyboard.map+='keycode 56 = Cmd Alt_L' ");
			}
			else if ( fexist( "/etc/wscons.conf" ) == 1 )     // netbsd 
			{
				printf( "NetBSD\n" );
				nsystem( "  wsconsctl -k -w map+='keycode 56 = Cmd Alt_L'   " );
				//nsystem( "  sysctl -w ddb.onpanic=1 " ); //for crash ddb
			}
			return 0;
	}








	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "unzip" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-unzip" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--unzip" ) ==  0 ) )
	{
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else
					{
						printf( "> Unzip process.\n" );
						printf( "- File unzip process (status: begin).\n" );
						printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
						strncpy( cmdi, "", PATH_MAX );
						strncat( cmdi , "  unzip -o  " , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , "  \"" , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , "\"  " , PATH_MAX - strlen( cmdi ) -1 );
						nsystem( cmdi );
						printf( "- File unzip with -o, overwrite, process (status: completed).\n" );
					}
				}
			}
			return 0;
	}












	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "--mpg123" ) ==  0 ) || ( strcmp( argv[1] , "--mp3" ) ==  0 ) 
	  || ( strcmp( argv[1] , "-mpg123" ) ==  0 ) || ( strcmp( argv[1] , "-mp3" ) ==  0 ) )
	{
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
					}
					else if ( i >= 2 )
					{
		                                if ( fexist( "/usr/pkg/bin/mpg123" ) == 1 )  // netbsd  
						  strncpy( cmdi,  " /usr/pkg/bin/mpg123 ", PATH_MAX );
						else
						  strncpy( cmdi,  " mpg123 ", PATH_MAX );
						strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi ,  argv[ i ]  , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
						strncat( cmdi , "\"  "  , PATH_MAX - strlen( cmdi ) -1 );
						system( cmdi );
					}
				}
			}
			return 0;
	}











	if ( argc >= 3 )
	if ( ( strcmp( argv[1] ,   "--lfplayer" ) ==  0 ) || ( strcmp( argv[1] ,   "-lfplayer" ) ==  0 ) )
	{
		printf( " -- FOR LFPLAYER -- \n" ); 
		snprintf( charo , sizeof( charo ), "       mplayer  -vo fbdev:/dev/fb0  -loop 0  -vf scale=310:230  \"%s\"  " ,  argv[ 2 ] ); 
		nsystem( charo );  
		return 0; 
	}












	if ( argc == 2 )
	if ( strcmp( argv[1] , "--last-file" ) ==  0 ) 
	{
              nsystem( " ls -1Art  " ); 
	      return 0; 
        }








	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "ddvdisk" ) ==  0 ) 
	{
               nsystem( "  dd if=/dev/zero of=vdisk.img   bs=1m count=2000 " ); 
	       return 0; 
        }








	if ( argc == 3 )
	if ( strcmp( argv[1] , "-decode" ) ==  0 ) 
	{
		snprintf( charo , sizeof( charo ), "  echo \"%s\" | base64 -d " ,  argv[ 2 ] ); 
		nsystem( charo ); 
		return 0; 
	}









	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "-path" ) ==  0 ) 
	|| ( strcmp( argv[1] , "PWD" ) ==  0 ) 
	|| ( strcmp( argv[1] , "pwd" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--pwd" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--path" ) ==  0 ) )
	{
		printf( "Current path: %s\n", getcwd( cmdi , PATH_MAX ) );
		return 0;
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] ,  "--dirname" ) ==  0 ) 
	{
	       printf(  "%s\n" , dirnameunix( argv[ 2 ] ) ); 
	       return 0; 
        }






	if ( argc == 2 )
	if ( strcmp( argv[1] , "rc-freebsd" ) ==  0 ) 
	{
		printf(  "\n" );
		printf( "sshd_enable=\"YES\"\n");
		printf(  "\n" );
		printf( "#hostname=\"freebsd\"\n" );
		printf( "ifconfig_DEFAULT=\"DHCP\"\n" );
		printf( "growfs_enable=\"YES\"\n" );
		printf( "keymap=de\n" );
		printf( "ntpd_enable=\"YES\"\n" );
		printf( "ntpd_sync_on_start=\"YES\"\n" );
		printf( "sshd_enable=\"YES\"\n" );
		printf( "fusefs_enable=\"YES\"\n" );
		printf( "#apache24_enable=\"YES\"\n" );
		printf( "#allscreens_flags=\"-f terminus-b32\"\n" );
		printf( "fusefs_enable=\"YES\"  \n" );
		printf( "fusefs_load=\"YES\"  \n" );
		printf( "fsck_y_enable=\"YES\"  \n" );
		printf(  "\n" );
		printf( "ifconfig_re0=\"DHCP\"\n");
		printf( "ifconfig_re0_ipv6=\"inet6 accept_rtadv\"\n");
		printf( "ifconfig_ue0=\"dhcp\"\n" );
		printf(  "\n" );
		printf( "wlans_rtwn0=\"wlan0\"\n");
		printf( "ifconfig_wlan0=\"WPA DHCP\"\n");
		printf( "ifconfig_wlan0_ipv6=\"inet6 accept_rtadv\"\n");
		printf(  "\n" );
		return 0;
	}











	if ( argc == 2)
	if ( ( strcmp( argv[1] , "netbsd-rc" ) ==  0 ) || ( strcmp( argv[1] , "netbsd-rc.conf" ) ==  0 ) )
	{
	        // from mid armv7
		printf(  "\n" );
		printf( "if [ -r /etc/defaults/rc.conf ]; then\n" );
		printf( "	. /etc/defaults/rc.conf\n" );
		printf( "fi\n" );
		printf( "\n" );
		printf( "# If this is not set to YES, the system will drop into single-user mode.\n" );
		printf( "#\n" );
		printf( "rc_configured=NO\n" );
		printf( "\n" );
		printf( "# Add local overrides below.\n" );
		printf( "#\n" );
		printf( "dev_exists() {\n" );
		printf( "	if /sbin/drvctl -l $1 >/dev/null 2>&1 ; then\n" );
		printf( "		printf YES\n" );
		printf( "	else\n" );
		printf( "		printf NO\n" );
		printf( "	fi\n" );
		printf( "}\n" );
		printf( "\n" );
		printf( "rc_configured=YES\n" );
		printf( "hostname=armv7cm\n" );
		printf( "no_swap=YES\n" );
		printf( "savecore=NO\n" );
		printf( "sshd=YES\n" );
		printf( "ifconfig_re0=dhcp\n" );
		printf( "ifconfig_mue0=dhcp\n" );
		printf( "ifconfig_usmsc0=dhcp\n" );
		printf( "dhcpcd=YES\n" );
		printf( "ntpd=YES\n" );
		printf( "ntpd_flags=\"-g\"\n" );
		printf( "creds_msdos=YES\n" );
		printf( "creds_msdos_partition=/boot\n" );
		printf( "resize_disklabel=YES\n" );
		printf( "resize_root=YES\n" );
		printf( "resize_root_flags=\"-p\"\n" );
		printf( "resize_root_postcmd=\"/sbin/reboot -n\"\n" );
		printf( "mdnsd=YES\n" );
		printf( "devpubd=YES\n" );
		printf( "wscons=$(dev_exists wsdisplay0)\n" );
		printf( "\n" );
		return 0;
	}


















	if ( argc == 2)
	if ( strcmp( argv[1] , "rc-netbsd" ) ==  0 ) 
	{
		// netbsd here 
		// really new !!
		printf( "sshd=YES\n" );
		printf( "ntpd=YES\n" );
		printf( "ntpdate=YES\n" );
		printf( "ifconfig_urtwn0=dhcp\n" );
		printf( "dhcpcd=YES\n" );
		printf( "dhcpcd_flags=\"-q -b\"\n" );
		printf( "wpa_supplicant=YES\n" );
		printf( "wpa_supplicant_flags=\"-B -i urtwn0 -c /etc/wpa_supplicant.conf\"\n" );

		printf( "ifconfig_re0=dhcp\n" );
		printf( "ifconfig_ne2=dhcp\n" );  // pcmcia on i386

		printf( "ifconfig_axen0=dhcp\n" );
		printf( "ifconfig_usmsc0=dhcp\n" );  //  on RPI3b  (classic)
		printf( "ifconfig_mue0=dhcp\n" );    //  on RPI3b+ (model plus) 
		// usmsc0 
		printf( "fsck_y_enable=\"YES\"  \n");
		/// apache=yes
		printf( "#apache=YES\n" );  
		printf( "#autologin_user=netbsd\n" );  
		printf( "#mountd=YES\n" );  
		printf( "\n" );  
		return 0;
	}


	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "rc-netbsd" ) ==  0 ) || ( strcmp( argv[1] , "netbsd-rc" ) ==  0 ) ) 
	{
		// netbsd here 
		// really new !!
		printf( "sshd=YES\n" );
		printf( "ntpd=YES\n" );
		printf( "ntpdate=YES\n" );
		printf( "ifconfig_urtwn0=dhcp\n" );
		printf( "dhcpcd=YES\n" );
		printf( "dhcpcd_flags=\"-q -b\"\n" );
		printf( "wpa_supplicant=YES\n" );
		printf( "wpa_supplicant_flags=\"-B -i urtwn0 -c /etc/wpa_supplicant.conf\"\n" );

		printf( "ifconfig_re0=dhcp\n" );
		printf( "ifconfig_ne2=dhcp\n" );  // pcmcia on i386

		printf( "ifconfig_axen0=dhcp\n" );
		printf( "ifconfig_usmsc0=dhcp\n" );  //  on RPI3b  (classic)
		printf( "ifconfig_mue0=dhcp\n" );    //  on RPI3b+ (model plus) 
		// usmsc0 
		printf( "fsck_y_enable=\"YES\"  \n");
		/// apache=yes
		printf( "#apache=YES\n" );  
		printf( "#autologin_user=netbsd\n" );  
		printf( "#mountd=YES\n" );  
		printf( "\n" );  
		return 0;
	}






	if ( argc == 2 )
	if ( strcmp( argv[1] , "freebsd-rc" ) ==  0 ) 
	{
			printf( "#hostname=\"freebsd\"\n" );
			printf( "ifconfig_DEFAULT=\"DHCP\"\n" );
			printf( "growfs_enable=\"YES\"\n" );
			printf( "keymap=de\n" );
			printf( "ntpd_enable=\"YES\"\n" );
			printf( "ntpd_sync_on_start=\"YES\"\n" );
			printf( "sshd_enable=\"YES\"\n" );
			printf( "fusefs_enable=\"YES\"\n" );
			printf( "#apache24_enable=\"YES\"\n" );
			printf( "#allscreens_flags=\"-f terminus-b32\"\n" );
			printf( "fusefs_enable=\"YES\"  \n" );
			printf( "fusefs_load=\"YES\"  \n" );
			printf( "wlans_rtwn0=\"wlan0\"\n" );
			printf( "fsck_y_enable=\"YES\"  \n" );
			printf(  "\n" );
			printf( "ifconfig_re0=\"DHCP\"\n");
			printf( "ifconfig_re0_ipv6=\"inet6 accept_rtadv\"\n");
			printf( "ifconfig_ue0=\"dhcp\"\n" );
			printf(  "\n" );
			printf( "wlans_rtwn0=\"wlan0\"\n");
			printf( "ifconfig_wlan0=\"WPA DHCP\"\n");
			printf( "ifconfig_wlan0_ipv6=\"inet6 accept_rtadv\"\n");
			printf( "sshd_enable=\"YES\"\n");
			printf(  "\n" );
		printf( "\n" );  
		return 0;
	}








	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "rc" ) ==  0 ) 
	|| ( strcmp( argv[1] , "rc.conf" ) ==  0 ) )
	{
		// freebsd check 
		if ( fexist( "/etc/freebsd-update.conf" ) == 1 )  // freebsd 
		{
			printf( "#hostname=\"freebsd\"\n" );
			printf( "ifconfig_DEFAULT=\"DHCP\"\n" );
			printf( "growfs_enable=\"YES\"\n" );
			printf( "keymap=de\n" );
			printf( "ntpd_enable=\"YES\"\n" );
			printf( "ntpd_sync_on_start=\"YES\"\n" );
			printf( "sshd_enable=\"YES\"\n" );
			printf( "fusefs_enable=\"YES\"\n" );
			printf( "#apache24_enable=\"YES\"\n" );
			printf( "#allscreens_flags=\"-f terminus-b32\"\n" );
			printf( "fusefs_enable=\"YES\"  \n" );
			printf( "fusefs_load=\"YES\"  \n" );
			printf( "wlans_rtwn0=\"wlan0\"\n" );
			printf( "fsck_y_enable=\"YES\"  \n" );
			printf(  "\n" );
			printf( "ifconfig_re0=\"DHCP\"\n");
			printf( "ifconfig_re0_ipv6=\"inet6 accept_rtadv\"\n");
			printf( "ifconfig_ue0=\"dhcp\"\n" );
			printf(  "\n" );
			printf( "wlans_rtwn0=\"wlan0\"\n");
			printf( "ifconfig_wlan0=\"WPA DHCP\"\n");
			printf( "ifconfig_wlan0_ipv6=\"inet6 accept_rtadv\"\n");
			printf( "sshd_enable=\"YES\"\n");
			printf(  "\n" );
		}
		else
		{
		        // netbsd 
			// really new !!
			printf( "sshd=YES\n" );
			printf( "ntpd=YES\n" );
			printf( "ntpdate=YES\n" );
			printf( "ifconfig_urtwn0=dhcp\n" );
			printf( "dhcpcd=YES\n" );
			printf( "dhcpcd_flags=\"-q -b\"\n" );
			printf( "wpa_supplicant=YES\n" );
			printf( "wpa_supplicant_flags=\"-B -i urtwn0 -c /etc/wpa_supplicant.conf\"\n" );

                        /* wifi netbsd: 
				printf( "ifconfig_urtwn0=dhcp\n" );
				printf( "dhcpcd=YES\n" );
				printf( "dhcpcd_flags=\"-q -b\"\n" );
				printf( "wpa_supplicant=YES\n" );
				printf( "wpa_supplicant_flags=\"-B -i urtwn0 -c /etc/wpa_supplicant.conf\"\n" );
			*/

			printf( "ifconfig_re0=dhcp\n" );
			printf( "ifconfig_ne2=dhcp\n" );  // pcmcia on i386


			// usmsc0 on pi ... on router.
			printf( "ifconfig_usmsc0=dhcp\n" );  //  on RPI3b  (classic)
			printf( "ifconfig_mue0=dhcp\n" );    //  on RPI3b+ (model plus) 

			// axen0 on pi ... usb 
			printf( "ifconfig_axen0=dhcp\n" );

			// wm0 on qemu on i386, qemu ... running on live 
			printf( "ifconfig_wm0=dhcp\n" );  

			/// apache=yes
			printf( "#apache=YES\n" );  
			printf( "#autologin_user=netbsd\n" );  
			printf( "#mountd=YES\n" );  
			printf( "\n" );  
		}
		return 0;
	}



















	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "disk"  ) ==  0 ) 
	{
			if ( MYOS == 1 ) 
			{
				system( "  fdisk -l  "); 
			}
			else
			{
				nsystem( " sysctl -a | grep 'diskname' " );
			}
			return 0;
	}








	if ( argc >= 3 )
	if ( strcmp( argv[1] , "--audioplay" ) ==  0 ) 
	{
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
					}
					else if ( i >= 2 )
					{
						strncpy( cmdi,  " audioplay ", PATH_MAX );
						strncat( cmdi , " \"" , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi ,  argv[ i ]  , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
						strncat( cmdi , "\"  "  , PATH_MAX - strlen( cmdi ) -1 );
						system( cmdi );
					}
				}
			}
			return 0;
	}








	if ( argc >= 3 )
	if ( strcmp( argv[1] , "disklabel" ) ==  0 ) 
	{
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
					}
					else if ( i >= 2 )
					{
						//printf( "[ DISK /dev/%s ]\n" , fbasename( argv[ i ] ) ); 
	                                        fprintf( stderr, "[ DISK /dev/%s ]\n", fbasename( argv[ i ] ) ); 
						strncpy( cmdi,  " ", PATH_MAX );
						strncat( cmdi , " disklabel /dev/" , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
						strncat( cmdi , "  | grep '# (Cyl'    "  , PATH_MAX - strlen( cmdi ) -1 );
						system( cmdi );
					}
				}
			}
			return 0;
	}











	if ( argc >= 3 )
	if ( strcmp( argv[1] , "fsck" ) ==  0 ) 
	{
			printf( "Automatic processing ...:\n" );
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else if ( i >= 2 )
					{
						printf( "> File process.\n" );
						printf( "- File process (status: begin).\n" );
						printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
						printf( "> Arg2 : %s\n",   argv[ 2 ]   );
						printf( "> Item : %s\n",   argv[ i ]   );
						printf( "> Item file type: %s (%d)\n",   argv[ i ]   ,  fexist(   argv[ i ] )   );

						strncpy( cmdi,  " ", PATH_MAX );
						strncpy( cmdi,  " ", PATH_MAX );
		                                if ( MYOS == 1 )
						   strncat( cmdi , "  fsck.ext3  -y /dev/" , PATH_MAX - strlen( cmdi ) -1 );
		                                //else if ( os_bsd_type == 3 )
						//   strncat( cmdi , "  fsck  -y " , PATH_MAX - strlen( cmdi ) -1 );
						else
						   strncat( cmdi , "  fsck -y  /dev/" , PATH_MAX - strlen( cmdi ) -1 );
		                                //if ( MYOS == 1 )
						//  strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination, new 
						//else
						  strncat( cmdi ,  fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
						strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
						nsystem( cmdi );

						printf( "- List process (status: completed).\n" );
					}
				}
			}
			return 0;
	}










	if ( argc >= 3 )
	if ( strcmp( argv[1] , "mount" ) ==  0 ) 
	{
			printf( "Automatic Mount:\n" );
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						//printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else if ( i >= 2 )
					{
					      /*
						printf( "> File process.\n" );
						printf( "- File process (status: begin).\n" );
						printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
						printf( "> Arg2 : %s\n",   argv[ 2 ]   );
						printf( "> Item : %s\n",   argv[ i ]   );
						printf( "> Item file type: %s (%d)\n",   argv[ i ]   ,  fexist(   argv[ i ] )   );
						*/

						{
							strncpy( cmdi,  " ", PATH_MAX );
							strncpy( cmdi,  " ", PATH_MAX );
							strncat( cmdi , " mkdir   /media ; mkdir /media/" , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi ,  fbasename( argv[ i ] )  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , "   " , PATH_MAX - strlen( cmdi ) -1 );
							nsystem( cmdi );

							strncpy( cmdi,  " ", PATH_MAX );
							strncpy( cmdi,  " ", PATH_MAX );
		                                        if ( strcmp( argv[1] , "mountro" ) ==  0 ) 
							{
							   if ( MYOS == 1 ) 
							     strncat( cmdi , "  mount -o ro  /dev/" , PATH_MAX - strlen( cmdi ) -1 );
							   else
							     // mount freebsd  running on netbsd 
							     // mount -o ro -t ffs /dev/dk0 /media/dk0 
							     strncat( cmdi , "  mount -o ro -t ffs  /dev/" , PATH_MAX - strlen( cmdi ) -1 );
							}
							else
							{
							     strncat( cmdi , "  mount /dev/" , PATH_MAX - strlen( cmdi ) -1 );
							}
							strncat( cmdi ,  fbasename(  argv[ i ] )  , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
							strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , "  /media/"  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi ,  fbasename(   argv[ i ] )  , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
							strncat( cmdi , "     "  , PATH_MAX - strlen( cmdi ) -1 );
							nsystem( cmdi );

							printf( "- List mount process (status: completed).\n" );
						}
					}
				}
			}
			printf( "=== LIST MEDIA DIRECTORY === \n" ); 
			system( "  mount | grep media "); 
			printf( "============================ \n" ); 
			return 0;
		}











	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "bridge" ) ==  0 ) || ( strcmp( argv[1] ,   "bridge0" ) ==  0 ) )
	{
             nsystem( " cd ; ifconfig bridge0 create up ;  brconfig bridge0 add mue0  ;   brconfig bridge0 add axen0   " ); 
	     return 0; 
        }





	if ( argc == 2 )
	if ( strcmp( argv[1] , "rescue" ) ==  0 ) 
	{
		printf( "\n" );
		printf( "\n" );
		printf( "menuentry 'NetBSD Chainloader (Rescue, Main OS, Partition 1, Grub)' {\n" );
		printf( "   set root='hd0,msdos1'\n" );
		printf( "   chainloader +1\n" );
		printf( "}\n" );
		printf( "\n" );
		printf( "\n" );
		return 0; 
        }















	if ( argc == 3 )
	if ( strcmp( argv[1] , "--grub-install" ) ==  0  ) 
	if ( strcmp( argv[2] , "sda1" ) ==  0  ) 
	{
		nsystem( "  mconfig mount sda1 " ); 
		nsystem( "  grub-install --no-floppy --root-directory=/media/sda1  /dev/sda   " );
		nsystem( "  mconfig grub >> /media/sda1/boot/grub/grub.cfg  " ); 
		return 0; 
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] , "--grub-install" ) ==  0  ) 
	if ( strcmp( argv[2] , "sdb1" ) ==  0  ) 
	{
		nsystem( "  mconfig mount sdb1 " ); 
		nsystem( "  grub-install --no-floppy --root-directory=/media/sdb1  /dev/sdb   " );
		nsystem( "  mconfig grub >> /media/sdb1/boot/grub/grub.cfg  " ); 
		return 0; 
	}





	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "grub-manjaro" ) ==  0  ) 
	{
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "set timeout=25\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux manjaro kernel 6.1 from sda1' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos1'\n" );
			printf( "	linux	/boot/vmlinuz-6.1-x86_64 root=/dev/sda1  rw \n" );   
			printf( "	initrd	/boot/initramfs-6.1-x86_64.img\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			return 0; 
	}








	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "grub-ubuntu-efi" ) ==  0  ) 
	{
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux Ubuntu AMD64 msdos1 sda1 5.19 26 efi sig for AMD64 PC Acer' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos1'\n" );
			printf( "	linux	/boot/vmlinuz-5.19.0-26-generic root=/dev/sda1  rw \n" );   
			printf( "	initrd	/boot/initrd.img-5.19.0-26-generic\n" );
			printf( "}\n" );
			printf( "\n" );
			return 0; 
	}







	if ( argc == 2 )
	if ( strcmp( argv[1] ,  "grub-linuxlive" ) ==  0  ) 
	{
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "set timeout=25\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Devuan Live (toram), from Partition 1 (Ext3)' {\n" );
			printf( "set root='hd0,msdos1'\n" );
			printf( "linux  /linuxlive/vmlinuz boot=live toram=linuxlive.squashfs  live-media-path=linuxlive    \n" );
			printf( "initrd /linuxlive/initrd.img\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			return 0; 
	}








	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,     "grub" ) ==  0  ) 
	|| ( strcmp( argv[1] ,       "GRUB" ) ==  0  ) 
	|| ( strcmp( argv[1] ,       "grub-linux" ) ==  0  ) )
	{
			printf( "\n" );
			printf( "\n" );
			printf( "set timeout=25\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
	                if ( strcmp( argv[1] ,       "grub-linux" ) ==  0  ) 
			{
			        // ===
			        // ===
			        // ===
				printf( "\n" );
				printf( "\n" );
				printf( "\n" );
				printf( "\n" );
				printf( "\n" );
				printf( "menuentry 'Linux OpenSuse with KDE Desktop, sdb2, Porto ' --class devuan --class gnu-linux --class gnu { \n" );
				printf( "	insmod gzio\n" );
				printf( "	insmod part_msdos\n" );
				printf( "	insmod ext2\n" );
				printf( "	set root='hd0,msdos2'\n" );
				printf( "	linux	/boot/vmlinuz-5.3.18-57-default root=/dev/sdb2 rw \n" );   
				printf( "	initrd	/boot/initrd-5.3.18-57-default\n" );
				printf( "}\n" );
				printf( "\n" );
				printf( "\n" );
				printf( "\n" );
			        // ===
			        // ===
			}



			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			/// to start boot.cfg and the main OS, using amd64 with grub2, and sysinst
			printf( "menuentry 'NetBSD Main OS, Chainloader (Current, BSD/Unix, Partition 1, Grub)' {\n" );
			printf( "   set root='hd0,msdos1'\n" );
			printf( "   chainloader +1\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			// virtualbox may crash with this:
			printf( "menuentry 'RAMDISK Slackware-64 Rescue, from NetBSD Rescue (hd0,msdos1)' {\n" );
			printf( "   set root='hd0,msdos1'\n" );
			printf( "   linux  /root/EFI/BOOT/huge.s\n" );
			printf( "   initrd /root/EFI/BOOT/initrd.img\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'FreeBSD Chainloader (hd0, msdos2)' {\n" );
			printf( "   set root='hd0,msdos2'\n" );
			printf( "   chainloader +1\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux devuan ascii live toram ' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos5'\n" );
			printf( "	linux	/devuan/live/vmlinuz boot=live toram=filesystem.squashfs live-media-path=devuan/live  username=devuan nonet  \n" );   
			printf( "	initrd	/devuan/live/initrd.img\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			/*
			printf( "\n" );
			printf( "menuentry 'Windows 8 Chainloader (hd0, msdos2)' {\n" );
			printf( "   set root='hd0,msdos2'\n" );
			printf( "   chainloader +1\n" );
			printf( "}\n" );
			*/
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux DEVUAN AMD64 sda3 for AMD64 PC (default, standard)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos3'\n" );
			printf( "	linux	/boot/vmlinuz-4.9.0-11-amd64 root=/dev/sda3  rw \n" );   
			printf( "	initrd	/boot/initrd.img-4.9.0-11-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux DEVUAN AMD64 gpt3 for AMD64 PC (default, standard)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,gpt3'\n" );
			printf( "	linux	/boot/vmlinuz-4.9.0-11-amd64 root=/dev/sda3  rw \n" );   
			printf( "	initrd	/boot/initrd.img-4.9.0-11-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux DEVUAN AMD64 sda4 for AMD64 PC (default, standard)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-4.9.0-11-amd64 root=/dev/sda4  rw \n" );   
			printf( "	initrd	/boot/initrd.img-4.9.0-11-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry '==========================' {\n" );
			printf( "   set root='hd0,msdos1'\n" );
			printf( "   chainloader +1\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux OpenSuse AMD64 disk on sda4 AMD64 5.10 7 muticore kernel chimaera PC (default, standard)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-5.10.0-7-amd64 root=/dev/sda4 rw \n" );   
			printf( "	initrd	/boot/initrd.img-5.10.0-7-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux DEVUAN AMD64 sdb4 for AMD64 PC, kernel 4.9.x (light desktop)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-4.9.0-11-amd64 root=/dev/sdb4  rw \n" );   
			printf( "	initrd	/boot/initrd.img-4.9.0-11-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux OpenSuse AMD64 sda4, kernel chimaera multicore i8' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-5.10.0-7-amd64 root=/dev/sda4 rw \n" );   
			printf( "	initrd	/boot/initrd.img-5.10.0-7-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux OpenSuse AMD64 sdb4, kernel chimaera multicore i8' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-5.10.0-7-amd64 root=/dev/sdb4  rw \n" );   
			printf( "	initrd	/boot/initrd.img-5.10.0-7-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );


			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux Devuan 4.9 686-pae x86 (partition 2, sdb2)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos2'\n" );
			printf( "	linux	/boot/vmlinuz-4.9.0-11-686-pae root=/dev/sdb2  rw \n" );   
			printf( "	initrd	/boot/initrd.img-4.9.0-11-686-pae\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux Devuan 4.9 686-pae x86 (partition 2, sdb3)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos3'\n" );
			printf( "	linux	/boot/vmlinuz-4.9.0-11-686-pae root=/dev/sdb3 rw \n" );   
			printf( "	initrd	/boot/initrd.img-4.9.0-11-686-pae\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );



			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux Devuan 4.9 686-pae x86 (partition 2, sda2)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos2'\n" );
			printf( "	linux	/boot/vmlinuz-4.9.0-11-686-pae root=/dev/sda2  rw \n" );   
			printf( "	initrd	/boot/initrd.img-4.9.0-11-686-pae\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			/*
			printf( "\n" );
			printf( "menuentry 'Linux Devuan 4.9 686-pae x86 (partition 4, sda4)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-4.9.0-11-686-pae root=/dev/sda4  rw \n" );   
			printf( "	initrd	/boot/initrd.img-4.9.0-11-686-pae\n" );
			printf( "}\n" );
			printf( "\n" );
			*/
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			/*
			   printf( "\n" );
			   printf( "menuentry 'Linux OpenSuse with KDE Desktop, sda2, Porto ' --class devuan --class gnu-linux --class gnu { \n" );
			   printf( "	insmod gzio\n" );
			   printf( "	insmod part_msdos\n" );
			   printf( "	insmod ext2\n" );
			   printf( "	set root='hd0,msdos2'\n" );
			   printf( "	linux	/boot/vmlinuz-5.3.18-57-default root=/dev/sda2 rw \n" );   
			   printf( "	initrd	/boot/initrd-5.3.18-57-default\n" );
			   printf( "}\n" );
			   printf( "\n" );
			 */
			printf( "\n" );
			/*
			printf( "\n" );
			printf( "menuentry 'Linux DEVUAN AMD64 sda4 AMD64 5.10 7 muticore chimaera PC (default, standard)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-5.10.0-7-amd64 root=/dev/sda4  rw \n" );   
			printf( "	initrd	/boot/initrd.img-5.10.0-7-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			*/
			// kernel, it works fine on intel latitude dell notebook, i8  
			printf( "\n" );
			printf( "menuentry 'Linux DEVUAN AMD64 gpt2 mmcblk0p2 AMD64 5.10 7 muticore chimaera PC (default, standard)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,gpt2'\n" );
			printf( "	linux	/boot/vmlinuz-5.10.0-7-amd64 root=/dev/mmcblk0p2  rw \n" );   
			printf( "	initrd	/boot/initrd.img-5.10.0-7-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			// kernel, it works fine on intel latitude dell notebook, i8  
			/*
			printf( "menuentry 'Linux DEVUAN AMD64 sdb4 AMD64 5.10 7 muticore chimaera PC (default, standard)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-5.10.0-7-amd64 root=/dev/sdb4  rw \n" );   
			printf( "	initrd	/boot/initrd.img-5.10.0-7-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			*/
			printf( "\n" );
			/*
			printf( "\n" );
			printf( "menuentry 'Linux DEVUAN AMD64 MMC-SD on P2 AMD64 5.10 7 muticore chimaera PC (default, standard)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos2'\n" );
			printf( "	linux	/boot/vmlinuz-5.10.0-7-amd64 root=/dev/mmcblk0p2  rw \n" );   
			printf( "	initrd	/boot/initrd.img-5.10.0-7-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			*/
			printf( "\n" );
			/*
			printf( "menuentry 'Linux DEVUAN AMD64 sda4 AMD64 5.10 16 muticore chimaera PC (default, standard)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-5.10.0-16-amd64 root=/dev/sda4  rw \n" );   
			printf( "	initrd	/boot/initrd.img-5.10.0-16-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
                        // initrd.img-5.10.0-7-amd64  
                        // vmlinuz-5.10.0-7-amd64   
			printf( "\n" );
			printf( "\n" );
			// it works fine on intel latitude dell notebook, i8  
			printf( "menuentry 'Linux DEVUAN AMD64 sdb4 AMD64 5.10 16 muticore chimaera PC (default, standard)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-5.10.0-16-amd64 root=/dev/sdb4  rw \n" );   
			printf( "	initrd	/boot/initrd.img-5.10.0-16-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			*/
			printf( "\n" );
                        // initrd.img-5.10.0-7-amd64  
                        // vmlinuz-5.10.0-7-amd64   
			printf( "\n" );
			printf( "\n" );

			printf( "\n" );
			/// FreeBSD Desktop over kfreebsd ... 
			/// on sda2 msdos2  
			printf( "menuentry 'FreeBSD Boot Loader, (hd0, msdos2)' {\n" );
			printf( "   set root='hd0,msdos2'\n" );
			printf( "   kfreebsd /boot/loader\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
                        // initrd.img-5.10.0-7-amd64  
                        // vmlinuz-5.10.0-7-amd64   
			printf( "\n" );
			printf( "\n" );
			/*
			printf( "\n" );
			printf( "menuentry 'Linux EFI DEVUAN AMD64 5.15.0-43 on sda3 from gpt3 for AMD64 PC (light desktop)' --class devuan --class gnu-linux --class gnu {   \n" );
			printf( "        insmod gzio\n" );
			printf( "        insmod part_msdos\n" );
			printf( "        insmod ext2\n" );
			printf( "        set root='hd0,gpt3'\n" );
			printf( "        linux   /boot/vmlinuz-5.15.0-43-generic root=/dev/sda3  rw\n" );
			printf( "        initrd  /boot/initrd.img-5.15.0-43-generic\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux DEVUAN AMD64 4.9.0-11 on sda3 from gpt3 for AMD64 PC (light desktop)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "        insmod gzio\n" );
			printf( "        insmod part_msdos\n" );
			printf( "        insmod ext2\n" );
			printf( "        set root='hd0,gpt3'\n" );
			printf( "        linux   /boot/vmlinuz-4.9.0-11-amd64 root=/dev/sda3  rw \n" );
			printf( "        initrd  /boot/initrd.img-4.9.0-11-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			*/
			printf( "\n" );
			/// FreeBSD Desktop over chainloader ... 
			///
			/*
			printf( "menuentry 'FreeBSD Desktop, Stable, Chainloader (Partition 2, Grub)' {\n" );
			printf( "   set root='hd0,msdos2'\n" );
			printf( "   chainloader +1\n" );
			printf( "}\n" );
			printf( "\n" );
			*/
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			// linux16  on grub2: 
			// initrd16 
			///printf( "menuentry 'RAMDISK Slackware Rescue efi86, from NetBSD Rescue (hd0,msdos1)' {\n" );
			printf( "menuentry 'RAMDISK Slackware-10 i386 Rescue, from NetBSD Rescue (hd0,msdos1)' {\n" );
			printf( "   set root='hd0,msdos1'\n" );
	 	        printf( "   linux16  /root/slackware10/bzImage load_ramdisk=1 prompt_ramdisk=0 ramdisk_size=6464 rw root=/dev/ram SLACK_KERNEL=bare.i \n"); 
			printf( "   initrd16 /root/slackware10/initrd.img\n" ); 
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			/*
			printf( "\n" );
			printf( " menuentry 'RAMDISK FreeDOS from NetBSD partition 1, image with raw parameter' {\n" );
			printf( "   set root=(hd0,msdos1)\n" );
			printf( "   linux16 /root/freedos/memdisk raw\n" );
			printf( "   initrd16 /root/freedos/fdos1440.img \n" );
			printf( "}\n" );
			*/
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			/*
			printf( "\n" );
			printf( "menuentry 'Linux DEVUAN i386 sda3 (x86, 4.9.0-11-686)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos3'\n" );
			printf( "	linux	/boot/vmlinuz-4.9.0-11-686 root=/dev/sda3  rw \n" );   
			printf( "	initrd	/boot/initrd.img-4.9.0-11-686\n" );
			printf( "}\n" );
			printf( "\n" );
			*/
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			/*
			/// FreeBSD Desktop over kfreebsd ... 
			/// on sda2 msdos2  
			printf( "\n" );
			printf( "menuentry 'FreeBSD Boot Loader, (hd0, msdos2)' {\n" );
			printf( "   set root='hd0,msdos2'\n" );
			printf( "   kfreebsd /boot/loader\n" );
			printf( "}\n" );
			printf( "\n" );
			*/
			printf( "\n" );
			printf( "\n" );
			/*
			printf( "\n" );
			printf( "menuentry 'Linux DEVUAN AMD64 sda3, Ascii PC (rescue only, devuan tiny)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos3'\n" );
			printf( "	linux	/boot/vmlinuz-4.9.0-11-amd64 root=/dev/sda3  rw \n" );   
			printf( "	initrd	/boot/initrd.img-4.9.0-11-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux DEVUAN AMD64 sda4 for AMD64 PC (default, standard)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-4.9.0-11-amd64 root=/dev/sda4  rw \n" );   
			printf( "	initrd	/boot/initrd.img-4.9.0-11-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux DEVUAN AMD64 sdb4 for AMD64 PC (default, standard)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-4.9.0-11-amd64 root=/dev/sdb4  rw \n" );   
			printf( "	initrd	/boot/initrd.img-4.9.0-11-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			*/
			printf( "\n" );
			printf( "\n" );
			/*
			printf( "menuentry 'Linux Devuan X11, WM on sda4 (i386, 4.9.0-11-686, stable)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos3'\n" );
			printf( "	linux	/boot/vmlinuz-4.9.0-11-686 root=/dev/sda4 rw \n" );   
			printf( "	initrd	/boot/initrd.img-4.9.0-11-686\n" );
			printf( "}\n" );
			*/
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			/*
			printf( "menuentry 'Linux DEVUAN i386 sda3 with 3.16 pae x86 (partition 3)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos3'\n" );
			printf( "	linux	/boot/vmlinuz-3.16.0-6-686-pae   root=/dev/sda3  rw \n" );   
			printf( "	initrd	/boot/initrd.img-3.16.0-6-686-pae\n"); 
			printf( "}\n" );
			printf( "\n" );
			*/
			printf( "\n" );
			printf( "\n" );
			/*
			printf( "menuentry 'Linux OpenSuse with KDE Desktop, sda2, Tokyo' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos2'\n" );
			printf( "	linux	/boot/vmlinuz-5.3.18-57-default root=/dev/sda2  rw \n" );   
			printf( "	initrd	/boot/initrd-5.3.18-57-default\n" );
			printf( "}\n" );
			printf( "\n" );
			*/
			printf( "\n" );
			printf( "\n" );
			/*
			printf( "menuentry '(Area-2, Tokyo) Linux OpenSuse with KDE Desktop, sda2' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos2'\n" );
			printf( "	linux	/boot/vmlinuz-5.3.18-57-default root=/dev/sda2  rw \n" );   
			printf( "	initrd	/boot/initrd-5.3.18-57-default\n" );
			printf( "}\n" );
			*/
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'Linux OpenSuse with KDE Desktop, sda4, Paris' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-5.3.18-57-default root=/dev/sda4  rw \n" );   
			printf( "	initrd	/boot/initrd-5.3.18-57-default\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			/*
			printf( "menuentry 'Linux Devuan 4.9 i386-pae (partition 4)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-4.9.0-11-686-pae root=/dev/sda4  rw \n" );   
			printf( "	initrd	/boot/initrd.img-4.9.0-11-686-pae\n" );
			printf( "}\n" );
			*/
			printf( "\n" );
			printf( "\n" );
			/*
			printf( "\n" );
			printf( "menuentry 'Linux Devuan i386 with 3.16 pae for PRINTER (partition 4)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-3.16.0-6-686-pae   root=/dev/sda4  rw \n" );   
			printf( "	initrd	/boot/initrd.img-3.16.0-6-686-pae\n"); 
			printf( "}\n" );
			printf( "\n" );
			*/
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			/*
			printf( "\n" );
			printf( "menuentry 'Linux OpenSuse with KDE Desktop, sda4' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-5.3.18-57-default root=/dev/sda4  rw \n" );   
			printf( "	initrd	/boot/initrd-5.3.18-57-default\n" );
			printf( "}\n" );
			printf( "\n" );
			*/

			//// 
			//    linux   /boot/vmlinuz-4.9.0-11-amd64 root=/dev/vda2  rw   vga=normal console=tty0 console=ttyS0,115200n8  single
			//// 
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			/*
			printf( "\n" );
			printf( "menuentry 'Linux DEVUAN AMD64 Hypervisor VMM vda4 for AMD64 PC (main)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	insmod gzio\n" );
			printf( "	insmod part_msdos\n" );
			printf( "	insmod ext2\n" );
			printf( "	set root='hd0,msdos4'\n" );
			printf( "	linux	/boot/vmlinuz-4.9.0-11-amd64  root=/dev/vda4 rw vga=normal console=tty0 console=ttyS0,115200n8 single \n" );   
			printf( "	initrd	/boot/initrd.img-4.9.0-11-amd64\n" );
			printf( "}\n" );
			printf( "\n" );
			*/
			printf( "\n" );

			printf( "\n" );
			printf( "\n" );
			/*
			printf( "menuentry 'FreeBSD Boot Loader, (hd0, msdos4)' {\n" );
			printf( "   set root='hd0,msdos4'\n" );
			printf( "   kfreebsd /boot/loader\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			*/
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			/*
			// linux16  on grub2: 
			// initrd16 
			///printf( "menuentry 'RAMDISK Slackware Rescue efi86, from NetBSD Rescue (hd0,msdos1)' {\n" );
			printf( "menuentry 'RAMDISK Slackware-10 i386 Rescue, from NetBSD Rescue (hd0,msdos1)' {\n" );
			printf( "   set root='hd0,msdos1'\n" );
	 	        printf( "   linux16  /root/slackware10/bzImage load_ramdisk=1 prompt_ramdisk=0 ramdisk_size=6464 rw root=/dev/ram SLACK_KERNEL=bare.i \n"); 
			printf( "   initrd16 /root/slackware10/initrd.img\n" ); 
			printf( "}\n" );
			printf( "\n" );
			*/

			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'NetBSD Chainloader (Rescue, Main OS, Partition 1, Grub)' {\n" );
			printf( "   set root='hd0,msdos1'\n" );
			printf( "   chainloader +1\n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "\n" );
			printf( "menuentry 'PC Halt (Power Off, Shutdown Personal Computer)' --class devuan --class gnu-linux --class gnu { \n" );
			printf( "	halt \n" );
			printf( "}\n" );
			printf( "\n" );
			printf( "\n" );
			return 0; 
	}
	// end of grub








	if ( argc == 2 )
	if ( strcmp( argv[1] ,     "update" ) ==  0  ) 
	{
                npkg_update(); 
		return 0;
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] ,     "update-pkg" ) ==  0  ) 
	{
	        nsystem( " mconfig create sources.list " );  // <--- clean up soon here  
                npkg_update();   
		return 0;
	}

	if ( argc == 3 )
	if ( strcmp( argv[1] , "sources.list" ) ==  0 ) 
	if ( strcmp( argv[2] , "stretch" ) ==  0 ) 
	{
           printf( "deb http://mirrordirector.raspbian.org/raspbian/ stretch main contrib non-free rpi\n" ); 
	   return 0; 
        }




        ///////////////////////////////////////
	if ( argc == 2 )
	if ( strcmp( argv[1] , "sources.list" ) ==  0 ) 
	{
	        /*
		if ( fexist( "/usr/bin/emulationstation" ) == 1 ) 
		{
			printf( "deb http://raspbian.raspberrypi.org/raspbian/   bullseye main contrib non-free rpi\n" ); 
			printf( "# default sid, deb http://ftp.debian.org/debian sid main ");
			printf( "\n" ); 
		}
		*/
                if ( fexist( "/lib/modules/4.9.59-v7+" ) == 2 ) 
		{
			printf( "deb http://mirrordirector.raspbian.org/raspbian/ stretch main contrib non-free rpi\n" ); 
		}
		else if ( fexist( "lib/modules/4.9.0-11-amd64/" ) == 2 ) 
		{
			printf( "# https://www.devuan.org/os/packages\n" );
			printf( "deb http://archive.devuan.org/merged ascii          main\n" );
			printf( "deb http://archive.devuan.org/merged ascii-security main\n" );
	                printf( "# deb http://archive.debian.org/debian stretch main \n" ); 
			printf( "\n" ); 
		}
		return 0; 
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] , "fbterm-fb1" ) ==  0 ) 
	{
             nsystem( "  export FRAMEBUFFER=/dev/fb1 ; /usr/bin/fbterm –r 1 -- /bin/login < /dev/tty1  " );
	     return 0;
	}








	if ( argc == 3 )
	if ( strcmp( argv[1] , "create" ) ==  0 ) 
	if ( strcmp( argv[2] , "sources.list" ) ==  0 ) 
	{
	        nsystem( " mconfig sources.list > /etc/apt/sources.list " );   
		return 0; 
	}
        ///////////////////////////////////////








	if ( argc == 3 )
	if ( strcmp( argv[1] , "update" ) ==  0 ) 
	if ( strcmp( argv[2] , "grub" ) ==  0 ) 
	{
		nsystem( "  /usr/local/bin/mconfig  grub > /grub/grub.cfg " ); 
		nsystem( "  cat /grub/grub.cfg " ); 
		printf(  " = Grub Updated =\n" ); 
		return 0;
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] , "update" ) ==  0 ) 
	if ( strcmp( argv[2] , "grub-linux" ) ==  0 ) 
	{
		nsystem( "  /usr/local/bin/mconfig  grub-linux  > /grub/grub.cfg " ); 
		nsystem( "  cat /grub/grub.cfg " ); 
		printf(  " = Grub Updated =\n" ); 
		return 0;
	}








	// custom automatic
	if ( argc == 3 )
	if ( strcmp( argv[1] ,       "install" ) ==  0  ) 
	if ( ( strcmp( argv[2] ,       "grub-wd0" ) ==  0 )  || ( strcmp( argv[2] ,       "grub-sd0" ) ==  0 ) )
	{
		if ( MYOS != 1 ) 
		{
			printf( "HOME: %s\n", getenv( "HOME" ));
			chdir( getenv( "HOME") );
			printf( "%s\n", getenv( "HOME") );
			if ( strcmp( argv[2] ,       "grub-sd0" ) ==  0 ) 
				nsystem( " grub-install --no-floppy /dev/rsd0 " );
			else
				nsystem( " grub-install --no-floppy /dev/rwd0 " );
			nsystem( " mconfig grub           > /grub/grub.cfg " );   // setup the chainloader for partition1 for netbsd 
			nsystem( " mconfig boot           > /boot.cfg " );        // setup the vesa for startx  
		}
		else if ( MYOS == 1 ) 
		{
		  printf( " Note: hey, this is Linux! \n" ); 
		}
		return 0;
	}









            



	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "boot-netbsd" ) ==  0 ) || ( strcmp( argv[1] ,   "boot.cfg" ) ==  0 ) )
	{
	       void_print_boot_banner();
	       return 0;
	}

	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "boot.cfg.default" ) ==  0 ) 
	{
	        /// from netbsd 8
		// remind me ... 
		printf( "menu=Boot normally:boot\n" ); 
		printf( "menu=Boot single-user:boot -s\n" ); 
		printf( "menu=Boot with serial console:consdev com0;boot\n" ); 
		printf( "menu=Boot with module foo:load /foo.kmod;boot\n" ); 
		printf( "menu=:boot hd1a:netbsd -as\n" ); 
		return 0;
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] ,   "help" ) ==  0 ) 
        if ( ( strcmp( argv[2] , "adduser" ) ==  0 ) || ( strcmp( argv[2] , "useradd" ) ==  0 ))
	{
		nsystem( "  useradd -m  -G  wheel  -u 3010 netbsd \n" );
		return 0;
	}







	if ( argc == 2)
	if ( strcmp( argv[1] , "dyndns" ) ==  0 ) 
	{
		//nwebbrowser( "checkip.dyndns.com/" );
                //procedure_webbrowser( "www.duckduckgo.com" ); 
		nsystem( "  links http://checkip.dyndns.com   " );
		return 0;
	}









        // for the panel 
	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "--firefox" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--fr" ) ==  0 )
	|| ( strcmp( argv[1] , "--firefox" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--new-firefox" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-firefox" ) ==  0 ) )
	{
		printf( " ============== \n "); 
	        printf( " Command with FIREFOX first\n" ); 
		printf( " ============== \n "); 
		for( i = 2 ; i < argc ; i++) 
		{
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );

			if ( fexist( "/usr/bin/firefox" ) == 1 ) 
			    ncmdwith( " cd ; /usr/bin/firefox -new-window " , argv[ i ]  );

			else if ( fexist( "/usr/bin/firefox-esr" ) == 1 ) 
			    ncmdwith( " cd ; /usr/bin/firefox-esr -new-window " , argv[ i ]  );

			else if ( fexist( "/usr/bin/firefox-esr" ) == 1 ) 
			    ncmdwith( " /usr/bin/firefox-esr -new-window " , argv[ i ]  );
			else if ( fexist( "/usr/bin/chromium" ) == 1 ) 
			    ncmdwith( " /usr/bin/chromium --new-window " , argv[ i ]  );
			else if ( fexist( "/usr/bin/chromium-browser" ) == 1 ) 
			    ncmdwith( " /usr/bin/chromium-browser --new-window " , argv[ i ]  );
			else 
			    procedure_webbrowser( argv[ i ]  );
			    //ncmdwith( " dillo  " , argv[ i ]  );
		}
		printf( " ============== \n "); 
		return 0; 
	}




	if ( argc >= 3 )
	if ( strcmp( argv[1] , "--vim" ) ==  0 ) 
	{
		printf( " ============== \n "); 
		printf( " ============== \n "); 
		for( i = 2 ; i < argc ; i++) 
		{
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
			//procedure_webbrowser_console( argv[ i ]  );
			snprintf( charo , sizeof( charo ), " /usr/bin/vim \"%s\"  " , argv[i] ); 
			nsystem( charo ); 
		}
		printf( " ============== \n "); 
		return 0; 
	}




	if ( argc >= 3 )
	if ( strcmp( argv[1] , "--med" ) ==  0 ) 
	{
		printf( " ============== \n "); 
		printf( " ============== \n "); 
		for( i = 2 ; i < argc ; i++) 
		{
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
			//procedure_webbrowser_console( argv[ i ]  );
			snprintf( charo , sizeof( charo ), " /usr/games/mednafen  \"%s\"  " , argv[i] ); 
			nsystem( charo ); 
		}
		printf( " ============== \n "); 
		return 0; 
	}







	if ( argc >= 3 )
	if (  ( strcmp( argv[1] , "--links" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--console-web-browser" ) ==  0 )  
	|| ( strcmp( argv[1] , "-links" ) ==  0 )  
	|| ( strcmp( argv[1] , "--gg" ) ==  0 ) ) 
	{
		printf( " ============== \n "); 
	        printf( " Command with links or similar, console mode.\n" ); 
		printf( " ============== \n "); 
		for( i = 2 ; i < argc ; i++) 
		{
			printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
			procedure_webbrowser_console( argv[ i ]  );
		}
		printf( " ============== \n "); 
		return 0; 
	}












	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "ncr" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "-ncr" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "--ncr" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "--web" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "-web" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "--chromium" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "-chromium" ) ==  0 ) ) 
	{
	      //	nsystem( "  chromium --new-window " ); 
	       if ( fexist( "/usr/bin/chromium" ) == 1 )  
	       {
		 snprintf( charo , sizeof( charo ), " chromium --new-window  \"%s\"  " ,  "www.duckduckgo.com" ); 
		 nsystem( charo ); 
	        }	
	        else if ( fexist( "/usr/bin/firefox" ) == 1 )  
	       {
		 snprintf( charo , sizeof( charo ), " firefox -new-window  \"%s\"  " ,  "www.duckduckgo.com" ); 
		 nsystem( charo ); 
	        }	
		else 
		  procedure_webbrowser( "www.duckduckgo.com" ); 
		return 0;
	}






	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "cr" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "--chromium" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-ncr" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--ncr" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-web" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--web" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--web-chromium-browser" ) ==  0 ) 
	|| ( strcmp( argv[1] , "xcr" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--cr" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--chromium" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-chromium" ) ==  0 ) ) 
	{
		printf( " ============== \n "); 
	        printf( " Command with CHROMIUM first\n" ); 
		printf( " ============== \n "); 

	        if  ( fexist( "/snap/bin/chromium" ) == 1 ) 
		{
		  snprintf( charo , sizeof( charo ), "  /snap/bin/chromium --new-window  \"%s\"  " ,  argv[ 2 ] ); 
		  nsystem( charo ); 
		}
	        else if  ( fexist( "/usr/bin/chromium-browser" ) == 1 ) 
		{
		  snprintf( charo , sizeof( charo ), "  /usr/bin/chromium-browser --new-window  \"%s\"  " ,  argv[ 2 ] ); 
		  nsystem( charo ); 
		}
	        else if  ( fexist( "/usr/bin/chromium" ) == 1 ) 
		{
		  //nsystem( charo ); 
		  snprintf( charo , sizeof( charo ), "  /usr/bin/chromium --new-window  \"%s\"  " ,  argv[ 2 ] ); 
		  nsystem( charo ); 
		}
	        else if  ( strcmp( argv[1] ,    "xcr" ) ==  0 ) 
		{
		  snprintf( charo , sizeof( charo ), " export DISPLAY=:0 ;  chromium --new-window  \"%s\"  " ,  argv[ 2 ] ); 
		  nsystem( charo ); 
		}
		else
		  procedure_webbrowser(  argv[ 2 ] ); 
		return 0;
	}









        /// to keep  to keep, flvoip uses it.
	if ( argc >= 3 )
	if (  ( strcmp( argv[1] , "--webbrowser" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-web-browser" ) ==  0 )    
	|| ( strcmp( argv[1] , "--web-browser" ) ==  0 )  )  
	{
	        printf( " > Web Browser (search an appropriate web browser for your service.\n" ); 
		procedure_webbrowser(  argv[ 2 ] ); 
		return 0;
	}




	if ( argc >= 2 )
	if ( ( strcmp( argv[1] , "--weburl" ) ==  0 ) ||  ( strcmp( argv[1] , "-weburl" ) ==  0 ) )
	{
                procedure_webbrowser( argv[ 2 ] ); 
		return 0;
	}





       if ( argc == 3 )
       if ( strcmp( argv[1] ,    "--source-archive" )   ==  0   )
       {
	       //fetch_file_ftp( "http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-7.1.1/source/sets/syssrc.tgz" );  
	       //fetch_file_ftp( "http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-7.1.1/source/sets/src.tgz" );  
	       //fetch_file_ftp( "http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-7.1.1/source/sets/sharesrc.tgz" );  
	       //fetch_file_ftp( "http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-7.1.1/source/sets/gnusrc.tgz" );  
	       //fetch_file_ftp( "http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-7.1.1/source/sets/xsrc.tgz" );  
	       snprintf( charo , sizeof( charo ), "http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-%s/source/sets/syssrc.tgz", argv[ 2 ] );
	       fetch_file_ftp( charo ); 

	       snprintf( charo , sizeof( charo ), "http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-%s/source/sets/src.tgz", argv[ 2 ] );
	       fetch_file_ftp( charo ); 

	       snprintf( charo , sizeof( charo ), "http://archive.netbsd.org/pub/NetBSD-archive/NetBSD-%s/source/sets/sharesrc.tgz", argv[ 2 ] );
	       fetch_file_ftp( charo ); 
	       return 0; 
       }








	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "untar" ) ==  0 ) || ( strcmp( argv[1] , "-untar" ) ==  0 ) || ( strcmp( argv[1] , "--untar" ) ==  0 ) )
	{
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else
					{
						printf( "> untar process.\n" );
						printf( "- File untar process (status: begin).\n" );
						printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
						strncpy( cmdi, "", PATH_MAX );
						strncat( cmdi , "  tar xvpfz  " , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , "  \"" , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , "\"  " , PATH_MAX - strlen( cmdi ) -1 );
						nsystem( cmdi );
					}
				}
			}
			return 0;
	}








	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "kernelbuild" ) ==  0 ) || ( strcmp( argv[1] , "buildkernel" ) ==  0 ) )
	{
		printf( "  cd / ;  rm *.tgz ; mconfig source 9.1 ; mconfig --untar *.tgz ; cd /usr/src ; mconfig build xxx "); 
		printf( "\n"); 
		nsystem( "  cd /usr/ ; mkdir src ; mkdir /usr/src ; cd / ; chmod 777 /usr/src ;  mconfig source 9.1 ; pwd ;  cd / ; mconfig --untar  syssrc.tgz  src.tgz  sharesrc.tgz  ;  cd /usr/src ; mconfig build  amd64 " ); 
		//printf( "  cd / ;  rm *.tgz ; mconfig source 9.1 ; mconfig --untar *.tgz ; cd /usr/src ; mconfig build xxx "); 
		//printf( "\n"); 
		printf( "  Your kernel shall appear in: /usr/src/sys/arch/amd64/compile/obj/GENERIC/netbsd \n" ); 
		//nsystem( "  ls -ltra /usr/src/sys/arch/amd64/compile/obj/GENERIC/netbsd   " ); 
		//nsystem( "  ls -ltra /usr/src/sys/arch/amd64/compile/obj/MYKERNEL/netbsd   " ); 
		//nsystem( "  md5 /usr/src/sys/arch/amd64/compile/obj/GENERIC/netbsd   " ); 
		//nsystem( "  md5 /usr/src/sys/arch/amd64/compile/obj/MYKERNEL/netbsd   " ); 
		return 0; 
	}








       // ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/xsrc.tgz      
       // ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/xsrc.tgz      
       // ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/xsrc.tgz      
       ///
       // ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/syssrc.tgz      
       // ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/src.tgz      
       // ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/xsrc.tgz      
       // ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/sharesrc.tgz      
       if ( argc >= 3 )
       if ( strcmp( argv[1] ,    "source" )   ==  0   )
       /// 2 the release e.g. 9.1
       {
	       snprintf( charo , sizeof( charo ), "http://cdn.netbsd.org/pub/NetBSD/NetBSD-%s/source/sets/syssrc.tgz", argv[ 2 ] );
	       fetch_file_ftp( charo ); 

	       snprintf( charo , sizeof( charo ), "http://cdn.netbsd.org/pub/NetBSD/NetBSD-%s/source/sets/src.tgz", argv[ 2 ] );
	       fetch_file_ftp( charo ); 

	       snprintf( charo , sizeof( charo ), "http://cdn.netbsd.org/pub/NetBSD/NetBSD-%s/source/sets/sharesrc.tgz", argv[ 2 ] );
	       fetch_file_ftp( charo ); 
	       return 0; 
       }
















	if ( argc == 3 )
	if (   strcmp( argv[1] , "compile" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "links" ) ==  0 ) || ( strcmp( argv[2] , "links2" ) ==  0 ) || ( strcmp( argv[2] , "cmlinks" ) ==  0 ) )
	{
		fetch_file_ftp(   "http://links.twibright.com/download/links-2.20.2.tar.gz" );
		nsystem(  "   tar xvpfz links-2.20.2.tar.gz ; cd  links-2.20.2/ ; ./configure ; make ;  cp links cmlinks " );
		return 0;
	}





	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "compile" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-compile" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--compile" ) ==  0 ) )
	if ( strcmp( argv[2] , "ncftp" ) ==  0 ) 
	{
		fetch_file_ftp(  "ftp://ftp.ncftp.com/ncftp/ncftp-3.2.6-src.tar.gz" );
		nsystem( "  tar xvpfz ncftp-3.2.6-src.tar.gz ;  cd ncftp-3.2.6 ; ./configure ; make ; cp bin/ncftp /usr/local/bin/ncftp  ;  cp bin/ncftpput  /usr/local/bin/ncftpput ; cp bin/ncftpget  /usr/local/bin/ncftpget  " ); 
		return 0;
	}




	if ( argc == 3 )
	if ( strcmp( argv[1] , "compile" ) ==  0) 
	if ( strcmp( argv[2] , "dillo" ) ==  0) 
	{
		fetch_file_ftp( "http://deb.debian.org/debian/pool/main/d/dillo/dillo_3.0.5.orig.tar.bz2"  );
		nsystem( "  tar xvf dillo_3.0.5.orig.tar.bz2 ; cd dillo-3.0.5 ;  ./configure ; make  "); 
		nsystem( "  cp dillo-3.0.5/src/dillo  /usr/local/bin/cmdillo  " );    // cmdillo, custom dillo  
		return 0; 
	}

	if ( argc == 3 )
	if ( strcmp( argv[1] , "compile" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "links-gui" ) ==  0 ) || ( strcmp( argv[2] , "cmlinks-gui" ) ==  0 ) )
	{
		fetch_file_ftp(   "http://links.twibright.com/download/links-2.20.2.tar.gz" );
		nsystem(  "   tar xvpfz links-2.20.2.tar.gz ; cd  links-2.20.2/ ; ./configure  --enable-graphics ; make ;  cp links cmlinks-gui  " );
		return 0;
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( ( strcmp( argv[2] , "links" ) ==  0 ) || ( strcmp( argv[2] , "links2" ) ==  0 ) )
	{
		printf( " fetch -R  http://links.twibright.com/download/links-2.20.2.tar.gz ;  tar xvpfz links-2.20.2.tar.gz ; cd  links-2.20.2/ ; ./configure ; make ;  cp links cmlinks " );
		return 0;
	}





        ////////////////////////////////////
	//// nsystem(  "   cd  links-2.20.2/ ;  cp links /usr/local/bin/links    " );
	//// nsystem(  "   cd  links-2.20.2/ ;  cp links /usr/local/bin/links2    " );
	if ( argc == 2 )
	if ( strcmp( argv[1] , "links" ) ==  0 ) 
	{
		printf(  " wget -c --no-check-certificate http://links.twibright.com/download/links-2.20.2.tar.gz ; tar xvpfz links-2.20.2.tar.gz ; cd  links-2.20.2/ ; ./configure ; make \n" ); 
		printf( "HOME: %s\n", getenv( "HOME" ));
		chdir( getenv( "HOME") );
		fetch_file_ftp(   "http://links.twibright.com/download/links-2.20.2.tar.gz" );
		nsystem(  "   mkdir /usr/local ; mkdir /usr/local/bin/ ; tar xvpfz links-2.20.2.tar.gz ; cd  links-2.20.2/ ; ./configure ; make " );
		nsystem(  "   cd  links-2.20.2/ ;  cp links /usr/local/bin/cmlinks    " );
		return 0;
	}
        ////////////////////////////////////




	if ( argc == 3 )
	if ( strcmp( argv[1] , "get" )    ==  0 ) 
	if ( strcmp( argv[2] , "links" ) ==  0 ) 
	{
	   fetch_file_ftp(  "http://links.twibright.com/download/links-2.20.2.tar.gz" );
	   return 0; 
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] , "compile" ) ==  0 ) 
	if ( strcmp( argv[2] , "lynx" ) ==  0 ) 
	{
			fetch_file_ftp( "https://invisible-mirror.net/archives/lynx/tarballs/lynx2.8.9rel.1.tar.gz" ); 
			nsystem( "   tar xvpfz  lynx2.8.9rel.1.tar.gz ; cd  lynx2.8.9rel.1  ; ./configure --with-ssl --with-gnutls --with-gnutls-openssl ; make ; make install " );
			return 0;
	}




	if ( argc == 3 )
	if ( strcmp( argv[1] , "compile" )    ==  0 ) 
	if ( strcmp( argv[2] , "mutt" )    ==  0 ) 
	{
	              if ( MYOS == 1 ) 
			   npkg( "  libssl-dev libcrypto++-dev   "); 
		      fetch_file_ftp(  "ftp://ftp.mutt.org/pub/mutt/mutt-1.7.2.tar.gz" );
		      nsystem( " tar xpfz mutt-1.7.2.tar.gz " ); 
		      if ( fexist( "mutt-1.7.2" ) == 2 )  
			      nsystem(  " cd mutt-1.7.2 ; ./configure --with-ssl --enable-imap  ; make " ); 
		      return 0;
	}










	if ( argc == 3 )
	if ( strcmp( argv[1] , "compile" ) ==  0 ) 
	if ( strcmp( argv[2] , "xweb" ) ==  0 ) 
	{
		fetch_file_ftp(   "http://links.twibright.com/download/links-2.20.2.tar.gz" );
		nsystem(  "  tar xvpfz links-2.20.2.tar.gz ; cd  links-2.20.2/ ; ./configure --enable-graphics ; make ; cp links xweb  " );
		return 0;
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "xlock" ) ==  0 ) 
	{
		//nsystem( " cd ; echo abstractile ; xscreensaver-command   " );
		nsystem( " cd ; xscreensaver & ;  cd ; xscreensaver-command  -lock " ); 
		return 0;
	}



	if ( argc >= 3 )
        if ( strcmp( argv[1] ,   "--xzcat" ) ==  0 ) 
	{

			printf( " =>  Processing example --zcat /mydir/file.gz sda sdb sdc.\n" ); 
			//if ( fexist( argv[ 2 ] ) == 1 ) 
			{
				printf( " The image file with extension .img.gz %s is available, and lets go!! \n" , argv[ 2 ] ); 
				if ( argc >= 2)
				{
					for( i = 1 ; i < argc ; i++) 
					{
						if ( i == 1 )
						{
							printf( "%d/%d: %s (skip) (command)\n", i, argc-1 ,  argv[ i ] );
						}
						else if ( i == 2 )
						{
							printf( "%d/%d: %s (skip source)\n", i, argc-1 ,  argv[ i ] );
						}
						else if ( i >= 3 )
						{
							printf( "=====================================================.\n" );
							printf( "=====================================================.\n" );
							printf( "> File process.\n" );
							printf( "- File process (status: begin).\n" );
							printf( "  %d/%d: File (%s)\n", i, argc-1 ,  argv[ i ] );
							printf( "> Source: %s\n", argv[ 2 ] );
							printf( "> Target: %s\n", argv[ i ] );
							strncpy( cmdi, " ", PATH_MAX );
							strncpy( cmdi, " ", PATH_MAX );
							strncat( cmdi , "  xzcat  " , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , "  \""  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , argv[ 2 ] , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , "\" "  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , " > \"/dev/" , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , fbasename( argv[ i ] ) , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , "\"  " , PATH_MAX - strlen( cmdi ) -1 );
							nsystem( cmdi );
							printf( "- File image.img.xz (%s) => (/dev/%s) process (status: completed).\n", argv[ 2 ], argv[ i ] );
						}
					}
				}
			}
			return 0;
	}










	if ( argc == 3 )
	if ( strcmp( argv[1] , "compile" ) ==  0 ) 
	if ( strcmp( argv[2] , "vim" ) ==  0 ) 
	{
			printf( " cd ; tar xzf vim_8.0.0197.orig.tar.gz ; cd vim-8.0.0197 ; ./configure ; make " );  
			printf( "\n" ); 
			fetch_file_ftp( "http://deb.debian.org/debian/pool/main/v/vim/vim_8.0.0197.orig.tar.gz" ); 
			fetch_file_ftp( "https://gitlab.com/openbsd98324/vim-src/-/raw/main/src/vim_8.0.0197.orig.tar.gz" ); 
	   //fetch_file_ftp( "http://deb.debian.org/debian/pool/main/v/vim/vim_8.0.0197.orig.tar.gz" ); 
			if( fexist(  "vim_8.0.0197.orig.tar.gz" ) == 1 ) 
			  nsystem( " cd ; tar xzf vim_8.0.0197.orig.tar.gz ; cd vim-8.0.0197 ; ./configure ; make " );  
			return 0; 
	}













	if ( argc == 3 )
	if ( strcmp( argv[1] , "compile" ) ==  0 ) 
	if ( strcmp( argv[2] , "dillo" ) ==  0 ) 
	{
		if ( MYOS == 1 )
		{
			npkg_update(); 
			npkg( "  zlib1g-dev " ); 
		}
		printf( "PATH: %s\n", getcwd( charo , PATH_MAX ) );
		fetch_file_ftp( "http://deb.debian.org/debian/pool/main/d/dillo/dillo_3.0.5.orig.tar.bz2"  );
		  nsystem( "  tar xvf dillo_3.0.5.orig.tar.bz2 ; cd dillo-3.0.5 ;  ./configure ; make ;  cp src/dillo   cmdillo   " ); // custom dillo  
		return 0; 
	}











	if ( argc == 2 )
	if ( strcmp( argv[1] , "lsloop" ) ==  0 )
	{
		printf( "Little hack to keep active some connections... \n");  
		while( 1 ) 
		{
					printf( "===\n" );
					printf( "[%d] ", (int)time(NULL));
					printf( " [Current path: %s]\n", getcwd( cmdi , PATH_MAX ) );
			                nls( "." ); 
					printf( "[%d]\n", (int)time(NULL));
					if ( os_bsd_type == 3 ) 
					{
					   printf( "==- SLEEP (system) -==\n"); 
					   nsystem( " sleep 5 " );  
					}
					else
					{
					  printf( "==- USLEEP -==\n"); 
					  usleep(   5 *   10 * 20 * 10000 );     // more than 2sec
					}
		}
		return 0;
	}








	if ( argc == 2 )
	if ( strcmp( argv[1] , "tmuxrc" ) ==  0 )  
	{
			printf("Util: Create .tmux.conf!\n");
			chdir( getenv( "HOME" ) );
			fpout = fopen( ".tmux.conf", "wb" );
			fputs( "\n", fpout );
			fputs( "\n", fpout );
			fputs( "bind-key -n F5 previous-window", fpout );
			fputs( "\n", fpout );
			fputs( "\n", fpout );
			fputs( "bind-key -n F6 next-window", fpout );
			fputs( "\n", fpout );
			fputs( "\n", fpout );
			fclose( fpout );
			return 0;
	}







	if ( argc == 2)
	if ( strcmp( argv[1] , "pmap" ) ==  0 ) 
	{
	                printf( "> Function: a little ping to map the net             \n" ); 
	                printf( "           tinyping.c (available, if needed).       \n" );
			for( i = 1 ; i <= 160 ; i++) 
			{
				strncpy( cmdi , " " , PATH_MAX );
				strncat( cmdi , " ping  -c 1 192.168.1." , PATH_MAX - strlen( cmdi ) -1 );
				snprintf( charo , sizeof(charo ), "%d",  i );
				strncat( cmdi , charo , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , " | grep 192 | grep ttl " , PATH_MAX - strlen( cmdi ) -1 );
				system( cmdi );
			}
			return 0;
	}














	if ( argc == 3 )
	if ( strcmp( argv[1] , "--list" ) ==  0  ) 
	if ( strcmp( argv[2] , "pkg" ) ==  0  ) 
	{
		printf( " > List installed packages on this machine \n" ); 
		if ( MYOS == 1 ) 
		{
			if ( fexist( "/usr/bin/zypper" ) == 1 ) 
				nsystem( " zypper se -s " ); 
			else 
				nsystem( " dpkg -l " ); 
		}
		return 0; 
	}





	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "list" ) ==  0  ) || ( strcmp( argv[1] , "help" ) ==  0  ) )
	if ( strcmp( argv[2] , "pkgsrc" ) ==  0  ) 
	{
	        printf( " = HELP PKGSRC = \n" ); 
	        printf( "> Packages: \n");  
	        printf( " mail/mutt         \n");  
	        printf( " editors/beaver        \n");  
	        printf( " editors/abiword       \n");  
	        printf( " editors/gobby         \n");  
	        printf( " editors/ted           \n");  
	        printf( " devel/subversion-base   \n");  
	        printf( " graphics/scrot    \n");  
	        printf( " emulators/dosbox  \n");  
	        printf( " emulators/hatari  \n");  
	        printf( " www/netsurf  \n");  
	        printf( " lang/wsbasic  \n");  
	        printf( " lang/yabasic  \n");  
	        printf( " games/fortune  \n");  
	        printf( " wm/evilwm  \n");  
	        printf( " print/xpdf   \n");  
	        printf( " audio/xmms   \n");  
	        printf( " (...)   \n");  
	        printf( "         \n");  
	        printf( "> Example: \n");  
	        printf( "  mconfig autopkg  www/netsurf     (creates: netsurf-gtk3)  \n");  
		return 0; 
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] , "get" ) ==  0  ) 
	if ( strcmp( argv[2] , "pkgsrc" ) ==  0 ) 
	{
		fetch_file_ftp( "https://cdn.NetBSD.org/pub/pkgsrc/stable/pkgsrc.tar.xz" ); 
		if ( fexist( "pkgsrc" ) == 0 ) 
			nsystem( " tar xf pkgsrc.tar.xz " ); 
		return 0; 
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "ascii" ) ==  0 ) || ( strcmp( argv[1] , "ascii-table" ) ==  0 ) )
	{
		for( fookey = 1 ; fookey < 255 ; fookey++) 
		{
		   printf( " Char (d) %d, Char (c) %c \n", fookey , fookey ); 
		}
		return 0; 
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] , "menu" ) ==  0  ) 
	if ( strcmp( argv[2] , "pkgsrc" ) ==  0 ) 
	{
		printf(      " =============== \n" ); 
		printf(      " |     MENU    |\n" ); 
		printf(      " =============== \n" ); 
		printf(      " 1: 9.1 amd64.,  2: 9.1 earmv7hf \n" ); 
		printf(      " =============== \n" ); 
		fookey = ckeypress();
		if ( fookey == '1' ) 
                   procedure_console_webbrowser( "http://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/amd64/9.1/All" ); 
		else if ( fookey == '2' ) 
		    procedure_console_webbrowser( "http://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/earmv7hf/9.1/All" ); 
		return 0; 
        }





	if ( argc == 2 )
	if ( strcmp( argv[1] , "pkgsrc" ) ==  0  ) 
	{
		printf(  " == PKGSRC == \n" ); 
		nsystem( " echo 'PKG_DBDIR=/var/db/pkg' >> /etc/mk.conf  " ); 
		nsystem( " echo easy to compile but unsecured ; mkdir /usr/src ; chmod 777 /usr/src  " ); 
		fetch_file_ftp( "https://cdn.NetBSD.org/pub/pkgsrc/stable/pkgsrc.tar.xz" ); 
		///nsystem( " tar xf pkgsrc.tar.xz " ); 
	        ///printf( " Alternative: cd pkgsrc/bootstrap  ; ./bootstrap --prefix /opt/pkg-2021Q3 --prefer-pkgsrc yes --make-jobs 4" );
		return 0; 
	}









	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "prepare" ) ==  0  ) || ( strcmp( argv[1] , "fix" ) ==  0  ) )
	if ( strcmp( argv[2] , "pkgsrc" ) ==  0  ) 
	{
		printf(  " == PKGSRC == \n" ); 
		nsystem( " echo 'PKG_DBDIR=/var/db/pkg' >> /etc/mk.conf  " ); 
		nsystem( " mkdir /usr/src " ); 
		nsystem( " chmod 777  /usr/src " ); 
		return 0; 
	}












	if ( argc == 2 )
	if ( strcmp( argv[1] , "listdir" ) ==  0 ) 
	{
	   listdir_origin( ".", 0 ); 
	   return 0; 
	}











	if ( argc >= 3 )
	if ( ( strcmp( argv[1] , "-pkgsrc-build" ) ==  0 )
	|| ( strcmp( argv[1] , "autobuild" ) ==  0 )  // shorter to type 
	|| ( strcmp( argv[1] , "autopkg" ) ==  0 )    // shorter to type 
	|| ( strcmp( argv[1] , "autopkgsrc" ) ==  0 )    // shorter to type 
	|| ( strcmp( argv[1] , "pkgbuild" ) ==  0 )   // shorter to type 
	|| ( strcmp( argv[1] , "--pkgsrc-build" ) ==  0 ) )
	{
		printf( " [%d] \n", (int)time(NULL));
		strncpy( pathbefore, getcwd( charo, PATH_MAX ) , PATH_MAX ); 

		//printf( " == Hello PKGSRC == \n" ); 
		printf( " =[ BSD and Packages From Source ]=\n" ); 
		if ( MYOS == 1 ) 
		{
		        printf( " == LINUX ==  " ); 
			npkg(" gcc " ); 
			npkg(" clang " ); 
			npkg(" g++ " ); 
			npkg(" libstdc++  " ); 
			npkg(" ncurses-dev " ); 
			npkg(" libncurses-devel " ); 
			npkg(" zlib  " ); 
			npkg(" zlib-devel  " ); 
			npkg(" openssl-devel  " ); 
			npkg(" libudev-dev " ); 
			npkg(" make     " ); 
			npkg(" bmake    " ); 
		        nsystem( " mkdir /usr/src " ); 
		}

	        if (  ( strcmp( argv[1] , "autopkg" ) ==  0 ) ||  ( strcmp( argv[1] , "autobuild"  ) ==  0 ) )
		{
	           nsystem( " mkdir      /usr/src " ); 
	           nsystem( " chmod 777  /usr/src " ); 
   	           chdir(   "/usr/src" ); 
		   nsystem( " chmod 777     /usr/src ;  chmod 777 /usr/src/pkgsrc  " ); 
		   nsystem( " chmod 777 -R  /usr/src/pkgsrc  " ); 
	           chdir( "/usr/src" ); 
		   strncpy( pathbefore, getcwd( charo, PATH_MAX ) , PATH_MAX ); 
	        }

		if ( fexist( "pkgsrc.tar.xz" ) == 0 ) 
		{
			fetch_file_ftp( "https://cdn.NetBSD.org/pub/pkgsrc/stable/pkgsrc.tar.xz" ); 
			if ( fexist( "pkgsrc" ) == 0 ) 
				nsystem( " tar xf pkgsrc.tar.xz " ); 
		}

		chdir( pathbefore ); 
		if ( fexist( "pkgsrc" ) == 0 ) 
		{
			fetch_file_ftp( "https://cdn.NetBSD.org/pub/pkgsrc/stable/pkgsrc.tar.xz" ); 
			nsystem( " tar xf pkgsrc.tar.xz " ); 
		}

		if ( fexist( "pkgsrc" ) == 2 ) 
		{
			for( i = 2 ; i < argc ; i++) 
			{
				printf( "[%d] \n", (int)time(NULL));
				printf( "=> %d/%d %s \n", i , argc , argv[ i ] );

				chdir( pathbefore ); 
				chdir( "pkgsrc" ); 

				printf( "Current path: %s\n", getcwd( cmdi , PATH_MAX ) );
				snprintf( charo , sizeof( charo ), "%s",  argv[ i ] );
				if ( fexist( charo ) == 0 ) 
				{
					printf( "[Directory / Package *NOT* Found] \n" ); 
					printf( "[ => Package path/name: ( %s )  ] \n", charo ); 
				}
				else if ( fexist( charo ) == 2 ) 
				{
					printf( "[Directory / Package Found]\n" ); 
					printf( "[ => Package path/name: ( %s )  ] \n", charo ); 
					if ( MYOS == 1 ) 
					{
						printf( " == LINUX ==  " ); 
						snprintf( charo , sizeof( charo ), "  cd ; cd  /usr/src/pkgsrc/%s ; bmake ",  argv[ i ] );
					}
					else
						snprintf( charo , sizeof( charo ), "  cd ; cd  /usr/src/pkgsrc/%s ; make ",  argv[ i ] );
					nsystem(  charo );
				}
			}
		}
		printf( " ============== \n "); 
		return 0;
        }

















	if ( argc == 2 )
        if ( strcmp( argv[1] , "useradd" ) ==  0 )
	{
			if ( fexist( "/etc/wscons.conf" ) == 1 )   // netbsd
			{
                                printf( " == NetBSD == \n"); 
				nsystem( "  useradd -m -G wheel -u 3010 netbsd " );
                                printf( " == NetBSD == \n"); 
				nsystem( "  passwd  netbsd " ); 
                                //printf( " == ROOT == \n"); 
				//nsystem( "  passwd  root " ); 
			}

			else if ( MYOS == 1 ) 
			{
				nsystem( "  adduser --uid 3010 netbsd "); 
				nsystem( "  adduser netbsd audio " );
				nsystem( "  adduser netbsd video  " );
				nsystem( "  adduser netbsd scanner  " );
				nsystem( "  adduser netbsd printer  " );
				nsystem( "  mkdir /home/netbsd " ); 
				nsystem( "  chown netbsd /home/netbsd " ); 
				nsystem( "  passwd root " ); 
			}
			return 0;
	}












        // on freebsd, and netbsd  
	if ( argc == 2)
	if ( strcmp( argv[1] , "adhoc" ) ==  0 ) 
	{
			printf(  "\n" );
			// freebsd 
			printf( "ifconfig_ue0=\"inet 192.168.123.100 255.255.255.0\"\n" );
			printf( "\n" );
			printf( "\n" );
			// pi: usmsc0 on rpi3b with netbsd 
			printf( "# classic rpi3b \n" ); 
			printf( "# ifconfig_usmsc0=\"inet 192.168.123.100 255.255.255.0\"\n"    );  // the local file server                  on 100. (mue0)
			printf(  "\n" );
			// pi: mue0  on rpi3b+ with netbsd 
			printf( "# rpi3b+ \n" ); 
			printf( "# ifconfig_mue0=\"inet 192.168.123.100 255.255.255.0\"\n"    );  // the local file server                  on 100. (mue0)
			printf(  "\n" );
			printf( "# Client here using ethernet usb adapter \n" );
			printf( "# ifconfig_axen0=\"inet 192.168.123.101 255.255.255.0\"\n"   );  // the pi with axen0 usb ethernet adapter on 101. (axen0)
			printf(  "\n" );
			return 0;
        }

















        if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "interfaces" ) ==  0 ) || ( strcmp( argv[1] ,   "interface" ) ==  0 ) )  
	{
			printf( "\n" );
			printf( "\n" );
			printf( "iface eth0 inet dhcp\n" );
			printf( "iface eth1 inet dhcp\n" ); // <- amilo pc 
			printf( "\n" );
			printf( "\n" );
			printf( "iface wlan0 inet dhcp\n" );
			printf( "wpa-conf /etc/wpa_supplicant.conf\n" );
			printf( "\n" );
			printf( "iface wlan1 inet dhcp\n" );
			printf( "wpa-conf /etc/wpa_supplicant.conf\n" );
			printf( "\n" );
			printf( "iface wlan2 inet dhcp\n" );
			printf( "wpa-conf /etc/wpa_supplicant.conf\n" );
			printf( "\n" );
			printf( "iface wlan3 inet dhcp\n" );
			printf( "wpa-conf /etc/wpa_supplicant.conf\n" );
			printf( "\n" );
			printf( "auto  eth0 wlan0 eth1 wlan1 wlan2 wlan3 \n" );
			printf( "\n" );
			return 0;
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] , "create" ) ==  0 ) 
	if ( strcmp( argv[2] , "interfaces" ) ==  0 ) 
	{
		nsystem( "  mconfig interfaces > /etc/network/interfaces " ); 
		return 0;
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] , "--loc" ) ==  0 ) 
	if ( strcmp( argv[2] , "interfaces" ) ==  0 ) 
	{
		nsystem( "  mconfig interfaces > etc/network/interfaces " ); 
		return 0; 
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] , "enx" ) ==  0 )
	if ( MYOS == 1 ) 
	{
		nsystem( " ip addr > /tmp/ipaddr  " ); 
		dhclient_enx_wlx( "/tmp/ipaddr" , ": enx" ); 
		return 0; 
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] , "wlx" ) ==  0 )
	if ( MYOS == 1 ) 
	{
		    nsystem( " ip addr > /tmp/ipaddr  " ); 
                    dhclient_enx_wlx( "/tmp/ipaddr" , ": wlx" ); 
		    return 0; 
	}














	if ( argc == 2)
	if ( strcmp( argv[1] , "re0" ) ==  0 ) 
	{
		printf(  " => dhcpcd\n" );
		nsystem( " dhcpcd   re0 " );
		nsystem( " dhclient re0 " );
		return 0;
	}








	///////////////////////////////////////////////////////
	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "screen2" ) ==  0 ) || ( strcmp( argv[1] , "scr2" ) ==  0 ) )
	{
                nsystem( " cd ; screen -c  ~/.screenrc2 " ); 
		return 0;
	}




	if ( argc == 2 )
	if ( ( strcmp( argv[1] ,   "cchr" ) ==  0 ) || ( strcmp( argv[1] ,   "chr" ) ==  0 ) )
	{
	     nsystem( " screen -S chr " );
	     return 0;
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "vchr" ) ==  0 ) 
	{
                nsystem( " screen -x -S chr  "); 
		return 0;
	}








	if ( argc == 2 )
	if ( strcmp( argv[1] ,   "import" ) ==  0 ) 
	{
		nsystem( "  import -window root /tmp/screenshot.png  " ); 
                //  I use: import -window root /tmp/screenshot.png
		return 0;
	}







	if ( argc == 3)
	if ( ( strcmp( argv[1] ,   "install" ) ==  0 ) || ( strcmp( argv[1] ,   "ins" ) ==  0 ) )
	if ( ( strcmp( argv[2] ,   "subversion" ) ==  0 ) || ( strcmp( argv[2] ,   "svn" ) ==  0 ) )
	{
				if ( MYOS == 1 ) 
					npkg( "  subversion " );
				else
					npkg( "  subversion-base " );
				return 0; 
	}















	if ( argc >= 3 )
	if ( strcmp( argv[1] ,   "netbsd-src" ) ==  0 )
	{
		///snprintf( charo , sizeof( charo ), "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-%s", set_system_release_nbr ); 
		snprintf( charo , sizeof( charo ), "http://cdn.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/%s.tgz", argv[2] );
		fetch_file_ftp( charo ); 
		/*
		fetch_file_ftp( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/src.tgz" ); 
		fetch_file_ftp( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/syssrc.tgz" ); 
		fetch_file_ftp( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/gnusrc.tgz" ); 
		fetch_file_ftp( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/sharesrc.tgz" ); 
		fetch_file_ftp( "ftp://ftp.netbsd.org/pub/NetBSD/NetBSD-9.1/source/sets/xsrc.tgz" ); 
		*/
		return 0; 
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "Xorg" ) ==  0 ) 
	{
	     printf( " /usr/libexec/Xorg \n" );
	     return 0;
	}




	if ( argc == 3 )
	if ( strcmp( argv[1] , "launch" ) ==  0 ) 
	if ( strcmp( argv[2] , "Xorg" ) ==  0 ) 
	{
	     printf( "  /usr/libexec/Xorg \n" );
	     printf( "  == FEDORA == \n" );
	     nsystem( "  screen -d -m  /usr/libexec/Xorg  ;  sleep 5 ; export DISPLAY=:0 ; /usr/bin/blackbox " );
	     return 0;
	}




        // part here  
        // restore windows 8 !! (non needed!!) 
	if ( argc == 3 )
	if ( ( strcmp( argv[1] ,   "--partrestore-ntfs" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "--restore" ) ==  0 ) 
	|| ( strcmp( argv[1] ,   "--partrestore.ntfs" ) ==  0 ) )
	if ( ( strcmp( argv[2] ,   "sda3" ) ==  0 ) || ( strcmp( argv[2] ,   "sda4" ) ==  0 ) )
	{
		npkg( " partclone  " );
		npkg( " ntfs-3g    " ); 
		npkg( " ntfsprogs  " ); 
		printf( " =============================================== \n" ); 
		printf( " \n" ); 
		printf( " TO RESTORE Windows:  partclone.ntfs -r -d -s partclone.sdb2.ntfs.img -o /dev/sda2  " ); 
		printf( " \n" ); 
		printf( " TO RESTORE Windows,  Force:  partclone.ntfs -r -d -C -F  -s partclone.sdb2.ntfs.img -o /dev/sda2  " ); 
		printf( " \n" ); 
		printf( " =============================================== \n" ); 
	        if ( strcmp( argv[2] ,   "sda3" ) ==  0 ) 
		{
		    nsystem( " umount /dev/sda3 " ); 
		    printf(  " TO RESTORE Windows:  partclone.ntfs -r -d -s partclone.sdb2.ntfs.img -o /dev/sda2  " ); 
		    nsystem( " partclone.ntfs -r -d -s partclone.sda3.ntfs.img -o /dev/sda3   " ); 
		}
		else
		if ( strcmp( argv[2] ,   "sda4" ) ==  0 ) 
		{
		    nsystem( " umount /dev/sda4 " ); 
		    printf(  " TO RESTORE Windows:  partclone.ntfs -r -d -s partclone.sdb2.ntfs.img -o /dev/sda2  " ); 
		    nsystem( " partclone.ntfs -r -d -s partclone.sda4.ntfs.img -o /dev/sda4   " ); 
		}
		return 0; 
	}





	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "puppy" ) ==  0 ) 
	|| ( strcmp( argv[1] , "puppylinux" ) ==  0 ) )
	{
		fetch_file_ftp( "https://rockedge.org/kernels/data/ISO/Bookworm_Pup64/BookwormPup64_10.0.2.iso" ); 
		fetch_file_ftp( "https://rockedge.org/kernels/data/ISO/Bionic32/Bionic32-overlayfs/Bionic32-OL.iso" );  
		printf( "\n" ); 
		return 0;
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] , "help" ) ==  0 ) 
	if ( strcmp( argv[2] , "wheel" ) ==  0 ) 
	{
			printf( " == FreeBSD == \n"); 
			printf( " \n"); 
			printf( "FreeBSD: you may enter this : pw user mod username -G wheel \n" );
			printf( " \n"); 
			printf( " == NetBSD == \n"); 
			printf( "  useradd -m -G wheel -u 2000 username " );
			printf( " \n"); 
			printf( " passwd  username " ); 
			printf( " \n"); 
			return 0;
	}








	if ( argc == 2 )
	if ( strcmp( argv[1] , "autocompile" ) ==  0 ) 
	{
		printf( " > Start Automatic Compile.\n" );
                printf( "   autoconf autogen autogen-doc automake autotools-dev guile-2.0-libs libopts25 libopts25-dev \n" ); 
	        nsystem( "  xmkmf ;  aclocal ; autoconf ; automake " ); 
		nsystem( " ./configure ; make " ); 
		printf( " > completed.\n" );
		return 0;
	}







	if ( argc == 2)
	if ( strcmp( argv[1] , "wheel" ) ==  0 ) 
	{
			printf( " == FreeBSD == \n"); 
			printf( " \n"); 
			printf( "FreeBSD: you may enter this : pw user mod username -G wheel \n" );
			printf( " \n"); 
			printf( " == NetBSD == \n"); 
			printf( "  useradd -m -G wheel -u 2000 username " );
			printf( " \n"); 
			printf( " passwd  username " ); 
			printf( " \n"); 
			return 0;
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] , "wget" )    ==  0 ) 
	if ( strcmp( argv[2] , "links" ) ==  0 ) 
	{
		fetch_file_ftp(  "http://links.twibright.com/download/links-2.20.2.tar.gz" );
		return 0; 
	}




	if ( argc == 2)
	if ( strcmp( argv[1] , "netlist" ) ==  0 ) 
	{
                nsystem( " cat t | awk '{print $5}' | cut -d ':' -f1  |  cut -d '.' -f1-4 | grep -v '127.0.0.1'  | grep -v '0x0' | grep -v '*.*'  | grep '\\.'  " ); 
		return 0;
	}



	if ( argc >= 4 )
	if ( ( strcmp( argv[1] ,  "--sedcut" ) ==  0 ) || ( strcmp( argv[1] ,  "--right" ) ==  0 ) )
	{
	       // count and string 
	       // hello to give llo with 3 hello 
	       printf(  "%s\n" , strsedcutright( atoi( argv[ 2 ] ) -1  , argv[ 3 ] ) ); 
	       return 0; 
        }






	if ( argc == 2 )
	if ( strcmp( argv[1] ,     "trip" ) ==  0 ) 
	{
		//fetch_file_ftp( "https://github.com/manjaro-arm/pbpro-images/releases/download/23.02/Manjaro-ARM-kde-plasma-pbpro-23.02.img.xz" );
		fetch_file_ftp( "https://github.com/manjaro-arm/pbpro-images/releases/download/22.12/Manjaro-ARM-kde-plasma-pbpro-22.12.img.xz" );
		fetch_file_ftp( "https://github.com/RetroPie/RetroPie-Setup/releases/download/4.5.1/retropie-4.5.1-rpi2_rpi3.img.gz" ); 

		nsystem(  "  nconfig raspios-full " );    // get a PI desktop full with wpa key  with a mouse 
		nsystem(  "  nconfig 22.06       " ); 
		nsystem(  "  nconfig devuan-iso  " );     // get a keyboard with the devuan live to ram    of 1.2 gb 
		nsystem(  "  nconfig zaxxon  " );         // get a wpa configuration  of 3 gb 
		nsystem(  "  nconfig rpi0  " ); 
		///nsystem(  "  nconfig flde    " ); 
		/// linux maths 
		nsystem(  "  nconfig devuan-iso  " ); 
		nsystem(  "  nconfig vm-ubuntu   " ); 
		nsystem(  "  nconfig armbian     " ); 
		nsystem(  "  nconfig pidesktop   " ); 
		return 0; 
	}



        // fltk flmplayer 
	if ( argc == 3 )
	if ( strcmp( argv[1] ,  "--audio-player" ) ==  0 ) 
	{
	       printf(  " > Playing ... %s\n" , argv[ 2 ] ); 
	       procedure_audio_player( argv[ 2 ] ); 
	       return 0; 
        }













	if ( argc == 3 )
	if ( strcmp( argv[1] ,  "--play-video-fb" )    ==  0 ) 
	{
		//printf( "> start time: %d\n", (int)time(null));
		// vlc first for rpi4 or rpi0 
		procedure_video_player_fb( argv[ 2 ] );   
		return 0;
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] ,  "--audio-player-standalone" ) ==  0 )  // rpi0 w on tv 
	{
		printf(  " > Playing ... %s\n" , argv[ 2 ] ); 
		if ( fexist( "/usr/bin/vlc" ) == 1 ) 
		{
		   snprintf( foostr , sizeof( foostr ), " vlc --no-video  --play-and-exit  \"%s\"  " , argv[ 2 ] ); 
		   nsystem( foostr );  
		}
		else
		{
		   procedure_audio_player( argv[ 2 ] ); 
		}
	       return 0; 
        }


	if ( argc == 3 )
	if ( strcmp( argv[1] ,  "--video-player-standalone" ) ==  0 )   // rpi0 w on tv
	{
		printf(  " > Playing ... %s\n" , argv[ 2 ] ); 
		if ( fexist( "/usr/bin/vlc" ) == 1 ) 
		{
		   snprintf( foostr , sizeof( foostr ), " vlc  --play-and-exit  \"%s\"  " , argv[ 2 ] ); 
		   nsystem( foostr );  
		}
		else
		{
		   procedure_video_player( argv[ 2 ] ); 
		}
	       return 0; 
        }








	if ( argc == 3 )
	if ( strcmp( argv[1] , "--music-player" ) ==  0 ) 
	{  
		music_player( argv[ 2 ]  );
		return 0; 
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] , "guitar" ) ==  0 ) 
	{
	        fetch_file_ftp( "https://gitlab.com/openbsd98324/sample-sound/-/raw/master/guitar4.wav" ); 
		music_player(   "guitar4.wav" ); 
		return 0; 
	}



	if ( argc == 3 )
	if ( strcmp( argv[1] ,  "--audio-player" ) ==  0 ) 
	{
	       printf(  " > Playing ... %s\n" , argv[ 2 ] ); 
               procedure_audio_player( argv[ 2 ] ); 
	       return 0; 
        }



	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "--musicplayer" ) ==  0 ) || ( strcmp( argv[1] , "-musicplayer" ) ==  0 ) )
	{
		if ( MYOS ==  1 ) 
		{
		  /// 
		}
		else
		{
			nsystem( "  mixerctl -w outputs.speaker=120 " ); 
			system(  "  mixerctl -w outputs.master=125  " );
			system(  "  mixerctl -w outputs.master=125  " );
			system(  "  mixerctl -w outputs.master=100  " );
		}
		procedure_audio_player( argv[ 2 ] ); 
		return 0; 
	}



	if ( argc == 2 )
	if ( strcmp( argv[1] , "wlan0.service" ) ==  0 ) 
	{
		printf( "\n" ); 
		printf( "# sudo nano /lib/systemd/system/wpa_supplicant@wlan0.service\n" ); 
		printf( "\n" ); 
		printf( "[Unit]\n" ); 
		printf( "Description=WPA-Supplicant-Daemon (wlan0)\n" ); 
		printf( "Requires=sys-subsystem-net-devices-wlan0.device\n" ); 
		printf( "BindsTo=sys-subsystem-net-devices-wlan0.device\n" ); 
		printf( "After=sys-subsystem-net-devices-wlan0.device\n" ); 
		printf( "Before=network.target\n" ); 
		printf( "Wants=network.target\n" ); 
		printf( "\n" ); 
		printf( "[Service]\n" ); 
		printf( "Type=simple\n" ); 
		printf( "RemainAfterExit=yes\n" ); 
		printf( "ExecStart=/sbin/wpa_supplicant -qq -c/etc/wpa_supplicant.conf -Dnl80211 -iwlan0\n" ); 
		printf( "Restart=on-failure\n" ); 
		printf( "\n" ); 
		printf( "[Install]\n" ); 
		printf( "Alias=multi-user.target.wants/wpa_supplicant@wlan0.service\n" ); 
		printf( "\n" ); 
		printf( "# Speichern und schliessen mit Strg + O, Return, Strg + X.\n" ); 
		printf( "# Dann aktivieren und starten wir die neue systemd-Unit.\n" ); 
		printf( "\n" ); 
		printf( "# sudo systemctl enable wpa_supplicant@wlan0.service\n" ); 
		printf( "# sudo systemctl start wpa_supplicant@wlan0.service\n" ); 
		printf( "\n" ); 
		printf( "\n" ); 
		return 0; 
	}






	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "--webbrowser" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-web-browser" ) ==  0 ) 
	|| ( strcmp( argv[1] , "--web-browser" ) ==  0 ) 
	|| ( strcmp( argv[1] , "-webbrowser" ) ==  0 ) )
	{
		procedure_webbrowser( argv[ 2 ] ); 
		return 0; 
	}






 /*
    int i = 7;
    printf("%#010x\n", i);  // gives 0x00000007
    printf("0x%08x\n", i);  // gives 0x00000007
    printf("%#08x\n", i);   // gives 0x000007
    Also changing the case of x, affects the casing of the outputted characters.
    printf("%04x", 4779); // gives 12ab
    printf("%04X", 4779); // gives 12AB
 */
	if ( argc == 3 )
	if ( strcmp( argv[1] , "--dec2hex" ) ==  0 ) 
	{
	        choix = atoi( argv[ 2 ] ); 
		printf( "%#010x \n", choix);  // gives 0x00000007
		printf( "0x%08x \n", choix);  // gives 0x00000007
		printf( "%#08x \n", choix);   // gives 0x000007
		//Also changing the case of x, affects the casing of the outputted characters.
		printf("%04x \n", choix ); // gives 12ab
		printf("%04X \n", choix ); // gives 12AB
                return 0; 
        }





	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "-asc" ) ==  0 ) || ( strcmp( argv[1] , "--ascii" ) ==  0 ) )
	{
		    printf( "%c" , atoi( argv[ 2 ] ) ); 
		    return 0; 
	}








	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "--ascii-table" ) ==  0 ) || ( strcmp( argv[1] , "--ascii-table-wide" ) ==  0 ) )
	{
		    for( i = 1 ; i<=255 ; i++)
		    {
	                  if ( strcmp( argv[1] , "--ascii-table-wide" ) ==  0 ) 
			    printf("%d %c, ", i, i);
			  else
			    printf("%d %c \n",i,i);
			  //i++;
		    }
		    return 0; 
	}







	if ( argc == 2 )
	if ( strcmp( argv[1] , "--hex-table" ) ==  0 ) 
	{
		    for( i = 1 ; i<=255 ; i++)
		    {
			  printf("%d %#010x \n",i,i);
			  //i++;
		    }
		    return 0; 
	}






	if ( argc == 3 )
	if ( strcmp( argv[1] , "--mpv-zcat"   ) ==  0 ) 
	{
		i = 2; 
		snprintf(  charo , sizeof( charo ),  "   wget --no-check-certificate -q -O-   \"%s\"  | gunzip - |  mpv -  " ,  argv[ i ] ); 
		nsystem( charo ); 
		return 0; 
	}


	if ( argc == 3 )
	if ( strcmp( argv[1] , "--vlc-zcat"   ) ==  0 ) 
	{
		i = 2; 
		if ( fexist( "/usr/bin/cvlc" ) == 1 ) 
		  snprintf( charo , sizeof( charo ), " wget --no-check-certificate -q -O-   \"%s\"  | gunzip - |  cvlc -q --play-and-exit -  " ,  argv[ i ] ); 
		else if ( fexist( "/usr/bin/omxplayer" ) == 1 ) 
		  snprintf( charo , sizeof( charo ), " wget --no-check-certificate -q -O-   \"%s\"  | gunzip - | omxplayer -  " ,  argv[ i ] ); 
		else if ( fexist( "/usr/bin/mpv" ) == 1 ) 
		  snprintf( charo , sizeof( charo ), " wget --no-check-certificate -q -O-   \"%s\"  | gunzip - | mpv -  " ,  argv[ i ] ); 
		else 
		  snprintf( charo , sizeof( charo ), " wget --no-check-certificate -q -O-   \"%s\"  | gunzip - | mpv -  " ,  argv[ i ] ); 
		nsystem( charo ); 
		return 0; 
	}




	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "--vlc-fb"   ) ==  0 ) 
	|| ( strcmp( argv[1] , "--vlc-fbdev"   ) ==  0 ) ) 
	{
             printf( " FBDEV Linux:   cvlc --vout fb --fbdev=/dev/fb0 v4l2:///dev/video0 'https://gitlab.com/openbsd98324/movie-bunny/-/raw/main/mirror/BigBuckBunny_320x180.mp4' \n " ); 
             //  --play-and-exit   
	     // ncmdwith( " /usr/bin/mpv  " , argv[ i ]  );
	     ncmdwith( "   cvlc -q --play-and-exit  --vout fb --fbdev=/dev/fb0 v4l2:///dev/video0  " , argv[ 2 ]  );
	     return 0; 
	}



	if ( argc >= 3 )
	if  ( ( strcmp( argv[1] ,   "--fbx" ) ==  0 ) 
	    || ( strcmp( argv[1] ,   "fbx" ) ==  0 ) 
	    || ( strcmp( argv[1] ,   "--mp-fb" ) ==  0 ) 
	    || ( strcmp( argv[1] ,   "--mp-fbdev" ) ==  0 ) 
	    || ( strcmp( argv[1] ,   "--mp" ) ==  0 ) 

            //  mpg123 -a hw:1,0 
	    || ( strcmp( argv[1] ,   "--mpg-hw1" ) ==  0 ) 

	    || ( strcmp( argv[1] ,   "--fbx-sound" ) ==  0 ) 
	    || ( strcmp( argv[1] ,   "--fbx-sound-320x240" ) ==  0 ) 
	    || ( strcmp( argv[1] ,   "--fbx-sound-640x480" ) ==  0 ) 
	    || ( strcmp( argv[1] ,   "--fb" ) ==  0 )    ) 
	{
	        if ( strcmp( argv[1] ,   "--fbx-sound-640x480" ) ==  0 ) 
		{
			printf( " -- FOR LFPLAYER -- \n" ); 
			snprintf( charo , sizeof( charo ), "       mplayer  -vo fbdev:/dev/fb0  -loop 0  -vf scale=630:470  \"%s\"  " ,  argv[ 2 ] ); 
		}
	        else if ( strcmp( argv[1] ,   "--fbx-sound-320x240" ) ==  0 ) 
		{
			printf( " -- FOR LFPLAYER -- \n" ); 
			snprintf( charo , sizeof( charo ), "       mplayer  -vo fbdev:/dev/fb0  -loop 0  -vf scale=310:230  \"%s\"  " ,  argv[ 2 ] ); 
		}
		else if ( strcmp( argv[1] ,   "--fbx-sound" ) ==  0 ) 
		{
			printf( " -- FOR LFPLAYER -- \n" ); 
			snprintf( charo , sizeof( charo ), "       mplayer  -vo fbdev:/dev/fb0  -loop 0  -vf scale=790:590  \"%s\"  " ,  argv[ 2 ] ); 
		}

	        ///  || ( strcmp( argv[1] ,   "--mpg-hw1" ) ==  0 ) 
                ///  mpg123 -a hw:1,0 
		else if  ( strcmp( argv[1] ,   "--mpg-hw1" ) ==  0 ) 
		{
			strncpy( charo , "  mpg123 -a hw:1,0  " , PATH_MAX ); 
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else
					{
						printf( "%d/%d: %s\n", i, argc-1 ,  argv[ i ] );   
						strncat( charo , " \"" , PATH_MAX - strlen( charo ) -1 );
						strncat( charo , argv[ i ]  , PATH_MAX - strlen( charo ) -1 );
						strncat( charo , "\" " , PATH_MAX - strlen( charo ) -1 );
					}
				}
			}
			////
		}

		else if  ( strcmp( argv[1] ,   "--mp" ) ==  0 ) 
		{
			strncpy( charo , "   mplayer  -fs -zoom  -loop 0  " , PATH_MAX ); 
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else
					{
						printf( "%d/%d: %s\n", i, argc-1 ,  argv[ i ] );   
						strncat( charo , " \"" , PATH_MAX - strlen( charo ) -1 );
						strncat( charo , argv[ i ]  , PATH_MAX - strlen( charo ) -1 );
						strncat( charo , "\" " , PATH_MAX - strlen( charo ) -1 );
					}
				}
			}
			////
		}


		else if  ( ( strcmp( argv[1] ,   "--fbx" ) ==  0 ) || ( strcmp( argv[1] ,   "fbx" ) ==  0 ) )
		{
			strncpy( charo , "   mplayer  -vo fbdev:/dev/fb0 -ao null  -loop 0     -vf scale=780:580  " , PATH_MAX ); 
			if ( argc >= 2)
			{
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else
					{
						printf( "%d/%d: %s\n", i, argc-1 ,  argv[ i ] );   
						strncat( charo , " \"" , PATH_MAX - strlen( charo ) -1 );
						strncat( charo , argv[ i ]  , PATH_MAX - strlen( charo ) -1 );
						strncat( charo , "\" " , PATH_MAX - strlen( charo ) -1 );
					}
				}
			}
			////
		}

		else if ( fexist( "/usr/bin/mplayer" ) == 0 ) 
		{
			snprintf( charo , sizeof( charo ), "   mconfig --vlc-fbdev  \"%s\"  " ,  argv[ 2 ] ); 
		}

		else
		{
			snprintf( charo , sizeof( charo ), "       mplayer  -vo fbdev:/dev/fb0 -ao null  -loop 0     -vf scale=640:480  \"%s\"  " ,  argv[ 2 ] ); 
		}
		nsystem( charo );  
		return 0;
	}











	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "--vlc"   ) ==  0 ) 
	|| ( strcmp( argv[1] , "--vlc-hw1"   ) ==  0 ) 
	|| ( strcmp( argv[1] , "-vlc-fs"   ) ==  0 ) 
	|| ( strcmp( argv[1] , "--vlc-fs"   ) ==  0 ) ) 
	{
		i = 2; 
                //    vlc -q --play-and-exit --alsa-audio-device=plughw:1,0 

		if ( fexist( "/usr/bin/cvlc" ) == 1 ) 
		   snprintf( charo , sizeof( charo ), "  vlc  --play-and-exit  --fullscreen  \"%s\" " ,  argv[ i ] ); 

		else if ( fexist( "/usr/bin/cvlc" ) == 1 ) 
		   snprintf( charo , sizeof( charo ), "  vlc  --play-and-exit \"%s\" " ,  argv[ i ] ); 

                else if ( strcmp( argv[1] ,  "--vlc-hw1" )  == 0 ) 
		   snprintf( charo , sizeof( charo ), "  vlc -q --play-and-exit --alsa-audio-device=plughw:1,0 \"%s\" " ,  argv[ i ] ); 

                else if ( strcmp( argv[1] ,  "--vlc-fs" )  == 0 ) 
		   snprintf( charo , sizeof( charo ), "  vlc -q --play-and-exit --fullscreen \"%s\" " ,  argv[ i ] ); 

                else if ( ( strcmp( argv[1] ,  "--vlc-fs" )  == 0 ) 
		&& ( fexist( "/usr/bin/vlc" ) == 0 ) 
		&& ( fexist( "/usr/bin/mpv" ) == 1 ) ) 
		   snprintf( charo , sizeof( charo ), "  mpv --fs \"%s\" " ,  argv[ i ] ); 

		else if ( fexist( "/usr/bin/cvlc" ) == 1 ) 
		   snprintf( charo , sizeof( charo ), "  cvlc -q --play-and-exit \"%s\" " ,  argv[ i ] ); 

		else if ( fexist( "/usr/bin/vlc" ) == 1 ) 
		   snprintf( charo , sizeof( charo ), " vlc -q --play-and-exit \"%s\" " ,  argv[ i ] ); 
		else 
		   snprintf( charo , sizeof( charo ), " mpv  \"%s\" " ,  argv[ i ] ); 
		nsystem( charo ); 
		return 0; 
	}







	if ( argc >= 3 )
	if ( strcmp( argv[1] , "--cvlc" ) ==  0 ) 
	{
		printf( "   mpv: %d \n",  fexist(  "/usr/bin/mpv"  ) );
		printf( "   vlc: %d \n",  fexist(  "/usr/bin/vlc"  ) );

			strncpy( cmdi,  " ", PATH_MAX );
			if ( argc >= 2 )
			{
			   if ( fexist( "/usr/bin/cvlc" ) == 1  ) 
	                        strncpy( cmdi,  " cvlc -q --play-and-exit ", PATH_MAX );
		    	   else 
	                        strncpy( cmdi,  " mpv  ", PATH_MAX );
   /*
	                if ( strcmp( argv[1] ,  "--vlc-hw1" )    ==  0 ) 
			   ncmdwith( "  cvlc -q --play-and-exit  --alsa-audio-device=plughw:1,0  " , argv[ i ]  );

	                else if ( strcmp( argv[1] ,  "--vlc-hw0" )    ==  0 ) 
			   ncmdwith( "  cvlc -q --play-and-exit  --alsa-audio-device=plughw:0,0  " , argv[ i ]  );
			   */
				for( i = 1 ; i < argc ; i++) 
				{
					if ( i == 1 )
					{
						printf( "%d/%d: %s (skip)\n", i, argc-1 ,  argv[ i ] );
					}
					else if ( i >= 2 )
					{

						strncat( cmdi , " \""  , PATH_MAX - strlen( cmdi ) -1 );
						strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );  /// <--- destination 
						strncat( cmdi , "\" "  , PATH_MAX - strlen( cmdi ) -1 );
					}
				}
		                nsystem( cmdi );

			}
			return 0;
	}











	if ( argc == 3 )
	if ( strcmp( argv[1] , "--mpg123"   ) ==  0 ) 
	{
		i = 2; 
	        snprintf( charo , sizeof( charo ), " mpg123  \"%s\" " ,  argv[ i ] ); 
		nsystem( charo ); 
		return 0; 
	}








	if ( argc >= 3 )
	if ( strcmp( argv[1] , "uapt" ) ==  0 )  // it is like in debian
	{
	                        npkg_update(); 

				if ( mode_gpg_allow_unauthenticated == 1 ) 
					strncpy( cmdi , " apt-get install --allow-unauthenticated  " , PATH_MAX );

			        else if ( fexist( "/usr/bin/pacman" ) == 1 )  //  manjaro pine aach64
					strncpy( cmdi , " pacman -S " , PATH_MAX );

			        else if ( fexist( "/usr/bin/zypper" ) == 1 )  // oh, man, opensuse... 
					strncpy( cmdi , " /usr/bin/zypper install " , PATH_MAX );

			        else if ( fexist( "/usr/sbin/zypper" ) == 1 )  // oh, man, opensuse... 
					strncpy( cmdi , " /usr/sbin/zypper install " , PATH_MAX );

				else
					strncpy( cmdi , " apt-get install " , PATH_MAX );

				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );

				for( i = 2 ; i < argc ; i++) 
				{
					printf( "=> %d/%d %s \n", i , argc , argv[ i ] );
					strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
					strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				}

				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				strncat( cmdi , " " , PATH_MAX - strlen( cmdi ) -1 );
				nsystem( cmdi );

				return 0; 
	}









	if ( argc >= 3 )
        if ( strcmp( argv[1] ,   "--zcat" ) ==  0 ) 
	{

			printf( " =>  Processing example --zcat /mydir/file.gz sda sdb sdc.\n" ); 
			//if ( fexist( argv[ 2 ] ) == 1 ) 
			{
				printf( " The image file with extension .img.gz %s is available, and lets go!! \n" , argv[ 2 ] ); 
				if ( argc >= 2)
				{
					for( i = 1 ; i < argc ; i++) 
					{
						if ( i == 1 )
						{
							printf( "%d/%d: %s (skip) (command)\n", i, argc-1 ,  argv[ i ] );
						}
						else if ( i == 2 )
						{
							printf( "%d/%d: %s (skip source)\n", i, argc-1 ,  argv[ i ] );
						}
						else if ( i >= 3 )
						{
							printf( "=====================================================.\n" );
							printf( "=====================================================.\n" );
							printf( "> File process.\n" );
							printf( "- File process (status: begin).\n" );
							strncpy( cmdi, " ", PATH_MAX );
							strncpy( cmdi, " ", PATH_MAX );
							strncat( cmdi , "  zcat  " , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , "  \""  , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , argv[ i ] , PATH_MAX - strlen( cmdi ) -1 );
							strncat( cmdi , "\" "  , PATH_MAX - strlen( cmdi ) -1 );
							nsystem( cmdi );
                                                        fflush(stdout); // Will now print everything in the stdout buffer 
						}
					}
				}
			}
			return 0;
	}





	if ( argc == 2 )
	if ( strcmp( argv[1] , "xrandr" ) ==  0 ) 
	{
		nsystem( " xrandr --output  HDMI-1  --mode 1680x1050   --output HDMI-2    --mode 1680x1050  --primary --right-of  HDMI-1  " ); 
		return 0;
	}




	if ( argc == 2 )
	if ( strcmp( argv[1] , "at" ) ==  0 )
	{
	        printf( "@" ); 
		return 0; 
	}

	if ( argc == 2 )
	if ( strcmp( argv[1] , "ampersand" ) ==  0 )
	{
		printf( "&" ); 
		return 0; 
	}


	if ( argc == 2 )
	if ( strcmp( argv[1] , "doublequote" ) ==  0 )
	{
		printf( "\"" ); 
		return 0; 
	}







	if ( argc == 3 )
	if ( strcmp( argv[1] , "keymap" ) ==  0 ) 
	if ( strcmp( argv[2] , "de" ) ==  0 ) 
	{
                nsystem( "loadkeys console-data-main/keymaps-console-data-linux1/i386/qwertz/de-latin1.kmap.gz " );  
		return 0;
	}





	if ( argc == 3 )
	if ( strcmp( argv[1] , "keymap" ) ==  0 ) 
	if ( strcmp( argv[2] , "us" ) ==  0 ) 
	{
                //nsystem( "loadkeys console-data-main/keymaps-console-data-linux1/i386/qwertz/de-latin1.kmap.gz " );  
                nsystem( " loadkeys console-data-main/keymaps-console-data-linux1/i386/qwerty/us-latin1.kmap.gz " ); 
		return 0;
	}








	if ( argc == 2 )
        if ( strcmp( argv[1] , "manjaro-arm-20.06" ) ==  0 ) 
	{
		fetch_file_ftp( "https://github.com/manjaro-arm/pbpro-images/releases/download/22.06/Manjaro-ARM-kde-plasma-pbpro-22.06.img.xz" ); 
		return 0;
	}





	if ( argc == 2)
	if ( strcmp( argv[1] , "hello" ) ==  0 ) 
	{
		printf("%d\n", (int)time(NULL));
		printf(" == Hello World == \n" ); 
		return 0;
	}








	if ( argc == 2 )
	if ( strcmp( argv[1] , "vol-" ) ==  0 ) 
	{
		nsystem( " amixer -c 0 sset Master 2-  " ); 
                nsystem( " amixer sset PCM 200-  " ); 
		return 0;
	}

	if ( argc == 2 )
	if ( strcmp( argv[1] , "vol+" ) ==  0 ) 
	{
		nsystem( " amixer -c 0 sset Master 2+  " ); 
                nsystem( " amixer sset PCM 200+ " ); 
		return 0;
	}







	if ( argc == 2 )
	if (  ( strcmp( argv[1] , "releases" ) ==  0 ) || ( strcmp( argv[1] , "rel" ) ==  0 ) ) 
	{
		printf( " !debian release < dpkg> Named after Toy Story characters. \n" ); 
		printf( " Buzz (1.1; 1996-03-14),\n" ); 
		printf( " Rex (1.2; 1996-10-28),\n" ); 
		printf( " Bo (1.3; 1997-05-01),\n" ); 
		printf( " Hamm (2.0; 1998-07-24),\n" ); 
		printf( " Slink (2.1; 1999-03-09),\n" ); 
		printf( " Potato (2.2; 2000-08-15),\n" ); 
		printf( " Woody (3.0; 2002-07-19),\n" ); 
		printf( " Sarge (3.1; 2005-06-06),\n" ); 
		printf( " Etch (4.0; 2007-04-08),\n" ); 
		printf( " Lenny (5.0; 2009-02-14),\n" ); 
		printf( " Squeeze (6.0; 2011-02-06),\n" ); 
		printf( " Wheezy (7; 2013-05-04),\n" ); 
		printf( " Jessie (8; 2015-04-25),\n" ); 
		printf( " Stretch (9; 2017-06-17),\n" ); 
		printf( " Buster (10; 2019-07-06). (rpi raspios)\n" ); 
		printf( " Bullseye (11;?2020-2021?).\n" ); 
		printf( " Bookworm (12). (SID 2021-12)\n" ); 
		printf( " ....Bookworm (12;) (next release, with openmw, dolphin-emu).\n" ); 
		printf( " SID (sid) is currently : Bookworm.\n" ); 
		return 0; 
	}





	// for pibox 
	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "evilwm-xterm" ) ==  0 ) 
	|| ( strcmp( argv[1] , "xevilwm-wm" ) ==  0 )  
	|| ( strcmp( argv[1] , "xevilwm" ) ==  0 ) ) 
	{
			printf( "========================\n" );
			printf( "Classic version (clean) \n" );
			printf( "========================\n" );
			nsystem( " cd ; echo                >    .xinitrc  " );
			nsystem( " cd ; echo  cd            >>   .xinitrc  " );
			nsystem( " cd ; echo   setxkbmap de   >>   .xinitrc  " );
			nsystem( " cd ; echo  ' xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xinitrc  " );
			nsystem( " cd ; echo  ' xsetfork s off ;  xsetfork -dpms ;  xsetfork s noblank  ; setterm -blank 0 ' >> .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    kicker &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo   xrdb  .Xresources               >> .xinitrc  " );
			nsystem( " cd ; echo   xsetroot -cursor_name left_ptr  >> .xinitrc " ); 
			nsystem( " cd ; echo                        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo     mixerctl -w outputs.speaker=116      >> .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris    evilwm  &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  '     xsetroot -solid #3A6EA5 '        >>       .xinitrc  " );
			nsystem( " cd ; echo  '  env TZ=Europe/Paris  xterm  &  '        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo  '     xsetroot -solid #000000 '        >>       .xinitrc  " );
			nsystem( " cd ; echo  '     xsetroot -solid #000000 '        >>       .xinitrc  " );
			nsystem( " cd ; echo                       >>       .xinitrc  " );
			nsystem( " cd ; echo    nloop              >>       .xinitrc  " );

			printf( "============\n" );

			nsystem( " cd ; echo setxkbmap de          >        .xsession " );
			nsystem( " cd ; echo  'xset s off ;  xset -dpms ;  xset s noblank  ; setterm -blank 0 ' >> .xsession  " );
			nsystem( " cd ; echo   xrdb  .Xresources    >>      .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '    env TZ=Europe/Paris      kicker &  '        >>       .xsession  " );
			nsystem( " cd ; echo   xrdb  .Xresources               >> .xsession   " );
			nsystem( " cd ; echo   xsetroot -cursor_name left_ptr  >> .xsession   " ); 
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo     mixerctl -w outputs.speaker=116      >> .xsession   " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '    env TZ=Europe/Paris      evilwm &  '        >>       .xsession  " );
			nsystem( " cd ; echo  '    env TZ=Europe/Paris    xterm &  '        >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo  '     xsetroot -solid #3A6EA5 '        >>       .xsession  " );
			nsystem( " cd ; echo  '     xsetroot -solid #000000 '        >>       .xsession  " );
			nsystem( " cd ; echo  '     xsetroot -solid #000000 '        >>       .xsession  " );
			nsystem( " cd ; echo                       >>       .xsession  " );
			nsystem( " cd ; echo    nloop              >>       .xsession  " );
			printf( "============\n" );

			/// new: nsystem( "    mixerctl -w outputs.speaker=116   " ); // for netbsd, pi desktop after a reboot. 
			return 0;
	}





	if ( argc == 3 )
	if ( ( strcmp( argv[1] , "debootstrap" ) ==  0 ) 
	||  ( strcmp( argv[1] , "-debootstrap" ) ==  0 )  
	||  ( strcmp( argv[1] , "strap" ) ==  0 )  
	||  ( strcmp( argv[1] , "strap+" ) ==  0 )  
	||  ( strcmp( argv[1] , "-strap" ) ==  0 )  
	||  ( strcmp( argv[1] , "--strap" ) ==  0 )  
	|| ( strcmp( argv[1] , "--debootstrap" ) ==  0 ) ) 
	{
		printf( "|------------------------------------|\n" );
		printf( "| >DEBIAN: \n" ); 

		printf( "|  1. check gpg       (recommended)  |\n" );
		// net-tools for ifconfig, added.
		printf( "|  2. without check gpg              |\n" );
		/// 2 no gpg 
		printf( "|  0. keys: (minimum ctwm, x11, bookworm, without x11, for speed) without check gpg. (fast) |\n" );

		printf( "|  3. check gpg for archive          |\n" );
		printf( "|  4. without check gpg for archive  |\n" );

		printf( "\n" ); 
                printf( "deb http://archive.debian.org/debian stretch main" ); 
		printf( "\n" ); 
		printf( "| >DEVUAN: \n" ); 
		// green 
		printf("%s", KGRE);
		printf( "|  5. check gpg for devuan (default *)   |\n" );
		printf("%s", KNRM);
		// normal 
		printf( "|  6. without check gpg devuan       |\n" );
		printf( "|  7. without no check certif devuan |\n" );
		// 8 with archive for ascii (new): 
		printf( "|  8. devuan archive, without no check certif devuan |\n" );
		printf( "|  9. Create mkdir -p var/cache/apt/archives\n" ); 
		printf( "|------------------------------------|\n" );
		printf( "|  x. debian with gcc,make, chromium, mpv, a system, chromium, and make (new) |\n" );
		printf( "|  a. debian with a system, chromium, and make (new) |\n" );
		printf( "|------------------------------------|\n" );
		printf( "  ==> Keypress - Choice ?            \n" );
		printf( "                                     \n" );
		choix = getchar();


	        if ( strcmp( argv[1] , "strap+" ) ==  0 )  
                {
			nsystem( "  apt-get update " ); 
			npkg(    "  debootstrap  ");
		}

		if ( choix == '0' )  // default and recommended for xinit x11 with xterm and startx
		{
                   //   --include=debootstrap,wpasupplicant    
	           npkg(    "  debootstrap " ); 
		   //snprintf( charo , sizeof( charo ), "   debootstrap  --no-check-gpg   --include=gcc,make,subversion,net-tools,wpasupplicant     %s  .   http://ftp.debian.org/debian/ "   ,  argv[ 2 ]  ); 
		   snprintf( charo , sizeof( charo ), "   debootstrap  --no-check-gpg   --include=debootstrap,wget,xinit,screen,ctwm,xserver-xorg,xterm,tmux,screen,net-tools,wpasupplicant     %s  .   http://ftp.debian.org/debian/ "   ,  argv[ 2 ]  ); 
		   printf( "Command: %s\n", charo ); 
		   nsystem( charo ); 
		}

		else if ( choix == '1' ) 
	        {
	           npkg(    "  debootstrap " ); 
		   snprintf( charo , sizeof( charo ), " echo DIRCHOICE1 ; uname -a ; hostname ; pwd ; debootstrap  %s  .   http://ftp.debian.org/debian/   "   ,  argv[ 2 ]  ); 
		   printf( "Command: %s\n", charo ); 
		   nsystem( charo ); 
		}


		else if ( choix == 'x' )  // default and recommended for xinit x11 with xterm and startx
		{
                   //   --include=debootstrap,wpasupplicant    
	           npkg(    "  debootstrap " ); 
		   //snprintf( charo , sizeof( charo ), "   debootstrap  --no-check-gpg   --include=gcc,make,subversion,net-tools,wpasupplicant     %s  .   http://ftp.debian.org/debian/ "   ,  argv[ 2 ]  ); 
		   snprintf( charo , sizeof( charo ), "   debootstrap  --no-check-gpg   --include=wget,mpv,curl,debootstrap,chromium,xinit,python3,mpv,yt-dlp,screen,ctwm,xserver-xorg,xterm,wget,curl,tmux,screen,gcc,make,subversion,net-tools,wpasupplicant     %s  .   http://ftp.debian.org/debian/ "   ,  argv[ 2 ]  ); 
		   printf( "Command: %s\n", charo ); 
		   nsystem( charo ); 
		}

		else if ( choix == 'a' ) 
		{
		   //snprintf( charo , sizeof( charo ), "   debootstrap  --no-check-gpg   --include=wget,gcc,make,mpv,dillo,links,links2,mpg123,subversion,gcc,dillo,x11-xserver-utils,vim,curl,abook,chromium,xserver-xorg,xterm,ctwm,screen,mpv,yt-dlp,screen,tmux,make,subversion,wpasupplicant,libfltk1.3-dev,g++   %s  .   http://ftp.debian.org/debian/ "   ,  argv[ 2 ]  ); 
		   snprintf( charo , sizeof( charo ), "   debootstrap  --no-check-gpg   --include=wget,gcc,make,mpv,dillo,links,subversion,gcc,dillo,x11-xserver-utils,vim,chromium,xserver-xorg,xterm,ctwm,screen,mpv,yt-dlp,screen,tmux,make,subversion,wpasupplicant,libfltk1.3-dev,g++   %s  .   http://ftp.debian.org/debian/ "   ,  argv[ 2 ]  ); 
		   nsystem( charo ); 
		}



		else if ( choix == '2' ) 
		{
	           npkg(    "  debootstrap " ); snprintf( charo , sizeof( charo ), "   debootstrap  --no-check-gpg   %s  .   http://ftp.debian.org/debian/ "   ,  argv[ 2 ]  ); 
		   printf( "Command: %s\n", charo ); 
		   nsystem( charo ); 
		}

		else if ( choix == '3' ) 
		{
			npkg(    "  debootstrap " ); snprintf( charo , sizeof( charo ), "   debootstrap      %s  .   http://archive.debian.org/debian/  "   ,  argv[ 2 ]  ); 
			printf( "Command: %s\n", charo ); 
			nsystem( charo ); 
		}
		else if ( choix == '4' ) 
		{
			npkg(    "  debootstrap " ); snprintf( charo , sizeof( charo ), "   debootstrap  --no-check-gpg   %s  .   http://archive.debian.org/debian/  "   ,  argv[ 2 ]  ); 
			printf( "Command: %s\n", charo ); 
			nsystem( charo ); 
		}

		else if ( choix == '5' ) 
		{
	          //nsystem( " debootstrap    --include=debootstrap,wpasupplicant  ceres .   http://pkgmaster.devuan.org/merged   ; mkdir usr/src  " ); 
		  snprintf( charo , sizeof( charo ), "   debootstrap   --include=debootstrap,wpasupplicant   %s  .    http://pkgmaster.devuan.org/merged  "   ,  argv[ 2 ]  ); 
		  printf( "Command: %s\n", charo ); 
		  nsystem( charo ); 
		}

		else if ( choix == '6' ) 
		{
	          //nsystem( " debootstrap  --no-check-gpg    --include=debootstrap,wpasupplicant  ceres .   http://pkgmaster.devuan.org/merged   ; mkdir usr/src  " ); 
		  snprintf( charo , sizeof( charo ), "   debootstrap --no-check-gpg  --include=debootstrap,wpasupplicant  %s  .    http://pkgmaster.devuan.org/merged   "   ,  argv[ 2 ]  );    nsystem( charo );
		}

		else if ( choix == '7' ) 
		{
	          //nsystem( " debootstrap  --no-check-gpg    --include=debootstrap,wpasupplicant  ceres .   http://pkgmaster.devuan.org/merged   ; mkdir usr/src  " ); 
		  snprintf( charo , sizeof( charo ), "   debootstrap --no-check-certificate  --include=debootstrap,wpasupplicant  %s  .    http://pkgmaster.devuan.org/merged   "   ,  argv[ 2 ]  );    nsystem( charo );
		}
                //    debootstrap --no-check-gpg  --include=debootstrap,wpasupplicant  ascii  .    http://archive.devuan.org/merged  

		else if ( choix == '8' ) 
		{
		  printf( "  /usr/share/debootstrap/scripts/ascii    http://archive.devuan.org/merged \n" );  
		  snprintf( charo , sizeof( charo ), "   debootstrap --no-check-certificate  --include=debootstrap,wpasupplicant  %s  .    http://archive.devuan.org/merged    "   ,  argv[ 2 ]  );    nsystem( charo );
		}

		else if ( choix == '9' ) 
		   nsystem( " mkdir  -p var/cache/apt/archives   " ); 

		return 0; 

	}












	if ( argc == 2 )
	if ( ( strcmp( argv[1] , "eof" ) ==  0 ) || ( strcmp( argv[1] , "hello" ) ==  0 ) )
	{
		printf("Hello Terminal (eof) \n");
		return 0;
	}


	fprintf( stderr, "** Welcome to mconfig (small busy application). **\n" ); 
	return 0;                                                                                                  
}                                                                                                                 

/// EOF EOF 



